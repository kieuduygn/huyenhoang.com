<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('https://api.mailgun.net/v3/sandbox8285a9992f154314b577391dd2b0ffaa.mailgun.org'),
        'secret' => env('key-fcdf3f73113220d261ac2f4d33fdc1a8'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'github' => [
        'client_id' => '78cbecaf04efc6abea93',
        'client_secret' => '94a433d8ba7f09d9b230187e0ac807f90a3e321d',
        'redirect' => 'http://premiumlens.com/login.html',
    ],
    'google' => [
        'client_id' => '719910723993-jhh12qc39h983b5rsq9rvf77hmbgvtlt.apps.googleusercontent.com',
        'client_secret' => 'McX6ae0sLptBIgN7RxKaKpUu',
        'redirect' => base_url('user/google/login/step/2'),
    ],
    'facebook' => [
        'client_id' => '1534881020143074',
        'client_secret' => 'eac9a4e60c09ecdd31bb4bf8f425113a',
        'redirect' => base_url('user/facebook/login/step/2'),
    ],

];
