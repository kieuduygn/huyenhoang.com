<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->integer('ward_id');
            $table->integer('district_id');
            $table->integer('province_id');
            $table->string('note');
            $table->string('shipping_name');
            $table->string('shipping_email');
            $table->string('shipping_phone');
            $table->string('shipping_address');
            $table->integer('shipping_ward_id');
            $table->integer('shipping_district_id');
            $table->integer('shipping_province_id');
            $table->integer('total');
            $table->integer('order_status_id')->unsigned();
            $table->timestamps();
        });
        Schema::create('order_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ward_id')->references('id')->on('wards')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('province_id')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('shipping_ward_id')->references('id')->on('wards')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('shipping_district_id')->references('id')->on('districts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('shipping_province_id')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('order_option', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('order_product_id')->unsigned();
            $table->integer('option_id')->unsigned();
            $table->integer('option_value_id')->unsigned();
            $table->string('name');
            $table->string('value');
            $table->string('type');
            $table->timestamps();
        });
        Schema::create('order_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('name');
            $table->integer('quantity');
            $table->integer('price');
            $table->integer('total');
            $table->integer('tax')->default(0);
            $table->timestamps();
        });
        Schema::table('order_option', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('order_product_id')->references('id')->on('order_product')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('option_value_id')->references('id')->on('option_values')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('order_product', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('order_total', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('code');
            $table->string('title');
            $table->integer('value');
        });

        Schema::table('order_total', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}


/*
 * order_id
invoice_no
invoice_prefix
store_id
store_name
store_url
customer_id
customer_group_id
firstname
lastname
email
telephone
fax
custom_field
payment_firstname
payment_lastname
payment_company
payment_address_1
payment_address_2
payment_city
payment_postcode
payment_country
payment_country_id
payment_zone
payment_zone_id
payment_address_format
payment_custom_field
payment_method
payment_code
shipping_firstname
shipping_lastname
shipping_company
shipping_address_1
shipping_address_2
shipping_city
shipping_postcode
shipping_country
shipping_country_id
shipping_zone
shipping_zone_id
shipping_address_format
shipping_custom_field
shipping_method
shipping_code
comment
total
order_status_id
affiliate_id
commission
marketing_id
tracking
language_id
currency_id
currency_code
currency_value
ip
forwarded_ip
user_agent
accept_language
date_added
date_modified
*/