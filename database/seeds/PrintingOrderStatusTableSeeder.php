<?php

use Illuminate\Database\Seeder;

class PrintingOrderStatusTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('printing_order_statuses')->insert([
            ['name' => 'Processing', 'color' => 'pink'],
            ['name' => 'Shipped', 'color' => 'violet'],
            ['name' => 'Canceled', 'color' => 'grey'],
            ['name' => 'Complete', 'color' => 'green'],
            ['name' => 'Denied', 'color' => 'red'],
            ['name' => 'Canceled Reversal', 'color' => 'grey'],
            ['name' => 'Failed', 'color' => 'red'],
            ['name' => 'Refunded', 'color' => 'brown'],
            ['name' => 'Reversed', 'color' => 'teal'],
            ['name' => 'Chargeback', 'color' => 'grey'],
            ['name' => 'Pending', 'color' => 'blue'],
            ['name' => 'Voided', 'color' => 'red'],
            ['name' => 'Processed', 'color' => 'green'],
            ['name' => 'Expired', 'color' => 'grey'],
            ['name' => 'Paid', 'color' => 'green'],
        ]);
    }
}
