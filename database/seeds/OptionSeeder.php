<?php

use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->delete();
        DB::table('options')->insert([
            ['type' => 'radio'],
            ['type' => 'checkbox'],
            ['type' => 'text'],
            ['type' => 'select'],
            ['type' => 'textarea'],
            ['type' => 'file'],
            ['type' => 'date'],
            ['type' => 'time'],
            ['type' => 'datetime'],
            ['type' => 'select'],
            ['type' => 'date'],
        ]);
    }
}
