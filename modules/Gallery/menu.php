<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-03-06
 * Time: 7:16 PM
 */

return [
        'head' => true,
        'head_title' => 'Gallery',
        'items' => [
            [
                'sub' => false,
                'items' => [
                    [
                        'name' => 'Danh mục',
                        'route' => 'gallery.backend.categories.index',
                    ],
                ]
            ],
            [
                'sub' => true,
                'items' => [
                    [
                        'name' => 'Album',
                        'route' => 'gallery.backend.albums.index',
                    ],
                    [
                        'name' => 'Thêm Album',
                        'route' => 'gallery.backend.albums.create',
                    ],
                ]
            ],
            [
                'sub' => false,
                'items' => [
                    [
                        'name' => 'Popular Photos',
                        'route' => 'gallery.backend.popularphotos.index',
                    ],
                ]
            ],
        ]
];