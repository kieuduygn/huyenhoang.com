<?php
return [
    'label' => 'danh mục',
    'name'  => 'tên danh mục',
    'name_description'  => 'Tên danh mục sẽ xuất hiện trên website của bạn',
    'parent'    => 'Danh mục cha',
    'parent_description'    => 'Danh mục cha ',
    'choose_parent'    => 'Chọn danh mục cha',
    'add_label'    => 'Thêm danh mục',
    'create_success_message'    => 'Thêm danh mục thành công',
    'delete'    => 'Xóa danh mục',
    'confirm_delete'    => 'Xác nhận xóa danh mục',
    'confirm_delete_message'    => 'Cảnh báo ! Bạn đang muốn xóa danh mục :name, việc này không thể phục hồi',
    'yes_delete'    => 'Vâng hãy xóa',
    'no_delete'    => 'Không đưa tôi trở lại',
    'delete_success_message'    => 'Xóa danh mục :name thành công',
    'duplicate'    => 'Nhân đôi danh mục',
    'duplicate_message'    => 'Bạn đã nhân đôi thành công danh mục :name, hãy sửa lại',
    'edit'    => 'Sửa danh mục',
    'update'    => 'Cập nhật danh mục',
    'update_success_message' => 'Cập nhật danh mục :name thành công',
    'who_can_view'  => 'Ai có thể xem danh mục này',

];