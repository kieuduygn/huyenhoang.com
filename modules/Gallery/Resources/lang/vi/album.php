<?php
return [
    'label' => 'album',
    'image' => 'Ảnh album',
    'add_images' => 'Thêm ảnh album',
    'name'  => 'tên album',
    'name_description'  => 'Tên album sẽ xuất hiện trên website của bạn',
    'parent'    => 'Danh mục cha',
    'parent_description'    => 'Danh mục cha ',
    'choose_parent'    => 'Chọn album cha',
    'add_label'    => 'Thêm album',
    'create_success_message'    => 'Thêm album thành công',
    'delete'    => 'Xóa album',
    'confirm_delete'    => 'Xác nhận xóa album',
    'confirm_delete_message'    => 'Cảnh báo ! Bạn đang muốn xóa album :name, việc này không thể phục hồi',
    'yes_delete'    => 'Vâng hãy xóa',
    'no_delete'    => 'Không đưa tôi trở lại',
    'delete_success_message'    => 'Xóa album :name thành công',
    'duplicate'    => 'Nhân đôi album',
    'duplicate_message'    => 'Bạn đã nhân đôi thành công album :name, hãy sửa lại',
    'edit'    => 'Sửa album',
    'update'    => 'Cập nhật album',
    'update_success_message' => 'Cập nhật album :name thành công',
    'feature'   => 'Tính năng chính',
    'put_on_home'   => 'Đặt album này lên trang chủ',
    'suggest' => 'Sản phẩm đề nghị',
    'preview'   => 'Xem trước',
    'create_cat_first'  => 'Bạn phải tạo ít nhất một danh mục trước khi tạo album',



];