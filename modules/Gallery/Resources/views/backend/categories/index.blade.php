@extends('main')

@section('title', 'Quản lý danh mục sản phẩm dịch vụ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('gallery_categories') !!}
@endsection

@section('content')

    <div class="col-md-5 col-lg-4">
        {!! Form::model($category,
            [
                'method' => $category->exists ? 'put' : 'post',
                'route' => $category->exists ? ['gallery.backend.categories.update', $category->id] : ['gallery.backend.categories.store'],
            ])
        !!}
        @include('errors.form')
                <!-- Tên danh mục  -->
        <div class="form-group">
            <label for="">{{ ucfirst(trans('gallery::cat.name')) }}</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <div class="help-block">{{ trans('gallery::cat.name_description') }}</div>
        </div>
        <!-- Tên danh mục  END-->

        <!-- SLUG  -->
        <div class="form-group">
            <label for="">{{ trans('common.slug') }}</label>
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            <div class="help-block">{{ trans('common.slug_description') }}</div>
        </div>
        <!-- SLUG  END-->

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>{{ trans('gallery::cat.parent') }}</label>
                    <select name="parent" class="form-control">
                        <option value="0">-- {{ trans('gallery::cat.choose_parent') }}</option>
                        @foreach($categories as $node)
                            {!! render_category($node) !!}
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>{{ trans('gallery::cat.who_can_view') }}</label>
                    {!! Form::select('who_can_view', config('gallery.who_can_view'), null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <img src="{{ ($category->thumbnail != '') ? $category->thumbnail : url('global/img/no_thumbnails.gif') }}" id="thumbnail" class="img-responsive" alt="">
                {!! Form::hidden('thumbnail', null, ['id' => 'input-thumbnail']) !!}
            </div>
        </div>

        <!-- Description  -->
        <div class="form-group">
            <label for="">{{ trans('common.description') }}</label>
            {!! Form::textarea('description', null, ['class' =>'form-control']) !!}
        </div>
        <!-- Description  END-->

        <div class="form-group">
            <label class="checkbox-inline">
                {!! Form::checkbox('permit', 1, null, ['class' => 'styled']) !!}
                Phải được cấp quyển mới xem được </label>
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                {!! Form::checkbox('trade', 1, null, ['class' => 'styled']) !!}
                Có thể mua ảnh </label>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                {{ ($category->exists) ? trans('gallery::cat.update') : trans('gallery::cat.add_label') }}
            </button>
        </div>
        {!! Form::close() !!}
    </div>


    <!-- List Blog Categories Start -->
    <div class="col-md-7 col-lg-8">
        {!! Form::open(['route' => 'gallery.backend.categories.bulk']) !!}
                <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
        </div>
        <!-- Bulk Action End -->

        <div class="clearfix"></div>


        <div class="panel panel-flat">
            <div class="panel-body no-padding">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="40"><input class="styled top-check" type="checkbox"></th>
                            <th>Ảnh đại diện</th>
                            <th>{{ ucfirst(trans('gallery::cat.name')) }}</th>
                            <th>{{ trans('common.slug') }}</th>
                            <th width="250">{{ trans('common.description') }}</th>
                            <th width="150"></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($categories as $category)
                            {!! gallery_render_category_tr($category) !!}
                        @endforeach
                        </tbody>

                        <tfoot>
                        <tr>
                            <th><input class="styled top-check" type="checkbox"></th>
                            <th>Ảnh đại diện</th>
                            <th>{{ ucfirst(trans('gallery::cat.name')) }}</th>
                            <th>{{ trans('common.slug') }}</th>
                            <th width="250">{{ trans('common.description') }}</th>
                            <th></th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>

        </div>

        <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
            <div class="pull-right">
                {!! $categories->render() !!}
            </div>
        </div>
        <!-- Bulk Action End -->
        {!! Form::close() !!}
    </div>
    <!-- List Blog Categories End -->


@endsection


@section('scripts')
    <script>
        $('#thumbnail').click(function (e) {
            e.preventDefault();
            CKFinder.modal({
                height: 600,
                language: 'vi',
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (event) {
                        var file = event.data.files.first();
                        $('#thumbnail').attr('src', file.getUrl());
                        $('#input-thumbnail').val(file.getUrl());
                    });
                }
            });
        });
    </script>

    @endsection


@section('themejs')
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    @endsection