@extends('main')

@section('title', trans('gallery::cat.confirm_delete'))
@section('breadcrumbs')
{!! Breadcrumbs::render('confirm_delete_gallery_categories', $category) !!}
@endsection


@section('content')
{!! Form::open(['method' => 'delete', 'route' => ['gallery.backend.categories.destroy', $category->id]]) !!}
        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="alert alert-danger">
            {{ trans('gallery::cat.confirm_delete_message', ['name' => $category->name]) }}
        </div>

        {!! Form::submit(trans('gallery::cat.yes_delete'), ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('gallery.backend.categories.index') }}" class="btn btn-success"> <strong>{{ trans('gallery::cat.no_delete') }}</strong>
        </a>
    </div>
</div><!-- Panel End -->
{!! Form::close() !!}@endsection
