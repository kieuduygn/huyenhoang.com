@extends('main')

@section('title', 'Popular Photos')


@section('breadcrumbs')
    {!! Breadcrumbs::render('gallery::backend.popular_photos') !!}
@endsection

@section('content')


<div class="col-md-12">
    {!! Form::open(['route' => 'gallery.backend.popularphotos.store']) !!}
    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <div id="images">

               <!-- Images Start -->
                        @foreach($images as $image)
                            <div class="col-lg-3 col-sm-6 gallery-item">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img src="{{ get_image_url($image->file_path) }}" alt="">
                                        <input type="hidden" value="{{ $image->file_path }}" name="images[]">
                                        <div class="caption-overflow">
                            <span>
                                <a href="{{ get_image_url($image->file_path) }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                                <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 btn-detach-image"><i class="icon-link2"></i></a>
                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    <div class="clearfix"></div>
                <!-- Images End -->


            </div>
        </div>
    </div>
    <!-- Panel End -->

    <button type="button" id="add-album-image" class="btn btn-primary">Thêm hình ảnh</button>
    <button type="submit" class="btn btn-success">Lưu lại</button>
    <a href="{{ url('/backend') }}" class="btn btn-default">Thoát ra</a>
{!! Form::close() !!}


</div>


@endsection

@section('themejs')
    <script src="{{ url('assets') }}/js/plugins/editors/ckeditor/ckeditor.js"></script>
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/tags/tokenfield.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/media/fancybox.min.js"></script>
@endsection

@section('pagejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/md5.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/uploader_bootstrap.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/gallery.js"></script>
@stop
@section('scripts')

    <script>

        $(document).ready(function () {

            $('.options-container').delegate('.btn-detach-option', 'click', function () {
                var option_id = $(this).data('option-id');
                var album_id = $(this).data('album-id');
                var option_value_id = $(this).parent().parent().find('select').val();
                $(this).closest('tr').remove();
                if (option_id != undefined && album_id != undefined) {
                    $.ajax({url: "{{ url() }}" + "/album/detach-option/" + option_id + '/' + album_id + '/' + option_value_id});
                }
            });


            $('#tags-input').tokenfield({
                //autocomplete_url:'http://myserver.com/api/autocomplete'
            });



            $('#add-album-image').click(function (e) {
                e.preventDefault();
                CKFinder.modal({
                    height: 600,
                    language: 'vi',
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (event) {
                            var files = event.data.files;
                            for (var id in files._byId) {
                                $.post(
                                        "{{ url('gallery/render-new-album-image') }}",
                                        {
                                            data: files._byId[id].getUrl(),
                                            '_token': $('meta[name=csrf-token]').attr('content'),
                                        },
                                        function (data) {
                                            $('#images').prepend(data);
                                        }
                                );
                            }
                        });
                    }
                });
            });


            $('#images').delegate('.btn-detach-image', 'click', function (e) {
                e.preventDefault();
                $(this).closest('.gallery-item').remove();
            });
        });

    </script>


    @include('partials.fill-slug-name')
@endsection

