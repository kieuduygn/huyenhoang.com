@extends('main')

@section('title', $album->exists ? trans('gallery::album.edit') : trans('gallery::album.add_label'))

@section('functions')
    <a href="{{ route('gallery.backend.albums.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>{{ trans('gallery::album.add_label') }}</span></a>
@endsection


@section('breadcrumbs')
    {!! Breadcrumbs::render('create_gallery_album') !!}
@endsection

@section('content'){!! Form::model($album, [
        'method' => $album->exists ? 'put' : 'post',
        'route' => $album->exists ? ['gallery.backend.albums.update', $album->id] : ['gallery.backend.albums.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}


<div class="col-md-9">
    <!-- Panel _start -->
    <div class="panel panel-flat">


        <div class="panel-body">

            @include('errors.form')

            <ul class="nav nav-tabs" id="theTab">
                <li class="active"><a data-target="#general" data-toggle="tab">{{ trans('common.general_information') }}</a></li>
                <li><a data-target="#full-description" data-toggle="tab">{{ trans('common.description') }}</a></li>
                <li><a data-target="#images" data-toggle="tab">{{ trans('gallery::album.image') }}</a></li>
                <li><a data-target="#seo" data-toggle="tab">SEO</a></li>
            </ul>

            <div class="tab-content">
                <!-- General start -->
                <div class="tab-pane active" id="general">

                    <div class="row">
                        <div class="col-md-6">
                            <!-- Tên Sản Phẩm Start -->
                            <div class="form-group">
                                <label for="name">{{ trans('gallery::album.name') }}</label>
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                <div class="help-block">{{ trans('gallery::album.name_description') }}</div>
                            </div>
                            <!-- Tên Sản Phẩm END -->

                            <!-- Slug Start -->
                            <div class="form-group">
                                <label for="slug">{{ trans('common.slug') }}</label>
                                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                                <div class="help-block">{{ trans('common.slug_description') }}</div>
                            </div>
                            <!-- Slug END -->

                            <!-- Slug Start -->
                            <div class="form-group">
                                <label for="feature">{{ trans('gallery::album.feature') }}</label>
                                {!! Form::text('feature', null, ['class' => 'form-control']) !!}
                            </div>
                            <!-- Slug END -->

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>


                    <!-- Mô Tả ngắn Start -->
                    <div class="form-group">
                        <label for="short_description">{{ trans('common.short_description') }}</label>
                        {!! Form::textarea('short_description', null, ['id' => 'short_description']) !!}
                    </div>
                    <!-- Mô tả ngắn End -->


                    <!-- Tags -->

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5>{{ trans('common.tags') }}</h5>
                        </div>
                        <div class="panel-body">
                            <input type="text" name="tags" id="tags-input" value="{{ $tagString }}"
                                   class="form-control">

                            <div class="help-block">
                                {{ trans('common.tags_description') }}
                            </div>
                        </div>
                    </div>
                    <!-- End Tags -->
                </div>
                <!-- Genderal End -->


                <!-- Full Description Start -->
                <div class="tab-pane" id="full-description">
                    <div class="form-group">
                        <label for="description">{{ trans('common.full_description') }}</label>
                        {!! Form::textarea('description', null, ['id' => 'description']) !!}
                    </div>
                </div>
                <!-- Full Description End -->


                <!-- Images Start -->
                <div class="tab-pane" id="images">
                    <p>Xin lưu ý, nếu lần đầu tiên tạo album, phải lưu trước khi sửa mã ảnh</p>
                    @if($album->exists)
                        @foreach($album->images()->get() as $image)
                            <div class="col-lg-3 col-sm-6 gallery-item">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img src="{{ get_image_url($image->file_path) }}" alt="">
                                        <input type="hidden" value="{{ $image->file_path }}" name="images[]">
                                        <div class="caption-overflow">
                            <span>
                                <a href="{{ get_image_url($image->file_path) }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                                <a data-image-id="{{ $image->id }}" data-album-id="{{ $album->id }}" href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 btn-detach-image"><i class="icon-link2"></i></a>
								<input style="margin-top:10px;text-align:center;" data-image-id="{{ $image->id }}" name="code" value="{{ $image->name ? $image->name : strtoupper($image->id . '_' . $album->slug)}}" class="form-control image-name">
							</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="clearfix"></div>
                    <div id="add-album-image" class="btn btn-default">{{ trans('gallery::album.add_images') }}</div>
                </div>
                <!-- Images End -->


                <!-- SEO -->
                <div class="tab-pane" id="seo">

                    <div class="form-group" style="max-width: 600px">
                        @if(! $album->exists)
                            <div class="sample-title">This is sample title</div>
                            <div class="sample-url">{{ url('this-is-sample-title') }}</div>
                            <div class="sample-meta-description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto corporis deserunt
                                iusto laboriosam molestiae nostrum quaerat rerum sed, voluptatum? A dignissimos dolorem
                                error iusto molestias quas quisquam rem rerum. Error!
                            </div>
                        @else
                            <div class="sample-title">{{ str_limit(($album->seo_title != '') ? $album->seo_title : $album->name, 80) }}</div>
                            <div class="sample-url">{{ route('gallery.albums.show', [$album->id, $album->slug]) }}</div>
                            <div class="sample-meta-description">
                                {{ ($album->meta_description != '') ? $album->meta_description : $album->excerpt }}
                            </div>
                        @endif
                    </div>
                    <!-- Tiêu đề Start -->
                    <div class="form-group">
                        {!! Form::label('seo_title') !!}
                        {!! Form::text('seo_title', null, ['class' => 'form-control seo-title']) !!}
                    </div>
                    <!-- Tiêu đề END -->
                    <!-- Excerpt Start -->
                    <div class="form-group">
                        {!! Form::label('meta_description') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control meta-description']) !!}
                    </div>
                    <!-- Excerpt excerpt -->
                </div>
                <!-- SEO end -->

            </div>
        </div>
    </div>
    <!-- Panel End -->
</div>
<div class="col-md-3">

    <!-- Categories  -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>{{ ucfirst(trans('gallery::cat.label')) }}</h5>
            {!! Form::select('category_list[]', $categories, null, ['class' => 'form-control multi-select-full', 'id' => 'category_list']) !!}
        </div>
    </div>
    <!-- Categories  END-->


    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>{{ trans('common.thumbnail') }}</h5>
            <div class="form-group">
                <div style="margin-bottom: 20px;">
                        <img style="max-height: 200px;" id="thumbnail" class="img-responsive img-thumbnail"
                             src="{{ $album->exists ? $album->thumbnail : url('global/img/no_thumbnails.gif') }}">
                </div>
                {!! Form::hidden('thumbnail', $album->exists ? $album->thumbnail : url('global/img/no_thumbnails.gif'), ['id' => 'input-thumbnail']) !!}
            </div>


            <!-- Các nút chức năng  -->
            <div class="">
                <button name="button" value="save" class="btn btn-primary">{{ trans('common.save') }}</button>
                <button name="button" value="save_and_exists"
                        class="btn btn-info">{{ trans('common.save_and_exit') }}</button>
                <a href="{{ route('gallery.backend.albums.index') }}" class="btn btn-default">{{ trans('common.cancel') }}</a>
            </div>
            <!-- Các nút chức năng  END-->


        </div>
    </div>
    <!-- Panel End -->

    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>{{ trans('common.other_feature') }}</h5>
            <!-- Feature ? -->
            <div class="form-group">
                <label class="checkbox-inline">
                    {!! Form::checkbox('home', 1, null, ['class' => 'styled']) !!}
                    {{ trans('gallery::album.put_on_home') }} </label>
            </div>
            <!-- End Feature Option -->
            <!-- Suggest Product ? -->
            <div class="form-group">
                <label class="checkbox-inline">
                    {!! Form::checkbox('suggest', 1, null, ['class' => 'styled']) !!}
                    {{ trans('gallery::album.suggest') }} </label>
            </div>
            <!-- End Suggest Product Option -->
        </div>
    </div>
    <!-- Panel End -->

</div>




{!! Form::close() !!}
@endsection

@section('themejs')
    <script src="{{ url('assets') }}/js/plugins/editors/ckeditor/ckeditor.js"></script>
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/tags/tokenfield.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/media/fancybox.min.js"></script>
@endsection

@section('pagejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/md5.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/uploader_bootstrap.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/gallery.js"></script>
@stop
@section('scripts')

    <script>
        CKEDITOR.replace('short_description', {
            height: 200,
        });
        CKEDITOR.replace('description', {
            height: 600,
        });

        $(document).ready(function () {

            $('.options-container').delegate('.add-option-value', 'click', function () {
                var id = $(this).data('option-id');
                var tbody = $(this).closest('tr').parent().parent().find('tbody');
                var length = tbody.find('tr').length;
                $.ajax({
                    'url': "{{ url() }}" + '/album/add-option/' + id + '/' + length,
                    success: function (data) {
                        tbody.append(data);
                    }
                });
            })


            $('#btn-add-option').click(function () {
                var id = $('#add-option').val(),
                        length = $('.options-container table tbody').length;
                $.ajax({
                    'url': "{{ url() }}" + '/album/add-option/' + id + '/' + length,
                    success: function (data) {
                        $('.options-container table tbody').append(data);
                        $('.options-container tr').last().find('button').click(function () {
                            $(this).closest('tr').remove();
                        })
                    }
                });
            });


            $('#add-attribute, .add-options').selectpicker();

            $('#btn-add-attribute').click(function () {
                var id = $('#add-attribute').val();
                $.ajax({
                    'url': "{{ url() }}" + '/getattributehtml/' + id,
                    success: function (data) {
                        $('.attributes-container').append(data);
                    }
                });
            });


            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            //
            $('.options-container').delegate('.btn-detach-option', 'click', function () {
                var option_id = $(this).data('option-id');
                var album_id = $(this).data('album-id');
                var option_value_id = $(this).parent().parent().find('select').val();
                $(this).closest('tr').remove();
                if (option_id != undefined && album_id != undefined) {
                    $.ajax({url: "{{ url() }}" + "/album/detach-option/" + option_id + '/' + album_id + '/' + option_value_id});
                }
            });


            $('#tags-input').tokenfield({
                //autocomplete_url:'http://myserver.com/api/autocomplete'
            });

            $('#thumbnail').click(function (e) {
                e.preventDefault();
                CKFinder.modal({
                    height: 600,
                    language: 'vi',
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (event) {
                            var file = event.data.files.first();
                            $('#thumbnail').attr('src', file.getUrl());
                            $('#input-thumbnail').val(file.getUrl());
                        });
                    }
                });
            });
            $('#add-album-image').click(function (e) {
                e.preventDefault();
                CKFinder.modal({
                    height: 600,
                    language: 'vi',
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (event) {
                            var files = event.data.files;
                            for (var id in files._byId) {
                                $.post(
                                        "{{ url('gallery/render-new-album-image') }}",
                                        {
                                            data: files._byId[id].getUrl(),
                                            '_token': $('meta[name=csrf-token]').attr('content'),
                                        },
                                        function (data) {
                                            $('#images').prepend(data);
                                        }
                                );
                            }
                        });
                    }
                });
            });


            $('#images').delegate('.btn-detach-image', 'click', function (e) {
                e.preventDefault();

                if (!$(this).closest('.gallery-item').hasClass('new')) {
                    var albumID = $(this).data('album-id'),
                            imageID = $(this).data('image-id');
                    $.get("{{ url('gallery/backend/albums/detach-image/') }}/" + imageID + '/' + albumID);
                }

                $(this).closest('.gallery-item').remove();
            });


            $('.image-name').on('blur', function(){
                var imageID = $(this).data('image-id'),
                        imageName = $(this).val();


                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: "{{ url() }}/gallery/backend/image-name/",
                    type: 'POST',
                    data: {
                        _token: CSRF_TOKEN,
                        imageID: imageID,
                        imageName: imageName
                    },
                    dataType: 'JSON',
                });

            });
        });

    </script>


    @include('partials.fill-slug-name')
@endsection

