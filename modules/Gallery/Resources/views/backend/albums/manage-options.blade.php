<!-- Options Container -->
<div class="options-container">

    @if(! $product->exists || $product->options()->count() == 0)
        <div class="row">
            <div class="col-xs-3"> <!-- required for floating -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-left">
                    @foreach(options() as $i => $option)
                        <li class="{{ ($i == 0) ? 'active' : '' }}"><a href="#option-{{ $option->id }}"
                                                                       data-toggle="tab">{{ $option->name }}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="col-xs-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    @foreach(options() as $i => $option)
                        <div class="tab-pane {{ ($i == 0) ? 'active' : '' }}" id="option-{{ $option->id }}">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Lựa chọn</th>
                                    <th width="100">Số lượng</th>
                                    <th>Giá</th>
                                    <th width="50">
                                        <button data-option-id="{{ $option->id }}" type="button"
                                                class="btn btn-xs btn-success add-option-value"><i
                                                    class="icon-plus3"></i></button>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- End row -->
    @else
        <div class="row">
            <div class="col-xs-3"> <!-- required for floating -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-left">
                    @foreach(options() as $i => $option)
                        <li class="{{ ($i == 0) ? 'active' : '' }}"><a href="#option-{{ $option->id }}"
                                                                       data-toggle="tab">{{ $option->name }}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="col-xs-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    @foreach(options() as $i => $option)
                        <div class="tab-pane {{ ($i == 0) ? 'active' : '' }}" id="option-{{ $option->id }}">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Lựa chọn</th>
                                    <th width="100">Số lượng</th>
                                    <th>Giá</th>
                                    <th width="50">
                                        <button data-option-id="{{ $option->id }}" type="button"
                                                class="btn btn-xs btn-success add-option-value"><i
                                                    class="icon-plus3"></i></button>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($product->options()->get() as $n => $option_item)
                                    @if($option_item->id == $option->id)
                                        <tr>

                                            <!-- Name -->
                                            <td>{!! Form::select(
                                                'option_value['.$option->id.']['.$n.'][option_value_id]',
                                                $option->values()->lists('name', 'id') ,
                                                $option_item->pivot->option_value_id,
                                                ['class' => 'form-control'])
                                            !!}</td>
                                            <!-- End Name -->

                                            <!-- Quantity -->
                                            <td>{!! Form::text(
                                            'option_value['.$option->id.']['.$n.'][quantity]',
                                            $option_item->pivot->quantity,
                                            ['class' => 'form-control'])
                                            !!}</td>
                                            <!-- End Quantity -->


                                            <td>
                                                <!-- Price Prefix -->
                                                {!! Form::select(
                                                'option_value['.$option->id.']['.$n.'][price_prefix]',
                                                config('app.options_prefix'),
                                                $option_item->pivot->price_prefix,
                                                ['class' => 'form-control'])
                                                !!}
                                                        <!-- End Price Prefix -->

                                                <!-- Price -->
                                                {!! Form::text(
                                                'option_value['.$option->id.']['.$n.'][price]',
                                                $option_item->pivot->price,
                                                ['class' => 'form-control']) !!}
                                                        <!-- End Price -->
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger btn-detach-option"
                                                        data-option-id="{{ $option->id }}"
                                                        data-product-id="{{ $product->id }}">
                                                    <i class="icon-minus2"></i></button>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- End row -->
    @endif
    <p>Lưu ý: Hệ thống chưa phát triển quản lý kho, bạn có thể bỏ trống trường số lượng</p>
</div><!-- Options Container End -->
