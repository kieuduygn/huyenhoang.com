@extends('main')

@section('title', 'Quản lý album')

@section('breadcrumbs')
    {!! Breadcrumbs::render('gallery_album') !!}
@endsection

@section('functions')
    <a href="{{ route('gallery.backend.albums.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>{{ trans('gallery::album.add_label') }}</span></a>
    @endsection


    @section('content')
    {!! Form::open(['route' => 'gallery.backend.albums.bulk']) !!}
            <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $albums->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->

    <!-- List Items Start -->
    <div class="panel panel-flat">
        <div class="panel-body no-padding">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th style="width: 50px"><input type="checkbox" class="styled top-check"></th>
                        <th>{{ trans('gallery::album.image') }}</th>
                        <th>{{ trans('gallery::album.name') }}</th>
                        <th>{{ trans('common.slug') }}</th>
                        <th>{{ trans('common.order_number') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($albums as $album)

                        <tr>
                            <td><input type="checkbox" class="styled item-checkbox" name="id[]"
                                       value="{{ $album->id }}"></td>
                            <td>
                                <a href="{{ route('gallery.backend.albums.edit', $album->id) }}">
                                    <img class="img-thumbnail" width="80"
                                         src="{{ ($album->thumbnail != '') ? $album->thumbnail : url('global/img/no_thumbnails.gif') }}" alt="">
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('gallery.backend.albums.edit', $album->id) }}">{{ $album->name }}</a>
                            </td>
                            <td>{{ $album->slug }}</td>
                            <td>
                                <input type="text" class="text-center" value="{{ $album->order_number }}" style="width: 80px;">

                            </td>
                            <td>
                                <!-- edit button  -->
                                <a class="btn btn-xs btn-icon btn-default" data-popup="tooltip" title="Sửa album"
                                   data-placement="top"
                                   href="{{ route('gallery.backend.albums.edit', $album->id) }}">
                                    <i class="icon-pen"></i>
                                </a>
                                <!-- edit button  END-->

                                <!-- Delete  -->
                                <a class="btn btn-xs btn-icon btn-danger" data-popup="tooltip"
                                   title="{{ trans('gallery::album.delete') }}"
                                   data-placement="top"
                                   href="{{ route('gallery.backend.albums.confirm', $album->id) }}">
                                    <i class="icon-minus2"></i>
                                </a>
                                <!-- Delete  END-->

                                <!-- Delete  -->
                                <a class="btn btn-xs btn-icon btn-success" data-popup="tooltip"
                                   title="{{ trans('gallery::album.duplicate') }}"
                                   data-placement="top"
                                   href="{{ route('gallery.backend.albums.duplicate', $album->id) }}">
                                    <i class="icon-database-add"></i>
                                </a>
                                <!-- Delete  END-->

                                <!-- Preview  -->
                                <a target="_blank" class="btn btn-xs btn-icon btn-info" data-popup="tooltip"
                                   title="Xem trước"
                                   data-placement="top"
                                   href="{{ route('gallery.albums.show',$album->slug) }}">
                                    <i class="icon-eye2"></i>
                                </a>
                                <!-- Preview  END-->


                            </td>
                        </tr>
                    @endforeach


                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $albums->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->

    <!-- List Items End -->
    {!! Form::close() !!}
@endsection
