<div class="form-group">
    <div class="input-group">
        <select id="add-attribute" class="form-control">
            @foreach($attributeGroups as $group)
                <optgroup label="{{ $group->name }}">
                    @foreach($group->attributes()->get() as $attribute)
                        <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>

        <div class="input-group-btn">
            <button type="button" class="btn btn-info" id="btn-add-attribute">Thêm thuộc tính</button>
        </div>
    </div>
</div>
<!-- Attribute Container -->
<div class="attributes-container">

</div>
<!-- Attribute Container End -->