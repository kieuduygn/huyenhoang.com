@extends('main')

@section('title', 'Xóa album '.$album->name)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['gallery.backend.albums.destroy', $album->id]]) !!}
    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5>Xác nhận xóa album</h5>
            @include('partials.backend.heading-elements')
        </div>
        <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa album <strong>[{{ $album->name }}]</strong>, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa album này này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('gallery.backend.albums.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
        </div>
    </div>
    <!-- Panel End -->


   </form>
@endsection
