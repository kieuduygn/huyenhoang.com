<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleryOrder extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_order', function(Blueprint $table)
        {
            $table->increments('id');
			$table->string('name',60);
			$table->string('email',150);
			$table->string('phone',15);
			$table->string('address');
			$table->text('note',1000);
			$table->integer('image_id');
			$table->integer('album_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_order');
    }

}
