<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleryAlbums extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_albums', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('order_number');
            $table->string('seo_title');
            $table->text('description');
            $table->text('meta_description');
            $table->integer('thumbnail_id')->unsigned();
            $table->string('feature');
            $table->integer('home');
            $table->text('short_description');
            $table->integer('suggest');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_albums');
    }

}
