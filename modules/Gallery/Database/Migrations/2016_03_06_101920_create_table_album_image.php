<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlbumImage extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_album_image', function(Blueprint $table)
        {
            $table->integer('album_id')->unsigned();
            $table->integer('image_id')->unsigned();
        });
        Schema::table('gallery_album_image', function(Blueprint $table)
        {
            $table->foreign('album_id')->references('id')->on('gallery_albums')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('image_id')->references('id')->on('gallery_images')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('album_image');
    }

}
