<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleryAlbumTag extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_album_tag', function(Blueprint $table)
        {
            $table->integer('album_id')->unsigned();
            $table->integer('tag_id')->unsigned();
        });
        Schema::table('gallery_album_tag', function(Blueprint $table)
        {
            $table->foreign('album_id')->references('id')->on('gallery_albums')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_album_tag');
    }

}
