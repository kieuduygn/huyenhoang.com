<?php

//Category
Breadcrumbs::register('gallery_categories', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Danh mục ', route('gallery.backend.categories.index'));
});
Breadcrumbs::register('create_gallery_categories', function($breadcrumbs)
{
    $breadcrumbs->parent('gallery_categories');
    $breadcrumbs->push('Thêm danh mục ', route('gallery.backend.categories.create'));
});
Breadcrumbs::register('edit_gallery_categories', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('gallery_categories');
    $breadcrumbs->push('Sửa danh mục ', route('gallery.backend.categories.edit', [$category->id]));
});
Breadcrumbs::register('confirm_delete_gallery_categories', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('gallery_categories');
    $breadcrumbs->push('Xác nhận xóa danh mục ', route('gallery.backend.categories.confirm', [$category->id]));
});


//Product
Breadcrumbs::register('gallery_album', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Album', route('gallery.backend.albums.index'));
});
Breadcrumbs::register('gallery::backend.popular_photos', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Popular photos', '');
});
Breadcrumbs::register('create_gallery_album', function($breadcrumbs)
{
    $breadcrumbs->parent('gallery_album');
    $breadcrumbs->push('Thêm album', route('gallery.backend.albums.create'));
});
Breadcrumbs::register('edit_gallery_album', function($breadcrumbs, $album)
{
    $breadcrumbs->parent('gallery_album');
    $breadcrumbs->push('Sửa album', route('gallery.backend.albums.edit', [$album->id]));
});
Breadcrumbs::register('confirm_delete_gallery_album', function($breadcrumbs, $album)
{
    $breadcrumbs->parent('gallery_album');
    $breadcrumbs->push('Xác nhận xóa album', route('gallery.backend.albums.confirm', [$album->id]));
});


//Front end
Breadcrumbs::register('gallery', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Gallery', url('gallery'));
});
Breadcrumbs::register('gallery::category.show', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push($category->name, route('gallery.category.show', $category->slug));
});

Breadcrumbs::register('gallery::album.show', function($breadcrumbs, $category, $album)
{
    $breadcrumbs->parent('gallery::category.show', $category);
    $breadcrumbs->push($album->name);
});

Breadcrumbs::register('gallery::tag', function($breadcrumbs, $tag)
{
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push('Thẻ ' . $tag->name);
});
Breadcrumbs::register('gallery::login_required', function($breadcrumbs)
{
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push('Yêu cầu đăng nhập');
});

Breadcrumbs::register('gallery::album.order', function($breadcrumbs, $category, $album, $image)
{
    $breadcrumbs->parent('gallery::category.show', $category, $album);
    $breadcrumbs->push('Đặt hàng ảnh ' . image_code($image, $album));
});