<?php namespace Modules\Gallery\Entities;
   
use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;

class Album extends Model {

    protected $fillable = [
        'name',
        'slug',
        'order_number',
        'thumbnail',
        'seo_title',
        'description',
        'home',
        'suggest',
        'short_description',
        'meta_description',
        'feature'
    ];

    protected $table = 'gallery_albums';


    /**
     * Các ảnh của sản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany('Modules\Gallery\Entities\Image', 'gallery_album_image', 'album_id', 'image_id');
    }


    /**
     * Tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'gallery_album_tag', 'album_id', 'tag_id');
    }

    /**
     * Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('Modules\Gallery\Entities\Category', 'gallery_album_category', 'album_id', 'category_id');
    }


    /**
     * Bind Categories
     *
     * @return array
     */
    public function getCategoryListAttribute()
    {
        $list = [];
        $categories = $this->categories()->get();
        foreach($categories as $category){
            $list[] = $category->id;
        }
       return $list;
    }

}