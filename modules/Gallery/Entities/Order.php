<?php namespace Modules\Gallery\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $fillable = ['name', 'phone', 'email', 'address', 'note', 'image_id', 'album_id'];

    protected $table = 'gallery_order';

}