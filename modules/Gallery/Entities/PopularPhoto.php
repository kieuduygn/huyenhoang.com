<?php namespace Modules\Gallery\Entities;
   
use Illuminate\Database\Eloquent\Model;

class PopularPhoto extends Model {

    protected $fillable = ['file_path'];

    protected $table = 'gallery_popular_photos';

}