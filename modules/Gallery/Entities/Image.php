<?php namespace Modules\Gallery\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    protected $fillable = ['file_path', 'md5'];

    protected $table = 'gallery_images';
}