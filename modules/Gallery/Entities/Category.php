<?php namespace Modules\Gallery\Entities;

use Baum\Node;


class Category extends Node {

    protected $fillable = [
        'name',
        'slug',
        'description',
        'order_number',
        'who_can_view',
        'thumbnail',
        'permit',
        'trade'
    ];

    protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

    protected $table = 'gallery_categories';


    public function albums()
    {
        return $this->belongsToMany('Modules\Gallery\Entities\Album', 'gallery_album_category', 'category_id', 'album_id');
    }

}