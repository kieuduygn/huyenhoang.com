<?php namespace Modules\Gallery\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Modules\Gallery\Entities\Category;
use Pingpong\Modules\Routing\Controller;

class CategoriesController extends Controller {

	public function show($slug)
	{

		$query = Category::where('slug', $slug);
		//Không tồn tại thì redirect
		if($query->count() == 0){
			return redirect();
		}

		//view
		$category = $query->first();

		if(!auth()->check() && $category->permit == 1){
			return redirect(route('gallery.category.login').'?cat='.$category->id);
		}

		$albums = $category->albums()->get();
		return mview('gallery::albums.category', compact('category', 'albums'));
	}

	public function loginRequired()
	{

		//Không có id của cat trên url
		if(!isset($_GET['cat'])){
			return redirect();
		}

		$id = $_GET['cat'];

		//Không tồn tại cat
		if(Category::where('id', $id)->count() == 0){
			return redirect();
		}

		//Cat cho phép mua
		$category = Category::findOrFail($id);
		if($category->permit == 0){
			return redirect('');
		}

		return mview('gallery::login_required', compact('category'));
	}

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email',
			'password'	=> 'required',
			'category_id' => 'required|exists:gallery_categories,id'
		]);

		$auth = Auth::attempt([
			'email'	=> $request->input('email'),
			'password'	=> $request->input('password')
		]);


		if($auth){
			$category_id =$request->input('category_id');
			$category = Category::findOrFail($category_id);
			return redirect(route('gallery.category.show', $category->slug));
		}else{
			flash()->error('Lỗi', 'Tài khoản hoặc mật khẩu không đúng');

			return redirect()->back();
		}

	}
}