<?php namespace Modules\Gallery\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Gallery\Entities\Album;
use Modules\Gallery\Entities\Image;
use Modules\Gallery\Entities\Order;
use Mail;
use Pingpong\Modules\Routing\Controller;

class AlbumsController extends Controller {
	
	public function index()
	{
		$albums = Album::paginate(12);

		return mview('gallery::albums.index', compact('albums'));
	}


	/**
	 * @param $id
	 * @param $slug
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\View
	 */
	public function show($id, $slug)
	{

		$query = Album::where('slug', $slug)->where('id', $id);
		//Không tồn tại sẽ redirect
		if($query->count() == 0){
			return redirect();
		}

		$album = $query->with('tags')->first();
		return mview('gallery::albums.show', compact('album'));
	}


	public function order($image_id, $album_slug)
	{
		$imgQuery = Image::where('id', $image_id);
		$albumQuery = Album::where('slug', $album_slug);
		if($imgQuery->count() == 0 || $albumQuery->count() == 0){
			return redirect();
		}

		$data['image'] = $imgQuery->first();
		$data['album'] = $albumQuery->first();

		return mview('gallery::albums.order', $data);
	}

	public function postOrder(Request $request)
	{
		$this->validate($request, [
			'name'	=> 'required|max:60',
			'phone'	=> 'required|max:25',
			'address'	=> 'required',
			'note'	=> 'required|max:1000',
			'email'	=> 'required|email|max:150',
			'image_id'	=> 'required|exists:gallery_images,id',
			'album_id'	=> 'required|exists:gallery_albums,id'
		]);

		$album = Album::findOrFail($request->input('album_id'));
		$imageName = image_code(Image::findOrFail($request->input('image_id')), $album);

		$data=$request->all();
		$data['imageName'] = $imageName;

		Order::create($request->all());

		Mail::send('emails.order', ['data' => $data], function ($m) use ($request) {
			$m->from('info@huyenhoang.com', 'Huyền Hoàng');

			$m->to(get_settings('email'), $request->input('name'))->subject('Khách hàng đặt mua ảnh từ www.huyenhoang.com');
		});

		flash()->success('Thành công, ', 'Chúng tôi đã nhận được đơn hàng của quý khách, sẽ liên lạc lại sớm');

		return redirect()->back();
	}
	
}