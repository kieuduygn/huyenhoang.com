<?php namespace Modules\Gallery\Http\Controllers;

use App\Models\Tag;
use Modules\Gallery\Entities\Category;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller {
	
	public function index()
	{
		$categories = Category::all();
		return mview('gallery::index', compact('categories'));
	}


	public function renderNewProductImage(Request $request)
	{
		$image = $request->input('data');
		return mview('gallery::backend.albums.new_product_image', compact('image'));
	}

	public function tag($slug)
	{
		$query = Tag::where('slug', $slug);
		if($query->count() == 0){
			return redirect();
		}

		$tag = $query->first();

		$albums = $tag->albums()->get();

		return mview('gallery::albums.tag', compact('albums', 'tag'));

	}
}