<?php namespace Modules\Gallery\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Gallery\Entities\PopularPhoto;
use Pingpong\Modules\Routing\Controller as BaseController;

class PopularPhotosController extends BaseController {


	public function __construct()
	{
		return $this->middleware('admin');
	}

	public function index()
	{
        $images = PopularPhoto::all();

		return mview('gallery::backend.popular_photos', compact('images'));
	}


    public function store(Request $request)
    {
        PopularPhoto::truncate();
        foreach($request->input('images') as $image){
            PopularPhoto::create(['file_path' => $image]);
        }
        return redirect()->back();
    }
	
}