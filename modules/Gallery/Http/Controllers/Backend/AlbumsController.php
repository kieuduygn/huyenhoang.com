<?php namespace Modules\Gallery\Http\Controllers\Backend;

use App\Models\Tag;
use Modules\Gallery\Entities\Category;
use Modules\Gallery\Entities\Image;
use Modules\Gallery\Entities\Album;
use Modules\Gallery\Http\Requests\StoreAlbumRequest;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Modules\Gallery\Http\Requests\UpdateAlbumRequest;

class AlbumsController extends Controller
{


    public function __construct()
    {
        parent::__contruct();
    }

    /**
     * Liệt kê danh sách tất cả album
     *
     * @return \View
     */
    public function index()
    {
        $albums = Album::orderBy('created_at', 'desc')->paginate(config('app.items_per_page'));

        return module_view('gallery', 'backend.albums.index', compact('albums'));
    }


    public function bulk(Request $request, Album $album)
    {
        bulk_delete($request, $album);

        return redirect()->back();
    }


    /**
     * Form Tạo album
     *
     * @param Album $album
     * @return \View
     */
    public function create(Album $album)
    {
        $categories = $this->getCategories();

        //Không thể tạo album nếu không tồn tại danh mục
        if (count($categories) == 0) {
            flash(trans('gallery::album.create_cat_first'));
            return redirect(route('gallery.backend.albums.index'));
        }

        //Mặc định phải có tagString
        $tagString = '';

        return module_view('gallery', 'backend.albums.form', compact('album', 'tagString', 'categories'));
    }


    /**
     * Dựng các danh mục thành cây
     *
     * @return array
     */
    protected function getCategories()
    {
        $catItems = Category::all()->toHierarchy();

        $categories = [];

        foreach ($catItems as $node) {
            $categories += arrange_categories($node);
        }

        return $categories;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAlbumRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbumRequest $request)
    {
        //Lưu album
        $album = Album::create($request->all());

        //Attach Tags
        if ($request->input('tags') != '') {
            $album->tags()->attach($this->tagsProcess($request->input('tags')));
        }

        //Xử lý hình ảnh
        if ($request->input('images')) {
            foreach ($request->input('images') as $image) {
                $savedImage = $this->image_process($image);
                $album->images()->attach($savedImage->id);
            }
        }

        //Attach categories
        $album->categories()->attach($request->input('category_list'));

        flash()->success('Thành công.', 'Bạn đã thêm thành công album');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('gallery.backend.albums.edit', $album->id));
            default:
                return redirect(route('gallery.backend.albums.index'));
        }


    }


    /**
     *
     * @param $imageID
     * @param $albumID
     */
    public function detachImage($imageID, $albumID)
    {
        $album = Album::findOrFail($albumID);
        $album->images()->detach($imageID);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tagString = '';
        $album = Album::findOrFail($id);

        if ($album->tags->count() > 0) {
            foreach ($album->tags->toArray() as $tag) {
                $tagString .= $tag['name'] . ',';
            }
        }

        $categories = $this->getCategories();
        return module_view('gallery','backend.albums.form', compact('album', 'tagString', 'categories'));
    }

    /**
     * Cập nhật album
     *
     * @param UpdateAlbumRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAlbumRequest $request, $id)
    {
        //Update album
        $album = Album::findOrFail($id);

        //Sync the tags for this post
        if ($request->input('tags') != '') {
            $album->tags()->sync($this->tagsProcess($request->input('tags')));
        }

        //Sync danh mục
        $album->categories()->sync($request->input('category_list'));

        //Xử lý thumbnail
        $album->update($request->all());

        //Xử lý hình ảnh
        if ($request->input('images')) {
            foreach ($request->input('images') as $image) {
                $processedImage = $this->image_process($image);
                $album->images()->detach($processedImage->id);
                $album->images()->attach($processedImage->id);
            }
        }

        //Thoát
        flash()->success(trans('common.success'), trans('gallery::album.update_success_message', ['name' => $album->name]));
        switch ($request->input('button')) {
            case 'save':
                return redirect(route('gallery.backend.albums.edit', $album->id));
            default:
                return redirect(route('gallery.backend.albums.index'));
        }


    }





    public function duplicate($id)
    {
        $album = Album::findOrFail($id);
        $albumCategories = $album->getCategoryListAttribute();
        $albumArray = $album->toArray();

        //Đặt slug mới
        $albumArray['slug'] = $albumArray['slug'] . '_';

        //Tạo mới album
        $album = Album::create($albumArray);

        //Vẫn giữ danh mục cũ
        $album->categories()->attach($albumCategories);

        flash(trans('gallery::album.duplicate_message', ['name' => $album->name]));

        return redirect(route('gallery.backend.albums.edit', $album->id));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);

        $album->delete();

        flash()->success('Thành công', 'Bạn đã xóa album [' . $album->name . '] thành công');

        return redirect(route('gallery.backend.albums.index'));
    }


    public function imageName(Request $request)
    {
        $imageID = $request->input('imageID');
        $imageName = $request->input('imageName');

        Image::where('id', $imageID)->update(['name' => $imageName]);
    }


    /**
     * Album Delete Confirm
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $album = Album::findOrFail($id);

        return mview('gallery::backend.albums.confirm', compact('album'));
    }


    /**
     * @param $option_id
     * @param $album_id
     */

    public function detachOption($option_id, $album_id, $option_value_id)
    {
        AlbumOption::where('option_id', $option_id)
            ->where('option_value_id', $option_value_id)
            ->where('album_id', $album_id)->delete();
        //
    }

    public function weareTestingThisController()
    {
        return view('AreYouOK');
    }

    public function addOptionAjax($option_id, $length)
    {
        $length++;
        if (!session('options_length')) {
            session()->put('options_length', $length);
        } else {
            $length = session('options_length');
            $length++;
            session()->put('options_length', $length);
            session()->save();
        }

        $option = Option::findOrFail($option_id);
        $values = OptionValue::lists('name', 'id');
        return view('backend.options.add_value', compact('option', 'values', 'length'));
    }


    /**
     * Xử lý Tag để lấy ra mảng tag của bài viết này
     *
     * @param $tagString
     * @return array
     */
    private function tagsProcess($tagString)
    {
        $tagList = explode(',', $tagString);
        $slugify = new Slugify();
        $returnTagsArray = array();
        //Duyệt qua các tag
        foreach ($tagList as $tagItem) {
            //Tạo tag slug
            $tagSlug = $slugify->slugify($tagItem);
            //Query xem có trùng không
            $query = Tag::where('slug', $tagSlug);

            if ($query->count() == 0) {
                $returnTagsArray[] = Tag::create(['name' => $tagItem, 'slug' => $tagSlug])->id;
            } else {
                $returnTagsArray[] = $query->first()->id;
            }

        }
        return $returnTagsArray;
    }


    /**
     * Thêm ảnh
     *
     * @param $file_path
     * @return static
     */
    private function image_process($file_path)
    {
        $md5 = md5($file_path);

        $query = Image::where('md5', $md5);

        if ($query->count() == 0) {
            $image = Image::create(['file_path' => $file_path, 'md5' => $md5]);
            return $image;
        } else {
            return $query->first();
        }
    }

}























