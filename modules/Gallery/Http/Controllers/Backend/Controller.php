<?php namespace Modules\Gallery\Http\Controllers\Backend;

use Pingpong\Modules\Routing\Controller as BaseController;

class Controller extends BaseController {

	public function __contruct()
	{
		$this->middleware('admin');
	}
	
}