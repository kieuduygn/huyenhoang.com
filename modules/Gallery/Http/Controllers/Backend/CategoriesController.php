<?php namespace Modules\Gallery\Http\Controllers\Backend;

use Modules\Gallery\Entities\Category;
use Modules\Gallery\Http\Requests\StoreCategoryRequest;
use Modules\Gallery\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;

class CategoriesController extends Controller {


	public function __construct()
	{
		parent::__contruct();
	}

	/**
	 * Danh sách các danh mục
	 *
	 * @param Category $category
	 * @return \View
	 */
	public function index(Category $category)
	{
		$categories = Category::orderBy('order_number', 'asc')->paginate(config('app.items_per_page'));

		return mview('gallery::backend.categories.index', compact('categories', 'category'));
	}


	/**
	 * Xóa hàng loạt
	 *
	 * @param Request $request
	 * @param Category $category
	 */
	public function bulk(Request $request, Category $category)
	{
		bulk_delete($request, $category);

		return redirect()->back();
	}


	/**
	 * Thêm 1 danh mục
	 *
	 * @param StoreCategoryRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(StoreCategoryRequest $request)
	{
		//Khởi tạo danh mục
		$category = Category::create($request->all());

		//Kiểm tra có danh mục cha hay không
		if($request->input('parent') != 0){
			$query = Category::where('id', $request->input('parent'));
			if($query->count() > 0){
				$category->makeChildOf($query->first());
			}
		}

		flash()->success(trans('common.success'), trans('gallery::cat.create_success_message'));

		return redirect()->back();
	}


	/**
	 * Nhân đôi một danh mục
	 *
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function duplicate($id)
	{
		//Query
		$category = Category::findOrFail($id)->toArray();

		//Tạo slug mới
		$category['slug'] = $category['slug'] . '_';

		//Tạo category mới
		$category = Category::create($category);

		flash(trans('gallery::cat.duplicate_message', ['name' => $category->name]));

		//Đển trang sửa
		return redirect(route('gallery.backend.categories.edit', $category->id));
	}


	/**
	 * Chỉnh sửa danh mục
	 *
	 * @param $id
	 * @return \View
	 */
	public function edit($id)
	{
		//Danh mục sẽ chỉnh sửa
		$category = Category::findOrFail($id);

		//Các danh mục sẽ show bên phải trang
		$categories = Category::where('id', '!=', $id)
			->orderBy('order_number', 'asc')->paginate(config('app.items_per_page'));

		return module_view('gallery', 'backend.categories.index', compact('category', 'categories'));
	}


	/**
	 * Cập nhật danh mục dịch vụ
	 *
	 * @param UpdateCategoryRequest $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(UpdateCategoryRequest $request, $id)
	{
		//Kiểm tra slug có trùng với các danh mục khác hay không
		$slug = $request->input('slug');
		if(Category::where('id', '!=', $id)->where('slug', $slug)->count() > 0){
			flash(trans('common.slug_duplicated'));
			return redirect()->back();
		}

		//Fill các giá trị mới cho danh mục đang sửa
		$category = Category::findOrFail($id);
		$category->update($request->all());

		//Kiểm tra có danh mục cha hay không
		if($request->input('parent') != 0){
			$query = BlogCategory::where('id', $request->input('parent'));
			if($query->count() > 0){
				$category->makeChildOf($query->first());
			}
		}

		flash()->success(trans('common.success'), trans('gallery::cat.update_success_message', ['name' => $category->name]));

		return redirect(route('gallery.backend.categories.index'));

	}

	/**
	 * Xác nhận xóa danh mục
	 *
	 * @param $id
	 * @return \View
	 */
	public function confirm($id)
	{
		$category = Category::findOrFail($id);

		return module_view('gallery', 'backend.categories.confirm', compact('category'));
	}


	/**
	 * Tiến hành xóa nhanh mục
	 *
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy(Request $request, $id)
	{
		$category = Category::findOrFail($id);
		$category->delete();

		flash()->success(trans('common.success'), trans('gallery::cat.delete_success_message', ['name' => $category->name]));

		return redirect(route('gallery.backend.categories.index'));
	}
}
