<?php namespace Modules\Gallery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'	=> 'required|max:255',
			'slug'	=> 'required|unique:gallery_categories,slug',
			'parent_id'	=> 'numeric',
			'description'	=> 'max:1000'
		];
	}

}
