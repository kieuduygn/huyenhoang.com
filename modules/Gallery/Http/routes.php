<?php

Route::group(['prefix' => 'gallery', 'namespace' => 'Modules\Gallery\Http\Controllers'], function()
{
	Route::group(['prefix' => 'backend', 'namespace' => '\Backend'],  function(){
		//Category
		Route::resource('categories', 'CategoriesController');
		Route::get('categories/{id}/confirm', ['as' => 'gallery.backend.categories.confirm', 'uses'	=> 'CategoriesController@confirm']);
		Route::post('categories/bulk', ['as' => 'gallery.backend.categories.bulk', 'uses'	=> 'CategoriesController@bulk']);
		Route::get('categories/{id}/duplicate', ['as' => 'gallery.backend.categories.duplicate', 'uses'	=> 'CategoriesController@duplicate']);
		Route::post('image-name', ['as' => 'gallery.backend.image.name', 'uses'	=> 'AlbumsController@imageName']);
		//Product
		Route::resource('albums', 'AlbumsController');
		Route::get('albums/{id}/confirm', ['as' => 'gallery.backend.albums.confirm', 'uses'	=> 'AlbumsController@confirm']);
		Route::get('albums/detach-image/{album_id}/{image_id}', ['as' => 'gallery.backend.albums.detachImage', 'uses'	=> 'AlbumsController@detachImage']);
		Route::post('albums/bulk', ['as' => 'gallery.backend.albums.bulk', 'uses'	=> 'AlbumsController@bulk']);
		Route::get('albums/{id}/duplicate', ['as' => 'gallery.backend.albums.duplicate', 'uses'	=> 'AlbumsController@duplicate']);

		Route::resource('orders', 'OrdersController');
		Route::resource('popularphotos', 'PopularPhotosController');


	});
	Route::get('/', 'GalleryController@index');
	Route::get('album.html', ['as' => 'gallery.galbum.index', 'uses' => 'AlbumsController@index']);
	Route::get('album/{id}.{slug}.html', ['as' => 'gallery.album.show', 'uses' => 'AlbumsController@show']);
	Route::get('album/order/{image_id}_{album_slug}.html', ['as' => 'gallery.album.order', 'uses' => 'AlbumsController@order']);
	Route::post('album/order', ['as' => 'gallery.album.postOrder', 'uses' => 'AlbumsController@postOrder']);
	Route::get('category/login-required', ['as' => 'gallery.category.login', 'uses' => 'CategoriesController@loginRequired']);
	Route::post('category/login', ['as' => 'gallery.category.postLogin', 'uses' => 'CategoriesController@postLogin']);
	Route::get('category/{slug}.html', ['as' => 'gallery.category.show', 'uses' => 'CategoriesController@show']);
	Route::get('tag/{slug}.html', ['as' => 'gallery.tag', 'uses' => 'GalleryController@tag']);
	Route::resource('albums', 'AlbumsController', ['excerpt' => 'show']);
	Route::post('render-new-album-image', 'GalleryController@renderNewProductImage');


});