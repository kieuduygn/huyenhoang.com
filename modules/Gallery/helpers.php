<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-03-02
 * Time: 10:33 PM
 */

if (!function_exists('module_view')) {

    /**
     * @param $module
     * @param $view
     * @return View
     */
    function module_view($module, $view, $data = [])
    {
        if (View::exists($module . '.' . $view)) {
            return view($module . '.' . $view, $data);
        }
        return view($module . '::' . $view, $data);
    }
}

if (!function_exists('mview')) {

    /**
     * @param $module
     * @param $view
     * @return View
     */
    function mview($module_view, $data = [])
    {
        $array = explode('::', $module_view);
        $module = $array[0];
        $view = $array[1];

        if (View::exists($module . '.' . $view)) {
            return view($module . '.' . $view, $data);
        }
        return view($module . '::' . $view, $data);
    }
}


if (!function_exists('gallery_render_category_tr')) {

    /**
     * @param $node
     * @return string
     */
    function gallery_render_category_tr($node)
    {
        $html = '<tr>
                <td><input name="id[]" value="' . $node->id . '" class="styled item-checkbox" type="checkbox"></td>
                <td><img width="80" src="'.(($node->thumbnail == '') ? url('global/img/no_thumbnails.gif') :$node->thumbnail).'" /></td>
                <td>
                    <a href="' . route('gallery.backend.categories.edit', $node->id) . '">' . get_space($node->depth, '&mdash;') . $node->name . '</a>
                </td>
                <td>' . $node->slug . '</td>
                <td>' . $node->description . '</td>
                <td>
                    <a  data-popup="tooltip" title="' . trans('gallery::cat.edit') . '" data-placement="top" class="btn btn-xs btn-icon btn-default"
                       href="' . route('gallery.backend.categories.edit', $node->id) . '">
                        <i class="icon-pen"></i>
                    </a>
                    <a  data-popup="tooltip" title="' . trans('gallery::cat.duplicate') . '" data-placement="top" class="btn btn-xs btn-icon btn-success"
                       href="' . route('gallery.backend.categories.duplicate', $node->id) . '">
                        <i class="icon-database-add"></i>
                    </a>
                    <a  data-popup="tooltip" title="' . trans('gallery::cat.delete') . '" data-placement="top" class="btn btn-xs btn-icon btn-danger"
                       href="' . route('gallery.backend.categories.confirm', $node->id) . '">
                        <i class="icon-minus2"></i>
                    </a>
                </td>
            </tr>';
        if ($node->isLeaf()) {
            return $html;
        } else {
            foreach ($node->children as $child) {
                $html .= render_category_tr($child);
            }
            return $html;
        }
    }
}


if(! function_exists('gallery_render_categories')){
    /**
     * @param string $ulClass
     * @return string
     */
    function gallery_render_categories($ulClass = '')
    {
        $nodes = \Modules\Printing\Entities\Category::get()->toHierarchy();
        $html = '<ul class="' . $ulClass . '">';
        foreach ($nodes as $node) {
            $html .= gallery_continue_render_categories($node);
        }
        $html .= '</ul>';

        return $html;
    }
}

if(! function_exists('gallery_continue_render_categories')){
    /**
     * @param $node
     * @return string
     */
    function gallery_continue_render_categories($node)
    {
        if ($node->isLeaf()) {
            return '<li><a href="' . route('gallery.category.show', $node->slug) . '" title="' . $node->name . '">' . $node->name . '</a></li>';
        } else {
            $html = '<li><a href="' . route('gallery.category.show', $node->slug) . '" title="' . $node->name . '">' . $node->name . '</a>';
            $html .= '<ul>';
            foreach ($node->children as $child) {
                $html .= gallery_continue_render_categories($child);
            }
            $html .= '</ul></li>';

            return $html;
        }
    }

}

if(! function_exists('image_code')){
    function image_code($image, $album){
        if($image->name)
            return $image->name;
        return strtoupper($image->id . '_' . $album->slug);
    }
}


