<?php

Route::group(['prefix' => 'contact', 'namespace' => 'Modules\Contact\Http\Controllers'], function()
{
	Route::get('/', 'ContactController@index');
	Route::post('post-message', 'ContactController@postMessage');
});