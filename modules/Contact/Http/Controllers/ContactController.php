<?php namespace Modules\Contact\Http\Controllers;

use Modules\Contact\Entities\Message;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller {

	protected $message;

	public function __construct(Message $message)
	{
		$this->message = $message;
	}
	
	public function index()
	{
		return view('Contact::index');
	}


	public function postMessage(Request $request)
	{
		$this->validate($request, [
			'name'  => 'required|max:255',
			'phone' => 'required|numeric',
			'email' => 'email|required',
			'address'   => 'required|max:255',
			'subject'   => 'required',
			'content'   => 'required:max:1000'
		]);

		$this->message->create($request->all());

		/*Mail::send('emails.contact_message', ['data' => $request->all()], function ($m) use ($request) {
			$m->from('info@huyenhoang.com', 'Huyen Hoang Photo');

			$m->to($request->input('email'), $request->input('name'))->subject('Tin nhắn khách hàng ' . $request->input('name'));
		});*/

		flash('Cảm ơn bạn đã gửi tin nhắn, chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất');

		return redirect()->back();
	}
	
}