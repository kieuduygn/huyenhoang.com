<?php namespace Modules\Contact\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    protected $fillable = ['name', 'phone', 'email', 'subject', 'content', 'read', 'address'];

    protected $table = 'contact_messages';
}