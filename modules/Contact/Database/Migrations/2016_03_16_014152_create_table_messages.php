<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMessages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_messages', function(Blueprint $table)
        {
            $table->increments('id');
			$table->string('name', 60);
			$table->string('email', 150);
			$table->string('phone', 15);
			$table->string('address');
			$table->string('subject');
			$table->text('content',1000);
			$table->integer('read');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_messages');
    }

}
