<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class CreateTableFieldTypes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('alias', 30);
        });

        DB::table('field_types')->insert([
            [
                'name' => 'Text',
                'alias' => 'text'
            ],
            [
                'name' => 'TextArea',
                'alias' => 'textarea'
            ],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('field_types');
    }

}
