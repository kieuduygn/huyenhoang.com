<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFieldList extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields_list', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('field_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->string('key');
            $table->timestamps();
        });
        Schema::table('fields_list', function(Blueprint $table)
        {
            $table->foreign('field_id')->references('id')->on('fields')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_id')->references('id')->on('field_types')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields_list');
    }

}
