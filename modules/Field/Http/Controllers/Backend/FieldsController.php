<?php namespace Modules\Field\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Field\Entities\Field;
use Modules\Field\Entities\Lists;
use Modules\Field\Entities\Value;
use Pingpong\Modules\Routing\Controller;

class FieldsController extends Controller {


	public function __construct()
	{
		$this->middleware('admin');
	}


	/**
	 * Index
	 *
	 * @param Field $field
	 * @return \Illuminate\Support\Facades\View|\View
	 */
	public function index(Field $field)
	{
		$fields = Field::paginate(15);

		return mview('field::backend.fields.index', compact('fields', 'field'));
	}


	/**
	 * Xóa hàng loạt
	 *
	 * @param Request $request
	 * @param Field $field
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function bulk(Request $request, Field $field)
	{
		bulk_delete($request, $field);

		return redirect(route('field.backend.fields.index'));
	}


	/**
	 * Lưu tùy chọn
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'key' => 'required|alpha_dash',
			'name'	=> 'required',
			'description'	=> 'max:1000'
		]);

		//Phòng kẻ gian sửa html
		if(!$request->input('key') || !$request->input('type_id')){
			return redirect();
		}

		$field = Field::create($request->all());
		//List Array for insert
		$listArray = [];
		foreach($request->input('_key') as $i => $key){
			$listArray[$i]['key'] = $key;
			$listArray[$i]['field_id'] = $field->id;
		}
		foreach($request->input('type_id') as $i => $type_id){
			$listArray[$i]['type_id'] = $type_id;
		}

		foreach($listArray as $record){
			Lists::create($record);
		}

		flash()->success(trans('common.success') , trans('field::field.create_success_message'));

		return redirect(route('field.backend.fields.edit',  $field->id));
	}


	/**
	 * Sửa tùy chọn
	 *
	 * @param $id
	 * @return \Illuminate\Support\Facades\View|\View
	 */
	public function edit($id)
	{
		$field = Field::with('fields_list')->findOrFail($id);

		$fields = Field::paginate(15);

		return mview('field::backend.fields.index', compact('field', 'fields'));
	}


	/**
	 * Cập nhật
	 *
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(Request $request, $id)
	{

		$field = Field::findOrFail($id);

		$this->validate($request, [
			'key' => 'required|alpha_dash',
			'name'	=> 'required',
			'description'	=> 'max:1000'
		]);



		$field->update($request->all());


		//Phòng kẻ gian sửa html
		if(!$request->input('key') || !$request->input('type_id')){
			return redirect();
		}

		//List Array for insert
		$listArray = [];
		foreach($request->input('_key') as $i => $key){
			$listArray[$i]['key'] = $key;
			$listArray[$i]['field_id'] = $field->id;
		}
		foreach($request->input('type_id') as $i => $type_id){
			$listArray[$i]['type_id'] = $type_id;
		}

		Lists::where('field_id', $field->id)->delete();
		foreach($listArray as $record){
			Lists::create($record);
		}

		flash()->success(trans('common.success') , trans('field::field.update_success_message', ['name' => $field->name]));

		return redirect(route('field.backend.fields.edit',  $field->id));
	}


	/**
	 * Xác nhận xóa
	 *
	 * @param $id
	 * @return \Illuminate\Support\Facades\View|\View
	 */
	public function confirm($id)
	{
		$field = Field::findOrFail($id);

		return mview('field::backend.fields.confirm', compact('field'));
	}

	/**
	 * Xóa
	 *
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$field = Field::findOrFail($id);

		$field->delete();

		flash()->success(trans('common.success') , trans('field::field.delete_success_message', ['name' => $field->name]));

		return redirect(route('field.backend.fields.index'));
	}


	/**
	 * @param $num
	 * @return \Illuminate\Support\Facades\View|\View
	 */
	public function numFields($num)
	{
		return mview('field::backend.fields.num_fields', compact('num'));
	}


	/**
	 * @param $id
	 * @return \Illuminate\Support\Facades\View|\View
	 */
	public function value($id)
	{
		$field = Field::with('fields_list')->with('values')->findOrFail($id);
		return mview('field::backend.fields.value', compact('field'));
	}


	public function postValue(Request $request, $id)
	{

		if($request->input('value')){
			Value::where('field_id', $id)->delete();
			foreach($request->input('value') as $value){

				Value::create([
					'field_id' => $id,
					'value'	=> serialize($value)
				]);
			}
		}

		return redirect()->back();

	}


	/**
	 * @param $id
	 * @param $length
	 * @return \Illuminate\Support\Facades\View|\View
	 */
	public function getValueItem($id, $length)
	{
		$field = Field::with('fields_list')->findOrFail($id);
		return mview('field::backend.fields.get_value_item', compact('field', 'length'));
	}


	/**
	 * Xóa value qua ajax
	 */
	public function deleteValue($id)
	{
		Value::where('id', $id)->delete();
	}
	
}