<?php

Route::group(['prefix' => 'field', 'namespace' => 'Modules\Field\Http\Controllers'], function()
{
	Route::group(['prefix' => 'backend', 'namespace' => '\Backend'], function()
	{
		Route::resource('fields', 'FieldsController');
		Route::post('fields/{id}/bulk', ['as' => 'field.backend.fields.bulk', 'uses' => 'FieldsController@bulk']);
		Route::get('fields/{id}/confirm', ['as' => 'field.backend.fields.confirm', 'uses' => 'FieldsController@confirm']);
		Route::get('fields/{id}/duplicate', ['as' => 'field.backend.fields.duplicate', 'uses' => 'FieldsController@duplicate']);
		Route::get('fields/{id}/value', ['as' => 'field.backend.fields.value', 'uses' => 'FieldsController@value']);
		Route::get('fields/{id}/get-value-item/{length}', ['as' => 'field.backend.fields.getValueItem', 'uses' => 'FieldsController@getValueItem']);
		Route::post('fields/value/{id}', ['as' => 'field.backend.fields.postValue', 'uses' => 'FieldsController@postValue']);
		Route::get('fields/value/{value_id}/delete', ['as' => 'field.backend.fields.deleteValue', 'uses' => 'FieldsController@deleteValue']);
		Route::get('num-fields/{num}', ['as' => 'field.backend.fields.num', 'uses' => 'FieldsController@numFields']);
	});
});