<?php namespace Modules\Field\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Lists extends Model {

    protected $fillable = ['field_id', 'key', 'type_id'];

    protected $table = 'fields_list';

    public function type()
    {
        return $this->belongsTo('Modules\Field\Entities\Type', 'type_id');
    }

}