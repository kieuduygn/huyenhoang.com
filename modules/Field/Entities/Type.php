<?php namespace Modules\Field\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    protected $table = 'field_types';

}