<?php namespace Modules\Field\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Field extends Model {

    protected $fillable = ['name', 'key', 'description'];



    public function values()
    {
        return $this->hasMany('Modules\Field\Entities\Value', 'field_id');
    }


    public function fields_list()
    {
        return $this->hasMany('Modules\Field\Entities\Lists', 'field_id');
    }
}
