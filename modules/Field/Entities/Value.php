<?php namespace Modules\Field\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Value extends Model {

    protected $fillable = ['field_id', 'value'];

    protected $table = 'field_values';



}