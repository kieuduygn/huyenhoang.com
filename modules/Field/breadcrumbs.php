<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-03-16
 * Time: 7:18 PM
 */


Breadcrumbs::register('field', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Tùy chọn', route('field.backend.fields.index'));
});

Breadcrumbs::register('field::create', function($breadcrumbs)
{
    $breadcrumbs->parent('field');
    $breadcrumbs->push('Tùy chọn', route('field.backend.fields.index'));
});

Breadcrumbs::register('field::edit', function($breadcrumbs, $field)
{
    $breadcrumbs->parent('field');
    $breadcrumbs->push($field->name, route('field.backend.fields.edit', $field->id));
});

Breadcrumbs::register('field::confirm_delete', function($breadcrumbs)
{
    $breadcrumbs->parent('field');
    $breadcrumbs->push('Xác nhận xóa tùy chọn');
});

Breadcrumbs::register('field::value', function($breadcrumbs)
{
    $breadcrumbs->parent('field');
    $breadcrumbs->push('Quản lý giá trị');
});