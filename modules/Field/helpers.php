<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-03-16
 * Time: 8:23 PM
 */

if (!function_exists('types_list')) {
    /**
     * Lấy danh sách các types của 1 field
     *
     * @return mixed
     */
    function types_list()
    {
        return \Modules\Field\Entities\Type::lists('name', 'id');
    }
}

if (!function_exists('field_has_image')) {
    /**
     * Xác định xem field cung cấp có chứa hình ảnh không
     *
     * @param $field
     * @return bool
     */
    function field_has_image($field)
    {
        foreach ($field->fields_list()->get() as $item) {
            if ($item->key == 'image') {
                return true;
                break;
            }
        }

        return false;
    }
}


if (!function_exists('field_image')) {
    /**
     * Xác định xem field cung cấp có chứa hình ảnh không
     *
     * @param $field
     * @return bool
     */
    function field_image($value)
    {
        foreach (unserialize($value->value) as $key => $value) {
            if ($key == 'image') {
                return $value['value'];
            }
        }

        return false;
    }
}


if (!function_exists('field_values')) {
    /**
     * Lấy ra giá trị của 1 field
     *
     * @param $key
     * @return mixed
     */
    function field_values($key)
    {
        $field = \Modules\Field\Entities\Field::where('key', $key)->with('values')->first();
        return $field->values()->get();
    }
}

if (!function_exists('field_value')) {
    /**
     * Lấy ra giá trị của 1 field
     *
     * @param $key
     * @return mixed
     */
    function field_value($value, $key)
    {
        $value = unserialize($value->value);

        foreach ($value as $key2 => $item) {
            if ($key2 == $key) {
                return $item['value'];
            }
        }
        return false;
    }
}



















