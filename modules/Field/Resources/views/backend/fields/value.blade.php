@extends('main')

@section('title', 'Quản lý giá trị')

@section('breadcrumbs')
    {!! Breadcrumbs::render('field::value') !!}
@endsection

@section('functions')
    <a id="add-value-item" href="{{ route('field.backend.fields.getValueItem', $field->id) }}"
       class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm giá trị</span></a>
    @endsection


    @section('content')
            <!-- List Items Start -->
    {!! Form::open(['url' => route('field.backend.fields.postValue', $field->id)]) !!}
    <div class="panel panel-flat">
        <div class="panel-body no-padding">

            <div class="table-responsive">
                <h5 style="padding: 20px;margin:0;" class="text-uppercase">Quản lý các giá trị của trường: {{ $field->name }}</h5>
                <table class="table table-responsive value-table">
                    <thead>
                    <tr>
                        @if(field_has_image($field))
                            <th width="120">Ảnh đại diện</th>
                        @endif
                            @foreach($field->fields_list()->get() as $key => $item)
                                <th>{{ ucfirst($item->key) }}</th>
                            @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @if($field->values()->count() == 0)
                    <tr class="value-item">

                        @if(field_has_image($field))
                            <td><img  width="80"  class="img-thumbnail" src="{{ config('image.no_image') }}" alt=""></td>
                        @endif
                            @foreach($field->fields_list()->get() as $key => $item)
                                <td>
                                    <input placeholder="{{ ucfirst($item->key) }}" name="value[0][{{ $item->key }}][value]"
                                           type="{{ $item->type->alias }}" class="form-control {{ ($item->key == 'image') ? 'input-image' : '' }}">
                                    <input type="hidden" name="value[0][{{ $item->key }}][type]" value="{{ $item->type->alias }}">
                                </td>
                            @endforeach
                        <td width="32">
                            <button type="button" class="btn btn-icon btn-danger btn-xs"><i class="icon-minus2"></i>
                            </button>
                        </td>
                    </tr>
                    @else
                        
                        <?php $n = -1;?>
                        @foreach($field->values()->get() as $value)
                            <?php $n++;?>
                        <tr class="value-item">

                            @if(field_has_image($field))
                            <td><img  width="80"  class="img-thumbnail" src="{{ get_image_url(field_image($value)) }}" alt=""></td>
                            @endif

                            @foreach(unserialize($value->value) as $key => $item)

                                <td>
                                    <input  name="value[{{ $n }}][{{ $key }}][value]" value="{{ $item['value'] }}"
                                           type="{{ $item['type'] }}" class="form-control {{ ($key == 'image') ? 'input-image' : '' }}">
                                    <input type="hidden" name="value[{{ $n }}][{{ $key }}][type]" value="{{ $item['type'] }}">
                                </td>
                            @endforeach
                                <td width="32">
                                    <button data-link="{{ route('field.backend.fields.deleteValue', $value->id) }}" type="button" class="btn btn-icon btn-danger btn-xs"><i class="icon-minus2"></i>
                                    </button>
                                </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <button class="btn btn-primary">Lưu lại</button>
    {!! Form::close() !!}
@endsection


@section('scripts')
    <script>
        $(function () {
            $('#add-value-item').click(function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                var length = $('.value-table tbody tr').length;
                $.ajax({
                    url: url + '/' + length,
                    success: function (data) {
                        $('.value-table').append(data);
                    }
                });
            });

            $('.value-item').delegate('button', 'click', function(){
                $(this).closest('.value-item').remove();
                var link = $(this).data('link');
                $.get(link);
            });



            $('table').delegate('.img-thumbnail, .input-image', 'click', function (e) {
                e.preventDefault();
                var that = this;
                CKFinder.modal({
                    height: 600,
                    language: 'vi',
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (event) {
                            var file = event.data.files.first();
                            $(that).closest('tr').find('.img-thumbnail').attr('src', file.getUrl());
                            $(that).closest('tr').find('.input-image').val(file.getUrl());
                        });
                    }
                });
            });
        });
    </script>

@endsection


@section('themejs')
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
@endsection