@extends('main')

@section('title', trans('field::field.confirm_delete'))
@section('breadcrumbs')
{!! Breadcrumbs::render('field::confirm_delete', $field) !!}
@endsection


@section('content')
{!! Form::open(['method' => 'delete', 'route' => ['field.backend.fields.destroy', $field->id]]) !!}
        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="alert alert-danger">
            {{ trans('field::field.confirm_delete_message', ['name' => $field->name]) }}
        </div>

        {!! Form::submit(trans('field::field.yes_delete'), ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('field.backend.fields.index') }}" class="btn btn-success"> <strong>{{ trans('field::field.no_delete') }}</strong>
        </a>
    </div>
</div><!-- Panel End -->
{!! Form::close() !!}@endsection
