<table class="table table-responsive value-table">
    <tbody>
    @if($field->values()->count() == 0)
    <tr class="value-item">
        <td>
            <img src="{{ field_image($field) }}" alt="">
        </td>
        @foreach($field->fields_list()->get() as $key => $item)
        <td>
            <input placeholder="{{ ucfirst($item->key) }}" name="value[0][{{ $item->key }}][value]"
                   type="{{ $item->type->alias }}" class="form-control">
            <input type="hidden" name="value[0][{{ $item->key }}][type]" value="{{ $item->type->alias }}">
        </td>
        @endforeach
        <td width="32">
            <button type="button" class="btn btn-icon btn-danger btn-xs"><i class="icon-minus2"></i>
            </button>
        </td>
    </tr>
    @else
    @endif
    </tbody>
</table>