@for($i = 1; $i<=$num; $i++)
    <div class="form-group row">
        <div class="col-sm-5">
            <label for="">Key</label>
            {!! Form::text('_key['.$i.']', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-5">
            <label for="">Type</label>
            {!! Form::select('type_id['.$i.']', types_list(), null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-2">
            <label style="display: block" for="">.</label>
            <button type="button" class="btn btn-icon btn-xs btn-danger"><i class="icon-minus2"></i></button>
        </div>
    </div>
@endfor