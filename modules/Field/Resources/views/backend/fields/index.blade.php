@extends('main')

@section('title', 'Quản lý tùy chọn')

@section('breadcrumbs')
    {!! Breadcrumbs::render('field') !!}
@endsection

@section('content')

    <div class="col-md-5 col-lg-4">
        {!! Form::model($field,
            [
                'method' => $field->exists ? 'put' : 'post',
                'route' => $field->exists ? ['field.backend.fields.update', $field->id] : ['field.backend.fields.store'],
            ])
        !!}
        @include('errors.form')
                <!-- Tên danh mục  -->
        <div class="form-group">
            <label for="">{{ ucfirst(trans('field::field.name')) }}</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <div class="help-block">{{ trans('field::field.name_description') }}</div>
        </div>
        <!-- Tên danh mục  END-->

        <!-- KEY  -->
        <div class="form-group">
            <label for="">{{ trans('field::field.key') }}</label>
            {!! Form::text('key', null, ['class' => 'form-control']) !!}
            <div class="help-block">{{ trans('field::field.key_description') }}</div>
        </div>
        <!-- KEY  END-->

        <!-- Description  -->
        <div class="form-group">
            <label for="">{{ trans('common.description') }}</label>
            {!! Form::textarea('description', null, ['class' =>'form-control']) !!}
        </div>
        <!-- Description  END-->


        <!-- Num Fields List  -->
        <div class="form-group row">
            <label class="col-sm-12" for="">{{ trans('field::field.num_fields') }}</label>

            <div class="col-sm-6">
                <select id="num-fields" class="form-control">
                    @for($i = 1; $i<=10; $i++)
                        <option {{ ($field->fields_list()->count() == $i) ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>

        </div>

        <!-- End Num Fields -->


        <!-- Fields List  -->
        <div class="fields-list">
            @foreach($field->fields_list()->get() as $item)
            <div class="form-group row">
                <div class="col-sm-5">
                    <label for="">Key</label>
                    {!! Form::text('_key', $item->key, ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-5">
                    <label for="">Type</label>
                    {!! Form::select('type_id', types_list(), $item->type_id, ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-2">
                    <label style="display: block" for="">.</label>
                    <button type="button" class="btn btn-icon btn-xs btn-danger"><i class="icon-minus2"></i></button>
                </div>
            </div>
            @endforeach
        </div>
        <!-- Fields List  END-->


        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Tiếp tục
            </button>
        </div>
        {!! Form::close() !!}
    </div>


    <!-- List Blog Categories Start -->
    <div class="col-md-7 col-lg-8">
        {!! Form::open(['route' => 'field.backend.fields.bulk']) !!}
                <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
        </div>
        <!-- Bulk Action End -->

        <div class="clearfix"></div>


        <div class="panel panel-flat">
            <div class="panel-body no-padding">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="40"><input class="styled top-check" type="checkbox"></th>
                            <th>{{ ucfirst(trans('field::field.name')) }}</th>
                            <th>{{ trans('field::field.key') }}</th>
                            <th width="250">{{ trans('common.description') }}</th>
                            <th width="150"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fields as $field)
                            <tr>
                                <td><input type="checkbox" class="styled item-checkbox" name="id[]"
                                           value="{{ $field->id }}"></td>
                                <td>
                                    <a href="{{ route('field.backend.fields.edit', $field->id) }}">{{ $field->name }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('field.backend.fields.edit', $field->id) }}">{{ $field->key }}</a>
                                </td>
                                <td>{{ $field->description }}</td>
                                <td>
                                    <!-- edit button  -->
                                    <a class="btn btn-xs btn-icon btn-default" data-popup="tooltip" title="Sửa tùy chọn"
                                       data-placement="top"
                                       href="{{ route('field.backend.fields.edit', $field->id) }}">
                                        <i class="icon-pen"></i>
                                    </a>
                                    <!-- edit button  END-->

                                    <!-- Delete  -->
                                    <a class="btn btn-xs btn-icon btn-danger" data-popup="tooltip"
                                       title="{{ trans('field::field.delete') }}"
                                       data-placement="top"
                                       href="{{ route('field.backend.fields.confirm', $field->id) }}">
                                        <i class="icon-minus2"></i>
                                    </a>
                                    <!-- Delete  END-->

                                    <!-- Value manager  -->
                                    <a class="btn btn-xs btn-icon btn-info" data-popup="tooltip"
                                       title="{{ trans('field::field.value_manager') }}"
                                       data-placement="top"
                                       href="{{ route('field.backend.fields.value', $field->id) }}">
                                        <i class="icon-database-add"></i>
                                    </a>
                                    <!-- Value manager  END-->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                        <tfoot>
                        <tr>
                            <th><input class="styled top-check" type="checkbox"></th>
                            <th>{{ ucfirst(trans('field::field.name')) }}</th>
                            <th>{{ trans('field::field.key') }}</th>
                            <th width="250">{{ trans('common.description') }}</th>
                            <th></th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>

        </div>

        <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
            <div class="pull-right">
                {!! $fields->render() !!}
            </div>
        </div>
        <!-- Bulk Action End -->
        {!! Form::close() !!}
    </div>
    <!-- List Blog Categories End -->


@endsection


@section('scripts')
    <script>
        $('#thumbnail').click(function (e) {
            e.preventDefault();
            CKFinder.modal({
                height: 600,
                language: 'vi',
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (event) {
                        var file = event.data.files.first();
                        $('#thumbnail').attr('src', file.getUrl());
                        $('#input-thumbnail').val(file.getUrl());
                    });
                }
            });
        });


        function updateFieldsList(numFields) {

            $.ajax({
                url: "{{ url('field/backend/num-fields') }}/" + numFields,
                success: function (data) {
                    $('.fields-list').html(data);
                }
            });
        }
                @if(! $field->exists)
                var numFields = $('#num-fields').val();
        updateFieldsList(numFields);
        @endif

                $('#num-fields').on('change', function () {
                    var numFields = $(this).val();
                    updateFieldsList(numFields);

                });

        $('.fields-list').delegate('button', 'click', function () {
            var length = $('.fields-list .form-group').length;
            if (length > 1) {
                $(this).closest('.form-group').remove();
                $('#num-fields option').each(function (index, value) {

                });
            }
        });

    </script>

@endsection


@section('themejs')
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
@endsection