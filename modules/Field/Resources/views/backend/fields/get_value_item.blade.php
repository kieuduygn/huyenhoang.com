<tr class="value-item">
    @if(field_has_image($field))
        <td><img  width="80"  class="img-thumbnail" src="{{ config('image.no_image') }}" alt=""></td>
    @endif
    @foreach($field->fields_list()->get() as $key => $item)

        <td>
            <input placeholder="{{ ucfirst($item->key) }}" name="value[{{ $length }}][{{ $item->key }}][value]"
                   type="{{ $item->type->alias }}" class="form-control {{ ($item->key == 'image') ? 'input-image' : '' }}">
            <input type="hidden" name="value[{{ $length }}][{{ $item->key }}][type]" value="{{ $item->type->alias }}">
        </td>
    @endforeach
    <td width="32">
        <button type="button" class="btn btn-icon btn-danger btn-xs"><i class="icon-minus2"></i>
        </button>
    </td>
</tr>