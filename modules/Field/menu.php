<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-03-06
 * Time: 7:16 PM
 */

return [
    'head' => true,
    'head_title' => 'Các tùy chọn',
    'items' => [
        [
            'sub' => false,
            'items' => [
                [
                    'name' => 'Quản lý tùy chọn',
                    'route' => 'field.backend.fields.index',
                ],
            ]
        ],
    ]
];