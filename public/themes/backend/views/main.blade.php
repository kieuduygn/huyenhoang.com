<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.backend.metadata')
    <meta name="csrf-token" content="{!! Session::token() !!}">
    @yield('themejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/core/app.js"></script>
    @yield('pagejs')
</head>

<body>

<!-- Main navbar -->
@include('partials.backend.navbar')
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->@include('partials.backend.sidebar')<!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i>
                            <span class="text-semibold">@yield('title')</h4>
                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            @yield('functions')
                        </div>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    @yield('breadcrumbs')
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                @include('flash')
                @yield('content')
                        <!-- Footer -->
                @include('partials.backend.footer')
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>


<!-- /page container -->
@yield('scripts')

</body>
</html>
