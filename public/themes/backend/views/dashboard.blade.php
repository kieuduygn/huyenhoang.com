@extends('main')

        @section('title', 'Trang chủ')

@section('content')

        <p>Xin lưu ý, website Premiumlens không được phát triển các tính năng như quản lý marketing, đo lượng truy cập, hệ thống hỗ trợ. Vì vậy Dashboard này chỉ mang tính tham khảo, nếu sau này quý khách có nhu cầu phát triển sẽ thực hiện nhanh hơn</p>


        <!-- Main charts -->
        <div class="row">
            <div class="col-lg-7">

                <!-- Traffic sources -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Truy cập từ</h6>

                        <div class="heading-elements">
                            <form class="heading-form" action="#">
                                <div class="form-group">
                                    <label class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
                                        <input type="checkbox" class="switch" checked="checked"> Live update: </label>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4">
                                <ul class="list-inline text-center">
                                    <li>
                                        <a href="#" class="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-plus3"></i></a>
                                    </li>
                                    <li class="text-left">
                                        <div class="text-semibold">New visitors</div>
                                        <div class="text-muted">2,349 avg</div>
                                    </li>
                                </ul>

                                <div class="col-lg-10 col-lg-offset-1">
                                    <div class="content-group" id="new-visitors"></div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <ul class="list-inline text-center">
                                    <li>
                                        <a href="#" class="btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-watch2"></i></a>
                                    </li>
                                    <li class="text-left">
                                        <div class="text-semibold">New sessions</div>
                                        <div class="text-muted">08:20 avg</div>
                                    </li>
                                </ul>

                                <div class="col-lg-10 col-lg-offset-1">
                                    <div class="content-group" id="new-sessions"></div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <ul class="list-inline text-center">
                                    <li>
                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-people"></i></a>
                                    </li>
                                    <li class="text-left">
                                        <div class="text-semibold">Total online</div>
                                        <div class="text-muted">
                                            <span class="status-mark border-success position-left"></span> 5,378 avg
                                        </div>
                                    </li>
                                </ul>

                                <div class="col-lg-10 col-lg-offset-1">
                                    <div class="content-group" id="total-online"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="position-relative" id="traffic-sources"></div>
                </div>
                <!-- /traffic sources -->

            </div>

            <div class="col-lg-5">

                <!-- Sales stats -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Tình hình kinh doanh</h6>

                        <div class="heading-elements">
                            <form class="heading-form" action="#">
                                <div class="form-group">
                                    <select class="change-date select-sm" id="select_date">
                                        <optgroup label="<i class='icon-watch pull-right'></i> Time period">
                                            <option value="val1">June, 29 - July, 5</option>
                                            <option value="val2">June, 22 - June 28</option>
                                            <option value="val3" selected="selected">June, 15 - June, 21</option>
                                            <option value="val4">June, 8 - June, 14</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row text-center">
                            <div class="col-md-4">
                                <div class="content-group">
                                    <h5 class="text-semibold no-margin">
                                        <i class="icon-calendar5 position-left text-slate"></i> 5,689</h5>
                                    <span class="text-muted text-size-small">orders weekly</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="content-group">
                                    <h5 class="text-semibold no-margin">
                                        <i class="icon-calendar52 position-left text-slate"></i> 32,568</h5>
                                    <span class="text-muted text-size-small">orders monthly</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="content-group">
                                    <h5 class="text-semibold no-margin">
                                        <i class="icon-cash3 position-left text-slate"></i> $23,464</h5>
                                    <span class="text-muted text-size-small">average revenue</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content-group-sm" id="app_sales"></div>
                    <div id="monthly-sales-stats"></div>
                </div>
                <!-- /sales stats -->

            </div>
        </div>
        <!-- /main charts -->


        <!-- Dashboard content -->
        <div class="row">
            <div class="col-lg-8">



                <!-- Support tickets -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Support tickets</h6>

                        <div class="heading-elements">
                            <button type="button" class="btn btn-link daterange-ranges heading-btn text-semibold">
                                <i class="icon-calendar3 position-left"></i> <span></span> <b class="caret"></b>
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-xlg text-nowrap">
                            <tbody>
                            <tr>
                                <td class="col-md-4">
                                    <div class="media-left media-middle">
                                        <div id="tickets-status"></div>
                                    </div>

                                    <div class="media-left">
                                        <h5 class="text-semibold no-margin">14,327
                                            <small class="text-success text-size-base">
                                                <i class="icon-arrow-up12"></i> (+2.9%)
                                            </small>
                                        </h5>
                                        <span class="text-muted"><span class="status-mark border-success position-left"></span> Jun 16, 10:00 am</span>
                                    </div>
                                </td>

                                <td class="col-md-3">
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm-add"></i></a>
                                    </div>

                                    <div class="media-left">
                                        <h5 class="text-semibold no-margin">
                                            1,132
                                            <small class="display-block no-margin">total tickets</small>
                                        </h5>
                                    </div>
                                </td>

                                <td class="col-md-3">
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-spinner11"></i></a>
                                    </div>

                                    <div class="media-left">
                                        <h5 class="text-semibold no-margin">
                                            06:25:00
                                            <small class="display-block no-margin">response time</small>
                                        </h5>
                                    </div>
                                </td>

                                <td class="text-right col-md-2">
                                    <a href="#" class="btn bg-teal-400"><i class="icon-statistics position-left"></i> Report</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <thead>
                            <tr>
                                <th style="width: 50px">Due</th>
                                <th style="width: 300px;">User</th>
                                <th>Description</th>
                                <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="active border-double">
                                <td colspan="3">Active tickets</td>
                                <td class="text-right">
                                    <span class="badge bg-blue">24</span>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin">12
                                        <small class="display-block text-size-small no-margin">hours</small>
                                    </h6>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon"></span> </a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Annabelle Doney</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-blue position-left"></span> Active
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block">
                                        <span class="text-semibold">[#1183] Workaround for OS X selects printing bug</span>
                                        <span class="display-block text-muted">Chrome fixed the bug several versions ago, thus rendering this...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#"><i class="icon-checkmark3 text-success"></i> Resolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin">16
                                        <small class="display-block text-size-small no-margin">hours</small>
                                    </h6>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#"><img src="{{ url('assets/') }}/images/demo/users/face15.jpg" class="img-circle img-xs" alt=""></a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Chris Macintyre</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-blue position-left"></span> Active
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block">
                                        <span class="text-semibold">[#1249] Vertically center carousel controls</span>
                                        <span class="display-block text-muted">Try any carousel control and reduce the screen width below...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#"><i class="icon-checkmark3 text-success"></i> Resolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin">20
                                        <small class="display-block text-size-small no-margin">hours</small>
                                    </h6>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn bg-blue btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon"></span> </a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Robert Hauber</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-blue position-left"></span> Active
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block">
                                        <span class="text-semibold">[#1254] Inaccurate small pagination height</span>
                                        <span class="display-block text-muted">The height of pagination elements is not consistent with...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#"><i class="icon-checkmark3 text-success"></i> Resolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <h6 class="no-margin">40
                                        <small class="display-block text-size-small no-margin">hours</small>
                                    </h6>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn bg-warning-400 btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon"></span> </a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Dex Sponheim</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-blue position-left"></span> Active
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block">
                                        <span class="text-semibold">[#1184] Round grid column gutter operations</span>
                                        <span class="display-block text-muted">Left rounds up, right rounds down. should keep everything...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#"><i class="icon-checkmark3 text-success"></i> Resolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr class="active border-double">
                                <td colspan="3">Resolved tickets</td>
                                <td class="text-right">
                                    <span class="badge bg-success">42</span>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <i class="icon-checkmark3 text-success"></i>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn bg-success-400 btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon"></span> </a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default letter-icon-title">Alan Macedo</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-success position-left"></span> Resolved
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block"> [#1046] Avoid some unnecessary HTML string
                                        <span class="display-block text-muted">Rather than building a string of HTML and then parsing it...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-plus3 text-blue"></i> Unresolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <i class="icon-checkmark3 text-success"></i>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn bg-pink-400 btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon"></span> </a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default letter-icon-title">Brett Castellano</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-success position-left"></span> Resolved
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block"> [#1038] Update json configuration
                                        <span class="display-block text-muted">The <code>files</code> property is necessary to override the files property...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-plus3 text-blue"></i> Unresolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <i class="icon-checkmark3 text-success"></i>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#"><img src="{{ url('assets/') }}/images/demo/users/face3.jpg" class="img-circle img-xs" alt=""></a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default">Roxanne Forbes</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-success position-left"></span> Resolved
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block"> [#1034] Tooltip multiple event
                                        <span class="display-block text-muted">Fix behavior when using tooltips and popovers that are...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-plus3 text-blue"></i> Unresolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr class="active border-double">
                                <td colspan="3">Closed tickets</td>
                                <td class="text-right">
                                    <span class="badge bg-danger">37</span>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <i class="icon-cross2 text-danger-400"></i>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#"><img src="{{ url('assets/') }}/images/demo/users/face8.jpg" class="img-circle img-xs" alt=""></a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default">Mitchell Sitkin</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-danger position-left"></span> Closed
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block"> [#1040] Account for static form controls in form group
                                        <span class="display-block text-muted">Resizes control label's font-size and account for the standard...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropup">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#"><i class="icon-reload-alt text-blue"></i> Reopen issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    <i class="icon-cross2 text-danger"></i>
                                </td>
                                <td>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn bg-brown-400 btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon"></span> </a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="display-inline-block text-default letter-icon-title">Katleen Jensen</a>

                                        <div class="text-muted text-size-small">
                                            <span class="status-mark border-danger position-left"></span> Closed
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-default display-inline-block"> [#1038] Proper sizing of form control feedback
                                        <span class="display-block text-muted">Feedback icon sizing inside a larger/smaller form-group...</span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropup">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-plus3 text-blue"></i> Unresolve issue</a>
                                                </li>
                                                <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /support tickets -->


            </div>

            <div class="col-lg-4">


                <!-- Daily sales -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Trạng thái bán hàng theo giờ</h6>

                        <div class="heading-elements">
                            <span class="heading-text">Balance: <span class="text-bold text-danger-600 position-right">$4,378</span></span>
                            <ul class="icons-list">
                                <li class="dropdown text-muted">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i>
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                                        <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                                        <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div id="sales-heatmap"></div>
                    </div>


                </div>
                <!-- /daily sales -->


                <!-- My messages -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Tin nhắn</h6>

                        <div class="heading-elements">
                            <span class="heading-text"><i class="icon-history text-warning position-left"></i> Jul 7, 10:30</span>
                            <span class="label bg-success heading-text">Online</span>
                        </div>
                    </div>

                    <!-- Numbers -->
                    <div class="container-fluid">
                        <div class="row text-center">
                            <div class="col-md-4">
                                <div class="content-group">
                                    <h6 class="text-semibold no-margin">
                                        <i class="icon-clipboard3 position-left text-slate"></i> 2,345</h6>
                                    <span class="text-muted text-size-small">this week</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="content-group">
                                    <h6 class="text-semibold no-margin">
                                        <i class="icon-calendar3 position-left text-slate"></i> 3,568</h6>
                                    <span class="text-muted text-size-small">this month</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="content-group">
                                    <h6 class="text-semibold no-margin">
                                        <i class="icon-comments position-left text-slate"></i> 32,693</h6>
                                    <span class="text-muted text-size-small">all messages</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /numbers -->


                    <!-- Area chart -->
                    <div id="messages-stats"></div>
                    <!-- /area chart -->


                    <!-- Tabs -->
                    <ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">
                        <li class="active">
                            <a href="#messages-tue" class="text-size-small text-uppercase" data-toggle="tab"> Tuesday </a>
                        </li>

                        <li>
                            <a href="#messages-mon" class="text-size-small text-uppercase" data-toggle="tab"> Monday </a>
                        </li>

                        <li>
                            <a href="#messages-fri" class="text-size-small text-uppercase" data-toggle="tab"> Friday </a>
                        </li>
                    </ul>
                    <!-- /tabs -->


                    <!-- Tabs content -->
                    <div class="tab-content">
                        <div class="tab-pane active fade in has-padding" id="messages-tue">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face10.jpg" class="img-circle img-xs" alt="">
                                        <span class="badge bg-danger-400 media-badge">8</span>
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> James Alexander
                                            <span class="media-annotation pull-right">14:58</span> </a>

                                        <span class="display-block text-muted">The constitutionally inventoried precariously...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face3.jpg" class="img-circle img-xs" alt="">
                                        <span class="badge bg-danger-400 media-badge">6</span>
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Margo Baker <span class="media-annotation pull-right">12:16</span>
                                        </a>

                                        <span class="display-block text-muted">Pinched a well more moral chose goodness...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face24.jpg" class="img-circle img-xs" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Jeremy Victorino
                                            <span class="media-annotation pull-right">09:48</span> </a>

                                        <span class="display-block text-muted">Pert thickly mischievous clung frowned well...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face4.jpg" class="img-circle img-xs" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Beatrix Diaz <span class="media-annotation pull-right">05:54</span>
                                        </a>

                                        <span class="display-block text-muted">Nightingale taped hello bucolic fussily cardinal...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face25.jpg" class="img-circle img-xs" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Richard Vango
                                            <span class="media-annotation pull-right">01:43</span> </a>

                                        <span class="display-block text-muted">Amidst roadrunner distantly pompously where...</span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-pane fade has-padding" id="messages-mon">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face2.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Isak Temes
                                            <span class="media-annotation pull-right">Tue, 19:58</span> </a>

                                        <span class="display-block text-muted">Reasonable palpably rankly expressly grimy...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face7.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Vittorio Cosgrove
                                            <span class="media-annotation pull-right">Tue, 16:35</span> </a>

                                        <span class="display-block text-muted">Arguably therefore more unexplainable fumed...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face18.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Hilary Talaugon
                                            <span class="media-annotation pull-right">Tue, 12:16</span> </a>

                                        <span class="display-block text-muted">Nicely unlike porpoise a kookaburra past more...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face14.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Bobbie Seber
                                            <span class="media-annotation pull-right">Tue, 09:20</span> </a>

                                        <span class="display-block text-muted">Before visual vigilantly fortuitous tortoise...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face8.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Walther Laws
                                            <span class="media-annotation pull-right">Tue, 03:29</span> </a>

                                        <span class="display-block text-muted">Far affecting more leered unerringly dishonest...</span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-pane fade has-padding" id="messages-fri">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face15.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Owen Stretch
                                            <span class="media-annotation pull-right">Mon, 18:12</span> </a>

                                        <span class="display-block text-muted">Tardy rattlesnake seal raptly earthworm...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face12.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Jenilee Mcnair
                                            <span class="media-annotation pull-right">Mon, 14:03</span> </a>

                                        <span class="display-block text-muted">Since hello dear pushed amid darn trite...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face22.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Alaster Jain
                                            <span class="media-annotation pull-right">Mon, 13:59</span> </a>

                                        <span class="display-block text-muted">Dachshund cardinal dear next jeepers well...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face24.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Sigfrid Thisted
                                            <span class="media-annotation pull-right">Mon, 09:26</span> </a>

                                        <span class="display-block text-muted">Lighted wolf yikes less lemur crud grunted...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ url('assets/') }}/images/demo/users/face17.jpg" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#"> Sherilyn Mckee
                                            <span class="media-annotation pull-right">Mon, 06:38</span> </a>

                                        <span class="display-block text-muted">Less unicorn a however careless husky...</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /tabs content -->

                </div>
                <!-- /my messages -->


            </div>
        </div>
        <!-- /dashboard content -->



    @endsection

    @section('themejs')<!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('assets/') }}/js/pages/dashboard.js"></script>
    <!-- /theme JS files -->

@endsection