@extends('sub')
@section('title', 'Mua ảnh ' . image_code($image, $album))
@section('description', 'Đặt mua ảnh')
@section('url', route('gallery.album.order', [$image->id, $album->slug]))
@section('thumbnail', url() . $image->file_path)

@section('content')
    <section class="main-contents padding-bottom70">
        <div class="padding-top50 padding-bottom50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                        <header class="section-title section-title-style6">
                            <h3>BUY &amp; DOWNLOAD PHOTO</h3>
                            {!! Breadcrumbs::render('gallery::album.order', $album->categories()->first(), $album, $image) !!}
                        </header>
                    </div>
                    <!-- /col-md-12 -->

                </div>
                <!-- /row -->
                <div class="padding-top30 sm-padding-top30  main-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="widget widget-contact-form widget-contact-form-style1">
                                    <h6 class="widget-title heading-alt-style8 short-underline">Order Form</h6>
                                    @include('errors.form')
                                    @include('flash')
                                    {!! Form::open(['route' => 'gallery.album.postOrder']) !!}
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::text('name', null, ['placeholder' => 'Name']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::email('email', null, ['placeholder' => 'Email']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('address', null, ['placeholder' => 'Address']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('phone', null, ['placeholder' => 'Phone']) !!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::textarea('note', null, ['placeholder' => 'Note']) !!}
                                            </div>
                                            {!! Form::hidden('image_id', $image->id) !!}
                                            {!! Form::hidden('album_id', $album->id) !!}
                                            <input type="submit" class="red-bg form-submit" name="form-submit" value="ORDER">
                                        </div><!-- /row -->
                                    {!! Form::close() !!}

                                </div><!-- /widget-contact-form -->
                            </div><!-- /col-md-8 -->
                        </div><!-- /row -->
                    </div>
                    <!-- /container -->
                </div>

            </div>
            <!-- /container -->
        </div>
    </section>
    @stop