@extends('sub')
@section('title', get_title($tag))
@section('description', get_description($tag))
@section('url', route('gallery.category.show', $tag->slug))
@section('thumbnail', url() . $tag->thumbnail)
@section('content')
    <section class="main-contents padding-bottom70">
        <div class="padding-top50 padding-bottom50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInLeft">
                        <header class="section-title section-title-style6">
                            <h3>{{ $tag->name }}</h3>
                            {!! Breadcrumbs::render('gallery::tag', $tag) !!}
                        </header>
                    </div>
                    <!-- /col-md-12 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <div class="container">
            <div class="row">
                <div class="portfolio-container columns3">

                    <div class="portfolio-item-wrapper portfolio-item-wrapper-style2 row">
                        @foreach($albums as $album)
                        <div class="portfolio-item-container branding col-md-4">
                            <div class="portfolio-item">
                                <figure>
                                        <i class="image" style="background: url('{{ $album->thumbnail }}')"></i>
                                    <div class="overlay"> <a href="{{ route('gallery.album.show', [$album->id, $album->slug]) }}" class="icon-link"><i class="fa fa-pencil"></i></a></div>
                                    <!-- /overlay -->
                                </figure>
                                <div class="portfolio-item-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{ route('gallery.album.show', [$album->id, $album->slug]) }}"><h6 class="heading-alt-style9">{{ $album->name }}</h6></a>
                                        </div>
                                        <!-- /col-md-12 -->
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /portfolio-item-details -->
                            </div>
                            <!-- /portfolio-item -->
                        </div>
                        <!-- /col-md-4 -->
                        @endforeach
                    </div>
                    <!-- /portfolio-item-wrapper -->

                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>
@endsection