@extends('sub')

@section('title', get_title($album))
@section('description', get_description($album))
@section('url', route('gallery.album.show', [$album->id, $album->slug]))
@section('thumbnail', url() . $album->thumbnail)
@section('content')

    <section class="main-contents padding-bottom70">
        <div class="padding-top50 padding-bottom50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInLeft">
                        <header class="section-title section-title-style6">
                            <h3>{{ $album->name }}</h3>
                            {!! Breadcrumbs::render('gallery::album.show', $album->categories()->first(), $album) !!}
                        </header>
                    </div>
                    <!-- /col-md-12 -->
                </div>
                <!-- /row -->
            </div>

            <div class="portfolio-container portfolio-fullwidth no-gap columns4">
                <div class="container">
                    <div class="row">
                        <div class="portfolio-item-wrapper portfolio-item-wrapper-style2 row">
                            @foreach($album->images()->get() as $image)
                                <div class="portfolio-item-container col-md-3">
                                    <div class="portfolio-item">
                                        <figure>
                                            <i class="image" style="background: url('{{ $image->file_path }}')"></i>

                                            <div class="overlay">
                                                <a href="{{ $image->file_path }}"
                                                   class="icon-zoom view-image image-popup-link"><i
                                                            class="icon-knight-293"></i></a>
                                                @if($album->categories()->first()->trade == 1)
                                                    <a href="{{ route('gallery.album.order', [$image->id, $album->slug]) }}"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                @endif
                                            </div>
                                            <!-- /overlay -->
                                        </figure>
										@if($album->categories()->first()->trade == 1)
                                        <div class="portfolio-item-details">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h6 class="heading-alt-style9">{{ image_code($image, $album) }}</h6>
                                                </div>
                                                <!-- /col-md-12 -->
                                            </div>
                                            <!-- /row -->
                                        </div>
										@endif
                                    </div>
                                    <!-- /portfolio-item -->
                                </div><!-- /col-md-3 -->
                            @endforeach
                        </div>
                        @if($album->tags()->count() > 0)
                                <!-- /portfolio-item-wrapper -->
                        <div class="post-meta col-md-12 padding-top15">
                            <i class="fa fa-tags"></i> Tags:
                            @foreach($album->tags()->get() as $key =>  $tag)
                                <a href="{{ route('gallery.tag', $tag->slug) }}">{{ $tag->name }}</a>
                                @if($key < $album->tags()->count() - 2)
                                    <span>,</span>
                                @endif

                            @endforeach
                        </div>
                        @endif


                        <div class="post-meta col-md-12 padding-top15">

                            <div class="fb-like"
                                 data-href="{{ route('gallery.album.show', [$album->id, $album->slug]) }}"
                                 data-layout="button_count" data-action="like" data-show-faces="true"
                                 data-share="true"></div>
								 
							<div class="fb-comments" data-width="100%" data-href="{{ route('gallery.album.show', [$album->id, $album->slug]) }}" data-numposts="5"></div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- /container -->
    </section>

@endsection

@section('scripts')
    <script>
        $(function(){
            $('figure').delegate('.view-image', 'click', function(){
                var orderButton = $(this).next('a');
                orderButton.clone().prependTo('body')
                        .text('Order this image')
                        .css('z-index', '9999999999999999999')
                        .css('left', '50%')
                        .css('margin-left', '-60px')
                                .addClass('btn-success btn').css('position', 'fixed').css('bottom', '50px');
            })
        });
    </script>

    @stop


