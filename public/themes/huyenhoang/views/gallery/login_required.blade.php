@extends('sub')
@section('title', 'Login Required')
@section('description', 'Login Required')
@section('url', route('gallery.category.login'))
@section('thumbnail', url())

@section('content')
<section class="main-contents padding-bottom70">
    <div class="padding-top50 padding-bottom50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                    <header class="section-title section-title-style6">
                        <h3>EVENTS</h3>
                        {!! Breadcrumbs::render('gallery::login_required') !!}
                    </header>
                    @include('flash')
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->
        </div>

        <div class="portfolio-container portfolio-fullwidth columns4">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="account-form-wrapper login-form-wrapper">
                            <header class="section-title section-title-style2">
                                <h4>Thông báo</h4>
                                <p>Để xem được thông tin và hình ảnh thuộc Albums Events , quý khách vui lòng đăng nhập bằng Uset /  Pass do Huyehoang photo cung cấp. Xin cám ơn Quý khách đã ghé thăm website của chúng tôi.</p>
                            </header>
                            <div class="empty-space40"></div>
                        </div>
                        <!-- /account-form -->
                    </div>
                    <!-- /col-md-6 -->
                    <div class="sm-empty-space30"></div>
                    <div class="col-md-6">

                        <div class="account-form-wrapper login-form-wrapper">
                            @include('errors.form')
                            {!! Form::open(['route' => 'gallery.category.postLogin']) !!}
                                <input type="hidden" value="event" name="component">
                                <div class="input-container clearfix">
                                    <label for="username" class="col-md-4 col-sm4 col-xs-4">Username:*</label>
                                    {!! Form::email('email',null, ['class' => 'col-md-8 col-sm-8 col-xs-8', 'placeholder' => 'Email']) !!}
                                </div>
                                <!-- /input-container -->
                                <div class="input-container clearfix">
                                    <label for="password" class="col-md-4 col-sm4 col-xs-4">Password:*</label>
                                    {!! Form::password('password', ['class' => 'col-md-8 col-sm-8 col-xs-8', 'placeholder' => 'Password']) !!}
                                </div>
                            {!! Form::hidden('category_id', $category->id) !!}
                                <!-- /input-container -->
                                <div class="clearfix"></div>
                                <div class="empty-space22"></div>
                                <input type="submit" name="btnDangnhap" class="button dark-bg icon-button-style2 pull-right" value="Submit"> <i class="icon2x icon-knight-645"></i> <span>Login</span>
                            {!! Form::close() !!}
                        </div>

                        <!-- /account-form -->
                    </div>
                    <!-- /col-md-6 -->
                </div>
                <!-- /row -->
            </div>
        </div>
    </div>
    <!-- /container -->
</section>

    @endsection