@extends('sub')
@section('title', get_settings('shop_name'))
@section('description', get_settings('short_description'))
@section('url', url('gallery')))
@section('thumbnail', theme('images/logo4-corporation.png'))

    @section('content')
            <!-- Start main-header -->
    <section class="main-contents padding-bottom70">
        <div class=" padding-top30 padding-bottom50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInLeft">
                        <header class="section-title section-title-style6">
                            <h3>GALLERY</h3>
                            {!! Breadcrumbs::render('gallery') !!}
                        </header>
                    </div>
                    <!-- /col-md-12 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <div class="container">
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-4 col-sm-6  wow fadeInLeft">
                        <div class="cover-box cover-box-style1">
                            <figure><a href="{{ route('gallery.category.show', $category->slug) }}"><img src="{{ $category->thumbnail }}"
                                                                 alt="{{ $category->name }}"></a></figure>
                            <div class="cover-box-details"><a href="{{ route('gallery.category.show', $category->slug) }}">
                                    <h6 class="heading-alt-style12">{{ $category->name }}</h6>
                                </a></div>
                            <!-- /cover-box-details -->
                        </div>
                        <!-- /cover-box -->
                    </div>
                    @endforeach
                            <!-- /col-md-4 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>
    <!-- End main-header -->

@endsection