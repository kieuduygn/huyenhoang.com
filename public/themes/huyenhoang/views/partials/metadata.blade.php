
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Huyenhoang Photo</title>
<meta name="description" content="Responsive Multi-Purpose HTML Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#222222">
<!-- Android 5.0 Tab Color -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Google Fonts -->
<!--<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500' rel='stylesheet' type='text/css'>-->
<!--<link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>-->

<!-- Icon Fonts CSS -->
<link rel="stylesheet" href="{{ theme() }}/css/knight-iconfont.css">
<link rel="stylesheet" href="{{ theme() }}/css/font-awesome.min.css">
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> -->

<!-- Vendor CSS -->
<link rel="stylesheet" href="{{ theme() }}/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ theme() }}/css/reset.css">

<!-- Plugins CSS -->
<link rel="stylesheet" href="{{ theme() }}/css/jquery.fs.shifter.css">
<link rel="stylesheet" href="{{ theme() }}/css/magnific-popup.css">
<link rel="stylesheet" href="{{ theme() }}/css/owl.carousel.css">
<link rel="stylesheet" href="{{ theme() }}/css/settings.css">
<link rel="stylesheet" href="{{ theme() }}/css/animate.css">

<!-- Template CSS -->
<link rel="stylesheet" href="{{ theme() }}/css/main.css">
<link rel="stylesheet" href="{{ theme() }}/css/shortcodes.css">
<link rel="stylesheet" href="{{ theme() }}/css/custom-bg.css">
<link rel="stylesheet" href="{{ theme() }}/css/custom-rain.css">

<!-- JS -->
<script src="js/vendor/modernizr-2.6.2.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,vietnamese' rel='stylesheet' type='text/css'>