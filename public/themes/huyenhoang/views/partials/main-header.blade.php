<header class="main-header style2 transparent-menu hidden-xs">
    <div class="main-header-inner">
        <div class="top-bar style1 dark-bg">
            <div class="container clearfix">
                <div class="left-sec clearfix">
                    <ul class="contact-info clearfix">
                        <li> <i class="icon icon-knight-284"></i><span>Phone: {{ get_settings('phone') }}</span> </li>
                        <li> <i class="icon icon-knight-365"></i><span>Email: <a href="mailto:{{ get_settings('email') }}">{{ get_settings('email') }}</a></span> </li>
                    </ul>
                </div>
                <!-- /contact-info -->
                <div class="right-sec clearfix">
                    <ul class="socials style1 clearfix">
                        <li><a href="{{ get_settings('facebook_page') }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{ get_settings('google_page') }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="{{ get_settings('instagram_page') }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="{{ get_settings('flickr') }}" target="_blank"><i class="fa fa-flickr"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- /info -->
        </div>
        <!-- /top-bar -->
        <div class="main-bar padding-15 dark-bg ">
            <div class="container">
                <div class="logo-container"> <a href="{{ url() }}" class="logo"> <img src="{{ theme() }}/images/logo4-corporation.png" alt="Huyenhoang photo logo"> </a> </div>
                <!-- /logo-container -->
                <div class="menu-container clearfix">
                    <nav class="main-nav active-style1 style1" id="main-nav">
                        {!! render_menu('main-menu', 'clearfix') !!}
                    </nav>

                </div>
                <!-- /menu-container -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-bar -->
    </div>
    <!-- /main-header-inner -->
@yield('slider')
    <!-- /tp-wrapper -->
</header>