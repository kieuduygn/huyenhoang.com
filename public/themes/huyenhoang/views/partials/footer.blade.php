<footer class="main-footer padding-top20 sm-padding-top20 dark-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12 padding-bottom15">
                <div class="row">
                    @widget('Facebook')
                            <!-- /widget-about -->
                </div>
                <!-- /row -->
            </div>
            <!-- /col-md-3 -->


            @foreach($categories->chunk(3) as $set)
            <div class="col-md-2 col-sm-3 col-xs-6  padding-bottom15">
                @foreach($set as $item)
                <p><a href="{{ route('gallery.category.show', $item->slug) }}">{{ $item->name }}</a></p>
                @endforeach
            </div>
            @endforeach
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="widget widget-contact-info widget-contact-info-style1">
                    <address>
                        <p class="clearfix"> <i class="icon icon-knight-220"></i><span>{{ get_settings('address') }}</span> </p>
                        <p class="clearfix"> <i class="icon icon-knight-284"></i><span>PHONE: {{ get_settings('phone') }} </span> </p>
                        <p class="clearfix"> <i class="icon icon-knight-365"></i><span>EMAIL: <a href="mailto:{{ get_settings('email') }}">{{ get_settings('email') }}</a></span> </p>
                        <p class="clearfix"> <i class="icon large-icon icon-knight-283"></i><span>WEB: <a class="underline" href="http://huyenhoang.com">www.huyenhoang.com</a></span> </p>
                    </address>
                </div>
                <!-- /widget-contact-info -->
            </div>
            <!-- /col-md-8 -->

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</footer>