<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
@include('partials.metadata')
</head>
<body class="shifter shifter-left offcanvas-menu-left offcanvas-menu-dark mobile-header-style2 sticky-header sticky-dark">
@include('partials.fb-root')
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- mobile-header-wrapper -->
@include('partials.mobile-header')
<!-- /mobile-header-wrapper -->

<!-- OFF CANVAS MOBILE MENU -->
<nav class="main-nav offcanvas-menu mobile-nav shifter-navigation fullwidth-items fullwidth-menu white-bg">
    <form action="#" class="search-form">
        <input type="search" name="mobile-search-form" id="mobile-search-form" placeholder="Search">
        <input type="submit" value="">
    </form>
</nav>
<div class="main-wrapper shifter-page">

    <!-- Start main-header -->
@include('partials.main-header')
    <!-- End main-header -->

@yield('content')
    <!-- Start main-footer -->
    @include('partials.footer')
    <!-- End main-footer -->

</div>
<!-- /main-wrapper -->

<!-- JS -->
<script src="{{ theme('js') }}/vendor/jquery.min.js"></script>
<script src="{{ theme('js') }}/jquery.themepunch.tools.min.js"></script>
<script src="{{ theme('js') }}/jquery.themepunch.revolution.min.js"></script>
<script src="{{ theme('js') }}/jquery.magnific-popup.min.js"></script>
<script src="{{ theme('js') }}/imagesloaded.pkgd.min.js"></script>
<script src="{{ theme('js') }}/jquery.fs.shifter.min.js"></script>
<script src="{{ theme('js') }}/jquery.stellar.min.js"></script>
<script src="{{ theme('js') }}/owl.carousel.min.js"></script>
<script src="{{ theme('js') }}/isotope.pkgd.min.js"></script>
<script src="{{ theme('js') }}/jquery.countTo.js"></script>
<script src="{{ theme('js') }}/jquery.appear.js"></script>
<script src="{{ theme('js') }}/wow.min.js"></script>
<script src="{{ theme('js') }}/main.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.tp-banner').revolution(
                {
                    delay:5000,
                    startwidth:1170,
                    startheight:670,
                    fullScreen: 'on',
                    navigationType: 'bullet',
                    navigationArrows: 'none',
                    navigationStyle: 'knight-1',
                    hideTimerBar: 'on'
                });
    });
</script>
</body>
</html>