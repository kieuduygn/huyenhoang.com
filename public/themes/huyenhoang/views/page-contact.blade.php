@extends('sub')

@section('content')
    <section class="main-contents padding-bottom70">
        <div class="padding-top50 padding-bottom50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInLeft">
                        <header class="section-title section-title-style6">
                            <h3>CONTACT US</h3>
                            @include('flash')
                            {!! Breadcrumbs::render('frontend_page', $item) !!}
                        </header>
                    </div>
                    <!-- /col-md-12 -->

                </div>
                <!-- /row -->
                <div class="padding-top30 sm-padding-top30  main-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12 col-sm-6">
                                        <div class="widget widget-contact-info widget-contact-info-style1">
                                            <h6 class="widget-title heading-alt-style8 short-underline">Contact
                                                Info</h6>
                                            <address>
                                                <p class="clearfix">
                                                    <i class="icon icon-knight-220"></i><span>{{ get_settings('address') }}</span>
                                                </p>

                                                <div class="empty-space38 sm-empty-space0"></div>
                                                <p class="clearfix">
                                                    <i class="icon icon-knight-284"></i><span>PHONE: {{ get_settings('phone') }}</span>
                                                </p>

                                                <p class="clearfix">
                                                    <i class="icon icon-knight-365"></i><span>EMAIL: <a
                                                                href="mailto:{{ get_settings('email') }}">{{ get_settings('email') }}</a></span>
                                                </p>

                                                <p class="clearfix">
                                                    <i class="icon large-icon icon-knight-283"></i><span>WEB: <a
                                                                class="underline" href="{{ url() }}">{{ url() }}</a></span>
                                                </p>
                                            </address>
                                        </div>
                                        <!-- /widget-contact-info -->
                                    </div>
                                    <!-- /col-sm-6 -->
                                </div>
                                <!-- /row -->
                            </div>
                            <!-- /col-md-3 -->
                            <div class="sm-empty-space35"></div>
                            <div class="col-md-8 col-md-offset-1">
                                <div class="widget widget-contact-form widget-contact-form-style1">
                                    <h6 class="widget-title heading-alt-style8 short-underline">Contact Form</h6>
                                    @include('errors.form')
                                    {!! Form::open(['url' => 'contact/post-message']) !!}
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::text('name', null, ['placeholder' => 'Name']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('email', null, ['placeholder' => 'Email']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('address', null, ['placeholder' => 'Add']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('phone', null, ['placeholder' => 'Phone']) !!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::text('subject', null, ['placeholder' => 'Subject']) !!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::textarea('content', null, ['placeholder' => 'Content']) !!}
                                            </div>
                                            <input type="submit" class="red-bg form-submit" name="form-submit"
                                                   value="Submit">
                                        </div>
                                        <!-- /row -->
                                    {!! Form::close() !!}
                                    <div class="contact-form-message"></div>
                                </div>
                                <!-- /widget-contact-form -->
                            </div>
                            <!-- /col-md-8 -->
                        </div>
                        <!-- /row -->
                    </div>
                    <!-- /container -->
                </div>

            </div>
            <!-- /container -->
        </div>
    </section>
@endsection