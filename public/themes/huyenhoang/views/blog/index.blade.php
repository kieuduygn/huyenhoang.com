@extends('sub')
@section('title', 'Blog')
@section('content')

        <!-- Start main-header -->
<section class="main-contents padding-bottom70">
    <div class="padding-top50 padding-bottom50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeInLeft">
                    <header class="section-title section-title-style6">
                        <h3>BLOG</h3>

                        {!! Breadcrumbs::render('blog') !!}
                    </header>
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->
        </div>

        <div class="portfolio-container portfolio-fullwidth columns4">
            <div class="container">
                <div class="row">
                    <section class="main-contents">
                        <div class="container">
                            <div class="row">
                                <div class="post-container blog-grid columns3 col-md-12 margin-bottom30">

                                    <div class="row">
                                        @foreach($posts as $post)
                                        <div class="col-md-4 item">
                                            <article class="post format-standard wow fadeInDown">
                                                <figure class="entry-image">
                                                    <a href="{{ route('blog.show', $post->slug) }}">
                                                        <img src="{{ $post->thumbnail }}"
                                                             alt="{{ $post->title }}">
                                                    </a>
                                                </figure>
                                                <h1 class="entry-title"><a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a></h1>

                                                <div class="entry-content">
                                                    <p>{{ $post->excerpt }} </p>
                                                </div>
                                                <!-- /entry-content -->
                                                <footer class="entry-meta">
                                                    <div id="fb-root"></div>
                                                    <div class="fb-like"
                                                         data-href="{{ route('blog.show', $post->slug) }}"
                                                         data-layout="button_count" data-action="like"
                                                         data-show-faces="true" data-share="true"></div>
                                                </footer>
                                            </article>
                                        </div>
                                        <!-- /col-md-4 -->
                                        @endforeach
                                    </div>

                                </div>
                                <!-- /post-container -->
                            </div>
                            <!-- /row -->
                        </div>
                        <!-- /container -->
                    </section>

                    <!-- /portfolio-item-wrapper -->
                </div>
            </div>
        </div>

    </div>
    <!-- /container -->
</section>
<!-- Start main-footer -->

@endsection
 