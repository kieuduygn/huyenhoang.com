@extends('sub')
@section('title', get_post_title($post))
@section('description', get_post_description($post))
@section('url', route('blog.show', $post->slug))
@section('thumbnail', url() . $post->thumbnail)
@section('content')

  <!-- Start main-header -->
  <section class="main-contents padding-bottom20">
    <div class="padding-top50 padding-bottom20">
      <div class="container">
        <div class="row">
          <div class="col-md-12 wow fadeInLeft">
            <header class="section-title section-title-style6">
              <h3>BLOG</h3>
              {!! Breadcrumbs::render('blog_show', $post) !!}
            </header>
          </div>
          <!-- /col-md-12 --> 
        </div>
        <!-- /row --> 
      </div>
   
   	 <div class="portfolio-container portfolio-fullwidth columns4">
     	<div class="container">
        <div class="row">
     	 <section class="main-contents">
                <div class="container">
                    <div class="row">

                        <div class="post-container blog-post-single col-md-9 wow fadeInLeft">

                            <article class="post format-standard wow fadeInLeft">
                                <h1 class="entry-title">{{ $post->title }}</h1>

                                <div class="entry-meta">
                                    <span>Posted: <a href="#"><time datetime="{{ $post->created_at->format('Y-m-d') }}">{{ $post->created_at->format('d M, Y') }}</time></a></span>
                                </div>

                                <div class="entry-content">
                                    {!! $post->body !!}


                                    <div class="fb-like" data-href="{{ route('blog.show', $post->slug) }}" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                                </div><!-- /entry-content -->

                                @if($post->related()->count() > 0)
                                <div class="entry-related-post">
                                    <h6 class="heading-alt-style9">Related Articles</h6>
                                    <div class="row">
                                        @foreach($post->related()->limit(3)->get() as $item)
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <article>
                                                <figure>
                                                    <a href="{{ route('blog.show', $item->slug) }}"><img src="{{ $item->thumbnail }}" alt="{{ $item->title }}"></a>
                                                </figure>
                                                <a href="{{ route('blog.show', $item->slug) }}">{{ $item->title }}</a>
                                            </article>
                                        </div><!-- /col-md-4 -->
                                        @endforeach
                                    </div><!-- /row -->
                                </div><!-- /entry-related-post -->
                                @endif


                                <div class="entry-comments-container">
                                </div><!-- /entry-comments-container -->
                                <div class="comment-respond">
                                    <h6 class="heading-alt-style9">Leave A Comment</h6>
                                    <div class="fb-comments" data-width="100%" data-href="{{ route('blog.show', $post->slug) }}" data-numposts="5"></div>

                                </div>
                            </article>

                        </div><!-- /post-container -->

                        <aside class="main-sidebar col-md-3 wow fadeInLeft" data-wow-delay="0.2s">
                            @widget('AlbumCategories')

                            @widget('LatestPosts')

                            @widget('LatestPhotos')
                        </aside>

                    </div><!-- /row -->
                </div><!-- /container -->
            </section>                
     <!-- /portfolio-item-wrapper -->
     </div>
     	</div>
     </div>

    </div>
    <!-- /container --> 
  </section>
  <!-- Start main-footer -->
@endsection