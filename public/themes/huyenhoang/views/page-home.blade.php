@extends('main')

@section('slider')
    <div class="tp-wrapper hidden-xs hidden-sm">
        <div class="tp-banner-container">
            <div class="tp-banner hidden-xs hidden-sm">
                <ul>
                    @foreach(field_values('home_slider') as $item)
                        <li data-transition="fade" data-slotamount="4" data-masterspeed="400"><img
                                    src="{{ field_value($item, 'image') }}" alt="huyenhoangphoto"></li>
                    @endforeach
                </ul>
            </div>
            <!-- /tp-banner -->
        </div>
        <!-- /tp-banner-container -->
    </div>
    @endsection


    @section('content')
            <!-- Start main-header -->
    <section class="main-contents padding-bottom70">
        <div class=" padding-top30 padding-bottom50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInLeft">
                        <header class="section-title section-title-style6">
                            <h3>GALLERY</h3>
                        </header>
                    </div>
                    <!-- /col-md-12 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <div class="container">
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-4 col-sm-6  wow fadeInLeft">
                        <div class="cover-box cover-box-style1">
                            <figure><a href="{{ route('gallery.category.show', $category->slug) }}"><img
                                            src="{{ $category->thumbnail }}"
                                            alt="{{ $category->name }}"></a></figure>
                            <div class="cover-box-details"><a
                                        href="{{ route('gallery.category.show', $category->slug) }}">
                                    <h6 class="heading-alt-style12">{{ $category->name }}</h6>
                                </a></div>
                            <!-- /cover-box-details -->
                        </div>
                        <!-- /cover-box -->
                    </div>
                    @endforeach
                            <!-- /col-md-4 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>
    <!-- End main-header -->
    <!-- Start main-contents -->
    <section class="main-contents">
        <div class="portfolio-container portfolio-fullwidth no-gap columns4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <header class="section-title  margin-bottom50">
                            <h3>Popular photos</h3>
                        </header>
                    </div>
                    <!-- /col-md-7 -->
                </div>
                <!-- /row -->

            </div>
            <!-- /container -->
            <div class="container">
                <div class="portfolio-item-wrapper portfolio-item-wrapper-style1 row">
                    @foreach($popularPhotos as $photo)
                        <div class="col-md-3 col-sm-6">
                            <div class="portfolio-item">
                                <figure>
                                    <i class="image" style="background: url('{{ $photo->file_path }}')"></i>

                                    <div class="overlay"><a href="{{ $photo->file_path }}"
                                                            class="icon-zoom image-popup-link"><i
                                                    class="icon-knight-293"></i></a></div>
                                    <!-- /overlay -->
                                </figure>
                            </div>
                            <!-- /portfolio-item -->
                        </div>
                        <!-- /col-md-3 -->
                    @endforeach
                </div>
                <!-- /portfolio-item-wrapper -->
            </div>
        </div>
        <!-- /row -->
    </section>
    <!-- End main-contents -->
    <!-- Start main-contents -->
    <section class="main-contents padding-top55 padding-bottom20">
        <div class="container margin-bottom20">
            <div class="row">
                <div class="col-md-12">
                    <header class="section-title">
                        <h3>Lastest Blog</h3>
                    </header>
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
        <div class="container">
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-4 col-sm-6 wow flipInY">
                        <div class="latest-postbox latest-postbox-style1">
                            <article>
                                <figure class="post-thumb"><a href="{{ route('blog.show', $post->slug) }}"><img
                                                src="{{ $post->thumbnail }}" alt="{{ $post->title }}"></a></figure>
                                <div class="post-content">
                                    <h1><a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a></h1>

                                    <p>{{ $post->excerpt }}</p>
                                </div>
                                <!-- /post-content -->
                                <div class="post-meta">
                                    <time datetime="2014-02-25"><a href="#">{{ $post->created_at->format('Y M d') }}</a>
                                    </time>
                                </div>
                                <!-- /post-meta -->
                            </article>
                        </div>
                        <!-- /latest-postbox -->
                    </div>
                    <!-- /col-md-4 -->
                @endforeach
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>
    <!-- End main-contents -->

@endsection