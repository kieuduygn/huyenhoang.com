@extends('sub')

@section('title', get_title($item))
@section('description', get_description($item))
@section('url', route('page', $item->uri))
@section('thumbnail', url($item->thumbnail))

@section('content')
<section class="main-contents padding-bottom70">
    <div class="padding-top50 padding-bottom50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeInLeft">
                    <header class="section-title section-title-style6">
                        <h3>{{ $item->name }}</h3>
                        {!! Breadcrumbs::render('frontend_page', $item) !!}
                    </header>
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->
        </div>

        <div class="portfolio-container portfolio-fullwidth columns4">
            <div class="container">
                <div class="row">
                    <section class="main-contents">
                        <div class="container">
                            <div class="row">

                                <div class="post-container blog-post-single col-md-9 wow fadeInLeft">

                                    <article class="post format-standard wow fadeInLeft">
                                        <div class="entry-content">
                                            {!! $item->content !!}
                                        </div><!-- /entry-content -->
                                        <footer class="entry-footer">
                                            <div class="fb-like" data-href="{{ route('page', $item->uri) }}" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                                        </footer>

                                        <div class="comment-respond">
                                            <div class="fb-comments" data-width="100%" data-href="{{ route('page', $item->uri) }}" data-numposts="5"></div>

                                        </div>

                                    </article>

                                </div><!-- /post-container -->

                                <aside class="main-sidebar col-md-3 wow fadeInLeft" data-wow-delay="0.2s">
                                    @widget('AlbumCategories')

                                    @widget('LatestPosts')


                                    @widget('LatestPhotos')

                                </aside>

                            </div><!-- /row -->
                        </div><!-- /container -->
                    </section>
                    <!-- /portfolio-item-wrapper -->
                </div>
            </div>
        </div>

    </div>
    <!-- /container -->
</section>
@endsection