<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') - Huyen Hoang Photo</title>
    <meta name="description" content="@yield('description')">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#222222">
    <!-- Android 5.0 Tab Color -->
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}">

    <!-- Google Fonts -->
    <!--<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500' rel='stylesheet' type='text/css'>-->
    <!--<link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>-->

    <!-- Icon Fonts CSS -->
    <link rel="stylesheet" href="{{ theme() }}/css/knight-iconfont.css">
    <link rel="stylesheet" href="{{ theme() }}/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/{{ theme() }}/css/font-awesome.min.css"> -->

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ theme() }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ theme() }}/css/reset.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ theme() }}/css/jquery.fs.shifter.css">
    <link rel="stylesheet" href="{{ theme() }}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{ theme() }}/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ theme() }}/css/settings.css">
    <link rel="stylesheet" href="{{ theme() }}/css/animate.css">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ theme() }}/css/main.css">
    <link rel="stylesheet" href="{{ theme() }}/css/shortcodes.css">
    <link rel="stylesheet" href="{{ theme() }}/css/custom-bg.css">
    <link rel="stylesheet" href="{{ theme() }}/css/custom-rain.css">

    <meta property="og:url"           content="@yield('url')" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="@yield('title') - Huyen Hoang Photo" />
    <meta property="og:description"   content="@yield('description')" />
    <meta property="og:image"         content="@yield('thumbnail')" />

    <!-- JS -->
    <script src="{{ theme() }}/js/vendor/modernizr-2.6.2.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
</head>
<body class="shifter shifter-left offcanvas-menu-left offcanvas-menu-dark mobile-header-style2 sticky-header sticky-dark">
@include('partials.fb-root')
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- mobile-header-wrapper -->
@include('partials.mobile-header')
<!-- /mobile-header-wrapper -->

<!-- OFF CANVAS MOBILE MENU -->
<nav class="main-nav offcanvas-menu mobile-nav shifter-navigation fullwidth-items fullwidth-menu white-bg">
    <form action="#" class="search-form">
        <input type="search" name="mobile-search-form" id="mobile-search-form" placeholder="Search">
        <input type="submit" value="">
    </form>
</nav>
<div class="main-wrapper shifter-page">

    <!-- Start main-header -->
    @include('partials.main-header')
    <!-- End main-header -->

    <!-- Start main-header -->
    @yield('content')
    <!-- End main-header -->

    <!-- Start main-footer -->
    @include('partials.footer')
    <!-- End main-footer -->

</div>
<!-- /main-wrapper -->

<!-- JS -->
<script src="{{ theme() }}/js/vendor/jquery.min.js"></script>
<script src="{{ theme() }}/js/jquery.themepunch.tools.min.js"></script>
<script src="{{ theme() }}/js/jquery.themepunch.revolution.min.js"></script>
<script src="{{ theme() }}/js/jquery.magnific-popup.min.js"></script>
<script src="{{ theme() }}/js/imagesloaded.pkgd.min.js"></script>
<script src="{{ theme() }}/js/jquery.fs.shifter.min.js"></script>
<script src="{{ theme() }}/js/jquery.stellar.min.js"></script>
<script src="{{ theme() }}/js/owl.carousel.min.js"></script>
<script src="{{ theme() }}/js/isotope.pkgd.min.js"></script>
<script src="{{ theme() }}/js/jquery.countTo.js"></script>
<script src="{{ theme() }}/js/jquery.appear.js"></script>
<script src="{{ theme() }}/js/wow.min.js"></script>
<script src="{{ theme() }}/js/main.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.tp-banner').revolution(
                {
                    delay:5000,
                    startwidth:1170,
                    startheight:670,
                    fullScreen: 'on',
                    navigationType: 'bullet',
                    navigationArrows: 'none',
                    navigationStyle: 'knight-1',
                    hideTimerBar: 'on'
                });
    });
</script>

@yield('scripts')
</body>
</html>