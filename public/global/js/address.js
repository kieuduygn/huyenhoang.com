/**
 * Created by Rain on 2016-02-18.
 */

    function fillState(a)
    {
        $.ajax({
            url: $('base').attr('href') + '/get-district/' + a,
            success: function(b){
                b = $.parseJSON(b);
                $('#list-districts').html(b.districts);
                $('#list-wards').html(b.wards);
            }
        })
    }


    function fillStateWithoutWard(a)
    {
        $.ajax({
            url: $('base').attr('href') + '/get-district/' + a,
            success: function(b){
                b = $.parseJSON(b);
                $('#list-districts').html(b.districts);
                $('#list-wards').html(b.wards);
            }
        })
    }




    function fillWard(a)
    {
        $.ajax({
            url: $('base').attr('href') + '/get-ward/' + a,
            success: function(b){
                $('#list-wards').html(b);
            }
        })
    }

