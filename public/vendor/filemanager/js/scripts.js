/**
 * Created by Rain on 2016-02-23.
 */
$(function () {

    var _token = $('meta[name="csrf-token"]').attr('content');
    console.log(_token);
    var base = $('base').attr('href');
    $('.list-folders').delegate('ul li span', 'click', function () {
        $('.list-folders ul li').removeClass('active');
        var folder = $(this).parent('li');
        $(folder).addClass('active');
        if ($(folder).hasClass('has-sub')) {
            var directory = $(this).text();
            var that = this;
            console.log(this);


            var jqxhr = $.post(base + '/backend/get_subdirectories',
                {
                    _token: _token,

                    directory : directory,
                },
                function (data) {
                    $(that).parent().append(data);
                });

        }
    })
})