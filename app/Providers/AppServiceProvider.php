<?php

namespace App\Providers;

use App\Models\Product;
use App\Models\Setting;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use App\View\ThemeViewFinder;
use Illuminate\Contracts\View\View;
use App\Http\Requests;
use Illuminate\Http\Request;
use Modules\Gallery\Entities\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.backend.sidebar', function($view){
            $view->with('route', Route::getCurrentRoute()->getPath());
        });

        //suggest Product
        view()->composer('products.you_may_like', function($view){
            $view->with('suggestProducts', Product::where('suggest', 1)->limit(4)->get());
        });

        //suggest Product
        view()->composer('partials.footer', function($view){
            $view->with('categories', Category::limit(6)->get());
        });



        view()->composer('cart.bill', function($view){
            if(session('cart.subtotal')){
                $subtotal = session('cart.subtotal');
            }else{
                $subtotal = 0;
            }

            if (session('coupon')) {
                $total = $subtotal - session('coupon')->discount;
            }else{
                $total = $subtotal;
            }

            if(session()->has('coupon')){
                $discount = session('coupon')->discount;
            }else{
                $discount = 0;
            }

            $view->with([
                'subtotal'  => $subtotal,
                'total' => $total,
                'discount'  => $discount,
            ]);
        });

        view()->composer('cart.information', function($view){
            if(session('cart.subtotal')){
                $subtotal = session('cart.subtotal');
            }else{
                $subtotal = 0;
            }

            if (session('coupon')) {
                $total = $subtotal - session('coupon')->discount;
            }else{
                $total = $subtotal;
            }

            if(session()->has('coupon')){
                $discount = session('coupon')->discount;
            }else{
                $discount = 0;
            }

            $view->with([
                'subtotal'  => $subtotal,
                'total' => $total,
                'discount'  => $discount,
            ]);
        });


        view()->composer('*', function ($view) {
            $setting = Setting::all()->toArray();

            $array = [];

            foreach($setting as $item){
                $array[$item['key']] = $item['value'];
            }

        });

        $this->app['view']->setFinder($this->app['theme.finder']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('theme.finder', function($app){
            $finder = new ThemeViewFinder($app['files'], $app['config']['view.paths']);

            $config = $app['config']['cms.theme'];

            $finder->setBasePath($app['path.public']. '/' . $config['folder']);
            $finder->setActiveTheme($config['active']);

            return $finder;
        });
    }
}
