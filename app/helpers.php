<?php
use App\Models\BlogCategory;
use App\Models\District;
use App\Models\MenuPosition;
use App\Models\Message;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Page;
use App\Models\Product;
use App\Models\Image;
use App\Models\ProductOption;
use App\Models\Province;
use App\Models\Review;
use App\Models\ThemeOption;
use App\Models\Ward;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;

if (!function_exists('flash')) {
    /**
     * @param null $title
     * @param null $message
     * @return \Illuminate\Foundation\Application|mixed
     */

    function flash($title = null, $message = null)
    {
        $flash = app('App\Http\Flash');

        if (func_num_args() == 0) {
            return $flash;
        }

        return $flash->message($title, $message);
    }

}


/**
 * @param $path
 * @param string $class
 * @return string
 */
if (!function_exists('set_active')) {
    function set_active($path, $class = 'active')
    {
        $currentPath = Request::path();
        return (Request::is($path)) ? $class : '';
    }
}


/**
 * Hàm này cần phải sửa lại, vì nếu làm thế này sẽ có rất nhiều query được thực hiện
 *
 * @param $key
 * @return mixed
 */
function get_settings($key)
{
    $settings = App\Models\Setting::all()->toArray();
    foreach ($settings as $item) {
        if ($item['key'] == $key) {
            return $item['value'];
        }
    }

    return '';

}


/**
 * @return int
 */
function cart_num()
{
    $items = session()->get('cart.products');
    if ($items) {
        return count($items);
    }

    return 0;
}


function cart_items()
{
    return session()->get('cart.products');
}


function cart($key)
{
    return session('cart.' . $key);
}

/**
 *
 * @param $date
 * @param string $format
 * @param int $increase
 * @return bool|string
 */
function format_date($date, $format = 'd-m-Y', $increase = 0)
{
    $timestamp = strtotime($date) + $increase;

    return date($format, $timestamp);
}

/**
 *
 *
 * @param $filename
 * @return string
 */
function create_image_path($filename)
{
    $time = time();
    $path = date('Y', $time) . '/' . date('m', $time) . '/' . date('d', $time) . '/' . str_random(5) . '-' . $filename;
    return $path;
}

if (!function_exists('get_image_url')) {
    /**
     * Lấy đường dẫn ảnh
     *
     * @param $filepath
     * @return string
     */
    function get_image_url($filepath)
    {
        if (filter_var($filepath, FILTER_VALIDATE_URL) === true) {
            return $filepath;
        }

        return url() . $filepath;
    }
}


function product_thumbnail($id)
{
    $product = Product::findOrFail($id);
    return get_image_url($product->thumbnail->file_path);
}


function save_file($file)
{
    $extension = $file->getClientOriginalExtension();
    $filePath = create_image_path($file->getClientOriginalName());
    Storage::put($filePath, File::get($file));

    $entry = new Image();
    $entry->mine = $file->getClientMimeType();
    $entry->original_filename = $file->getClientOriginalName();
    $entry->filename = $file->getFilename() . '.' . $extension;
    $entry->file_path = $filePath;

    $entry->save();

    return $entry;
}

function thumbnail($object)
{
    return config('app.url') . $object->thumbnail;
}


function remove_options_length()
{
    session()->remove('options_length');
    session()->save();
}


function create_options_list($option, $product)
{
    $array = [];
    $productOptions = ProductOption::where('option_id', $option->id)->where('product_id', $product->id)->get();
    foreach ($productOptions as $productOption) {
        $optionValue = OptionValue::findOrFail($productOption->option_value_id);
        $array[$productOption->id] = $optionValue->name;
    }
    return $array;
}


function options()
{
    return Option::all();
}

/**
 * @return collection
 */
function get_message()
{
    return Message::where('read', 0)->get();
}

/**
 * @return number
 */
function get_total_message()
{
    return Message::where('read', 0)->count();
}

function get_vn_weekday($time)
{
    $weekday = date("l", $time);
    $weekday = strtolower($weekday);
    switch ($weekday) {
        case 'monday':
            $weekday = 'Thứ hai';
            break;
        case 'tuesday':
            $weekday = 'Thứ ba';
            break;
        case 'wednesday':
            $weekday = 'Thứ tư';
            break;
        case 'thursday':
            $weekday = 'Thứ năm';
            break;
        case 'friday':
            $weekday = 'Thứ sáu';
            break;
        case 'saturday':
            $weekday = 'Thứ bảy';
            break;
        default:
            $weekday = 'Chủ nhật';
            break;
    }

    return $weekday;
}


/**
 * @param $db_date_string
 * @return bool|string
 */
function get_vn_full_date($db_date_string)
{
    return date('H:i d-m-Y', strtotime($db_date_string));
}


/**
 * @param $item
 * @return mixed
 */
function get_order_total($item)
{
    return $item->total()->where('code', 'total')->first()->value;
}


/**
 * @param $item
 * @return mixed
 */
function get_order_subtotal($item)
{
    return $item->total()->where('code', 'subtotal')->first()->value;
}


function get_order_discount($item)
{
    return $item->total()->where('code', 'discount')->first()->value;
}

/**
 * @param $item
 * @return mixed
 */
function get_order_shipping($item)
{
    return $item->total()->where('code', 'shipping')->first()->value;
}

function get_order_options($orderProductID)
{
    return $orderOptions = \App\Models\OrderOption::where('order_product_id', $orderProductID)->get();
}

function get_order_statuses()
{
    return \App\Models\OrderStatus::all();
}

function theme($path = '')
{
    $config = config('cms.theme');

    return url($config['folder'] . '/' . $config['active'] . '/assets') . '/' . $path;
}

function renderNode($node)
{
    if ($node->isLeaf()) {
        $html = '
        <li id="menuItem_' . $node->id . '">
        <div class="menu-wrap"><div class="menu-item">
            <div class="item-title">' . $node->name . '</div>
            <div class="item-control">' . $node->type . '</div>
        </div>
        <div class="menu-item-setting hidden">
            <label>Label</label>
            <input type="text" class="form-control input-sm" value="' . $node->name . '">
            <div class="menu-setting-functions-wrap">
                <a href="' . route('backend.menus.remove', $node->id) . '" class="menu-delete text-danger">Remove</a> | <a href="#" class="menu-cancel-setting">Cancel</a>
            </div>
        </div>
        </div></li>';

    } else {
        $html = '<li id="menuItem_' . $node->id . '">' .
            '<div class="menu-wrap"><div class="menu-item">
            <div class="item-title">' . $node->name . '</div>
            <div class="item-control">' . $node->type . '</div>
        </div>
        <div class="menu-item-setting hidden">
            <label>Label</label>
            <input type="text" class="form-control input-sm" value="' . $node->name . '">
            <div class="menu-setting-functions-wrap">
                <a href="' . route('backend.menus.remove', $node->id) . '" class="menu-delete text-danger">Remove</a> | <a href="#" class="menu-cancel-setting">Cancel</a>
            </div>
        </div></div>';

        $html .= '<ol class="">';

        foreach ($node->children as $child)
            $html .= renderNode($child);

        $html .= '</ol>';

        $html .= '</li>';
    }

    return $html;
}

function render_menu($menu_slug, $class = "")
{

    $menu = MenuPosition::where('slug', $menu_slug)->first();
    $html = '<ul class="' . $class . '">';

    $nodes = $menu->nodes()->get()->toHierarchy();

    $n = 0;
    foreach ($nodes as $key => $node):
        $n++;
        $html .= continue_render_menu($node);
        if($n == 1){
            $categories = \Modules\Gallery\Entities\Category::all();
            $html .= '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="'.url('gallery').'">Gallery</a><ul class="sub-menu dropdown-menu white-bg">';
            foreach($categories as $category):
            $html .= '
                <li><a  href="'.route('gallery.category.show', $category->slug).'">'.$category->name.'</a></li>';
            endforeach;
            $html .='</ul></li>';
        }
    endforeach;

    $html .= "</ul>";


    return $html;
}

function continue_render_menu($node)
{
    if ($node->isLeaf()) {
        $html = '
            <li class="dropdown"><a href="' . node_value($node) . '" title="' . $node->name . '">' . $node->name . '</a></li>
        ';

    } else {
        $html = '<li class="dropdown ">
                <a class="dropdown-toggle" data-toggle="dropdown" href="' . node_value($node) . '" title="' . $node->name . '">' . $node->name . '</a>
                <ul class="sub-menu dropdown-menu white-bg">';
        foreach ($node->children as $child) {
            $html .= continue_render_menu($child);
        }
        $html .= '</ul>';
        $html .= '</li>';
    }

    return $html;
}


function node_value($node)
{
    switch (strtolower($node->type)) {
        case 'link' :
            if (is_link($node->value)) {
                return $node->value;
            } else {
                return url($node->value);
            }
        case 'page':
            return route('page', $node->page->uri);
        case 'product':
            $product = Product::findOrFail($node->value);
            return route('product.show', [$product->id, $product->slug]);
        case 'blogcategory':
            $category = BlogCategory::findOrFail($node->value);
            return route('blog.category', [$category->id, $category->slug]);
        default:
            break;
    }


}


function image_process($file_path)
{
    $md5 = md5($file_path);

    $query = Image::where('md5', $md5);

    if ($query->count() == 0) {
        $image = Image::create(['file_path' => $file_path, 'md5' => $md5]);
        return $image;
    } else {
        return $query->first();
    }
}


function product_price($product)
{
    if ($product->promote_price == 0) {
        return $product->price;
    }
    return $product->promote_price;
}

/**
 * @param $product_id
 * @return bool
 */
function can_review_product($product_id)
{
    if (!auth()->check()) {
        return false;
    }

    if (Review::where('user_id', auth()->user()->id)->where('product_id', $product_id)->count() == 0) {
        return true;
    }

    return false;
}


/**
 * @param $product Object
 * @return float|int
 */
function product_point($product)
{
    $numReviews = $product->reviews()->count();
    if ($numReviews == 0) {
        return 0;
    }

    $point = 0;
    foreach ($product->reviews()->get() as $review) {
        $point += $review->point;
    }

    return $point / $numReviews;

}


if (!function_exists('product_review_count')) {
    /**
     * Product Review Count
     *
     * @param $product
     * @return mixed
     */
    function product_review_count($product)
    {
        return $numReviews = $product->reviews()->count();
    }
}


/**
 * @param $option
 * @return bool
 */
function theme_option($option)
{
    $query = ThemeOption::where('option', $option);
    if ($query->count() > 0) {
        return $query->get();
    }
    return false;
}


/**
 * @return mixed
 */
function category_hierarchy()
{
    return BlogCategory::all()->toHierarchy();
}


/**
 * @param $node
 * @return string
 */
function render_category($node, $selected_id = '')
{
    if ($node->isLeaf()) {
        return '<option ' . (($selected_id == $node->id) ? 'selected' : '') . ' value="' . $node->id . '">' . get_space($node->depth) . $node->name . '</option>';
    } else {
        $html = '<option ' . (($selected_id == $node->id) ? 'selected' : '') . ' value="' . $node->id . '">' . get_space($node->depth) . $node->name . '</option>';
        foreach ($node->children as $child) {
            $html .= render_category($child, $selected_id);
        }
        return $html;
    }
}

function list_provinces()
{
    return Province::orderBy('name', 'desc')->lists('name', 'id');
}


if(!function_exists('list_districts')){
    function list_districts($province_id = null)
    {
        if($province_id == null)
        return District::orderBy('name', 'desc')->lists('name', 'id');

        return District::where('province_id', $province_id)->orderBy('name', 'desc')->lists('name', 'id');
    }
}


if (! function_exists('list_wards')){
    /**
     * @param null $district_id
     * @return mixed
     */
    function list_wards($district_id = null)
    {
        if ($district_id == null)
            return Ward::orderBy('name', 'desc')->lists('name', 'id');
        return Ward::where('district_id', $district_id)->orderBy('name', 'desc')->lists('name', 'id');
    }

}


function render_category_tr($node)
{
    $hideLabel = ($node->hide_from_blog == 1) ? '<span class="label label-primary">Yes</span>' : '<span class="label label-default">No</span>';
    $html = '<tr>
                <td><input name="id[]" value="' . $node->id . '" class="styled item-checkbox" type="checkbox"></td>
                <td>
                    <a href="' . route('backend.blogcategories.edit', $node->id) . '">' . get_space($node->depth, '&mdash;') . $node->name . '</a>
                </td>
                <td>' . $node->slug . '</td>
                <td>' . $node->description . '</td>
                <td>' . $hideLabel . '</td>
                <td>
                    <a class="btn btn-xs btn-icon btn-default"
                       href="' . route('backend.blogcategories.edit', $node->id) . '">
                        <i class="icon-pen"></i>
                    </a>
                    <a class="btn btn-xs btn-icon btn-danger"
                       href="' . route('backend.blogcategories.confirm', $node->id) . '">
                        <i class="icon-minus2"></i>
                    </a>
                </td>
            </tr>';
    if ($node->isLeaf()) {
        return $html;
    } else {
        foreach ($node->children as $child) {
            $html .= render_category_tr($child);
        }
        return $html;
    }
}

/**
 * @param $space
 * @return string
 */
function get_space($space, $char = "&mdash;")
{
    $return = '';
    for ($i = 0; $i < $space; $i++) {
        $return .= $char;
    }
    return $return;
}


function arrange_categories($node)
{
    $array = [$node->id => get_space($node->depth) . $node->name];
    if ($node->isLeaf()) {
        return $array;
    } else {
        foreach ($node->children as $child) {
            $array += arrange_categories($child);
        }
        return $array;
    }
}


function popular_posts($limit = 5)
{
    return \App\Models\Post::with('author')->orderBy('view', 'desc')->limit($limit)->get();
}

function time_created_post($created_date_string)
{

    $timestamp = strtotime($created_date_string);
    return date('d-m-Y', $timestamp);
    $timeframe = time() - $timestamp;
    if ($timeframe < 24 * 60 * 3600) {
        $hour = round($timeframe / 3600, 0);
        return $hour . ' giờ trước';
    } else {
        return date('d-m-Y', $timestamp);
    }
}

if (!function_exists('render_blog_categories')) {
    /**
     * @param string $ulClass
     * @return string
     */
    function render_blog_categories($ulClass = '')
    {
        $nodes = BlogCategory::where('hide_from_blog', 0)->get()->toHierarchy();
        $html = '<ul class="' . $ulClass . '">';
        foreach ($nodes as $node) {
            $html .= continue_render_blog_categories($node);
        }
        $html .= '</ul>';

        return $html;
    }
}


function continue_render_blog_categories($node)
{
    if ($node->isLeaf()) {
        return '<li><a href="' . route('blog.category', [$node->id, $node->slug]) . '" title="' . $node->name . '">' . $node->name . '</a></li>';
    } else {
        $html = '<li><a href="' . route('blog.category', [$node->id, $node->slug]) . '" title="' . $node->name . '">' . $node->name . '</a>';
        $html .= '<ul>';
        foreach ($node->children as $child) {
            $html .= continue_render_blog_categories($child);
        }
        $html .= '</ul></li>';

        return $html;
    }
}


function latest_posts($limit)
{
    return \App\Models\Post::with('author')->orderBy('view', 'desc')->limit($limit)->get();
}

/**
 * Hàm này lấy ra các bài post theo một cat_id nhất định
 */
function posts($category_id)
{
    $category = BlogCategory::where('id', $category_id)->with('posts')->first();
    return $category->posts();
}


/**
 * @return array
 */
function blog_category_array()
{
    $catItems = BlogCategory::all()->toHierarchy();

    $categories = [];

    foreach ($catItems as $node) {
        $categories += arrange_categories($node);
    }

    return $categories;
}


/**
 * @param $menuPosition
 * @return bool
 */
function has_menu($menuPosition)
{
    $query = MenuPosition::where('position', $menuPosition)->orderBy('order', 'asc');

    if ($query->count() > 0) {
        return $query->get();
    }

    return false;
}

function functions_button($route, $id)
{
    return '<a class="btn btn-xs btn-icon btn-default" href="' . route($route . '.edit', $id) . '">
                <i class="icon-pen"></i>
            </a>
            <a class="btn btn-xs btn-icon btn-danger" href="' . route($route . '.confirm', $id) . '">
                <i class="icon-minus2"></i>
            </a>
            <a class="btn btn-xs btn-icon btn-info" href="' . route($route . '.preview', $id) . '">
                <i class="icon-eye2"></i>
            </a>';
}

function blog_posts($category_id)
{
    $query = BlogCategory::where('id', $category_id)->with('posts');
    if ($query->count() == 0) {
        return false;
    }
    $category = $query->first();
    if ($category->posts()->count() == 0) {
        return false;
    }
    return $category->posts()->get();
}


/**
 * @param $request
 * @param $model
 */
function bulk_delete($request, $model)
{

    //Bulk Delete
    if ($request->input('bulk_action') == 'delete') {

        if (count($request->input('id')) > 0 && $request->input('id')) {
            foreach ($request->input('id') as $id) {
                $model::where('id', $id)->delete();
            }
        }
    }
}

/**
 * @param $product
 * @return string
 */
function product_link($product)
{
    return route('product.show', [$product->id, $product->slug]);
}


function is_admin()
{
    if (auth()->check() && auth()->user()->admin == 1) {
        return true;
    }
    return false;
}


/**
 * @param $coupon
 * @return stdClass
 */
function coupon_status($coupon)
{
    $item = new stdClass();

    $coupon_statuses_list = \App\Models\CouponStatus::all();


    if ($coupon->date_end < \Carbon\Carbon::now()) {
        $item->label = 'Expired';
        $item->class = 'default';
        return $item;
    }


    foreach ($coupon_statuses_list as $status) {
        if ($coupon->status == $status->id) {
            $item->label = $status->label;
            $item->class = $status->class;
            return $item;
        }
    }
}


/**
 * @return array
 */
function list_coupon_statuses()
{
    return \App\Models\CouponStatus::lists('label', 'id');
}


function base_url($uri = '', $scheme = 'http')
{
    $domain = env('DOMAIN');
    return $scheme . '://' . $domain . '/' . $uri;
}

/**
 * @param $id
 * @param $request
 * @param $routeExit
 * @param $routeEdit
 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
 */
function redirect_with_condition($id, $request, $routeExit, $routeEdit)
{

    if ($request->input('button') == 'save') {
        return redirect(route($routeEdit, $id));
    } else {
        return redirect(route($routeExit));
    }

}


function check_directory_child($directory)
{
    if (count(Storage::directories($directory)) == 0) {
        return false;
    }

    return true;
}


if (!function_exists('seo_title')) {
    /**
     * Lấy SEO title của đối tượng
     *
     * @param $object
     * @param $fallback_title
     * @return mixed
     */
    function seo_title($object, $fallback_title)
    {
        if ($object->seo_title != '') {
            return $object->seo_title;
        }
        return $fallback_title;
    }
}


if (!function_exists('meta_description')) {
    /**
     * get meta description of object
     *
     * @param $object
     * @param $fallback_description
     * @return mixed
     */
    function meta_description($object, $fallback_description)
    {
        if ($object->meta_description != '') {
            return $object->meta_description;
        }
        return $fallback_description;
    }
}


if (!function_exists('get_ads')) {
    /**
     * @param $slug
     * @return string
     */
    function get_ads($slug)
    {
        $query = \App\Models\Advertising::where('slug', $slug);
        if ($query->count() == 0) return '';
        $ads = $query->first();
        return <<<EOT
            <a href="{$ads->link}" title="{$ads->description}">
                <img class="img-responsive" src="{$ads->image}" alt="{$ads->description}">
            </a>
EOT;
    }
}


if(! function_exists('get_page_point')){
    function get_page_point($page){
        $num_rate = $page->rates()->count();
        if($num_rate == 0){
            return 0;
        }

        $total = 0;
        foreach ($page->rates()->get() as $rate) {
            $total += $rate->point;
        }

        return round($total/$num_rate, 2);
    }
}


if (!function_exists('mview')) {

    /**
     * @param $module
     * @param $view
     * @return View
     */
    function mview($module_view, $data = [])
    {
        $array = explode('::', $module_view);
        $module = $array[0];
        $view = $array[1];

        if (View::exists($module . '.' . $view)) {
            return view($module . '.' . $view, $data);
        }
        return view($module . '::' . $view, $data);
    }
}


if (! function_exists('get_title')){
    /**
     * @param $object
     * @return string
     */
    function get_title($object){
        if($object->seo_title != ''){
            return $object->seo_title;
        }

        return $object->name;
    }
}
if (! function_exists('get_post_title')){
    /**
     * @param $object
     * @return string
     */
    function get_post_title($object){
        if($object->seo_title != ''){
            return $object->seo_title;
        }

        return $object->title;
    }
}

if (! function_exists('get_description')){
    /**
     * @param $object
     * @return string
     */
    function get_description($object){
        if($object->meta_description != ''){
            return $object->meta_description;
        }

        return str_limit(strip_tags($object->description), 150);
    }
}

if (! function_exists('get_post_description')){
    /**
     * @param $object
     * @return string
     */
    function get_post_description($object){
        if($object->meta_description != ''){
            return $object->meta_description;
        }

        return str_limit(strip_tags($object->body), 150);
    }
}


if(! function_exists('list_posts')){
    function list_posts($post_id = null){
        if($post_id != null){
            return DB::table('posts')->where('id', '!=', $post_id)
                ->lists('title', 'id');
        }

        else{
            return DB::table('posts')
                ->lists('title', 'id');
        }
    }
}

$path = dirname(__DIR__) . '/modules';

$modules = scandir($path);
foreach ($modules as $module) :
    if ($modules != '.' or $modules != '..') {
        if(file_exists($path . '/' . $module . '/helpers.php' )){
            include_once $path . '/' . $module . '/helpers.php';
        }
    }
endforeach;
