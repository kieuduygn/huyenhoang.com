<?php

namespace App\Widgets;

use App\Models\Blog;
use App\Models\BlogCategory;
use Arrilot\Widgets\AbstractWidget;

class BlogCategories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view("widgets.blog_categories", [
            'config' => $this->config,
            'categories'    => BlogCategory::where('hide_from_blog', 0)->get()
        ]);
    }
}