<?php

namespace App\Widgets;

use App\Models\Post;
use Arrilot\Widgets\AbstractWidget;

class LatestPosts extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view("widgets.latest_posts", [
            'config' => $this->config,
            'posts' => Post::limit(3)->orderBy('created_at', 'desc')->get()
        ]);
    }
}