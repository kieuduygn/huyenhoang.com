<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Modules\Gallery\Entities\Image;

class LatestPhotos extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view("widgets.latest_photos", [
            'config' => $this->config,
            'images'    => Image::limit(5)->orderBy('created_at', 'desc')->get()
        ]);
    }
}