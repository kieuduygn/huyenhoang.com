<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Modules\Gallery\Entities\Category;

class AlbumCategories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view("widgets.album_categories", [
            'config' => $this->config,
            'categories'    => Category::all()
        ]);
    }
}