<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Branch;
use App\Http\Controllers\Controller;

class BranchesController extends Controller
{
    public function index()
    {
        return view('branches');
    }

    public function xml()
    {

        $branches = Branch::all();

        $dom = new \DOMDocument("1.0");
        $node = $dom->createElement("markers");
        $parnode = $dom->appendChild($node);

        foreach($branches as $branch){
            $node = $dom->createElement("marker");
            $newnode = $parnode->appendChild($node);
            $newnode->setAttribute("name",$branch->name);
            $newnode->setAttribute("address", $branch->address);
            $newnode->setAttribute("lat", $branch->lat);
            $newnode->setAttribute("lng", $branch->lng);
            $newnode->setAttribute("type", $branch->type);
        }

        echo  $dom->saveXML();
    }
}
