<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Product;
use Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

class FilesController extends Controller
{
    public function get($filename){

        $entry = File::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($entry->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);
    }


    public function get_product_image($id, $slug)
    {
        $filename = Product::where('id', $id)->where('slug', $slug)->first()->file->filename;
        $entry = File::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($entry->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);
    }
}
