<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use App\Models\ProductOption;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CartController extends Controller
{

    protected $products;

    public function __construct(Product $products)
    {
        $this->products = $products;
    }

    public function add(Request $request)
    {

        $product = Product::findOrFail($request->input('id'));

        $input_option_values = $request->input('option_value');

        $product = $this->products
            ->select('id', 'name', 'slug', 'price', 'promote_price', 'file_id')
            ->where('id', $request->input('id'))
            ->first()->toArray();


        $product['option_value'] = $request->input('option_value');
        $product['quantity'] = $request->input('quantity');
        $price = ($product['promote_price'] == 0) ? $product['price'] : $product['promote_price'];

        if ($input_option_values) {

            //Lấy ra các lựa chọn cho sản phẩm
            $product_option_value = [];
            foreach ($input_option_values as $option_id => $product_option_id) {
                $product_option_value[] = ProductOption::where('id', $product_option_id)
                    ->where('option_id', $option_id)->first();
            }

            //Cộng tất cả các lựa chọn lại
            $price_from_options = 0;
            foreach ($product_option_value as $item) {
                $price_from_options += (int)($item->price_prefix . $item->price);
            }

            //Giá sản phẩm gốc, chưa có lựa chọn

            $product_final_price = $price + $price_from_options;
            //Cộng giá lựa chọn.
            $product['price_after_choose_option'] = $product_final_price;
            $product['subtotal'] = ($price + $price_from_options) * $product['quantity'];
        } else {
            $product['price_after_choose_option'] = $price;
            $product['subtotal'] = $price * $product['quantity'];
        }


        $request->session()->push('cart.products', $product);
        $request->session()->save();

        //Cập nhật tổng giá trị Cart
        $this->updateCartTotal();

        return redirect(route('cart.show'));

    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {

        $data['items'] = [];
        if (session('cart')) {
            $data['items'] = session('cart.products');
        }

        return view('cart.index', $data);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function remove($id)
    {

        CartItem::destroy($id);
        return redirect('/cart');
    }

    /**
     * Làm rỗng cart
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function clear()
    {
        session()->remove('cart');

        return redirect(route('cart.show'));

    }

    /**
     * Xóa 1 sản phẩm khỏi cart (ajax)
     *
     * @param $key
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($key)
    {
        session()->pull('cart.products.' . $key);

        $this->updateCartTotal();

        return redirect(route('cart.show'));


    }

    public function deleteAjax($key)
    {
        session()->pull('cart.products.' . $key);

        $this->updateCartTotal();

        $array = session('cart');
        $array['cart_num'] = cart_num();
        $array['subtotal'] = number_format($array['subtotal']);
        $array['total'] = number_format($array['total']);
        return json_encode($array);

    }


    /**
     * Cập nhật từng item trong cart (Ajax)
     *
     * @param $key
     * @param $quantity
     * @return string
     */
    public function update($key, $quantity)
    {
        $item = session()->pull('cart.products.' . $key);
        //Update lại subtotal
        $item['subtotal'] = $item['price_after_choose_option'] * $quantity;
        //Update quantity
        $item['quantity'] = $quantity;
        session()->put('cart.products.' . $key, $item);
        session()->save();
        $this->updateCartTotal();

        $data['itemSubTotal'] = number_format($item['subtotal']) . 'đ';
        $data['total'] = number_format(session('cart.total')) . 'đ';

        return json_encode($data);
    }


    private function updateCartTotal()
    {
        $productsInCart = session('cart.products');

        //Set tổng cart
        $subtotal = 0;
        foreach ($productsInCart as $item) {
            $subtotal += $item['subtotal'];
        }

        if (session('coupon')) {
            $total = $subtotal - session('coupon')->discount;
        } else {
            $total = $subtotal;
        }

        session()->put('cart.subtotal', $subtotal);
        session()->put('cart.total', $total);
        session()->save();
    }
}