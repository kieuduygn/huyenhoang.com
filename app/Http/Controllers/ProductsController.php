<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderOption;
use App\Models\OrderProduct;
use App\Models\OrderStatus;
use App\Models\OrderTotal;
use App\Models\ProductOption;
use App\Models\Province;
use App\Models\Tag;
use App\Models\Ward;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Review;

class ProductsController extends Controller
{

    protected $products;

    public function __construct(Product $products)
    {
        $this->products = $products;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = $this->products->orderBy('created_at', 'desc')->paginate(15);

        return view('products.index', compact('products'));
    }


    /**
     * @param $slug
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @internal param $tag_id
     */
    public function tag($slug, $id)
    {

        $query = Tag::where('slug', $slug)->where('id', $id);

        if ($query->count() == 0) {
            return redirect('/');
        }
        $tag = $query->first();

        $products = $tag->products()->get();

        return view('products.index', compact('products'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->products->findOrFail($id);

        return view('products.show', compact('product'));
    }


    /**
     * Thanh toán
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function checkout(Request $request)
    {
        if (!session('cart'))
            return redirect(route('cart.show'));

        $data['provinces'] = Province::orderBy('name', 'desc')->lists('name', 'id');
        $data['districts'] = District::where('province_id', 1)->lists('name', 'id');
        $data['wards'] = Ward::where('district_id', 1)->lists('name', 'id');
        if (auth()->check()) {
            $provinceID = auth()->user()->province_id;
            $data['districts'] = District::where('province_id', $provinceID)->lists('name', 'id');
        }

        $data['items'] = session('cart.products');
        $data['total'] = session('cart.total');
        $data['subtotal'] = session('cart.subtotal');
        if (session('coupon')) {
            $data['discount'] = session('coupon')->discount;
        } else {
            $data['discount'] = 0;
        }
        $data['currentPath'] = $request->path();

        return view('products.checkout', $data);
    }


    /**
     * Đặt hàng
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function placeOrder(Request $request)
    {
        session()->pull('order');
        //dd($request->all());
        $input = $request->all();

        //Tạo người dùng nếu người dùng không đăng nhập
        if (!auth()->check()) {
            $this->validate($request, [
                'payment.name' => 'required',
                'payment.email' => 'required|email|unique:users,email',
                'payment.address' => 'required|min:10',
                'payment.province' => 'required|numeric',
                'payment.phone' => 'required|numeric',
                'payment.district' => 'required|numeric',
                'payment.ward' => 'required|numeric',
                'password' => 'required|min:6|max:32',
                'password_confirmation' => 'required|same:password'
            ]);
            $data['user_id'] = $this->create_user($input);
        } else {
            //Nếu đã đăng nhập rồi thì phẻ
            $data['user_id'] = auth()->user()->id;
        }
        //Insert into Orders
        foreach ($request->input('payment') as $key => $value) {
            $data[$key] = $value;
        }

        $data['ward_id'] = $input['payment']['ward'];
        $data['district_id'] = $input['payment']['district'];
        $data['province_id'] = $input['payment']['province'];

        if (isset($input['ship_to_different_address'])) {
            $this->validate($request, [
                'shipping.name' => 'required|max:32',
                'shipping.email' => 'required|email',
                'shipping.phone' => 'required|numeric',
                'shipping.address' => 'required',
                'shipping.ward' => 'required|numeric',
                'shipping.district' => 'required|numeric',
                'shipping.province' => 'required|numeric',
            ]);
            foreach ($input['shipping'] as $key => $value) {
                $data['shipping_' . $key] = $value;
            }

            $data['shipping_ward_id'] = $input['shipping']['ward'];
            $data['shipping_district_id'] = $input['shipping']['district'];
            $data['shipping_province_id'] = $input['shipping']['province'];
        } else {
            foreach ($input['payment'] as $key => $value) {
                $data['shipping_' . $key] = $value;
            }
            $data['shipping_ward_id'] = $input['payment']['ward'];
            $data['shipping_district_id'] = $input['payment']['district'];
            $data['shipping_province_id'] = $input['payment']['province'];
        }


        $data['note'] = $request->input('order_note');
        $data['total'] = session('cart.total');
        $data['order_status_id'] = 1;
        $data['payment_method_id'] = $request->input('payment_method');


        $order = Order::create($data);

        session()->push('order', $order);

        //Insert to Order Total
        $data = [
            [
                'order_id' => $order->id,
                'code' => 'subtotal',
                'title' => 'Sub Total',
                'value' => session('cart.subtotal')
            ],
            [
                'order_id' => $order->id,
                'code' => 'shipping',
                'title' => 'Phí giao hàng',
                'value' => 0
            ],
            [
                'order_id' => $order->id,
                'code' => 'total',
                'title' => 'Tổng cộng',
                'value' => session('cart.total')
            ],


        ];

        if (session('coupon')) {
            $data[] = [
                'order_id' => $order->id,
                'code' => 'discount',
                'title' => 'Giảm giá',
                'value' => session('coupon')->discount
            ];
        } else {
            $data[] = [
                'order_id' => $order->id,
                'code' => 'discount',
                'title' => 'Giảm giá',
                'value' => 0
            ];
        }

        foreach ($data as $item) {
            OrderTotal::create($item);
        }

        //Insert into Order Product
        $products = session('cart.products');

        foreach ($products as $item) {
            $data = [
                'order_id' => $order->id,
                'product_id' => $item['id'],
                'name' => $item['name'],
                'quantity' => $item['quantity'],
                'price' => ($item['promote_price'] > 0) ? $item['promote_price'] : $item['price'],
                'total' => $item['subtotal']
            ];

            $orderProduct = OrderProduct::create($data);

            if ($item['option_value'] != null) {
                foreach ($item['option_value'] as $key => $value) {
                    $productOption = ProductOption::findOrFail($value);
                    $option = Option::findOrFail($key);
                    $data = [
                        'order_id' => $order->id,
                        'order_product_id' => $orderProduct->id,
                        'option_id' => $key,
                        'option_value_id' => $productOption->value->id,
                        'name' => $option->name,
                        'value' => $productOption->value->name,
                        'type' => $option->type,
                    ];

                    OrderOption::create($data);
                }
            }
        }

        session()->pull('cart');
        session()->pull('session');
        session()->pull('coupon');
        session()->save();


        //Finish
        return redirect(route('product.ordersuccess'));

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderSuccess()
    {
        $order = session('order')[0];
        $suggestProducts = $this->products->where('suggest', 1)->limit(4)->get();
        return view('products.order_success', compact('suggestProducts', 'order'));
    }


    /**
     * @param $input
     * @return $this|mixed
     */
    private function create_user($input)
    {
        foreach ($input['payment'] as $key => $value) {
            $data[$key] = $value;
        }


        $data['ward_id'] = $input['payment']['ward'];
        $data['district_id'] = $input['payment']['district'];
        $data['province_id'] = $input['payment']['province'];
        $data['password'] = bcrypt($input['password']);

        if (User::where('email', $data['email'])->count() == 0) {
            $user = User::create($data);
            Auth::login($user);
        } else {
            return redirect(route('product.checkout'))->withErrors('Email người dùng đã tồn tại');
        }

        return $user->id;

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function review(Request $request)
    {
        //////////////////////////////////PHƯƠNG ÁN TẠM THỜI
        $all_order_of_current_user = auth()->user()->orders()->get();
        $has_order_this_product = false;
        foreach ($all_order_of_current_user as $order) {
            foreach($order->products()->get() as $product){
                if($product->id == $request->input('product_id')){
                    $has_order_this_product = true;
                    break;
                }
            }
        }

        if(!$has_order_this_product){
            flash('Bạn chưa mua sản phẩm này bao giờ, sao có thể đánh giá ?');

            return redirect()->back();
        }

        if (!$request->input('product_id')) {
            return redirect();
        }

        if (can_review_product($request->input('product_id'))) {
            Review::create($request->all() + ['user_id' => auth()->user()->id]);
        }

        return redirect(route('product.show', $request->input('product_id')));
    }


    /**
     * @param Request $request
     * @return string
     */
    public function updateShowPagePrice(Request $request)
    {
        $input_option_values = json_decode($request->input('option_values'));
        foreach ($input_option_values as $option_id => $product_option_id) {
            if (is_null($product_option_id)) {
                unset($input_option_values[$option_id]);
            }
        }

        //Lấy ra các lựa chọn cho sản phẩm
        $product_option_values = [];
        foreach ($input_option_values as $option_id => $product_option_id) {
            $product_option_values[] = ProductOption::where('id', $product_option_id)
                ->where('option_id', $option_id)->first();
        }

        //Cộng tất cả các lựa chọn lại
        $price_from_options = 0;
        foreach ($product_option_values as $item) {
            $price_from_options += (int)($item->price_prefix . $item->price);
        }


        $product = Product::findOrFail($request->input('id'));
        $product_price = ($product->promote_price === 0) ? $product->price : $product->promote_price;

        return number_format($price_from_options + $product_price) . ' đ';


    }

}
