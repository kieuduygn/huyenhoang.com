<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{

    public function index()
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function message(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|max:255',
            'phone' => 'required|numeric',
            'email' => 'email|required',
            'address'   => 'required|max:255',
            'subject'   => 'required',
            'message'   => 'required:max:1000'
        ]);

        Mail::send('emails.reminder', ['data' => $request->all()], function ($m) use ($request) {
            $m->from('info@huyenhoang.com', 'Huyen Hoang Photo');

            $m->to($request->input('email'), $request->input('name'))->subject('Tin nhắn khách hàng ' . $request->input('name'));
        });

        flash('Thành công', 'Cảm ơn bạn đã gửi tin nhắn, chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất');

        return redirect()->back();
    }
}
