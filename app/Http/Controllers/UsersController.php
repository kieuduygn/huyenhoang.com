<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\District;
use App\Models\Province;
use App\Models\Ward;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Socialite;

class UsersController extends Controller
{

    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }


    /**
     * Đăng nhập
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function login(Request $request)
    {
        if (auth()->check()) {
            return redirect(route('user.profile'));
        }


        $data['provinces'] = Province::orderBy('name', 'desc')->lists('name', 'id');
        $data['districts'] = District::where('province_id', 1)->lists('name', 'id');
        $data['wards'] = Ward::where('district_id', 1)->lists('name', 'id');
        return view('users.auth', $data);
    }


    public function githubLogin()
    {
        return Socialite::with('github')->redirect();
    }


    /**
     * Người dùng nhấn nút đăng nhập bằng  facebook
     *
     * @return mixed
     */
    public function facebookLogin()
    {
        return Socialite::with('facebook')->redirect();
    }


    /**
     * Hàm này do facebook trả về (callback)
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function facebookLoginStep2(Request $request)
    {
        if ($request->has('code')) {
            $userData = Socialite::driver('facebook')->user();
            $data = $userData->user;
            if (!is_null($userData->email)) {
                $query = User::where('email', $userData->email);
                if ($query->count() == 0) {
                    $user = User::create([
                        'name' => $data['first_name'] . ' ' . $data['last_name'],
                        'email' => $userData->email,
                        'gender' => ($data['gender'] == 'male') ? 1 : 0,
                        'avatar' => $userData->avatar,
                        'social' => 1,
                        'group_id' => 3,
                    ]);
                } else {
                    $user = $query->first();
                }

            } else {
                $query = User::where('email', 'fb_' + $userData->id + '@cass.vn');
                if ($query->count() == 0) {
                    $user = User::firstOrCreate([
                        'name' => $data['first_name'] . ' ' . $data['last_name'],
                        'email' => 'fb_' + $userData->id + '@cass.vn',
                        'gender' => ($data['gender'] == 'male') ? 1 : 0,
                        'avatar' => $userData->avatar,
                        'social' => 1,
                        'group_id'  => 3,
                    ]);
                } else {
                    $user = $query->first();
                }
            }

            Auth::login($user);

            return redirect(route('user.profile'));
        }

        return redirect(route('users.login'));
    }

    public function googleLogin()
    {
        return Socialite::with('google')->redirect();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function googleLoginStep2(Request $request)
    {
        if ($request->has('code')) {
            $global = Socialite::driver('google')->user();
            $user = (object)$global->user;
            $query = User::where('email', $global->email);
            //Kiểm tra xem người dùng này đã đăng nhập bao giờ chưa
            if ($query->count() == 0) {
                $user = User::create([
                    'name' => $global->name,
                    'email' => $global->email,
                    'gender' => ($user->gender == 'male') ? 1 : 0,
                    'avatar' => $global->avatar,
                    'social' => 1,

                ]);
            } else {
                $user = $query->first();
            }

            Auth::login($user);

            return redirect(route('user.profile'));
        }

        return redirect(route('users.login'));
    }

    /**
     * Đăng ký
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        $data['provinces'] = Province::orderBy('name', 'desc')->lists('name', 'id');
        $data['districts'] = District::where('province_id', 1)->lists('name', 'id');
        $data['wards'] = Ward::where('district_id', 1)->lists('name', 'id');
        return view('users.auth', $data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\RegisterUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\RegisterUserRequest $request)
    {
        $input = $request->all();

        //Khởi tạo user
        $user = $this->users->create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'address' => $input['address'],
            'phone' => $input['phone'],
            'province_id' => $input['province'],
            'ward_id' => $input['ward'],
            'district_id' => $input['district']
        ]);

        Auth::login($user);

        flash()->success('Thành công', 'Bạn đã đăng ký tài khoản thành công, xin chúc mừng !');

        return redirect(route('users.profile'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdateUserProfileRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateUserProfileRequest $request, $id)
    {
        $input = $request->all();

        $data = [
            'name' => $input['name'],
            'address' => $input['address'],
            'phone' => $input['phone'],
            'province_id' => $input['province'],
            'ward_id' => $input['ward'],
            'district_id' => $input['district']
        ];

        if ($request->input('password') != "") {
            $this->validate($request, [
                'password' => 'required|max:32',
                'password_confirmation' => 'required|max:32|same:password',
            ]);
            $data['password'] = bcrypt($input['password']);
        }

        $this->users->where('id', $id)->update($data);

        flash()->success('Thành công', 'Bạn đã cập nhật thông tin tài khoản thành công');

        return redirect(route('user.profile'));

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function profile()
    {
        if (!auth()->check()){
            flash('Bạn phải đăng nhập để thực hiện chức năng này');
            return redirect('/login.html');
        }
        $data['user'] = auth()->user();
        return view('users.profile', $data);
    }


    /**
     * Xử lý đăng nhập
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember = false;
        if ($request->input('remember')) {
            $remember = true;
        }

        $loginArray = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if (Auth::attempt($loginArray, $remember)) {
            return redirect(route('user.profile'));
        }
        return redirect(route('users.login'))->withErrors('Email hoặc mật khẩu không đúng');
    }

}
