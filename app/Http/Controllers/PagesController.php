<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\PageRate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param $uri
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($uri)
    {
        $item = Page::where('uri', $uri)->with('rates')->first();

        if($item->template == 'default'){
            $template = 'default';
        }else{
            $template = 'page-'.strtolower($item->template);
        }

        return view($template, compact('item'));
    }


    /**
     * Xem trước trang
     *
     * @param $uri
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview($uri)
    {
        $item = Page::where('id', $uri)->first();

        if($item->template == 'default'){
            return view('default', compact('item'));
        }else{
            return view('page-'.strtolower($item->template), compact('item'));
        }
    }


    /**
     * @param $id
     * @param $point
     */
    public function rate($id, $point)
    {
        if(! auth()->check()){
            return 'Bạn chưa đăng nhập, không thể đánh giá';
        }

        $user_id = auth()->user()->id;
        $query = PageRate::where('user_id', $user_id)
            ->where('page_id', $id);
        if($query->count() > 0){
            return 'Bạn đã đánh giá bài này rồi';
        }

        $page = Page::findOrFail($id);

        PageRate::create([
            'page_id'   => $id,
            'point' => $point,
            'user_id'   => $user_id
        ]);

        return 1;
    }
}
