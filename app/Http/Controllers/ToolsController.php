<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Image;
use App\Models\OptionValue;
use App\Models\Page;
use App\Models\Product;
use App\Models\Role;
use App\Models\Ward;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests;
use Cocur;
use App\Models\Option;
use Illuminate\Support\Facades\App;

class ToolsController extends Controller
{
    public function genSlug($string)
    {
        $string = urldecode($string);
        $slugify = new Cocur\Slugify\Slugify();

        $url = $slugify->slugify($string);

        echo $url;
    }

    public function createrole()
    {

    }


    /**
     *
     *
     * @param $attribute_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAttributeHtml($attribute_id)
    {
        $attribute = \App\Attribute::findOrFail($attribute_id);

        return view('backend.products.attributes', compact('attribute'));
    }


    public function getOptionsValueHtml($type)
    {
        return view('backend.options.options-value');
    }


    public function getOptionValue($id)
    {
        $option = Option::findOrFail($id);
        $values = OptionValue::lists('name', 'id');

        return view('backend.options.add_value', compact('option', 'values'));
    }


    /**
     * @param $province_id
     * @return mixed
     */
    public function getDistrict($province_id)
    {
        $districts = District::where('province_id', $province_id)->orderBy('name')->get();

        $options = "";
        foreach ($districts as $item) {
            $options .= '<option value="' . $item->id . '">' . (is_numeric($item->name) ? 'Quận ' . $item->name : $item->name) . '</option>' . PHP_EOL;
        }

        $data['districts'] = $options;
        $district_id = District::where('province_id', $province_id)->first()->id;
        $wards = Ward::where('district_id', $district_id)->orderBy('name')->lists('name', 'id');
        $options = '';
        foreach ($wards as $key => $value) {
            $options .= '<option value="' . $key . '">' . ((is_numeric($value)) ? 'Phường ' . $value : $value) . '</option>' . PHP_EOL;
        }
        $data['wards'] = $options;

        return json_encode($data);
    }

    /**
     * @param $district_id
     * @return string
     */
    public function getWard($district_id)
    {
        $wards = Ward::where('district_id', $district_id)->lists('name', 'id');
        $options = '';
        foreach ($wards as $key => $value) {
            $options .= '<option value="' . $key . '">' . ((is_numeric($value)) ? 'Phường ' . $value : $value) . '</option>' . PHP_EOL;
        }
        return $options;
    }


    public function toggleShippingAddress()
    {
        if (session('different_shipping_address')) {
            session()->pull('different_shipping_address');
            session()->save();
        } else {
            session()->put('different_shipping_address', 1);
            session()->save();
        }
    }


    public function renderNewProductImage(Request $request)
    {
        $image = $request->input('data');
        return view('backend.products.new_product_image', compact('image'));
    }


    public function sitemap()
    {
        $posts = \App\Models\Post::all();

        $post_url = '';
        foreach($posts as $post){
            $url = route('blog.show', $post->slug);
            $lastModified = $post->updated_at->format('Y-m-d');

            $post_url.=
                        <<<EOT
                           <url>

                              <loc>$url</loc>

                              <lastmod>$lastModified</lastmod>

                              <changefreq>monthly</changefreq>

                              <priority>0.8</priority>

                           </url>

EOT;
        }


        $products = Product::all();
        $productItem = '';
        foreach($products as $product){
            $url = route('product.show', [$product->id, $product->slug]);
            $lastModified = $product->updated_at->format('Y-m-d');
            $productItem .=
                            <<<EOT
                               <url>

                                  <loc>$url</loc>

                                  <lastmod>$lastModified</lastmod>

                                  <changefreq>weekly</changefreq>

                                  <priority>0.8</priority>

                               </url>

EOT;
        }

        $pages = Page::all();
        $pageItems = '';
        foreach($pages as $page){
            $url = route('page', $page->uri);
            $lastModified = $page->updated_at->format('Y-m-d');
            $pageItems .=
                <<<EOT
                               <url>

                                  <loc>$url</loc>

                                  <lastmod>$lastModified</lastmod>

                                  <changefreq>weekly</changefreq>

                                  <priority>0.8</priority>

                               </url>

EOT;
        }


        return <<<EOT
        <?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            $post_url . $productItem . $pageItems
        </urlset>
EOT;

    }
}
