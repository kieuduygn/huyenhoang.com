<?php

namespace App\Http\Controllers\Backend;

use App\Models\AttributeGroup;
use Illuminate\Http\Request;

use App\Http\Requests;

class AttributeGroupsController extends Controller
{

    protected $groups;

    public function __construct(AttributeGroup $groups)
    {
        $this->groups = $groups;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $this->groups->paginate(15);

        return view('backend.attribute_groups.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = $this->groups;

        return view('backend.attribute_groups.form', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        $this->groups->create($request->all());

        flash()->success('Thành công', 'Bạn đã thêm thành công nhóm thuộc tính sản phẩm');

        return redirect(route('backend.attributegroups.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->groups->findOrFail($id);

        return view('backend.attribute_groups.form', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required']);

        $group = $this->groups->findOrFail($id);
        $group->fill($request->all());

        flash()->success('Thành công', 'Bạn đã cập nhật thành công nhóm thuộc tính sản phẩm');

        return redirect(route('backend.attributegroups.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->groups->findOrFail($id);

        $item->delete();

        flash()->success('Thành công', 'Bạn đã xóa thành công nhóm thuộc tính sản phẩm');

        return redirect(route('backend.attributegroups.index'));
    }


    public function confirm($id)
    {
        $item = $this->groups->findOrFail($id);

        return view('backend.attribute_groups.confirm', compact('item'));
    }
}
