<?php

namespace App\Http\Controllers\Backend;

use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class CouponsController extends Controller
{

    protected $coupons;

    public function __construct(Coupon $coupons)
    {
        $this->coupons = $coupons;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Coupon $model)
    {
        bulk_delete($request, $model);

        $items = $this->coupons->paginate(15);

        return view('backend.coupons.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Coupon $item)
    {
        return view('backend.coupons.form', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StoreCouponRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreCouponRequest $request)
    {

        if($request->input('date_start') > $request->input('date_end')){
            return redirect(route('backend.coupons.create'))->withErrors('Ngày kết thúc không được nhỏ hơn ngày bắt đầu');
        }

        $item = $this->coupons->create($request->all());

        flash()->success('Thành công', 'Bạn đã tạo Coupon thành công');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.coupons.edit', $item->id));
            default:
                return redirect(route('backend.coupons.index'));
        }


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->coupons->findOrFail($id);

        return view('backend.coupons.form', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\StoreCouponRequest|Requests\UpdateCouponRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateCouponRequest $request, $id)
    {
        if($request->input('date_start') > $request->input('date_end')){
            return redirect(route('backend.coupons.edit', $id))->withErrors('Ngày kết thúc không được nhỏ hơn ngày bắt đầu');
        }

        $item = $this->coupons->findOrFail($id);

        if($request->input('date_end') < Carbon::now()){
            $item->status = 3;
            $item->save();
        }

        $item->update($request->all());

        flash()->success('Thành công', 'Bạn đã cập nhật Coupon thành công');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.coupons.edit', $item->id));
            default:
                return redirect(route('backend.coupons.index'));
        }


    }


    public function confirm($id)
    {
        $item = $this->coupons->findOrFail($id);

        return view('backend.coupons.confirm', compact('item'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->coupons->findOrFail($id);

        $item->delete();

        flash()->success('Thành công', 'Bạn đã xóa Coupon thành công');

        return redirect(route('backend.coupons.index'));
    }



}
