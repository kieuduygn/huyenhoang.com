<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\View\ThemeViewFinder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

abstract class Controller extends BaseController
{

    use ValidatesRequests;


    public function __construct()
    {
        $this->middleware('admin');
        if (session_id() == '') {
            @session_start();
            /* or Session:start(); */
        }
        $_SESSION['isLoggedIn'] = auth()->check() ? true : false;

    }
}
