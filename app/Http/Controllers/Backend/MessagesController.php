<?php

namespace App\Http\Controllers\Backend;

use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thisWeek = Message::where('created_at', '<=', Carbon::now())
                        ->where('created_at', '>=', Carbon::now()->subWeek())->count();
        $thisMonth = Message::where('created_at', '<=', Carbon::now())
                        ->where('created_at', '>=', Carbon::now()->subMonth())->count();

        for($i= 0; $i<7; $i++){
            $data[$i] = [
                'weekday' => get_vn_weekday(strtotime(Carbon::now()->subDay($i))),
                'msg'   => Message::where('created_at', '<=', Carbon::now()->subDay($i))
                                    ->where('created_at', '>=', Carbon::now()->subDay($i+1))
                                    ->orderBy('created_at', 'desc')
                                    ->get(),
                'html_id'    => 'day-'. $i
            ];
        }

        return view('backend.messages.index', compact('thisWeek', 'thisMonth', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function delete($id)
    {
        Message::where('id', $id)->delete();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Message::where('id', $id)->delete();


    }


    public function markRead($id)
    {
        Message::where('id', $id)->update(['read' => 1]);
    }
}
