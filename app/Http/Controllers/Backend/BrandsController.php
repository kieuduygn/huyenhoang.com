<?php

namespace App\Http\Controllers\Backend;

use App\Models\Brand;
use Illuminate\Http\Request;

use App\Http\Requests;

class BrandsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Brand $brand)
    {
        $brands = Brand::paginate(15);

        return view('backend.brands.index', compact('brands', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StoreBrandRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreBrandRequest $request)
    {
        Brand::create($request->all());

        flash()->success(trans('common.success'), trans('brand.create_success'));

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Brand::paginate(15);
        $brand = Brand::findOrFail($id);

        return view('backend.brands.index', compact('brands', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdateBrandRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateBrandRequest $request, $id)
    {
        $brand = Brand::findOrFail($id);
        $brand->update($request->all());

        flash(trans('brand.update_success'));

        return redirect('backend/brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);

        $brand->delete();

        return redirect('backend/brands');
    }


    /**
     * Confirm delete brand
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $brand = Brand::findOrFail($id);

        return view('backend.brands.confirm', compact('brand'));
    }
}
