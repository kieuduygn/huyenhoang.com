<?php
/**
 * Created by PhpStorm.
 * User: Rain
* Date: 09/01/2016
* Time: 6:12 PM
*/

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Route;

class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return redirect(route('gallery.backend.albums.index'));

        $name = Route::getCurrentRoute()->getPath();
        return view('dashboard');
    }
}