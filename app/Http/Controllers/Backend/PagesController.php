<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Http\Requests;

class PagesController extends Controller
{

    protected $pages;

    public function __construct(Page $pages)
    {
        $this->pages = $pages;

        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Page $model)
    {
        //Bulk Delete
        bulk_delete($request, $model);

        $pages = $this->pages->paginate(15);

        return view('backend.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Page $page)
    {
        $templates = $this->getPageTemplates();
        return view('backend.pages.form', compact('page', 'templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StorePageRequest $request)
    {
        $page = $this->pages->create($request->all());

        if($request->input('list_posts')){
            $page->list_posts = 1;
            $page->posts_category_id = $request->input('category');
            $page->save();
        }

        flash()->success('Thành công', 'Tạo trang thành công');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.pages.edit', $page->id));
            default:
                return redirect(route('backend.pages.index'));
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->pages->findOrFail($id);

        $templates = $this->getPageTemplates();

        return view('backend.pages.form', compact('page', 'templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdatePageRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdatePageRequest $request, $id)
    {
        if(Page::where('uri', $request->input('uri'))->where('id', '!=', $id)->count() > 0){
            flash()->error('Lỗi', 'uri tồn tại');
            return redirect()->back();
        }

        $data = $request->all();
        $page = Page::where('id', $id)->first();

        $i = 0;
        $uri = $request->input('uri');
        do {
           if($this->pages->where('id', '!=', $id)->where('uri', $uri)->count() > 0){
               $uri .= ($i+1);
           }else{
               break;
           }
        } while ($i > 0);
        $data['uri'] = $uri;
        $page->update($data);
        if($request->input('list_posts')){
            $page->list_posts = 1;
            $page->posts_category_id = $request->input('category');
            $page->save();

        }
        flash()->success('Thành công', 'Bạn đã nhập nhật trang  thành công');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.pages.edit', $page->id));
            default:
                return redirect(route('backend.pages.index'));
        }

    }


    public function confirm(Page $page)
    {
        return view('backend.pages.confirm', compact('page'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy($id)
    {
        $page = $this->pages->findOrFail($id);

        $page->delete();

        flash()->success('Thành công', 'Xòa trang thành công');

        return redirect(route('backend.pages.index'));
    }


    private function getPageTemplates()
    {

        $themeFolder = public_path() . '\\' . config('cms.theme.folder') .'\\'. config('cms.theme.frontend') . '\views';



        $fileList = scandir($themeFolder);
        $templates = ['default' => 'Mặc định'];

        foreach($fileList as $file){
            if(substr($file, 0, 5) == 'page-'){
                $tmp = ucfirst(substr(substr($file, 0, -10), 5, 1000));
                $templates += [$tmp => $tmp];
            }
        }

        return $templates;
    }

}
