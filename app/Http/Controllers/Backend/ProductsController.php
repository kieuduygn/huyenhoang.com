<?php

namespace App\Http\Controllers\Backend;

use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\Image;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Product;
use App\Models\ProductOption;
use App\Models\Tag;
use App\Http\Requests;
use App\ProductAttributeValue;
use Illuminate\Http\Request;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ProductsController extends Controller
{

    protected $products;

    public function __construct(Product $products)
    {
        $this->products = $products;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Product $model)
    {
        bulk_delete($request, $model);

        $products = $this->products->with('file')->orderBy('created_at', 'desc')->paginate(15);

        return view('backend.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $options = Option::lists('name', 'id');
        $attributeGroups = AttributeGroup::all();

        $tagString = '';
        return view('backend.products.form', compact('product', 'attributeGroups', 'options', 'tagString'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateProductRequest $request)
    {
        //Lưu sản phẩm
        $product = $this->products->create($request->all() + ['thumbnail_id' => image_process($request->input('thumbnail'))->id]);

        //Attach Tags
        if ($request->input('tags') != '') {
            $product->tags()->attach($this->tagsProcess($request->input('tags')));
        }

        if ($request->input('images')) {
            foreach ($request->input('images') as $image) {
                $savedImage = image_process($image);
                $product->images()->attach($savedImage->id);
            }
        }


        //Xử lý Attribute tại đây


        //Xử lý Options
        if ($request->input('option_value')) {
            foreach ($request->input('option_value') as $option_id => $items) {
                foreach ($items as $value) {
                    $pivot = [
                        'quantity' => $value['quantity'],
                        'price_prefix' => $value['price_prefix'],
                        'price' => $value['price'],
                        'option_value_id' => $value['option_value_id'],
                    ];
                    $product->options()->attach($option_id, $pivot);
                }
            }
        }


        flash()->success('Thành công.', 'Bạn đã thêm thành công sản phẩm');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.products.edit', $product->id));
            default:
                return redirect(route('backend.products.index'));
        }


    }


    /**
     *
     * @param $imageID
     * @param $productID
     */
    public function detachImage($imageID, $productID)
    {
        $product = $this->products->findOrFail($productID);
        $product->images()->detach($imageID);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\Illuminate\Http\Request $request, $id)
    {
        remove_options_length();
        $tagString = '';
        $options = Option::lists('name', 'id');
        $product = $this->products->findOrFail($id);
        $attributeGroups = AttributeGroup::all();

        if ($product->tags->count() > 0) {
            foreach ($product->tags->toArray() as $tag) {
                $tagString .= $tag['name'] . ',';
            }
        }

        //Test
//        foreach($product->options()->get() as $option)
//        {
//            dd($option->products()->first()->pivot->option_value_id);
//        }

        return view('backend.products.form', compact('product', 'attributeGroups', 'options', 'tagString'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdateProductRequest|\Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateProductRequest $request, $id)
    {
        //Update sản phẩm
        $product = $this->products->findOrFail($id);

        //Sync the tags for this post
        if ($request->input('tags') != '') {
            $product->tags()->sync($this->tagsProcess($request->input('tags')));
        }

        $thumbnail = image_process($request->input('thumbnail'));
        $product->update($request->all() + ['thumbnail_id' => $thumbnail->id]);

        //Xử lý Option

        if ($request->input('option_value')) {
            foreach ($request->input('option_value') as $option_id => $items) {
                $product->options()->detach($option_id);
                foreach ($items as $value) {

                    $pivot = [
                        'quantity' => $value['quantity'],
                        'price_prefix' => $value['price_prefix'],
                        'price' => $value['price'],
                        'option_value_id' => $value['option_value_id'],
                    ];
                    $product->options()->attach($option_id, $pivot);
                }
            }
        }

        //Xử lý hình ảnh
        if ($request->input('images')) {
            foreach ($request->input('images') as $image) {
                $processedImage = image_process($image);
                $product->images()->detach($processedImage->id);
                $product->images()->attach($processedImage->id);
            }
        }

        flash()->success('Thành công', 'Bạn đã nhập nhật sản phẩm  thành công');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.products.edit', $product->id));
            default:
                return redirect(route('backend.products.index'));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->products->findOrFail($id);

        $product->delete();

        flash()->success('Thành công', 'Bạn đã xóa sản phẩm [' . $product->name . '] thành công');

        return redirect(route('backend.products.index'));
    }


    /**
     * Product Delete Confirm
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $product = $this->products->findOrFail($id);

        return view('backend.products.confirm', compact('product'));
    }


    /**
     * @param $option_id
     * @param $product_id
     */

    public function detachOption($option_id, $product_id, $option_value_id)
    {
        ProductOption::where('option_id', $option_id)
            ->where('option_value_id', $option_value_id)
            ->where('product_id', $product_id)->delete();
        //
    }

    public function weareTestingThisController()
    {
        return view('AreYouOK');
    }

    public function addOptionAjax($option_id, $length)
    {
        $length++;
        if (!session('options_length')) {
            session()->put('options_length', $length);
        } else {
            $length = session('options_length');
            $length++;
            session()->put('options_length', $length);
            session()->save();
        }

        $option = Option::findOrFail($option_id);
        $values = OptionValue::lists('name', 'id');
        return view('backend.options.add_value', compact('option', 'values', 'length'));
    }


    /**
     * Xử lý Tag để lấy ra mảng tag của bài viết này
     *
     * @param $tagString
     * @return array
     */
    private function tagsProcess($tagString)
    {
        $tagList = explode(',', $tagString);
        $slugify = new Slugify();
        $returnTagsArray = array();
        foreach ($tagList as $tagItem) {
            $tagSlug = $slugify->slugify($tagItem);
            $query = Tag::where('slug', $tagSlug);
            if ($query->count() == 0) {
                $returnTagsArray[] = Tag::create(['name' => $tagItem, 'slug' => $tagSlug])->id;
            } else {
                $returnTagsArray[] = $query->first()->id;
            }

        }
        return $returnTagsArray;
    }


    public function anotherTestFunction(Request $request)
    {
        dd($request->all());
    }

}

















