<?php

namespace App\Http\Controllers\Backend;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests;

class OrdersController extends Controller
{

    protected $orders;

    public function __construct(Order $orders)
    {
        $this->orders = $orders;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->orders->paginate(15);

        return view('backend.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->orders->findOrFail($id);

        return view('backend.orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status($id, $status_id)
    {
        $order = $this->orders->where('id', $id)->update(['order_status_id' => $status_id]);

        return redirect()->back();
    }


    public function confirm($id)
    {
        $order = $this->orders->findOrFail($id);


        return view('backend.orders.confirm', compact('order'));
    }


    public function destroy($id)
    {
        $order = $this->orders->findOrFail($id);

        $order->delete();

        flash()->success('Thành công', 'Bạn đã xóa thành công đơn hàng #' . $order->id);

        return redirect('backend.orders.index');
    }

}
