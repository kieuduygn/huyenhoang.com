<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Advertising;
use App\Http\Requests;


class AdsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Ads $ads
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Advertising $ads)
    {
        $list_ads = Advertising::paginate(15);

        return view('backend.ads.index', compact('ads', 'list_ads'));
    }


    /**
     * Xóa hàng loạt
     *
     * @param Request $request
     * @param Advertising $ads
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulk(Request $request, Advertising $ads)
    {
        bulk_delete($request, $ads);

        return redirect(route('backend.ads.index'));
    }

    /**
     * Lưu quảng cáo
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //Validate
        $this->validate($request, [
            'name' => 'required|max:255',
            'slug' => 'required|max:255|unique:advertisings,slug',
            'description' => 'max:255',
            'image' => 'required|max:255',
            'link' => 'required|max:255'
        ]);

        //Tạo quảng cáo
        $ads = Advertising::create($request->all());

        //thoát
        flash()->success(trans('common.success'), trans('ads.create_success_message', ['name' => $ads->name]));

        return redirect(route('backend.ads.index'));

    }


    /**
     * Form edit quảng cáo
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //Lấy ra quảng cáo
        $ads = Advertising::findOrFail($id);

        //Danh sách tất cả ads
        $list_ads = Advertising::paginate(15);

        return view('backend.ads.index', compact('ads', 'list_ads'));
    }


    /**
     * Cập nhật quảng cáo
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        //Validate
        $this->validate($request, [
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'description' => 'max:255',
            'image' => 'required|max:255',
            'link' => 'required|max:255'
        ]);

        //Tìm quảng cáo cần chỉnh sửa
        $ads = Advertising::findOrFail($id);

        //Kiểm tra xem slug có trùng các quảng cáo khác hay không
        if(Advertising::where('slug', $ads->slug)->where('id', '!=', $ads->id)->count() > 0){
            flash(trans('common.slug_duplicate'));
            return redirect()->back();
        }

        //Tạo quảng cáo
        $ads->update($request->all());

        //thoát
        flash()->success(trans('common.success'), trans('ads.update_success_message', ['name' => $ads->name]));

        return redirect(route('backend.ads.index'));

    }


    /**
     * Nhân đôi quảng cáo
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function duplicate($id)
    {
        //Lấy ra quảng cáo cần nhân đôi
        $ads = Advertising::findOrFail($id)->toArray();

        //Thay đổi slug
        $ads['slug'] = $ads['slug'] . '_';

        //Tạo mới
        $ads = Advertising::create($ads);

        flash(trans('ads.duplicate_success_message', ['name' => $ads->name]));

        return redirect(route('backend.ads.edit',$ads->id));
    }


    /**
     * Xác nhận xóa quảng cáo
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        //Lấy ra quảng cáo cần xóa
        $ads = Advertising::findOrFail($id);

        return view('backend.ads.confirm', compact('ads'));
    }


    /**
     * Xóa quảng cáo
     *
     * @param $id
     */
    public function destroy($id)
    {
        //Lấy ra quảng cáo cần xóa
        $ads = Advertising::findOrFail($id);

        //Xóa
        $ads->delete();

        flash()->success(trans('common.success'), trans('ads.delete_success_message', ['name' => $ads->name]));

        return redirect(route('backend.ads.index'));
    }
}
