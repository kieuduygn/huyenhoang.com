<?php

namespace App\Http\Controllers\Backend;

use App\Models\Branch;
use App\Models\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;

class BranchesController extends Controller
{

    protected $branches;

    public function __construct(Branch $branches)
    {
        $this->branches = $branches;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, Branch $model)
    {
        //Bulk Deleting
        bulk_delete($request, $model);

        if($request->input('province') != null && $request->input('province') != -1){
            $items = Branch::where('province_id', $request->input('province'))->paginate(15);
            $province = $request->input('province');
        }else{
            $province = null;
            $items = Branch::paginate(15);
        }

        return view('backend.branches.home', compact('items', 'province'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Branch $item
     * @return Response
     */
    public function create(Branch $item)
    {
        $provinces = Province::lists('name', 'id');

        return view('backend.branches.form', compact('item', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StoreBranchRequest $request
     * @return Response
     */
    public function store(Requests\StoreBranchRequest $request)
    {
        $this->branches->create($request->all());

        flash()->success('Thành công', 'Bạn đã thêm chi nhánh mới thành công');

        return redirect(route('backend.branches.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $provinces = Province::lists('name', 'id');

        $item = $this->branches->findOrFail($id);

        return view('backend.branches.form', compact('item', 'provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\StoreBranchRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(Requests\StoreBranchRequest $request, $id)
    {
        $this->branches->where('id', $id)->update($request->only('name', 'address', 'phone', 'email', 'province_id', 'home', 'district_id', 'lng', 'lat'));

        flash()->success('Thành công', 'Bạn đã cập nhật chi nhánh thành công');

        return redirect(route('backend.branches.index'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $item = $this->branches->findOrFail($id);

        return view('backend.branches.confirm', compact('item'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->branches->findOrFail($id)->delete();

        flash()->success('Thành công', 'Bạn đã xóa chi nhánh thành công');

        return redirect(route('backend.branches.index'));
    }
}
