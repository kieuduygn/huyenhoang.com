<?php

namespace App\Http\Controllers\Backend;

use App\Models\BlogCategory;
use Illuminate\Http\Request;

use App\Http\Requests;

class BlogCategoriesController extends Controller
{

    protected  $categories;

    public function __construct(BlogCategory $categories)
    {
        $this->categories = $categories;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, BlogCategory $model)
    {
        bulk_delete($request, $model);

        if($request->input('filter') != null && $request->input('filter') != -1){
            $categories = BlogCategory::where('hide_from_blog', $request->input('filter'))->get()->toHierarchy();
            $filter = $request->input('filter');
        }else{
            $filter = -1;
            $categories = BlogCategory::all()->toHierarchy();
        }

        $filters = [
            '-1'    => 'Hide from blog',
            '1'     => 'Yes',
            '0'     => 'No'
        ];
        return view('backend.blog_categories.index', compact('categories', 'parents', 'filter', 'filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BlogCategory $category)
    {
        return view('backend.blog_categories.form', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreBlogCategoriesRequest $request )
    {

        $category = $this->categories->create($request->only('name', 'slug', 'description'));

        if($request->input('hide_from_blog')){
            $category->hide_from_blog = 1;
            $category->save();
        }

        if($request->input('parent') != 0 ){
            $parent = BlogCategory::where('id', $request->input('parent'))->first();
            $category->makeChildOf($parent);
        }

        flash()->success('Thành công', 'Tạo danh mục thành công');

       return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categories->findOrFail($id);

        return view('backend.blog_categories.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateBlogCategoriesRequest $request, $id)
    {
        //Không cho trùng slug
        $query = BlogCategory::where('slug', $request->input('slug'));
        if($query->count() > 0){
            flash('Slug bạn nhập đã tồn tại, vui lòng nhập lại');
            return redirect()->back();
        }

        $category = $this->categories->findOrFail($id);

        $category->fill($request->only('name', 'slug'))->save();

        flash()->success('Thành công', 'Bạn đã cập nhật thành công danh mục');

        return redirect(route('backend.blogcategories.edit', $category->id));
    }



    /**
     * Confirm trước khi delete
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $category = $this->categories->findOrFail($id);

        return view('backend.blog_categories.confirm', compact('category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->categories->findOrFail($id);
        $category->delete();

        flash()->success('Thành công', 'Bạn đã xóa bài thành công');

        return redirect(route('backend.blogcategories.index'));
    }
}
