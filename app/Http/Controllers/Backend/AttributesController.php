<?php

namespace App\Http\Controllers\Backend;

use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Models\AttributeGroup;
use App\Http\Requests;

class AttributesController extends Controller
{
    protected $attributes;

    public function __construct(Attribute $attributes)
    {
        $this->attributes = $attributes;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $this->attributes->paginate(15);

        return view('backend.attributes.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = $this->attributes;

        $groups = AttributeGroup::lists('name', 'id');

        return view('backend.attributes.form', compact('item', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'attribute_group_id' => 'required|numeric']);

        $this->attributes->create($request->all());

        flash()->success('Thành công', 'Bạn đã thêm thành công thuộc tính sản phẩm');

        return redirect(route('backend.attributes.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->attributes->findOrFail($id);

        $groups = AttributeGroup::lists('name', 'id');

        return view('backend.attributes.form', compact('item', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'attribute_group_id' => 'required|numeric']);

        $group = $this->attributes->findOrFail($id);
        $group->fill($request->all());

        flash()->success('Thành công', 'Bạn đã cập nhật thành công thuộc tính sản phẩm');

        return redirect(route('backend.attributes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->attributes->findOrFail($id);

        $item->delete();

        flash()->success('Thành công', 'Bạn đã xóa thành công thuộc tính sản phẩm');

        return redirect(route('backend.attributes.index'));
    }


    public function confirm($id)
    {
        $item = $this->attributes->findOrFail($id);

        return view('backend.attributes.confirm', compact('item'));
    }
}
