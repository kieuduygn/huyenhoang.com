<?php

namespace App\Http\Controllers\Backend;

use App\Models\Menu;
use App\Models\MenuPosition;
use App\Models\Page;
use App\Models\Post;
use Illuminate\Http\Request;

use App\Http\Requests;

class MenusController extends Controller
{

    protected $menus;

    public function __construct(Menu $menus)
    {


        $this->menus = $menus;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = MenuPosition::orderBy('order', 'asc')->paginate(15);

        return view('backend.menus.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = $this->menus;

        return view('backend.menus.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|max:255']);

        $menu = MenuPosition::create($request->all());

        return redirect(route('backend.menus.edit', $menu->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $menuTypes = config('app.menu_types');

        foreach ($menuTypes as $key => $type) {
            if ($key != 'Link') {
                $className = '\App\Models\\' . $key;
                $class = new $className;
                $data[strtolower($key) . '_items'] = $class->all();
            }
        }

        $item = MenuPosition::findOrFail($id);
        $menuItems = Menu::where('position_id', $item->id)->get()->toHierarchy();

        return view('backend.menus.edit', compact('item', 'menuItems') + $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->input('type')) {
            foreach ($request->input('type') as $key => $item) {
                foreach ($item as $k => $v) {
                    $className = '\App\Models\\' . $key;
                    $class = new $className;
                    $name = $class->where('id', $v)->first()->name;
                    if($key == 'Post') $name = $class->where('id', $v)->first()->title;
                $data = [
                    'name' => $name,
                    'type' => $key,
                    'value' => $v,
                    'position_id' => $id
                ];
                    Menu::create($data);

            }
            }
        }

        if($request->input('link.value') != '' && $request->input('link.label') != ''){
            $data = [
                'name' => $request->input('link.label'),
                'type' => 'Link',
                'value' => $request->input('link.value'),
                'position_id' => $id
            ];
            Menu::create($data);
        }


        return redirect(route('backend.menus.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = MenuPosition::where('id', $id)->delete();

        flash('Bạn đã xóa menu thành công');

        return redirect(route('backend.menus.index'));
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function remove($id)
    {
        $menu = Menu::where('id', $id)->first();
        $menu->delete();

        return redirect(route('backend.menus.edit', $menu->position_id));
    }


    /**
     * @param Request $request
     * @param $id
     */
    public function addItem(Request $request, $id)
    {
        dd($id);
    }


    /**
     * @param Request $request
     */
    public function saveMenu(Request $request)
    {
        $input = $request->all();
        if ($input['data']) {
            foreach ($input['data'] as $key => $item) {
                if ($key > 0) {
                    $data = [
                        'lft' => $item['left'],
                        'rgt' => $item['right'],
                        'depth' => $item['depth'],
                        'parent_id' => $item['parent_id']
                    ];
                    Menu::where('id', $item['id'])->update($data);
                }
            }
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function order(Request $request)
    {
        if($request->input('menu')){
            foreach($request->input('menu') as $menu => $order){
                if(is_numeric($order)){
                    MenuPosition::where('id', $menu)->update(['order' => $order]);
                }
            }
        }

        return redirect()->back();
    }


    public function confirm($id)
    {
        $item = MenuPosition::findOrFail($id);

        return view('backend.menus.confirm', compact('item'));
    }

}
