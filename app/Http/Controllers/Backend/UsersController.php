<?php

namespace App\Http\Controllers\Backend;

use App\Models\District;
use App\Models\Product;
use App\Models\Province;
use App\Models\Ward;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests;

class UsersController extends Controller
{

    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $model)
    {

        bulk_delete($request, $model);

        $users = $this->users->paginate(10);
        return view('backend.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $provinces = Province::lists('name', 'id');
        return view('backend.users.form', compact('user', 'districts', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreUserRequest $request)
    {
        $data = $request->all();

        $data['password'] = bcrypt($request->input('password'));
        $user = $this->users->create($data);

        flash()->success('Thành công', 'Tạo người dùng thành công');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.users.edit', $user->id));
            default:
                return redirect(route('backend.users.index'));
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->users->findOrFail($id);
        $districts = District::lists('name', 'id');
        $provinces = Province::lists('name', 'id');
        $wards = Ward::lists('name', 'id');

        return view('backend.users.form', compact('user', 'districts', 'provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdateUserRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateUserRequest $request, $id)
    {

        $user = $this->users->findOrFail($id);

        if ($request->input('password') != '') {
            $this->validate($request, [
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password'
            ]);
        }

        $input = $request->all();
        $updateArray = [
            'name' => $input['name'],
            'phone' => $input['phone'],
            'address'   => $input['address'],
            'district_id'   => $input['district_id'],
            'province_id'   => $input['province_id'],
            'ward_id'   => $input['ward_id']
        ];

        if($request->input('password') != ''){
            $updateArray['password'] = bcrypt($request->input('password'));
        }

        $user->update($updateArray);

        flash()->success('Thành công !', "Bạn đã cập nhật người dùng {$user->name}");

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.users.edit', $user->id));
            default:
                return redirect(route('backend.users.index'));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Obj $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->users->findOrFail($id);

        $user->delete();

        flash()->success('Thành công', 'Bạn vừa xóa người dùng thành công');

        return redirect(route('backend.users.index'));
    }


    /**
     * Trang xác nhận xóa người dùng
     *
     * @param $user --
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $user = $this->users->findOrFail($id);

        return view('backend.users.confirm', compact('user'));
    }
}
