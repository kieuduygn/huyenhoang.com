<?php

namespace App\Http\Controllers\Backend;

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Tag;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class BlogController extends Controller
{

    protected $posts;

    public function __construct(Blog $posts)
    {
        $this->posts = $posts;

        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->posts->with('author')->with('category')->orderBy('published_at', 'desc')->paginate(10);

        return view('backend.blog.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Blog $post)
    {


        $categories = $this->getCategories();

        if(count($categories) == 0)
        {
            flash('Lưu ý', 'Bạn phải tạo ít nhất một danh mục trước khi tạo bài viết');

            return redirect(route('backend.blogcategories.create'));
        }

        $tagString ='';

        return view('backend.blog.form', compact('post', 'categories', 'tagString'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StorePostRequest $request)
    {

        $file = Request::file('thumbnail');

        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put(md5($file->getFilename() . microtime()) . '.' . $extension, File::get($file));

        $entry = new File();
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $file->getFilename() . '.' . $extension;

        $entry->save();

        $post = $this->posts->create(
            ['author_id' => auth()->user()->id] + $request->all() + ['file_id' => $entry->id]
        );

        if($request->input('tags') != ''){
            $post->tags()->attach($this->tagsProcess($request->input('tags')));
        }

        $post->categories()->attach($request->input('category_list'));

        flash()->success('Thành công', 'Bạn đã thêm thành công bài viết');

        return redirect(route('backend.blog.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        exit();

        $post = $this->posts->findOrFail($id);
        $categories = $this->getCategories();
        $tagString = '';
        if($post->tags->count() > 0){
            foreach($post->tags->toArray() as $tag)
            {
                $tagString .= $tag['name'] . ',';
            }
        }
        return view('backend.blog.form', compact('post', 'categories', 'tagString'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\UpdatePostRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdatePostRequest $request, $id)
    {
        $post = $this->posts->findOrFail($id);

        $post->fill($request->all())->save();

        //Sync the tags for this post
        if($request->input('tags') != '')
        {
            $post->tags()->sync($this->tagsProcess($request->input('tags')));
        }

        //Sync the categories for this post
        $post->categories()->sync($request->input('category_list'));

        flash()->success('Thành công', 'Bạn vừa cập nhật bài viết thành công');

        return redirect(route('backend.blog.edit', $post->id));
    }


    public function confirm($id)
    {
        $post = $this->posts->findOrFail($id);

        return view('backend.blog.confirm', compact('post'));
    }
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->posts->findOrFail($id);

        $post->delete();

        flash()->success('Thành công', 'Xóa bài thành công');

        return redirect(route('backend.blog.index'));
    }


    /**
     *
     * @return array
     */
    protected function getCategories()
    {
        $catItems = BlogCategory::all()->toArray();
        $categories = [];
        foreach($catItems as $item)
        {
            $categories[$item['id']] = $item['name'];
        }
        return $categories;
    }


    /**
     * Xử lý Tag để lấy ra mảng tag của bài viết này
     *
     * @param $tagString
     * @return array
     */
    private function tagsProcess($tagString)
    {
        $tagList = explode(',', $tagString);
        $slugify = new Slugify();
        $returnTagsArray = array();
        foreach($tagList as $tagItem)
        {
            $tagSlug = $slugify->slugify($tagItem);
            $query = Tag::where('slug', $tagSlug);
            if($query->count() == 0){
                $returnTagsArray[] = Tag::create(['name' => $tagItem, 'slug' => $tagSlug])->id;
            }else{
                $returnTagsArray[] = $query->first()->id;
            }

        }
        return $returnTagsArray;
    }
}
