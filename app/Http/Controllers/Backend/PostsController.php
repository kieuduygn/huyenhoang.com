<?php

namespace App\Http\Controllers\Backend;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Tag;
use App\Models\BlogCategory;
use App\Http\Requests;
use Cocur\Slugify\Slugify;

class PostsController extends Controller
{
    protected $posts;

    public function __construct(Post $posts)
    {
        $this->posts = $posts;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Post $model)
    {
        //Bulk Delete
        bulk_delete($request, $model);

        if($request->input('category') != null && $request->input('category') != -1){
            $posts = BlogCategory::where('id', $request->input('category'))->first()->posts()->orderBy('published_at')->paginate(15);
            $_category = $request->input('category');
        }else{
            $_category = -1;
            $posts = Post::with('author')->with('category')->orderBy('published_at', 'desc')->paginate(15);
        }

        return view('backend.posts.index', compact('posts', '_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Post $post)
    {
        $categories = $this->getCategories();

        if(count($categories) == 0)
        {
            flash('Lưu ý', 'Bạn phải tạo ít nhất một danh mục trước khi tạo bài viết');

            return redirect(route('backend.blogcategories.create'));
        }

        $tagString ='';

        return view('backend.posts.form', compact('post', 'categories', 'tagString'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StorePostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StorePostRequest $request)
    {
        $post = $this->posts->create(
            ['author_id' => auth()->user()->id] + $request->all()
        );

        //Attach Tags
        if($request->input('tags') != ''){
            $post->tags()->attach($this->tagsProcess($request->input('tags')));
        }

        //Attach printing.Category
        $post->categories()->attach($request->input('category_list'));

        flash()->success('Thành công', 'Bạn đã thêm thành công bài viết');

        switch ($request->input('button')) {
            case 'save':
                return redirect(route('backend.posts.edit', $post->id));
            default:
                return redirect(route('backend.posts.index'));
        }


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->posts->findOrFail($id);
        $categories = $this->getCategories();
        $tagString = '';
        if($post->tags->count() > 0){
            foreach($post->tags->toArray() as $tag)
            {
                $tagString .= $tag['name'] . ',';
            }
        }
        return view('backend.posts.form', compact('post', 'categories', 'tagString'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdatePostRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdatePostRequest $request, $id)
    {
        //Kiểm tra slug có trùng hay không
        $query = Post::where('slug', $request->input('slug'))->where('id', '!=', $id);
        if($query->count() > 0){
            flash('Slug bạn nhập đã tồn tại, vui lòng nhập lại');
            return redirect()->back();
        }

        $thumbnail = $this->imageProcess($request->input('thumbnail'));

        $post = $this->posts->findOrFail($id);

        $post->update($request->all());

        //Sync the tags for this post
        if($request->input('tags') != '')
        {
            $post->tags()->sync($this->tagsProcess($request->input('tags')));
        }

        //Sync the categories for this post
        $post->categories()->sync($request->input('category_list'));
        if($request->input('related_posts')){
            $post->related()->sync($request->input('related_posts'));

        }

        flash()->success('Thành công', 'Bạn vừa cập nhật bài viết thành công');

        switch($request->input('button')){
            case 'save':
                return redirect(route('backend.posts.edit', $post->id));
            default:
                return redirect(route('backend.posts.index'));
        }

    }


    /**
     * Confirm trước khi delete
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $post = $this->posts->findOrFail($id);

        return view('backend.posts.confirm', compact('post'));
    }

    public function duplicate($id)
    {
        $post = $this->posts->findOrFail($id);

        $postArray = $post->toArray();

        $postArray['slug'] = $postArray['slug'] . '_';

        $post = Post::create($postArray);

        return redirect(route('backend.posts.edit', $post->id));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->posts->findOrFail($id);
        $post->delete();

        flash()->success('Thành công', 'Bạn đã xóa bài thành công');

        return redirect(route('backend.posts.index'));
    }



    /**
     *
     * @return array
     */
    protected function getCategories()
    {
        $catItems = BlogCategory::all()->toHierarchy();

        $categories = [];

        foreach($catItems as $node){
            $categories += arrange_categories($node);
        }

        return $categories;
    }


    /**
     * Xử lý Tag để lấy ra mảng tag của bài viết này
     *
     * @param $tagString
     * @return array
     */
    private function tagsProcess($tagString)
    {
        $tagList = explode(',', $tagString);
        $slugify = new Slugify();
        $returnTagsArray = array();
        foreach($tagList as $tagItem)
        {
            $tagSlug = $slugify->slugify($tagItem);
            $query = Tag::where('slug', $tagSlug);
            if($query->count() == 0){
                $returnTagsArray[] = Tag::create(['name' => $tagItem, 'slug' => $tagSlug])->id;
            }else{
                $returnTagsArray[] = $query->first()->id;
            }

        }
        return $returnTagsArray;
    }


    /**
     * @param $file_path
     * @return static
     */
    public function imageProcess($file_path)
    {
        $md5 = md5($file_path);

        $query = Image::where('md5', $md5);

        if($query->count() == 0){
            $image = Image::create(['file_path' => $file_path, 'md5' => $md5]);
            return $image;
        }else{
            return $query->first();
        }
    }



}
