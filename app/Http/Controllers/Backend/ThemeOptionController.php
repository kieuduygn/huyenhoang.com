<?php
namespace App\Http\Controllers\Backend;

use App\Models\ThemeOption;
use Illuminate\Http\Request;

use App\Http\Requests;

class ThemeOptionController extends Controller

{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        return view('backend.themeoption.index');
    }

    public function save(Request $request)
    {

        if ($request->input('options')) {
            foreach ($request->input('options') as $option => $item) {
                ThemeOption::where('option', $option)->delete();
                foreach ($item as $value) {
                    ThemeOption::create(['option' => $option, 'value' => htmlentities($value)]);
                }
            }
        }


        return redirect()->back();
    }
}
