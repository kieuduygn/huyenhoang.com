<?php

namespace App\Http\Controllers\Backend;

use App\Models\Option;
use App\Models\OptionValue;
use Illuminate\Http\Request;

use App\Http\Requests;

class OptionsController extends Controller
{

    protected $options;


    /**
     *
     *
     * @param $options
     */
    public function __construct(Option $options)
    {
        $this->options = $options;

        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Option $model)
    {
        bulk_delete($request, $model);

        $items = $this->options->paginate(15);

        return view('backend.options.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $item = $this->options;

        return view('backend.options.form', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|max:255', 'type' => 'required']);

        $option = $this->options->create($request->all());

        return redirect(route('backend.options.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->options->findOrFail($id);

        return view('backend.options.form', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->options->where('id', $id)->update($request->only('name'));

        if($request->input('value')){
            OptionValue::where('option_id', $id)->delete();
            foreach($request->input('value') as $value){
                OptionValue::create([
                    'option_id' => $id,
                    'name'  => $value
                ]);
            }
        }

        return redirect(route('backend.options.index'));
    }


    public function show()
    {

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->options->findOrFail($id);

        $item->delete();

        return redirect(route('backend.options.index'));
    }


    /**
     * Confirm Delete Option
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($id)
    {
        $item = $this->options->findOrFail($id);

        return view('backend.options.confirm', compact('item'));
    }
}
