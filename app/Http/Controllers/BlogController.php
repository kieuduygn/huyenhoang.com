<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\BlogCategory;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    protected $posts;
    protected $categories;

    public function __construct(Post $posts, BlogCategory $categories)
    {
        $this->posts = $posts;
        $this->categories = $categories;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->posts->with('categories')
            ->with('images')
            ->with('thumbnail')
            ->with('comments')
            ->orderBy('published_at', 'desc')->paginate(15);
        return view('blog.index', compact('posts', 'categories'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Request $request, $id)
    {
        if(! auth()->check()){
            flash('Bạn phải đăng nhập mới có thể bình luận');

            return redirect()->back();
        }

        $this->validate($request, [
            'point' => 'required|numeric',
            'content'   => 'max:2000|required'
        ]);

        $array = [
            'post_id'   => $id,
            'user_id'   => auth()->user()->id,
            'point' => $request->input('point'),
            'content'   => $request->input('content')
        ];

        $comment = Comment::create($array);

        flash('Cảm ơn bạn đã gửi bình luận');

        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $posts = $this->posts->paginate(10);
        $post = $this->posts->where('slug', $slug)->first();
        $post->view += $post->view +1;
        $post->save();
        $tags = $post->tags()->get();


        return view('blog.show', compact('post', 'categories', 'posts', 'tags'));
    }


    /**
     * Lấy ra danh sách các bài viết có catetegory với id và slug theo yêu cầu
     *
     * @param $id
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function category($slug)
    {
        $query = BlogCategory::where('slug', $slug);
        if($query->count() == 0){
            return redirect('/');
        }

        $desObject = $query->first();

        $posts = $desObject->posts()->paginate(15);

        return view('blog.index', compact('posts', 'categories', 'desObject'));
    }



    /**
     * Lấy ra danh sách các bài viết có tag id và slug theo yêu cầu
     *
     * @param $id
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function tag($id, $slug)
    {

        //Kiểm tra xem tag có tồn tại không, nếu không thì redirect
        $tagCount = Tag::where('id', $id)->where('slug', $slug)->count();
        if($tagCount == 0){
            return redirect('/');
        }

        $desObject = Tag::where('id', $id)->first();

        $posts = $desObject->posts()->paginate(15);

        return view('blog.index', compact('posts', 'categories', 'desObject'));
    }




}
