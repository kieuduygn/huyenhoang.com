<?php
namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Coupon;
use App\Models\Post;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Gallery\Entities\Category;
use Modules\Gallery\Entities\PopularPhoto;

class HomeController extends Controller
{

    public function index(Product $products, Branch $branches)
    {

        $products = \Modules\Gallery\Entities\Album::where('home', 1)
        ->orderBy('order_number', 'desc')
        ->limit(12)
        ->get();

        $popularPhotos = PopularPhoto::orderByRaw("RAND()")->limit(8)->get();

        $categories = Category::limit(6)->get();

        $posts = Post::orderBy('created_at', 'desc')->limit(3)->get();
        return view('page-home', compact('products', 'branches', 'posts', 'categories', 'popularPhotos'));
    }


    /**
     *
     * @param $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function applyCoupon($code)
    {
        $coupon = Coupon::where('code', $code);
        if($coupon->count() > 0){

            $coupon = $coupon->first();
            $lastUsed = $coupon->used;

            if($coupon->used >= $coupon->total){
                $key = 'danger';
                $message = 'Mã giảm giá ['.$code.'] đã sử dụng hết';
            }else{
                if($coupon->date_end < Carbon::now()){
                    $key = 'default';
                    $message = 'Mã giảm giá hết hạn';
                }else{
                $coupon->update(['used' => $lastUsed+1]);
                session()->put('coupon', $coupon);
                session()->save();
                $key = 'success';
                $message = 'Chúc mừng bạn, mã giảm giá ['.$coupon->name.'] có giá trị ['
                    .number_format($coupon->discount).'], hãy đặt hàng trước khi coupon hết hạn';
                }
            }
        }else{
            $key = 'danger';
            $message = 'Mã giảm giá không tồn tại';
        }

        return view('apply_coupon', compact('key', 'message'));



    }
}
