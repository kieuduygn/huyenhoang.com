<?php
// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', url());
});

Breadcrumbs::register('frontend_page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push($page->name, route('page', $page->slug));
});

Breadcrumbs::register('page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push($page->name, route('page', $page->id));
});


//Blog-
Breadcrumbs::register('blog', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog.index'));
});
Breadcrumbs::register('blog_show', function($breadcrumbs, $post)
{
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($post->title, route('blog.show', [$post->id, $post->slug]));
});

//Blog
Breadcrumbs::register('user', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Account', route('user.profile'));
});



/***************PRINTING MODULE*********************/
//Product
Breadcrumbs::register('printing::product', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Sản phẩm', route('printing.products.index'));
});
Breadcrumbs::register('printing::product_show', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('printing::product');
    $breadcrumbs->push($product->name, route('printing.product.show', $product->slug));
});

Breadcrumbs::register('printing::product_category', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('printing::product');
    $breadcrumbs->push($category->name, route('printing.category.show', $category->slug));
});




//Backend---------------------------------------------------------------------------------------------------------------

//Ads
Breadcrumbs::register('backend', function($breadcrumbs)
{
    $breadcrumbs->push('Backend', url('backend'));
});
Breadcrumbs::register('ads', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Quảng cáo', route('backend.ads.index'));
});
Breadcrumbs::register('create_post', function($breadcrumbs)
{
    $breadcrumbs->parent('ads');
    $breadcrumbs->push('Thêm quảng cáo', route('backend.ads.create'));
});
Breadcrumbs::register('edit_ads', function($breadcrumbs, $ads)
{
    $breadcrumbs->parent('ads');
    $breadcrumbs->push('Sửa quảng cáo', route('backend.ads.edit', [$ads->id]));
});

Breadcrumbs::register('delete_ads', function($breadcrumbs, $ads)
{
    $breadcrumbs->parent('ads');
    $breadcrumbs->push('Xóa quảng cáo', route('backend.ads.confirm', [$ads->id]));
});

//Blog

Breadcrumbs::register('post', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Bài viết', route('backend.posts.index'));
});
Breadcrumbs::register('create_post', function($breadcrumbs)
{
    $breadcrumbs->parent('post');
    $breadcrumbs->push('Thêm bài viết', route('backend.posts.create'));
});
Breadcrumbs::register('edit_post', function($breadcrumbs, $post)
{
    $breadcrumbs->parent('post');
    $breadcrumbs->push('Sửa bài viết', route('backend.posts.edit', [$post->id]));
});

//Blog categories
Breadcrumbs::register('blog_category', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Danh mục bài viết', route('backend.blogcategories.index'));
});
Breadcrumbs::register('create_blog_category', function($breadcrumbs)
{
    $breadcrumbs->parent('blog_category');
    $breadcrumbs->push('Thêm danh mục bài viết', route('backend.blogcategories.create'));
});
Breadcrumbs::register('edit_blog_category', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('blog_category');
    $breadcrumbs->push('Sửa danh mục bài viết', route('backend.blogcategories.edit', [$category->id]));
});


//Page
Breadcrumbs::register('page', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Trang', route('backend.pages.index'));
});
Breadcrumbs::register('create_page', function($breadcrumbs)
{
    $breadcrumbs->parent('page');
    $breadcrumbs->push('Thêm trang', route('backend.pages.create'));
});
Breadcrumbs::register('edit_page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('page');
    $breadcrumbs->push('Sửa trang', route('backend.pages.edit', [$page->id]));
});
Breadcrumbs::register('confirm_delete_page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('page');
    $breadcrumbs->push('Xác nhận xóa trang', route('backend.pages.confirm', [$page->id]));
});

//Menu
Breadcrumbs::register('menu', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Trình đơn', route('backend.menus.index'));
});
Breadcrumbs::register('create_menu', function($breadcrumbs)
{
    $breadcrumbs->parent('menu');
    $breadcrumbs->push('Thêm trình đơn', route('backend.menus.create'));
});
Breadcrumbs::register('edit_menu', function($breadcrumbs, $menu)
{
    $breadcrumbs->parent('menu');
    $breadcrumbs->push('Sửa trình đơn', route('backend.menus.edit', [$menu->id]));
});
Breadcrumbs::register('confirm_delete_menu', function($breadcrumbs, $menu)
{
    $breadcrumbs->parent('menu');
    $breadcrumbs->push('Xác nhận xóa trình đơn', route('backend.menus.confirm', [$menu->id]));
});


//User
Breadcrumbs::register('user', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Người dùng', route('backend.users.index'));
});
Breadcrumbs::register('create_user', function($breadcrumbs)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('Thêm người dùng', route('backend.users.create'));
});
Breadcrumbs::register('edit_user', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('Sửa người dùng', route('backend.users.edit', [$user->id]));
});
Breadcrumbs::register('confirm_delete_user', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('Xác nhận xóa người dùng', route('backend.users.confirm', [$user->id]));
});


/*********************************** PRINTING MODULE********************************/
//Category
Breadcrumbs::register('printing_categories', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Danh mục sản phẩm dịch vụ', route('printing.backend.categories.index'));
});
Breadcrumbs::register('create_printing_categories', function($breadcrumbs)
{
    $breadcrumbs->parent('printing_categories');
    $breadcrumbs->push('Thêm danh mục sản phẩm dịch vụ', route('printing.backend.categories.create'));
});
Breadcrumbs::register('edit_printing_categories', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('printing_categories');
    $breadcrumbs->push('Sửa danh mục sản phẩm dịch vụ', route('printing.backend.categories.edit', [$category->id]));
});
Breadcrumbs::register('confirm_delete_printing_categories', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('printing_categories');
    $breadcrumbs->push('Xác nhận xóa danh mục sản phẩm dịch vụ', route('printing.backend.categories.confirm', [$category->id]));
});


//Product
Breadcrumbs::register('printing_product', function($breadcrumbs)
{
    $breadcrumbs->parent('backend');
    $breadcrumbs->push('Sản phẩm', route('printing.backend.products.index'));
});
Breadcrumbs::register('create_printing_product', function($breadcrumbs)
{
    $breadcrumbs->parent('printing_product');
    $breadcrumbs->push('Thêm sản phẩm', route('printing.backend.products.create'));
});
Breadcrumbs::register('edit_printing_product', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('printing_product');
    $breadcrumbs->push('Sửa sản phẩm', route('printing.backend.products.edit', [$product->id]));
});
Breadcrumbs::register('confirm_delete_printing_product', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('printing_product');
    $breadcrumbs->push('Xác nhận xóa sản phẩm', route('printing.backend.products.confirm', [$product->id]));
});


$modules = scandir(base_path('modules'));
foreach ($modules as $module) :
    if ($modules != '.' or $modules != '..') {
        if(file_exists(base_path('modules') . '/' . $module . '/breadcrumbs.php' )){
            include_once base_path('modules') . '/' . $module . '/breadcrumbs.php';
        }
    }
endforeach;
