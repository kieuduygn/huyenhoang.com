<?php
Route::get('blog.html', 'BlogController@index');
Route::get('san-pham.html', function(){
    return view('products.index');
});
Route::get('san-pham/chi-tiet.html', function(){
    return view('products.show');
});

Route::get('tin-tuc.html', function(){
    return view('blog/index');
});

Route::get('tin-tuc/chi-tiet.html', function(){
    return view('blog/show');
});

Route::get('trang', function(){
    return view('page-default');
});


Route::get('sitemap.xml', ['as' => 'sitemap.xml', 'uses' => 'ToolsController@sitemap']);

//Backend-----------------------------------------------------------------------------------------------------------
Route::controller('auth/password', 'Auth\PasswordController', [
    'getEmail' => 'auth.password.email',
    'getReset' => 'auth.password.reset'
]);

Route::controller('auth', 'Auth\AuthController', [
    'getLogin' => 'auth.login',
    'getLogout' => 'auth.logout'
]);

//Advertising
Route::resource('backend/ads', 'Backend\AdsController', ['except' => ['show', 'create']]);
Route::post('backend/ads/bulk', ['as' => 'backend.ads.bulk', 'uses' => 'Backend\AdsController@bulk']);
Route::get('backend/ads/{id}/duplicate', ['as' => 'backend.ads.duplicate', 'uses' => 'Backend\AdsController@duplicate']);
Route::get('backend/ads/{id}/confirm', ['as' => 'backend.ads.confirm', 'uses' => 'Backend\AdsController@confirm']);

Route::get('blogcategories/{categories}/confirm', ['as' => 'backend.blogcategories.confirm', 'uses' => 'Backend\BlogCategoriesController@confirm']);
Route::post('backend/blogcategories/bulk', ['as' => 'backend.blogcategories.bulk', 'uses' => 'Backend\BlogCategoriesController@index']);
Route::resource('backend/blogcategories', 'Backend\BlogCategoriesController', ['except' => ['show']]);
Route::get('backend/productcategories/{categories}/confirm', ['as' => 'backend.productcategories.confirm', 'uses' => 'Backend\ProductCategoriesController@confirm']);
Route::resource('backend/productcategories', 'Backend\ProductCategoriesController', ['except' => ['show']]);
Route::resource('backend/messages', 'Backend\MessagesController', ['except' => ['show']]);
Route::get('backend/messages/{id}/mark-read', ['as' => 'backend.messages.markread', 'uses' => 'Backend\MessagesController@markRead']);
Route::get('backend/messages/{id}/delete', ['as' => 'backend.messages.delete', 'uses' => 'Backend\MessagesController@delete']);
//Menu
Route::get('backend/menus/{id}/confirm', ['as' => 'backend.menus.confirm', 'uses' => 'Backend\MenusController@confirm']);
Route::post('backend/menus/additem', ['as' => 'backend.menus.additem', 'uses' => 'Backend\MenusController@addItem']);
Route::post('backend/menus/save', ['as' => 'backend.menus.save', 'uses' => 'Backend\MenusController@saveMenu']);
Route::post('backend/menus/order', ['as' => 'backend.menus.order', 'uses' => 'Backend\MenusController@order']);
Route::get('backend/menus/{id}/remove', ['as' => 'backend.menus.remove', 'uses' => 'Backend\MenusController@remove']);
Route::resource('backend/menus', 'Backend\MenusController', ['except' => ['show']]);

Route::get('backend/themeoption', ['as' => 'backend.themeoption.index', 'uses' => 'Backend\ThemeOptionController@index']);
Route::post('backend/themeoption/save', ['as' => 'backend.themeoption.save', 'uses' => 'Backend\ThemeOptionController@save']);
Route::resource('backend/products', 'Backend\ProductsController');
Route::get('product/detach-image/{image_id}/{product_id}', 'Backend\ProductsController@detachImage');
Route::get('product/detach-option/{option_id}/{product_id}/{option_value_id}', 'Backend\ProductsController@detachOption');
Route::get('product/add-option/{type}/{length}', 'Backend\ProductsController@addOptionAjax');
Route::resource('backend/orders', 'Backend\OrdersController');
Route::get('backend/order/{id}/update_status/{status_id}', ['as' => 'backend.order.update_status', 'uses' => 'Backend\OrdersController@update_status']);
//File manager
Route::resource('backend/filemanager', 'Backend\FileManagerController');
Route::post('backend/get_subdirectories',  'Backend\FileManagerController@get_subdirectories');
Route::get('backend/orders/{id}/confirm', ['as' => 'backend.orders.confirm', 'uses' => 'Backend\OrdersController@confirm']);
Route::get('backend/attributegroups/{id}/confirm', ['as' => 'backend.attributegroups.confirm', 'uses' => 'Backend\AttributeGroupsController@confirm']);
Route::get('backend/attributes/{id}/confirm', ['as' => 'backend.attributes.confirm', 'uses' => 'Backend\AttributesController@confirm']);
Route::resource('backend/attributegroups', 'Backend\AttributeGroupsController');
Route::resource('backend/attributes', 'Backend\AttributesController');

Route::get('admin', ['as' => 'admin', 'uses' => 'Backend\DashboardController@index']);
Route::get('backend', ['as' => 'backend', 'uses' => 'Backend\DashboardController@index']);
Route::get('backend/dashboard', ['as' => 'backend.dashboard', 'uses' => 'Backend\DashboardController@index']);
Route::get('backend/products/{product}/confirm', ['as' => 'backend.products.confirm', 'uses' => 'Backend\ProductsController@confirm']);
Route::get('/home', function(){
   return view('backend');
});
Route::resource('backend/setting', 'Backend\SettingsController');
Route::resource('backend/branch', 'Backend\BranchesController');

//FrontEnd
Route::get('file/get/{filename}', [
    'as' => 'getentry', 'uses' => 'FilesController@get']);

Route::get('image/products/{id}/{slug}.jpg', [
    'as' => 'product_img', 'uses' => 'FilesController@get_product_image']);


Route::get('/', 'HomeController@index');






//FrontEnd

Route::post('product/order/place-order', ['as' => 'order.place', 'uses' => 'ProductsController@placeOrder']);
Route::post('product/review', ['as' => 'product.review', 'uses' => 'ProductsController@review']);
Route::post('product/update-price',  'ProductsController@updateShowPagePrice');
Route::get('tool/slug/genslug/{string}', 'ToolsController@genSlug');
Route::get('getattributehtml/{id}', 'ToolsController@getAttributeHtml');
Route::post('render-new-product-image', 'ToolsController@renderNewProductImage');
Route::get('getoptionvalue/{id}', 'ToolsController@getOptionValue');
Route::get('get-district/{province_id}', 'ToolsController@getDistrict');
Route::get('selected-ship-to-different-address', 'ToolsController@toggleShippingAddress');
Route::get('get-ward/{district_id}', 'ToolsController@getWard');
Route::get('apply-coupon/{code}', 'HomeController@applyCoupon');

Route::get('create-admin-role', 'ToolsController@createrole');

Route::get('session', function(){
    dd(session());
});

Route::resource('user', 'UsersController', ['except' => ['create', 'destroy', 'show', 'index']]);

Route::get('login.html', [
    'as' => 'users.login', 'uses' => 'UsersController@login']);

Route::get('register.html', [
    'as' => 'users.register', 'uses' => 'UsersController@register']);

Route::get('profile.html', [
    'as' => 'user.profile', 'uses' => 'UsersController@profile']);

Route::post('postlogin', [
    'as' => 'user.login.post', 'uses' => 'UsersController@postLogin']);
Route::get('postregister', [
    'as' => 'user.register.post', 'uses' => 'UsersController@postRegister']);


Route::get('tags/product/{slug}.{id}.html', ['as' => 'product.tag', 'uses' => 'ProductsController@tag']);
Route::get('user/github/login', ['as' => 'user.github.login', 'uses' => 'UsersController@githubLogin']);
Route::get('user/facebook/login', ['as' => 'user.facebook.login', 'uses' => 'UsersController@facebookLogin']);
Route::get('user/facebook/login/step/2', ['as' => 'user.facebook.login.step2', 'uses' => 'UsersController@facebookLoginStep2']);
Route::get('user/google/login', ['as' => 'user.google.login', 'uses' => 'UsersController@googleLogin']);
Route::get('user/google/login/step/2', ['as' => 'user.google.login.step2', 'uses' => 'UsersController@googleLoginStep2']);
Route::get('user/twitter/login', ['as' => 'user.twitter.login', 'uses' => 'UsersController@twitterLogin']);
Route::get('user/twitter/login/step/2', ['as' => 'user.twitter.login.step2', 'uses' => 'UsersController@twitterLoginStep2']);

Route::get('cart/information', function(){
   return view('cart.information');
});


//Product
//Product-Frontend
Route::get('product/dat-hang-thanh-cong.html', ['as' => 'product.ordersuccess', 'uses' => 'ProductsController@orderSuccess']);

Route::get('product/checkout', ['as' => 'product.checkout', 'uses' => 'ProductsController@checkout']);
Route::resource('product', 'ProductsController');
Route::get('product.html', ['as' => 'product', 'uses' => 'ProductsController@index']);

Route::post('product/add-to-cart', ['as' => 'cart.add', 'uses' => 'CartController@add']);
Route::get('cart/view-cart', ['as' => 'cart.show', 'uses' => 'CartController@show']);
Route::get('cart/clear', ['as' => 'cart.clear', 'uses' => 'CartController@clear']);
Route::get('cart/delete/{key}', ['as' => 'cart.delete', 'uses' => 'CartController@delete']);
Route::get('cart/delete/ajax/{key}', ['as' => 'cart.ajax.delete', 'uses' => 'CartController@deleteAjax']);
Route::get('cart/update/{key}/{quantity}', ['as' => 'cart.update', 'uses' => 'CartController@update']);
Route::get('product/{id}-{slug}.html', ['as' => 'product.show', 'uses' => 'ProductsController@show']);
//Product--Backend
Route::post('backend/products/bulk', ['as' => 'backend.products.bulk', 'uses' => 'Backend\ProductsController@index']);
Route::get('backend/options/{options}/confirm', ['as' => 'backend.options.confirm', 'uses' => 'Backend\OptionsController@confirm']);
Route::post('backend/options/bulk', ['as' => 'backend.options.bulk', 'uses' => 'Backend\OptionsController@index']);
Route::resource('backend/options', 'Backend\OptionsController');
Route::get('backend/coupons/{id}/confirm', ['as' => 'backend.coupons.confirm', 'uses' => 'Backend\CouponsController@confirm']);
Route::post('backend/coupons/bulk', ['as' => 'backend.coupons.bulk', 'uses' => 'Backend\CouponsController@index']);
Route::resource('backend/coupons', 'Backend\CouponsController', ['except' => ['show']]);
Route::get('backend/branches/{id}/confirm', ['as' => 'backend.branches.confirm', 'uses' => 'Backend\BranchesController@confirm']);
Route::post('backend/branches/bulk', ['as' => 'backend.branches.bulk', 'uses' => 'Backend\BranchesController@index']);
Route::resource('backend/branches', 'Backend\BranchesController', ['except' => ['show']]);
Route::resource('backend/brands', 'Backend\BrandsController', ['except' => ['show']]);
Route::post('backend/brands/bulk', ['as' => 'backend.brands.bulk', 'uses' => 'Backend\BrandsController@bulk']);
Route::get('backend/brands/{id}/confirm', ['as' => 'backend.brands.confirm', 'uses' => 'Backend\BrandsController@confirm']);
Route::get('nhan-hieu/{id}.{slug}.html', ['as' => 'brands.show', 'uses' => 'BrandsController@show']);

//Blog
//Blog--FrontEnd

Route::resource('blog', 'BlogController');
Route::get('category/{slug}.html', ['as' => 'blog.category', 'uses' => 'BlogController@category']);
Route::get('tags/post/{id}-{slug}.html', ['as' => 'blog.tag', 'uses' => 'BlogController@tag']);
Route::get('tin-tuc.html', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
Route::get('bao-viet/{slug}.html', ['as' => 'blog.show', 'uses' => 'BlogController@show']);
Route::post('bao-viet/{id}/comment', ['as' => 'blog.comment', 'uses' => 'BlogController@comment']);
//Blog--Backend
Route::get('backend/posts/{posts}/confirm', ['as' => 'backend.posts.confirm', 'uses' => 'Backend\PostsController@confirm']);
Route::get('backend/posts/{posts}/duplicate', ['as' => 'backend.posts.duplicate', 'uses' => 'Backend\PostsController@duplicate']);
Route::post('backend/posts/bulk', ['as' => 'backend.posts.bulk', 'uses' => 'Backend\PostsController@index']);
Route::resource('backend/posts', 'Backend\PostsController');



//Page
//Page--FrontEnd
Route::get('{slug}.html', ['as' => 'page', 'uses' => 'PagesController@show']);
Route::get('page/{id}/rate/{point}', ['as' => 'page.rate', 'uses' => 'PagesController@rate']);
Route::get('trang/preview/{id}', ['as' => 'backend.pages.preview', 'uses' => 'PagesController@preview']);
//Page--Backend
Route::post('backend/pages/bulk', ['as' => 'backend.pages.bulk', 'uses' => 'Backend\PagesController@index']);
Route::get('backend/pages/{pages}/confirm', ['as' => 'backend.pages.confirm', 'uses' => 'Backend\PagesController@confirm']);
Route::resource('backend/pages', 'Backend\PagesController', ['except' => ['show']]);


//User
//User--Backend
Route::get('backend/users/{users}/confirm', ['as' => 'backend.users.confirm', 'uses' => 'Backend\UsersController@confirm']);
Route::post('backend/users/bulk', ['as' => 'backend.users.bulk', 'uses' => 'Backend\UsersController@index']);
Route::resource('backend/users', 'Backend\UsersController', ['except' => ['show']]);

Route::get('author/{id}', ['as' => 'author', 'uses' => 'AuthorsController@show']);

