<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users,email|email',
            'password'  => 'required|max:32',
            'password_confirmation' => 'required|max:32|same:password',
            'accept_term'   => 'required',
            'address'   => 'required',
            'phone' => 'required',
            'province'  => 'required|numeric|exists:provinces,id',
            'district'  => 'required|numeric|exists:districts,id',
            'ward'  => 'required|numeric|exists:wards,id',
        ];
    }
}
