<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreBranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:80',
            'address'   => 'required',
            'email'     => 'email',
            'phone' => 'numeric',
            'province_id'   => 'required|numeric',
            'district_id'   => 'required|numeric',
            'lat'   => 'required|numeric',
            'lng'   => 'required|numeric',
        ];
    }
}
