<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateUserProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'   => 'required',
            'phone' => 'required',
            'province'  => 'required|numeric|exists:provinces,id',
            'district'  => 'required|numeric|exists:districts,id',
            'ward'  => 'required|numeric|exists:wards,id',
        ];
    }
}
