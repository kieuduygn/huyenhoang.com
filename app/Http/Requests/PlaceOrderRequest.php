<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlaceOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment.name'  => 'required',
            'payment.email' => 'required|email',
            'payment.phone' => 'required|numeric',
            'payment.address'   => 'required',
            'payment.ward'  => 'required|numeric',
            'payment.province'  => 'required|numeric',
            'payment.district'  => 'required|numeric',
            'payment.order_note'    => 'max:255'
        ];
    }
}
