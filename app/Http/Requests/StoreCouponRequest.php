<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreCouponRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>'required',
            'type'  => 'required',
            'code'  => 'required|unique:coupons,code',
            'discount'  => 'required|numeric',
            'total'  => 'required|numeric',
            'status'  => 'required|in:active,cancel,pending',
            'date_start' => 'required|date',
            'date_end'  => 'required|date'
        ];
    }
}
