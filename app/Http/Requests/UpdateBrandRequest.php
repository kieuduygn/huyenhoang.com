<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateBrandRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:80',
            'slug'  => 'required|max:80',
            'logo_image'    => 'required|max:255',
            'seo_title' => 'max:80',
            'description'   => 'max:255'
        ];
    }
}
