<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'phone',
        'address',
        'ward_id',
        'district_id',
        'province_id',
        'note',
        'shipping_name',
        'shipping_email',
        'shipping_phone',
        'shipping_address',
        'shipping_ward_id',
        'shipping_district_id',
        'shipping_province_id',
        'total',
        'order_status_id',
        'payment_method_id',
    ];


    /**
     * User make this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


    /**
     * Order By gateway
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gateway()
    {
        return $this->belongsTo('App\Models\Gateway');
    }


    /**
     * Tình trạng đơn hàng
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Models\OrderStatus', 'order_status_id');
    }


    /**
     * Ship đi bằng đơn vị nào
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shipper()
    {
        return $this->belongsTo('App\Models\Shipper');
    }


    /**
     * Xem chi tiết một đơn hàng
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detail()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }


    public function total()
    {
        return $this->hasMany('App\Models\OrderTotal');
    }


    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot(['id', 'name', 'quantity', 'price', 'total']);
    }



}
