<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $fillable = ['attribute_id', 'product_id', 'value'];


    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }


    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo     *
     */
    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute');
    }


    public function value()
    {
        return $this->belongsTo('App\Models\ProductAttributeValue');
    }

}
