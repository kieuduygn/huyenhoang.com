<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertisingPosition extends Model
{
    protected $fillable = [
        'name',
        'slug'
    ];

    public function ads()
    {
        return $this->hasMany('App\Models\Advertising');
    }
}
