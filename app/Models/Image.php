<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected  $fillable = [
        'file_path',
        'md5',
    ];


    /**
     * Tìm sản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsToMany('App\Product');
    }

}
