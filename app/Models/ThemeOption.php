<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThemeOption extends Model
{
    protected $fillable = ['option', 'value'];

}
