<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuPosition extends Model
{
    protected $fillable = ['name', 'slug', 'position', 'order'];

    public function nodes()
    {
        return $this->hasMany('App\Models\Menu', 'position_id', 'id');
    }
}
