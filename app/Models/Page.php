<?php

namespace App\Models;

use Baum\Node;

class Page extends Node
{
    protected $fillable = ['uri', 'name',
        'content', 'template', 'list_posts',
        'posts_category_id', 'seo_title',
        'meta_description', 'thumbnail'];

    protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value ?: null;
    }


    public function setTemplateAttribute($value)
    {
        $this->attributes['template'] = $value ?: null;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates()
    {
        return $this->hasMany('App\Models\PageRate');
    }


    /**
     * @return float|int
     */
    public function ratePoint()
    {
        $num_rate = $this->rates()->count();
        if($num_rate == 0){
            return 0;
        }

        $total = 0;
        foreach ($this->rates()->get() as $rate) {
            $total += $rate->point;
        }

        return round($total/$num_rate, 2);

    }

}
