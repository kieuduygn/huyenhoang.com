<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{


    protected $fillable = ['name', 'slug'];


    /**
     * Get all posts by tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blogs()
    {
        return $this->belongsToMany('App\Models\Blog');
    }


    /**
     * Các sản phẩm dùng tag này
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_tag');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function albums()
    {
        return $this->belongsToMany('Modules\Gallery\Entities\Album', 'gallery_album_tag', 'tag_id', 'album_id');
    }
}
