<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{

    protected $fillable = ['name'];

    protected $table = 'order_status';

    public function orders()
    {
        $this->hasMany('App\Models\Order');
    }
}
