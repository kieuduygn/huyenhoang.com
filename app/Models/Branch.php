<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name', 'address', 'phone', 'email', 'province_id', 'district_id', 'home', 'lat', 'lng'];

    protected $table = 'branches';


    /**
     * Thuộc tỉnh/thành phố nào
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function home($limit = 6)
    {
        $query = $this->where('home', 1)->orderBy('province_id')->groupBy('province_id')->limit($limit);
        if($query->count() == 0){
            return false;
        }
        return $query->get();
    }
}
