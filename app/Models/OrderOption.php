<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOption extends Model
{
    protected $table = 'order_option';

    protected $fillable = [
            'order_id',
            'order_product_id',
            'option_id',
            'option_value_id',
            'name',
            'value',
            'type',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order_product()
    {
        return $this->belongsTo('App\Models\OrderProduct');
    }
}
