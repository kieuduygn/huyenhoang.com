<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'post_id',
        'user_id',
        'content',
        'point'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
