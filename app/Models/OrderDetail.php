<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'orders_detail';

    protected $fillable = [
        'order_id',
        'product_id',
        'price',
        'discount',
        'total',
        'quantity',
        'left_level',
        'right_level',
        'ship_date' ,
        'bill_date',
    ];


    /**
     * Xem chi record thuộc đơn hàng nào
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }


    public function product()
    {
        return $this->belongsTo('App\Product');
    }


}
