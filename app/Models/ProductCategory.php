<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    protected $guarded = ['name', 'slug', 'description', 'parent_id', 'thumbnail_id'];

    /**
     * Tìm các sản phẩm có category này
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_category');
    }

}
