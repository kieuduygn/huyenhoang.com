<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $fillable = [
        'name',
        'code',
        'type',
        'discount',
        'total',
        'date_start',
        'date_end',
        'used',
        'status'
    ];
}
