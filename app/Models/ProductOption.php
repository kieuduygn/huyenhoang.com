<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{

    protected $table = 'product_option';


    /**
     * Các sản phẩm có option này
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }


    /**
     * Lấy giá trị của option
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function value()
    {
        return $this->belongsTo('App\Models\OptionValue', 'option_value_id', 'id');
    }
}
