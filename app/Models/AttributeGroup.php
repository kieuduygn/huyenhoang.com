<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    protected $fillable = ['name', 'sort_order'];

    protected $table = 'atrribute_groups';


    /**
     * Tìm tất cả các thuộc tính của nhóm này
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany('App\Models\Attribute');
    }
}
