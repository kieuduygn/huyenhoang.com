<?php

namespace App\Models;
use Baum\Node;

class BlogCategory extends Node
{
    protected $fillable = [
        'name',
        'slug',
        'description',
        'hide_from_blog'
    ];

    protected $table = 'blog_categories';

    protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

    /**
     * Quản trị các bài viết có trong category này
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post', 'post_category', 'category_id', 'post_id');
    }
}
