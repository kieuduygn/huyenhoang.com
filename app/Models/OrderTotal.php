<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTotal extends Model
{
    protected $fillable = ['code', 'title', 'value', 'order_id'];

    protected $table = 'order_total';


    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
