<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = ['author_id', 'title', 'slug', 'body', 'excerpt', 'published_at', 'thumbnail', 'seo_title', 'meta_description'];

    protected $dates = ['published_at'];

    /**
     * @param $value
     */
    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ?: null;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\BlogCategory');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\BlogCategory', 'post_category', 'post_id', 'category_id');
    }


    /**
     * Lấy ra danh sách category của bài viết hiện tại
     *
     * @return array
     */
    public function getCategoryListAttribute()
    {
        return $this->categories->lists('id')->toArray();
    }

    /**
     * Lấy ra các file liên quan đến bài viết
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function images()
    {
        return $this->belongsToMany('App\Models\Image', 'post_image');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Image');
    }


    public function get_and_paginate($paginate)
    {
        $query = $this->with('author')->with('category')->orderBy('published_at', 'desc');
        if($query->count() == 0){
            return false;
        }
        return $query->paginate($paginate);
    }


    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    /**
     * Tính điểm đánh giá của bài viết
     *
     * @return float|int
     */
    public function point()
    {
        $num_comment = $this->comments()->count();
        if($num_comment == 0) return 0;

        $total = 0;
        foreach ($this->comments()->get() as $comment) {
            $total += $comment->point;
        }

        return round($total/$num_comment, 2);

    }


    public function related()
    {
        return $this->belongsToMany('App\Models\Post', 'related_posts', 'post_id', 'related_id');
    }

    public function getRePostsAttribute()
    {
        return $this->related->lists('id')->toArray();
    }

}
