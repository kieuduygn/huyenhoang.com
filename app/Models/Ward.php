<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    //Quản lý các quận huyện trong cả nước
    //Database có sẵn do Vũ Thanh Lai share, không cần tạo CRUD


    /**
     * Tìm các thành viên có Phường/Xã này
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }



}
