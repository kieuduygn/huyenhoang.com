<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageRate extends Model
{
    protected $fillable =['page_id', 'point', 'user_id'];

}
