<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $table = 'posts';

    protected $fillable = ['author_id', 'title', 'slug', 'body', 'excerpt', 'published_at', 'file_id', 'category_id'];

    protected $dates = ['published_at'];

    /**
     * @param $value
     */
    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ?: null;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }




    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'post_tag')->withTimestamps();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\BlogCategory', 'post_category')->withTimestamps();
    }


    /**
     * Lấy ra danh sách category của bài viết hiện tại
     *
     * @return array
     */
    public function getCategoryListAttribute()
    {
        return $this->categories->lists('id')->toArray();
    }

}
