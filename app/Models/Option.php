<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = ['type', 'name'];


    /**
     * Belong to OptionValue
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function values()
    {
        return $this->hasMany('App\Models\OptionValue');
    }


    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_option')->withPivot('quantity', 'price_prefix', 'price', 'option_value_id');
    }
}
