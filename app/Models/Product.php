<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'short_description',
        'description',
        'price',
        'thumbnail_id',
        'feature',
        'promote_price',
        'suggest',
        'guide',
        'home',
        'seo_title',
        'meta_description',
        'brand_id'
    ];


    /**
     * File thumbnail của sản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }


    /**
     * Tìm các danh mục sản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\ProductCategory', 'product_category');
    }


    /**
     * Tìm các order chi tiết liên quan đến sản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detail()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }


    /**
     * Tìm tất cả các hình ảnh liên quan đến ản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->belongsToMany('App\Models\Image', 'product_image')->withTimestamps();
    }


    /**
     * Product thumbnail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Image', 'thumbnail_id', 'id');
    }


    /**
     * Product Attribute
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Models\Attribute')->withTimestamps();
    }


    /**
     * Các tag liên quan đến sản phẩm
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'product_tag')->withTimestamps();
    }


    /**
     * List all options of this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function options()
    {
        return $this->belongsToMany('App\Models\Option', 'product_option')->withPivot('quantity', 'price_prefix', 'price', 'option_value_id');
    }


    /**
     * List all product options of this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productOptions()
    {
        return $this->hasMany('App\Models\ProductOption');
    }


    /**
     * List all reviews of this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }


    /**
     * Brand of this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }


}
