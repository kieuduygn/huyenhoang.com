<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{

    /**
     * Tìm những User có Quận/Huyện
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function province()
    {
        $this->belongsTo('App\Models\Province');
    }
}
