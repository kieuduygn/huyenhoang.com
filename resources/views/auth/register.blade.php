@extends('auth')

@section('title', 'Đăng ký')

@section('content')
    <form action="" method="post">
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                <h5 class="content-group">Đăng ký tài khoản
                    <small class="display-block">Nhập thông tin của bạn</small>
                </h5>
            </div>
        {!! csrf_field() !!}
        <!-- Name Start -->
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
        </div>
        <!-- Name END -->

        <!-- Email Start -->
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="{{old('email')}}" class="form-control">
        </div>
        <!-- Email END -->

        <!-- Password Start -->
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="{{old('password')}}" class="form-control">
        </div>
        <!-- Password END -->

        <!-- Nhập lại mật khẩu Start -->
        <div class="form-group">
            <label for="password_confirmation">Nhập lại mật khẩu</label>
            <input type="password" name="password_confirmation" id="password_confirmation" value="{{old('password_confirmation')}}" class="form-control">
        </div>
        <!-- Nhập lại mật khẩu END -->

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Đăng ký</button>
        </div>
    </div>
    </form>
@endsection