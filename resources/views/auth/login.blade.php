@extends('auth')

@section('title', 'Đăng nhập')

@section('content')

{!! Form::open() !!}<!-- Email Start -->

<div class="panel panel-body login-form">
    <div class="text-center">
        <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
        <h5 class="content-group">Đăng nhập hệ thống
            <small class="display-block">Nhập thông tin của bạn</small>
        </h5>
    </div>

    <div class="form-group has-feedback has-feedback-left">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        <div class="form-control-feedback">
            <i class="icon-user text-muted"></i>
        </div>
    </div>

    <div class="form-group has-feedback has-feedback-left">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        <div class="form-control-feedback">
            <i class="icon-lock2 text-muted"></i>
        </div>
    </div>
    <input type="hidden" value="/backend" name="redirectPath">
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">Đăng nhập
            <i class="icon-circle-right2 position-right"></i></button>
    </div>

    <div class="text-center">
        <a href="{{ route('auth.password.reset') }}">Forgot password?</a>
    </div>
</div>

{!! Form::close() !!}


@endsection

