@extends('main')
@section('title', 'Thông tin tài khoản')
@section('content')


        <!--Account Personal Info-->
<section>
    <div class="container shopping-cart">
        <div class="row space-top">

            <!--Items List-->
            <div class="col-md-8 space-bottom">

                <h2>Thông tin cá nhân</h2>
                <ul class="list-unstyled space-bottom">
                    <li><a class="large" href="">Lịch sử đặt hàng</a></li>
                </ul>


                        @include('errors.form')

                {!! Form::model($user, ['method' => 'put' ,'route' => ['user.update', $user->id],]) !!}
                        <!-- Name and Email -->
                <div class="form-group group row">
                    <div class="col-sm-6">
                        {!! Form::label('name', 'Họ và tên') !!}
                        {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::email('email', $user->email, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
                <!-- Name And Email End -->

                <!-- Phone -->
                <div class="form-group">
                    {!! Form::label('phone', 'Điện thoại') !!}
                    {!! Form::text('phone', $user->phone, ['class' => 'form-control', 'placeholder' => 'VD: 0909123456']) !!}
                </div>
                <!-- Phone End -->

                <!-- Password -->
                <div class="form-group group row">
                    <div class="col-sm-6">
                        {!! Form::label('password', 'Mật khẩu') !!}
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::label('password_confirmation', 'Xác thực mật khẩu') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-12 help-block">Để trống sẽ không sửa mật khẩu</div>
                </div>
                <!-- Password End -->

                <!-- Address -->
                <div class="form-group">
                    {!! Form::label('address', 'Địa chỉ') !!}
                    {!! Form::text('address', $user->address, ['class' => 'form-control']) !!}
                </div>
                <!-- Address End -->

                <!-- Ward - district - Province -->
                <div class="form-group row">
                    <div class="col-sm-4">
                        {!! Form::label('province', 'Tỉnh/Thành phố') !!}
                        {!! Form::select('province', $provinces, $user->province_id, ['class' => 'form-control', 'id'  => 'list-provinces']) !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('district', 'Quận/Huyện') !!}
                        {!! Form::select('district', $districts, $user->district_id, ['class' => 'form-control', 'id' => 'list-districts']) !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('ward', 'Phường/Xã') !!}
                        {!! Form::select('ward', $wards, $user->ward_id, ['class' => 'form-control', 'id'  => 'list-wards']) !!}
                    </div>
                </div>
                <!-- Ward - district - Province End-->
                <input type="submit" class="btn btn-success" value="Lưu lại">
                {!! Form::close() !!}
            </div>


            <div class="col-md-1"></div>

            <!--Sidebar-->
            @include('cart.bill')
        </div>
    </div>

    @include('products.you_may_like');
</section><!--Account Personal Info Close-->

@stop