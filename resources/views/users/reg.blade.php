{!! Form::open(['url' => route('user.store'), 'class' => 'registr-form']) !!}
<h2>Đăng ký</h2>
<!-- Name and Email -->
<div class="form-group group row">
    <div class="col-sm-6">
        {!! Form::label('name', 'Họ và tên') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'VD: Nguyễn Văn A']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Nhập email']) !!}
    </div>
</div>
<!-- Name And Email End -->

<!-- Password -->
<div class="form-group group row">
    <div class="col-sm-6">
        {!! Form::label('password', 'Mật khẩu') !!}
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Nhập mật khẩu']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('password_confirmation', 'Xác thực mật khẩu') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Xác thực mật khẩu']) !!}
    </div>
</div>
<!-- Password End -->

<!-- Phone -->
<div class="form-group">
    {!! Form::label('phone', 'Điện thoại') !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'VD: 0909123456']) !!}
</div>
<!-- Phone End -->

<!-- Address -->
<div class="form-group">
    {!! Form::label('address', 'Địa chỉ') !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'VD: 140 Nguyễn Đình Chiểu']) !!}
</div>
<!-- Address End -->

<!-- Ward - district - Province -->
<div class="form-group row">
    <div class="col-sm-4">
        {!! Form::label('province', 'Tỉnh/Thành phố') !!}
        {!! Form::select('province', list_provinces(), null, ['class' => 'form-control', 'id'  => 'select-province']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::label('district', 'Quận/Huyện') !!}
        {!! Form::select('district', list_districts(), null, ['class' => 'form-control', 'id'  => 'select-district']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::label('ward', 'Phường/Xã') !!}
        {!! Form::select('ward', list_wards(), null, ['class' => 'form-control', 'id'  => 'select-ward']) !!}
    </div>
</div>
<!-- Ward - district - Province End-->

<div class="checkbox">
    <label><input type="checkbox" name="accept_term" checked> Tôi đồng ý với điều khoản sử dụng</label>
</div>
<button class="btn btn-success" type="submit">Đăng ký</button>


{!! Form::close() !!}