@extends('main')

@section('title', 'Đăng nhập hoặc đăng ký')

@section('content')
        <!--Login / Register-->
<section class="log-reg container">
    @include('errors.form')

    <div class="row">
        <!--Login-->
        <div class="col-lg-5 col-md-5 col-sm-5">
            <h2>Đăng nhập</h2>
            <p class="large">Đăng nhập bằng tài khoản mạng xã hội</p>
            @include('partials.social-login')

            {!! Form::open(['url' => route('user.login.post')]) !!}

            <div class="form-group group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Nhập email']) !!}
            </div>


            <div class="form-group group">
                {!! Form::label('password', 'Mật khẩu') !!}
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Nhập mật khẩu']) !!}
                <a class="help-link" href="{{ route('auth.password.email') }}">Quên mật khẩu?</a>
            </div>


            <div class="checkbox">
                <label><input type="checkbox" name="remember" value="true"> Ghi nhớ</label>
            </div>
            <input class="btn btn-success" type="submit" value="Đăng nhập">
            {!! Form::close() !!}
        </div>
        <!--Registration-->
        <div class="col-lg-7 col-md-7 col-sm-7">
            @include('users.reg')
        </div>
        <!-- Registration End -->
    </div>
</section><!--Login / Register Close-->
@stop