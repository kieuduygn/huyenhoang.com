<h1>
    <img src="{{ theme('img/logo.png') }}" alt="">
</h1>

<p>Xin chào !</p>
<p>Bạn hoặc ai đó đã tiến hành đặt lại mật khẩu trên <a href="http://premiumlens.vn">http://premiumlens.vn</a>. Nếu không phải bạn, xin hãy bỏ qua
email này.</p>
<p>Nếu bạn thực hiện điều này, xin vui lòng click vào đây để tiến hành bước tiếp theo</p>

{{ URL::to('auth/password/reset', array($token)) }}.