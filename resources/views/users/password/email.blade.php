@extends('main')

@section('title', 'Lấy lại mật khẩu')

@section('content')
        <!--Login / Register-->
<section class="log-reg container">
    @include('errors.form')

    <div class="row">
        <!--Login-->
        <div class="col-lg-5 col-md-5 col-sm-5">
            <h2>Quên mật khẩu</h2>
            <p class="large">Điền địa chỉ email của bạn</p>

            {!! Form::open(['url' => route('auth.password.email')]) !!}

            <div class="form-group group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Nhập email']) !!}
            </div>


            <div class="form-group group">
                {!! app('captcha')->display([], 'vi') !!}
            </div>


            <p>Bạn sẽ nhận được email để tiền hành bước tiếp theo</p>
            <input class="btn btn-success" type="submit" value="Tiếp tục">
            {!! Form::close() !!}
        </div>
        <!--Registration-->
        <div class="col-lg-7 col-md-7 col-sm-7">
            @include('users.reg')
        </div>
        <!-- Registration End -->
    </div>
</section><!--Login / Register Close-->
@stop