
<!DOCTYPE html>
<html lang="en">
<head>

    @include('partials.backend.metadata')

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/invoice_grid.js"></script>
    <!-- /theme JS files -->

</head>

<body class="sidebar-xs">

<!-- Main navbar -->
@include('partials.backend.navbar')
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        @include('partials.backend.sidebar')
        <!-- /main sidebar -->


        <!-- Secondary sidebar -->
        @include('partials.backend.secondary-sidebar')
        <!-- /secondary sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-transparent">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> @yield('title')</h4>

                        <ul class="breadcrumb position-right">
                            <li><a href="{{ route('backend') }}">Home</a></li>
                            <li><a href="">Đơn hàng</a></li>
                        </ul>
                    </div>

                    <div class="heading-elements">
                        <a href="{{ route('backend.orders.create') }}" class="btn bg-blue btn-labeled heading-btn"><b><i class="icon-calculator"></i></b> Tạo đơn hàng</a>
                        <a href="{{ route('backend.setting.index') }}" class="btn btn-default btn-icon heading-btn"><i class="icon-gear"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                @yield('content');

                <!-- Footer -->
                @include('partials.backend.footer')
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
