@if(session()->has('flash_message'))<!-- Panel _start -->
<div class="flash alert alert-{!! session('flash_message.level') !!} alert-styled-left">
    <span class="text-semibold">{!! session('flash_message.title') !!}!</span> {!! session('flash_message.message') !!}
    <div class="heading-elements">
        <ul class="icons-list">
            <li><a class="close" data-action="close"></a></li>
        </ul>
    </div>
</div>
@endif