<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Jan 2016 17:09:18 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{url('assets')}}/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{url('assets')}}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{url('assets')}}/css/core.css" rel="stylesheet" type="text/css">
    <link href="{{url('assets')}}/css/components.css" rel="stylesheet" type="text/css">
    <link href="{{url('assets')}}/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{url('assets')}}/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="{{url('assets')}}/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="{{url('assets')}}/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{url('assets')}}/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('assets')}}/js/core/app.js"></script>
    <!-- /theme JS files -->

</head>

<body>


<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Simple login form -->
                @yield('content')
                <!-- /simple login form -->


                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Jan 2016 17:09:18 GMT -->
</html>
