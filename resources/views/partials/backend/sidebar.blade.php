<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Nội dung</span> <i class="icon-menu" title="Main pages"></i>
                    </li>
                    <li class="{{set_active('backend/posts') . set_active('backend/posts/create') . set_active('backend/posts/edit') . set_active('backend/posts/index')}}">
                        <a href="#"><i class="icon-stack"></i> <span>Nội dung</span></a>
                        <ul>
                            <li><a href="{{ route('backend.posts.index') }}">Tất cả bài viết</a></li>
                            <li><a href="{{ route('backend.posts.create') }}">Thêm bài viết</a></li>
                            <li><a href="{{ route('backend.blogcategories.index') }}">Danh mục</a></li>
                            {{--<li><a href="">Tags</a></li>--}}
                        </ul>
                    </li>
                    <li class="{{set_active('backend/pages') . set_active('backend/pages/create') . set_active('backend/pages/edit') . set_active('backend/pages/index')}}">
                        <a href="#"><i class="icon-copy"></i> <span>Trang</span></a>
                        <ul>
                            <li><a href="{{ route('backend.pages.index') }}">Tất cả trang</a></li>
                            <li><a href="{{ route('backend.pages.create') }}">Thêm trang mới</a></li>
                        </ul>
                    </li>
                    <li class="{{ set_active('backend/users') }}">
                        <a href="{{ route('backend.users.index') }}"><i class="icon-user"></i>
                            <span>Người dùng</span></a>
                    </li>


                    <!-- Giao diện -->
                    <li class="navigation-header"><span>Giao diện</span> <i class="icon-menu" title="Giao diện"></i>
                    </li>

                    <li class="">
                        <a href="{{ route('backend.menus.index') }}"><i class="icon-paragraph-justify3"></i>
                            <span>Trình đơn</span></a>
                    </li>
                    <li class="">
                        <a href="{{ route('backend.ads.index') }}"><i class="icon-bookmark"></i>
                            <span>Quảng cáo</span></a>
                    </li>

                    <!-- End Giao diện -->

                    @if(count(config('modules.menu') > 0))
                        @foreach(config('modules.menu') as $menu)
                            @if($menu['head'])
                                <li class="navigation-header"><span>{{ $menu['head_title'] }}</span> <i
                                            class="icon-menu" title="{{ $menu['head_title'] }}"></i>
                                </li>
                            @endif
                            @foreach($menu['items'] as $item)
                                @if($item['sub'])
                                    <li class="">
                                        <a href="#"><i class="icon-list-ordered"></i>
                                            <span>{{ $item['items'][0]['name'] }}</span></a>
                                        <ul>
                                            @foreach($item['items'] as $key => $sub_item)
                                                <li class="">
                                                    <a href="{{ route($sub_item['route']) }}">
                                                        <span>{{ ($key == 0) ? 'Tất cả' : '' }} {{ $sub_item['name'] }}</span></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>

                                @else
                                    @foreach($item['items'] as $sub_item)
                                        <li class="">
                                            <a href="{{ route($sub_item['route']) }}"><i class="icon-list-ordered"></i>
                                                <span>{{ $sub_item['name'] }}</span></a>
                                        </li>
                                        @endforeach
                                        @endif

                                        @endforeach
                                        @endforeach
                                        @endif


                                                <!-- Forms -->
                                        <li class="navigation-header"><span>Công cụ</span> <i class="icon-menu"
                                                                                              title="Forms"></i></li>
                                        <li class=="{{ set_active('backend/settings') }}">
                                            <a href="{{ route('backend.setting.index') }}"><i class="icon-user"></i>
                                                <span>Cài đặt</span></a>
                                        </li>

                                        <!-- /forms -->


                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>