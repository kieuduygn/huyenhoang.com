<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title') &mdash; Premeumlens</title>
<base href="{{ url() }}">
<!-- Global stylesheets -->
<link href="{{ url('assets') }}/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="{{ url('assets') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="{{ url('assets') }}/css/core.css" rel="stylesheet" type="text/css">
<link href="{{ url('assets') }}/css/components.css" rel="stylesheet" type="text/css">
<link href="{{ url('assets') }}/css/colors.css" rel="stylesheet" type="text/css">
<link href="{{ url('assets') }}/css/custom.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->


<script type="text/javascript" src="{{ url('assets') }}/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/pages/form_checkboxes_radios.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/selects/bootstrap_select.min.js"></script>
<script type="text/javascript" src="{{ url('assets') }}/js/pages/components_popups.js"></script>
<script type="text/javascript" src="{{ url('global') }}/js/address.js"></script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<!-- /core JS files -->


