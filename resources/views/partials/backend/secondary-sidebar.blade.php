<div class="sidebar sidebar-secondary sidebar-default">
    <div class="sidebar-content">

        <!-- Actions -->
        <div class="sidebar-category">
            <div class="category-title">
                <span>Hành động</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content">
                <div class="row row-condensed">
                    <div class="col-xs-6">
                        <button type="button" class="btn bg-teal-400 btn-block btn-float btn-float-lg"><i class="icon-file-plus"></i> <span>Thêm mới</span></button>
                        <button type="button" class="btn bg-purple-300 btn-block btn-float btn-float-lg"><i class="icon-archive"></i> <span>Lưu trữ</span></button>
                    </div>

                    <div class="col-xs-6">
                        <button type="button" class="btn bg-warning-400 btn-block btn-float btn-float-lg"><i class="icon-stats-bars"></i> <span>Trạng thái</span></button>
                        <a  href="{{ route('backend.setting.index') }}" class="btn bg-blue btn-block btn-float btn-float-lg"><i class="icon-cog3"></i> <span>Cài đặt</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /actions -->


        <!-- Navigation -->
        <div class="sidebar-category">
            <div class="category-title">
                <span>Navigation</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-alt navigation-accordion">
                    <li class="navigation-header">Đơn hàng</li>
                    <li><a href="#"><i class="icon-files-empty"></i> Tất cả đơn hàng</a></li>
                    <li><a href="#"><i class="icon-file-plus"></i> Pending <span class="badge badge-info">16</span></a></li>
                    <li><a href="#"><i class="icon-file-check"></i> Hoàn thành <span class="badge badge-success">50</span></a></li>
                    <li><a href="#"><i class="icon-file-check"></i> Quá hạn <span class="badge badge-danger">50</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /navigation -->


        <!-- Filter -->
        <div class="sidebar-category">
            <div class="category-title">
                <span>Lọc</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content">
                <form action="#">
                    <div class="form-group">
                        <label class="text-semibold">Giá trị từ</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                0 - 1.000.000 đ
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                1.000.000 - 2.000.000 đ
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                2.000.000 - 4.000.000 đ
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                4.000.000 - 8.000.000 đ
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                10.000.000 +
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="text-semibold">Phương thức thanh toán:</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                Chuyển khoản
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                Bảo kim
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                Ngân lượng
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                123 Pay
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                Tiền mặt
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="text-semibold">Invoice status:</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                Quá hạn
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                Tạm giữ
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                Pending
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                Paid
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled" checked="checked">
                                Invalid
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="styled">
                                Canceled
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <button type="reset" class="btn btn-default btn-block btn-xs">Đặt lại</button>
                        </div>

                        <div class="col-xs-6">
                            <button type="submit" class="btn btn-info btn-block btn-xs">Lọc</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /filter -->


        <!-- Latest updates -->
        <div class="sidebar-category">
            <div class="category-title">
                <span>Hoạt động mới nhất</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content">
                <ul class="media-list">
                    <li class="media">
                        <div class="media-left"><a href="#" class="btn border-success text-success btn-flat btn-icon btn-sm btn-rounded"><i class="icon-checkmark3"></i></a></div>
                        <div class="media-body">
                            <a href="#">Richard Vango</a> paid invoice #0020
                            <div class="media-annotation">4 minutes ago</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left"><a href="#" class="btn border-slate text-slate btn-flat btn-icon btn-sm btn-rounded"><i class="icon-infinite"></i></a></div>
                        <div class="media-body">
                            Invoice #0029 status has been changed to "On hold"
                            <div class="media-annotation">36 minutes ago</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left"><a href="#" class="btn border-success text-success btn-flat btn-icon btn-sm btn-rounded"><i class="icon-checkmark3"></i></a></div>
                        <div class="media-body">
                            <a href="#">Chris Arney</a> paid invoice #0031 with Paypal
                            <div class="media-annotation">2 hours ago</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left"><a href="#" class="btn border-danger text-danger btn-flat btn-icon btn-sm btn-rounded"><i class="icon-cross2"></i></a></div>
                        <div class="media-body">
                            Invoice #0041 has been canceled
                            <div class="media-annotation">Dec 18, 18:36</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left"><a href="#" class="btn border-primary text-primary btn-flat btn-icon btn-sm btn-rounded"><i class="icon-plus3"></i></a></div>
                        <div class="media-body">
                            New invoice #0029 has been sent to <a href="#">Beatrix Diaz</a>
                            <div class="media-annotation">Dec 12, 05:46</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /latest updates -->

    </div>
</div>