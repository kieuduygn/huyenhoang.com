<div class="footer text-muted">
    &copy; 2015. <a href="http://cloud.cass.vn">Cloudcass CMS & eCommerce System</a> by <a href="http://cass.vn" target="_blank">Duy Kiều</a>
    <p>Sản phẩm đã được đăng ký tại Cục sở hữu trí tuệ. Mọi vi phạm bản quyền đều bị xử lý theo pháp luật.</p>
</div>