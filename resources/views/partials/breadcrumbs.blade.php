@if ($breadcrumbs)
    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        @foreach ($breadcrumbs as $key => $breadcrumb)
            @if (!$breadcrumb->last)
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="{{ $breadcrumb->url }}">
                        <span itemprop="name">{{ $breadcrumb->title }}</span>
                    </a>
                    <meta itemprop="position" content="{{ $key + 1 }}"/>
                </li>
            @else
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active">
                    <span itemprop="item" >
                        <span itemprop="name">
                            {{ $breadcrumb->title }}
                        </span>
                    </span>
                    <meta itemprop="position" content="{{ $key + 1 }}"/>
                </li>
            @endif
        @endforeach
    </ol>
@endif