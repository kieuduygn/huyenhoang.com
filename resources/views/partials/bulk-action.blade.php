<div class="input-group input-group-xs">
    <select name="bulk_action" class="select-bulk-action">
        <option value="-1">-- {{ trans('common.bulk_action') }}</option>
        <option value="delete">Delete</option>
    </select>
                  <span class="input-group-btn">
                    <button class="btn btn-default" value="bulk_action">{{ trans('common.apply') }}</button>
                  </span>
</div><!-- /input-group -->