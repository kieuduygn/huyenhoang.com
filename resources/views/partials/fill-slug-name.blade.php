

    <script>
        $(document).ready(function(){
            $('input[name=name]').on('blur', function(){
                var value = $(this).val();
                $.ajax({
                    url: "{{ url('tool/slug/genslug') }}/" + value,
                    success: function(data){
                        if($('input[name=slug]').val() == ""){
                            $('input[name=slug]').val(data);
                        }
                    }
                });
            });
        });
    </script>

