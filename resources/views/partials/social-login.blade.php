<div class="social-login">
    <a class="facebook" href="{{ route('user.facebook.login') }}"><i class="fa fa-facebook-square"></i></a>
    <a class="google" href="{{ route('user.google.login') }}"><i class="fa fa-google-plus-square"></i></a>
</div>