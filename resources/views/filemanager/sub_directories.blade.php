<ul class="list-unstyled">
    @foreach($folders as $folder)
        <li class="{{ (check_directory_child($folder)) ? 'has-sub' : '' }}">
            <span>{{ str_limit($folder, 15) }}</span>
        </li>
    @endforeach
</ul>