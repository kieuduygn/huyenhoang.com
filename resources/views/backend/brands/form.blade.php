{!! Form::model($brand, [
        'method' => $brand->exists ? 'put' : 'post',
        'route' => $brand->exists ? ['backend.brands.update', $brand->id] : ['backend.brands.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}
        <!-- Name -->
<div class="form-group">
    {!! Form::label('name', trans('brand.name')) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    <div class="help-block">{{ trans('brand.name_description') }}</div>
</div>
<!-- Name End -->

<!-- Slug -->
<div class="form-group">
    {!! Form::label('slug', trans('common.slug')) !!}
    {!! Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) !!}
    <div class="help-block">{{ trans('common.slug_desc') }}</div>
</div>
<!-- Slug End -->

<!-- Description -->
<div class="form-group">
    {!! Form::label('seo_title', trans('common.seo_title')) !!}
    {!! Form::text('seo_title', null, ['class' =>'form-control', 'id' => 'seo_title']) !!}
    <div class="help-block">{{ trans('common.seo_title_description') }}</div>
</div>
<!-- Description End -->


<!-- Description -->
<div class="form-group">
    {!! Form::label('description', trans('common.description')) !!}
    {!! Form::textarea('description', null, ['class' =>'form-control']) !!}
</div>
<!-- Description End -->

<!-- Logo -->
<div class="form-group row">
    <div class="col-sm-6">
        <button type="button" class="btn btn-default" id="change-logo">{{ trans('common.add_logo') }}</button>
    </div>
    <div class="col-sm-6">
        {!! Form::label('logo_image', trans('common.logo')) !!}
        <img width="150" class="img-responsive" id="logo-img-contain" src="{{ config('image.no_image') }}" alt="{{ trans('common.logo') }}">
        {!! Form::hidden('logo_image', null) !!}
    </div>
</div>
<!-- Logo End -->

<!-- Submit Button -->
<div class="form-group">
    <button type="submit" class="btn btn-primary">{{ trans('common.save') }}</button>
</div>
<!-- Submit Button End -->
{!! Form::close() !!}

<script>
    //Select logo by CKFinder when user click on #change-logo button
    $(function(){
        $('#change-logo').click(function () {
            CKFinder.modal({
                height: 600,
                language: 'vi',
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (event) {
                        var file = event.data.files.first();
                        $('#logo-img-contain').attr('src', file.getUrl());
                        $('#logo_image').val(file.getUrl());
                    });
                }
            });
        });
    })
</script>