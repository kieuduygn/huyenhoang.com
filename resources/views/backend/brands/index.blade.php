@extends('main')

@section('title', trans('brand.index_page_title'))

@section('head_column')
    <tr>
        <th><input class="styled top-check" type="checkbox"></th>
        <th>{{ trans('brand.logo') }}</th>
        <th>{{ trans('brand.name') }}</th>
        <th width="250">{{ trans('common.description') }}</th>
        <th></th>
    </tr>
    @stop <!-- End head table column -->


    @section('content')

            <!-- Adding Brand Form Start -->
    <div class="col-md-5 col-lg-4">
        @include('errors.form')
        @include('backend.brands.form')
    </div>
    <!-- Add Brand Form End -->


    <!-- List Brands Start -->
    <div class="col-md-7 col-lg-8">
        {!! Form::open(['route' => 'backend.brands.bulk']) !!}
                <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
        </div>
        <!-- Bulk Action End -->

        <div class="clearfix"></div>

        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body no-padding">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        @yield('head_column')
                        </thead>
                        <tbody class="list-items">

                        <!-- Render list brands -->
                        @foreach($brands as $brand)
                            <tr>
                                <td>
                                    {!! Form::checkbox('id[]', $brand->id, false, [ 'class' => "styled"]) !!}
                                </td>
                                <td>
                                    <img width="10" src="{{ get_image_url($brand->logo_image) }}"
                                         alt="{{ $brand->name }}">
                                </td>
                                <td>{{ $brand->name }}</td>
                                <td>{{ $brand->description }}</td>
                                <td>
                                    <a class="btn btn-xs btn-default  btn-icon"
                                       href="{{ route('backend.brands.edit', $brand->id) }}">
                                        <i class="icon-pen"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger  btn-icon"
                                       href="{{ route('backend.brands.confirm', $brand->id) }}">
                                        <i class="icon-minus2"></i>
                                    </a>
                                    <a target="_blank" class="btn btn-icon btn-xs btn-info"
                                       href="{{ route('brands.show', [$brand->id, $brand->slug]) }}">
                                        <i class="icon-eye2"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            <!-- End Render list brands -->
                        </tbody>

                        <tfoot>
                        @yield('head_column')
                        </tfoot>

                    </table>
                </div>
            </div>

        </div>
        <!-- End panel -->

        <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
        </div>
        <!-- Bulk Action End -->
        {!! Form::close() !!}
    </div>
    <!-- List Brands End -->
    @endsection
            <!-- End content section  -->

@section('themejs')
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    @endsection
            <!-- End theme javascript section -->