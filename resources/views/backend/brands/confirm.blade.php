@extends('main')

@section('title', trans('brand.confirm_delete'). ' ' .$brand->name)

@section('content')

{!! Form::open(['method' => 'delete', 'route' => ['backend.brands.destroy', $brand->id]]) !!}
        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>{{ trans('brand.confirm_delete') }}</h5>
    </div>
    <div class="panel-body">
        <div class="alert alert-danger">
            <strong>{{ trans('common.warning') }} !</strong> {{ trans('brand.confirm_delete_message', ['brand_name' => $brand->name]) }}
        </div>

        {!! Form::submit(trans('brand.yes_delete_this_brand'), ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.brands.index') }}" class="btn btn-success"> <strong>{{ trans('common.no_take_me_back') }}</strong>
        </a>
    </div>
</div><!-- Panel End -->
{!! Form::close() !!}

@endsection
