@extends('main')

@section('title', 'Xóa người dùng '.$item->name)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['backend.options.destroy', $item->id]]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xóa thuộc tính</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <form action="{{ url('backend.options.destroy') }}">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa Lựa chọn <strong>[{{ $item->name }}]</strong>, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa Lựa chọn này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('backend.options.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
        </form>
    </div>
</div><!-- Panel End -->
@endsection
