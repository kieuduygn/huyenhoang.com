@extends('main')

@section('title', 'Quản lý lựa chọn sản phẩm')

@section('functions')
    <a href="{{ route('backend.options.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm lựa chọn</span></a>
    @endsection

    @section('content')
    @if($items->total() > 0)
    {!! Form::open(['route' => 'backend.options.bulk']) !!}
            <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $items->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->
    <div class="panel panel-flat">

        <div class="table-responsive panel-body no-padding">
            <table class="table table-data">
                <thead>
                <tr>
                    <th><input type="checkbox" class="styled top-check"></th>
                    <th>Tên lựa chọn</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td><input type="checkbox" class="styled item-checkbox" name="id[]" value="{{ $item->id }}"></td>
                        <td>
                            <a href="{{ route('backend.options.edit', $item->id) }}">{{ $item->name }}</a>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-icon btn-default"
                               href="{{ route('backend.options.edit', $item->id) }}">
                                <i class="icon-pen"></i>
                            </a>
                            <a class="btn btn-icon btn-xs btn-danger"
                               href="{{ route('backend.options.confirm', $item->id) }}">
                                <i class="icon-minus2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th><input type="checkbox" class="styled top-check"></th>
                    <th>Tên lựa chọn</th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $items->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->
    {!! Form::close() !!}

    @else
    <p class="no-item">Không có lựa chọn nào, <a href="{{ route('backend.options.create') }}">Nhấn vào đây để thêm một lựa chọn</a></p>
    @endif
@endsection
