@extends('main')

@section('title', $item->exists ? 'Sửa lựa chọn '.$item->name : 'Thêm lựa chọn mới')

@section('content')

{!! Form::model($item, [
        'method' => $item->exists ? 'put' : 'post',
        'route' => $item->exists ? ['backend.options.update', $item->id] : ['backend.options.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>{{ $item->exists ? 'Sửa lựa chọn: '.$item->name : 'Thêm lựa chọn mới' }}</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">

        @include('errors.form')

                <!-- Tiêu đề Start -->
        <div class="form-group">
            <label for="name">Tên lựa chọn</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <div class="help-block">VD: Độ cận trái</div>
        </div>
        <!-- Tiêu đề END -->

        @if(! $item->exists)
                <!-- Tiêu đề Start -->
        <div class="form-group">
            <label for="name">Loại</label>
            {!! Form::select('type', config('app.options_type'), null, ['class' => 'form-control options-type']) !!}
            <div class="help-block">
                Chức năng này hiện tại chỉ sử dụng được 1 loại là <strong>select</strong> để phục vụ cho web premiumlens
            </div>
        </div>
        <!-- Tiêu đề END -->
        @endif

        <div class="options-value-container">
            @if($item->exists)
                    <!-- Panel _start -->
            <div class="panel panel-flat">
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Tên giá trị</th>
                            <th style="width: 110px;">
                                <button type="button" class="btn btn-sm btn-success add-me">+</button>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item->values()->get() as $value)
                            <tr>
                                <td>
                                    <input placeholder="Giá trị" value="{{ $value->name }}" name="value[]" type="text"
                                           class="form-control">
                                </td>
                                <td>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Panel End -->


            @endif
        </div>


        {!! Form::submit($item->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}

    </div>
</div><!-- Panel End -->


{!! Form::close() !!}

@endsection


@section('themejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/selects/bootstrap_select.min.js"></script>
@endsection


@section('scripts')
    <script>
        $('.add-me').click(function () {
            var tr = '<tr><td><input type="text" class="form-control" name="value[]"></td><td><button type="button" class="btn btn-xs btn-icon btn-danger delete-row"><i class="icon-minus2"></i></button></td></tr>';
            $('.options-value-container tbody').append('<tr>' + tr + '</tr>');
        })
        $('table').delegate('.delete-row', 'click', function(){
            console.log(this);
            $(this).closest('tr').remove();
        });
    </script>

@stop
