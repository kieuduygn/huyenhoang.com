<!-- Panel _start -->
<tr id="option-{{ $option->id }}">
        <td>{!! Form::select('option_value['.$option->id.']['.($length).'][option_value_id]', $option->values()->lists('name', 'id') ,null, ['class' => 'form-control']) !!}</td>

        <td>{!! Form::text('option_value['.$option->id.']['.($length).'][quantity]'.$option->id, null, ['class' => 'form-control']) !!}</td>
        <td>
                {!! Form::select('option_value['.$option->id.']['.($length).'][price_prefix]', config('app.options_prefix'), null, ['class' => 'form-control']) !!}
                {!! Form::text('option_value['.$option->id.']['.($length).'][price]', null, ['class' => 'form-control']) !!}
        </td>
        <td>
                <button type="button" class="btn btn-sm btn-danger btn-detach-option">
                        <i class="icon-minus2"></i></button>
        </td>
</tr>
