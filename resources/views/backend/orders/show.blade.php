@extends('main')

@section('title', 'Quản lý đơn hàng')


@section('content')

    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Xem hóa đơn</h6>
            <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

        <div class="panel-body no-padding-bottom">
            <div class="row">
                <div class="col-md-6 content-group">
                    <img src="{{ url() }}/assets/images/logo.png" class="content-group mt-10" alt="" style="width: 120px;">
                    <ul class="list-condensed list-unstyled">
                        <li>{{ get_settings('address') }}</li>
                    </ul>
                </div>

                <div class="col-md-6 content-group">
                    <div class="invoice-details">
                        <h5 class="text-uppercase text-semibold">Số phiều: #{{ $order->id }}</h5>
                        <ul class="list-condensed list-unstyled">
                            <li>Ngày ra phiếu: <span
                                        class="text-semibold">{{ get_vn_full_date($order->created_at) }}</span>
                            </li>
                            <li>Ngày quá hạn: <span
                                        class="text-semibold">{{ format_date($order->created_at,'d-m-Y', 3*24*3600) }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-9 content-group">
                    <span class="text-muted">Khách hàng:</span>
                    <ul class="list-condensed list-unstyled">
                        <li><h5>{{ $order->user->name }}</h5></li>
                        <li><span class="text-semibold">{{ $order->user->address }}</span></li>
                        <li>{{ $order->user->district->name }}</li>
                        <li>{{ $order->user->province->name }}</li>
                        <li>{{ $order->user->phone }}</li>
                        <li><a href="#">{{ $order->user->email }}</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-lg-3 content-group">
                    <span class="text-muted">Thông tin thanh toán:</span>
                    <ul class="list-condensed list-unstyled invoice-payment-details">
                        <li><h5>Tổng tiền: <span class="text-right text-semibold">{{ number_format(get_order_total($order)) }}
                                    đ</span></h5></li>
                        <li>Phương thức thanh toán: <span
                                    class="text-semibold">{{ $order->payment_method->name }}</span></li>
                        <li>Trạng thái: <span>{{ $order->status->name }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-lg">
                <thead>
                <tr>
                    <th>Sản phẩm</th>
                    <th class="col-sm-1">Đơn giá</th>
                    <th class="col-sm-1">Số lượng</th>
                    <th class="col-sm-1">Tổng cộng</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->products()->get() as $item)

                    <tr>
                        <td>
                            <h6 class="no-margin">{{ $item->name }}</h6>
                            <span class="text-muted">
                                @foreach(get_order_options($item->pivot->id) as $key => $orderOption)
                                    {{ $orderOption->name }}: {{ $orderOption->value }},

                                @endforeach
                            </span>
                        </td>
                        <td>{{ number_format($item->pivot->price) }}</td>
                        <td>{{ $item->pivot->quantity }}</td>
                        <td><span class="text-semibold">{{ number_format($item->pivot->total) }}</span></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="panel-body">
            <div class="row invoice-payment">
                <div class="col-sm-5 col-sm-offset-7">
                    <div class="content-group">
                        <h6>Tổng cộng</h6>
                        <div class="table-responsive no-border">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>Subtotal:</th>
                                    <td class="text-right">{{ number_format(get_order_subtotal($order)) }} đ</td>
                                </tr>
                                <tr>
                                    <th>Tax: <span class="text-regular">(0%)</span></th>
                                    <td class="text-right">0 đ</td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td class="text-right text-primary"><h5
                                                class="text-semibold"> {{ number_format(get_order_total($order)) }}
                                            đ</h5></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        @if($order->order_status_id == 1)
                            <div class="text-right">
                                <a href="{{ route('backend.order.update_status', [$order->id, 3]) }}" class="btn btn-danger">Đánh
                                    dấu hủy</a>
                                <a href="{{ route('backend.order.update_status', [$order->id, 15]) }}" class="btn btn-success">Đánh dấu
                                    đã thanh toán</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
