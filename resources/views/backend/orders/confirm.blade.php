@extends('main')

@section('title', 'Xóa đơn hàng #'.$order->id)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['backend.orders.destroy', $order->id]]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xóa đơn hàng</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa  đơn hàng <strong>[#{{ $order->id }}]</strong>, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa  đơn hàng này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('backend.orders.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
    </div>
</div><!-- Panel End -->

{!! Form::close() !!}

@endsection

