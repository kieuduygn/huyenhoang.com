@extends('main')

@section('title', 'Quản lý đơn hàng')


@section('content')


        <!-- Invoice grid -->
@if(count($orders) > 0)
    @foreach($orders->chunk(2) as $set)
        <div class="row">
            @foreach($set as $item)
                <div class="col-md-6">
                    <div class="panel invoice-grid">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6 class="text-semibold no-margin-top"><a
                                                href="{{ route('backend.orders.show', $item->id) }}">{{ $item->user->name }}</a>
                                    </h6>
                                    <ul class="list list-unstyled">
                                        <li>Số phiếu #: &nbsp;{{ $item->id }}</li>
                                        <li>Ngày ra hóa đơn: <span
                                                    class="text-semibold">{{ format_date($item->created_at, 'd-m-Y') }}</span>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-sm-6">
                                    <h6 class="text-semibold text-right no-margin-top">{{ number_format(get_order_total($item)) }}
                                        đ</h6>
                                    <ul class="list list-unstyled text-right">
                                        <li>Phương thức thanh toán: <span
                                                    class="text-semibold">{{ $item->payment_method->name }}</span></li>
                                        <li class="dropdown">
                                            Trạng thái: &nbsp;
                                            <a href="#" class="label bg-{{ $item->status->color }} dropdown-toggle"
                                               data-toggle="dropdown">{{ $item->status->name }} <span
                                                        class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @foreach(get_order_statuses() as $status)
                                                    <li>
                                                        <a href="{{ route('backend.order.update_status', [$item->id, $status->id]) }}">{{ $status->name }}</a>
                                                    </li>
                                                @endforeach
                                                <li>
                                                    <a href="{{ route('backend.orders.confirm', $item->id) }}">Delete</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <ul>
                                <li><span class="status-mark border-blue position-left"></span> Nợ: <span
                                            class="text-semibold">{{ format_date($item->created_at, 'd-m-Y') }}</span>
                                </li>
                            </ul>

                            <ul class="pull-right">
                                <li><a href="{{ route('backend.orders.show', $item->id) }}"><i
                                                class="icon-eye8"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
@else
    <p>Không có đơn hàng nào trên hệ thống</p>
    @endif
            <!-- /invoice grid -->


    <!-- Pagination -->
    <div class="text-center content-group-lg pt-20">
        {!! $orders->render() !!}
    </div>
    <!-- /pagination -->
    @endsection
