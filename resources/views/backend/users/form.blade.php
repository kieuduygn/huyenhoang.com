@extends('main')

@section('title', $user->exists ? 'Sửa người dùng '.$user->name : 'Tạo người dùng mới')

@section('breadcrumbs')
    @if($user->exists)
        {!! Breadcrumbs::render('edit_user', $user) !!}
    @else
        {!! Breadcrumbs::render('create_user') !!}
    @endif

@endsection

@section('functions')
    <a href="{{ route('backend.users.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm người dùng mới</span></a>
    @endsection

    @section('content')
    {!! Form::model($user, [
        'method' => $user->exists ? 'put' : 'post',
        'route' => $user->exists ? ['backend.users.update', $user->id] : ['backend.users.store']
    ]) !!}

            <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            @include('errors.form')

            <div class="row">
                <div class="col-md-4">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div><!-- Form-group Item End-->

                </div>
                <div class="col-md-4">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('email') !!}
                        @if($user->exists)
                            {!! Form::text('email', null, ['class' => 'form-control', 'disabled']) !!}
                        @else
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        @endif
                    </div><!-- Form-group Item End-->
                </div>
                <div class="col-md-4">

                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('phone', 'Điện thoại') !!}
                        {!! Form::text('phone',null, ['class' => 'form-control']) !!}
                    </div><!-- Form-group Item End-->
                </div>

                <div class="col-md-6">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('password') !!}
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div><!-- Form-group Item End-->

                </div>
                <div class="col-md-6">

                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Xác nhận mật khẩu') !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div><!-- Form-group Item End-->
                </div>

                <div class="col-md-12">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('address', 'Địa chỉ') !!}
                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    </div><!-- Form-group Item End-->
                </div>

                <div class="col-md-4">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('province_id', 'Tỉnh/Thành Phố') !!}
                        {!! Form::select('province_id', $provinces, null, ['class' => 'form-control select2', 'id' => 'list-provinces']) !!}
                    </div><!-- Form-group Item End-->
                </div>
                <div class="col-md-4">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('district_id', 'Quận/Huyện') !!}
                        @if(!$user->exists)
                            <select class="form-control" name="district_id" id="list-districts"></select>
                        @else
                            {!! Form::select('district_id', $districts, $user->district_id, ['class' => 'form-control', 'id' => 'list-districts']) !!}
                        @endif
                    </div><!-- Form-group Item End-->
                </div>
                <div class="col-md-4">
                    <!-- Form-group Item -->
                    <div class="form-group">
                        {!! Form::label('ward_id', 'Phường/Xã') !!}
                        @if(!$user->exists)
                            <select class="form-control" name="ward_id" id="list-wards"></select>
                        @else
                            {!! Form::select('ward_id', $districts, $user->district_id, ['class' => 'form-control', 'id' => 'list-wards']) !!}
                        @endif
                    </div><!-- Form-group Item End-->
                </div>
            </div>

            <div class="">
                <button name="button" value="save" class="btn btn-primary">Lưu lại</button>
                <button name="button" value="save_and_exists" class="btn btn-info">Lưu & thoát</button>
                <a href="{{ route('backend.users.index') }}" class="btn btn-default">Hủy</a>
            </div>
        </div>
    </div>
    <!-- Panel End -->


    {!! Form::close() !!}
@endsection


@section('scripts')
    <script>
        $(function () {
            var province_id = $('#list-provinces').val();
            fillState(province_id);
            $('body').delegate('#list-provinces', 'change', function () {
                var a = $(this).val();
                fillState(a);
            });
            $('body').delegate('#list-districts', 'change', function () {
                var a = $(this).val();
                fillWard(a);
            });
        });
    </script>

@stop
