@extends('main')

@section('title', 'Xóa người dùng '.$user->name)
@section('breadcrumbs')
{!! Breadcrumbs::render('confirm_delete_user', $user) !!}
@endsection
@section('content')

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xác nhận xóa người dùng</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'delete', 'route' => ['backend.users.destroy', $user->id]]) !!}
        <div class="alert alert-danger">
            <strong>Cảnh báo!</strong> Bạn muốn xóa người dùng <strong>{{ $user->name }}</strong>? Hãy cân nhắc !
        </div>

        {!! Form::submit('Vâng, hãy xóa user này!', ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.users.index') }}" class="btn btn-success"> <strong>Đưa tôi ra khỏi đây!</strong> </a>
        {!! Form::close() !!}
    </div>
</div><!-- Panel End -->


@endsection
