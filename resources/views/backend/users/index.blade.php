@extends('main')

@section('title', 'Quản lý người dùng')

@section('breadcrumbs')
    {!! Breadcrumbs::render('user') !!}
@endsection

@section('functions')
    <a href="{{ route('backend.users.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm người dùng</span></a>
    @endsection

    @section('content')
    {!! Form::open(['route' => 'backend.users.bulk']) !!}
            <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>

        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $users->render(null, 'pagination-xs pull-right') !!}
        </div>

    </div>
    <!-- Bulk Action End -->
    <div class="panel panel-flat">
        <div class="panel-body no-padding">
            <div class="table-responsive">
                <table class="table table-data">
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="styled top-check"></th>
                        <th>Họ và tên</th>
                        <th>Địa chỉ E-mail</th>
                        <th>Điện thoại</th>
                        <th>Địa chỉ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $user)
                        <tr>
                            <td><input type="checkbox" value="{{ $user->id }}" name="id[]" class="styled item-checkbox"></td>
                            <td>
                                <a href="{{ route('backend.users.edit', $user->id) }}">{{ $user->name }}</a>
                            </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->address }}</td>
                            <td>
                                <a class="btn btn-xs btn-icon btn-default" href="{{ route('backend.users.edit', $user->id) }}">
                                    <i class="icon-pen"></i>
                                </a>
                                <a class="btn btn-xs btn-icon btn-danger"
                                   href="{{ route('backend.users.confirm', $user->id) }}">
                                    <i class="icon-minus2"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>

        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $users->render(null, 'pagination-xs pull-right') !!}
        </div>

    </div>
    <!-- Bulk Action End -->
    <!-- Bulk Action End -->
    {!! Form::close() !!}

@endsection
