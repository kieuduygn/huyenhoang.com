@extends('main')

@section('title', 'Quản lý menu')

@section('breadcrumbs')
    {!! Breadcrumbs::render('menu') !!}
@endsection

@section('functions')
    <a href="{{ route('backend.menus.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm trình đơn</span></a>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h5>Thêm trình đơn</h5>
            <hr>
            {!! Form::open(['route' => 'backend.menus.store']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Tên trình đơn') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                <div class="help-block">Chủ yếu để bạn dễ nhớ, tên này không sử dụng trên website</div>
            </div>
            <div class="form-group">
                {!! Form::label('slug', 'Slug') !!}
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                <div class="help-block">Lập trình viên dùng slug này để render trên theme</div>
            </div>
            <div class="form-group">
                {!! Form::label('position', 'Vị trí') !!}
                {!! Form::select('position', config('theme.position'), null, ['class' => 'form-control']) !!}
                <div class="help-block">Vị trí được cài đặt trên giao diện và trong config/theme.php</div>
            </div>
            <button class="btn btn-primary">Thêm trình đơn</button>

            {!! Form::close() !!}
        </div>
        <div class="col-md-7">
            <h5>Danh sách trình đơn</h5>
            <hr>
            @if($items->count() > 0)
                {!! Form::open(['route' => 'backend.menus.order']) !!}
                <div class="panel panel-flat">
                    <div class="table-responsive">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Tên trình đơn</th>
                                <th>Slug</th>
                                <th>Vị trí</th>
                                <th>Thứ tự</th>
                                <th style="width: 150px;" class="text-center"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>
                                        <a href="{{ route('backend.menus.edit', $item->id) }}">{{ $item->name }}</a>
                                    </td>
                                    <td>{{ $item->slug }}</td>
                                    <td>{{ config('theme.position')[$item->position] }}</td>
                                    <td>{!! Form::text('menu['.$item->id.']order', $item->order, ['style'=>'width:50px', 'class' => 'text-center']) !!}</td>
                                    <td>
                                        <a class="btn btn-xs btn-default"
                                           href="{{ route('backend.menus.edit', $item->id) }}">
                                            <i class="icon-pen"></i>
                                        </a>
                                        <a class="btn btn-xs btn-icon btn-danger"
                                           href="{{ route('backend.menus.confirm', $item->id) }}">
                                            <i class="icon-minus2"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {!! $items->render() !!}

                    </div>
                </div>
                <button class="btn btn-primary">Cập nhật</button>
                {!! Form::close() !!}
            @else
                <p>Không có trình đơn nào</p>
            @endif
        </div>
    </div>



@endsection
