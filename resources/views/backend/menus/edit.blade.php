@extends('main')

@section('title', $item->exists ? 'Sửa thuộc tính '.$item->name : 'Thêm thuộc tính mới')
@section('breadcrumbs')
    {!! Breadcrumbs::render('edit_menu', $item) !!}
@endsection
@section('content')

    {!! Form::model($item, [
            'method' => 'put',
            'route' => ['backend.menus.update', $item->id],
            'enctype'   => 'multipart/form-data'
        ]) !!}

    <div class="row">
        {!! Form::open(['route' => ['backend.menus.additem', $item->id]]) !!}
        <div class="col-lg-3 col-md-4">
            <!-- Panel _start -->
            <div class="panel panel-flat menus-selection">
                <div class="panel-body">
                    <ul class="list-unstyled wrap-type-list">
                        <li class="type-list">
                            <a class="title" href="#">Trang</a>
                            <div class="list-items hidden">

                                @foreach($page_items as $page)
                                    <div class="checkbox">
                                        <label>
                                            <input name="type[Page][]" type="checkbox" value="{{ $page->id }}" class="styled">
                                            {{ $page->name }}
                                        </label>
                                    </div>
                                @endforeach


                            </div>
                        </li>
                        <li class="type-list">
                            <a class="title" href="#">Danh mục bài viết</a>
                            <div class="list-items hidden">

                                @foreach($blogcategory_items as $blogCategory)
                                    <div class="checkbox">
                                        <label>
                                            <input name="type[BlogCategory][]" type="checkbox" value="{{ $blogCategory->id }}" class="styled">
                                            {{ $blogCategory->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </li>
                        <li class="type-list">
                            <a class="title" href="#">Bài viết</a>
                            <div class="list-items hidden">

                                @foreach($post_items as $post)
                                    <div class="checkbox">
                                        <label>
                                            <input name="type[Post][]" type="checkbox" value="{{ $post->id }}" class="styled">
                                            {{ $post->title }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </li>

                        {{--<li class="type-list">--}}
                            {{--<a class="title" href="#">Danh mục sản phẩm</a>--}}
                            {{--<div class="list-items hidden">--}}

                                {{--@foreach($productcategory_items as $productCategory)--}}
                                    {{--<div class="checkbox">--}}
                                        {{--<label>--}}
                                            {{--<input name="type[ProductCategory][]" type="checkbox" value="{{ $productCategory->id }}" class="styled">--}}
                                            {{--{{ $productCategory->name }}--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        <li class="type-list">
                            <a class="title" href="#">Sản phẩm</a>
                            <div class="list-items hidden">

                                @foreach($product_items as $product)
                                    <div class="checkbox">
                                        <label>
                                            <input name="type[Product][]" type="checkbox" value="{{ $product->id }}" class="styled">
                                            {{ $product->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </li>
                        <li class="type-list">
                            <a class="title" href="#">Liên kết</a>
                            <div class="list-items hidden">
                                <div class="form-group">
                                    <label for="">Nhãn</label>
                                    <input type="text" name="link[label]" class="input-sm form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Link</label>
                                    <input name="link[value]" placeholder='VD: "http://cass.vn" hoặc "san-pham" ' type="text" class="input-sm form-control">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div style="padding: 5px;">
                    <button type="submit" class="btn btn-xs btn-primary">Thêm</button>
                </div>
            </div><!-- Panel End -->
        </div>
        {!! Form::close() !!}


        <div class="col-md-8 col-lg-9">
            <!-- Panel _start -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5>Cấu trúc menu</h5>
                    <p>Kéo và thả để sắp xếp menu theo ý bạn</p>
                    @include('partials.backend.heading-elements')
                </div>
                <div class="panel-body">
                    <ol class="sortable">
                        <?php foreach ($menuItems as $node): ?>
                            <?php echo renderNode($node); ?>
                         <?php endforeach; ?>
                    </ol>
                </div>
                <div class="panel-footer" style="padding-top: 10px;padding-bottom: 10px;">
                    <button id="toArray" class="btn btn-lg btn-primary">Lưu lại</button>
                    <a href="{{ route('backend.menus.index') }}" class="btn btn-lg btn-default">Trở lại</a>
                </div>
            </div><!-- Panel End -->
        </div>
    </div>




    {!! Form::close() !!}


    <style>
        ol {
            list-style: none;
        }

        .menu-item, .menu-item-setting {
            margin-bottom: 10px;
        }
    </style>

@endsection

@section('themejs')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ theme('js/jquery.mjs.nestedSortable.js') }}"></script>
@endsection


@section('scripts')
    <script>
        $().ready(function () {
            var ns = $('ol.sortable').nestedSortable({
                forcePlaceholderSize: true,
                handle: '.menu-wrap',
                helper: 'clone',
                items: 'li',
                opacity: .6,
                placeholder: 'placeholder',
                revert: 250,
                tabSize: 25,
                tolerance: 'pointer',
                toleranceElement: '> .menu-wrap',
                maxLevels: 4,
                isTree: true,
                expandOnHover: 700,
                startCollapsed: false,
                change: function () {
                }
            });


            $('.expandEditor').attr('title', 'Click to show/hide item editor');
            $('.disclose').attr('title', 'Click to show/hide children');
            $('.deleteMenu').attr('title', 'Click to delete item.');

            $('.disclose').on('click', function () {
                $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
                $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
            });

            $('.expandEditor, .itemTitle').click(function () {
                var id = $(this).attr('data-id');
                $('#menuEdit' + id).toggle();
                $(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
            });

            $('.deleteMenu').click(function () {
                var id = $(this).attr('data-id');
                $('#menuItem_' + id).remove();
            });

            $('#serialize').click(function () {
                serialized = $('ol.sortable').nestedSortable('serialize');
                $('#serializeOutput').text(serialized + '\n\n');
            })

            $('#toHierarchy').click(function (e) {
                hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
                hiered = dump(hiered);
                (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
                        $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
            })

            $('#toArray').click(function (e) {
                arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
                console.log(arraied);

                $.post("{{ route('backend.menus.save') }}",
                        {
                            data: arraied,
                            '_token': $('meta[name=csrf-token]').attr('content'),
                        },
                        function(data){
                            window.location.replace("{{ route('backend.menus.edit', $item->id) }}");
                        }
                );
            });
        });

        $('.menu-item').click(function (e) {
            e.preventDefault();
            $(this).next('.menu-item-setting').toggleClass('hidden');
            $(this).toggleClass('active');
        })

        $('.menu-cancel-setting').click(function (e) {
            e.preventDefault();
            $(this).closest('.menu-item-setting').addClass('hidden');
            $(this).closest('.menu-item').removeClass('active');
        })

        function dump(arr, level) {
            var dumped_text = "";
            if (!level) level = 0;

            //The padding given at the beginning of the line.
            var level_padding = "";
            for (var j = 0; j < level + 1; j++) level_padding += "    ";

            if (typeof(arr) == 'object') { //Array/Hashes/Objects
                for (var item in arr) {
                    var value = arr[item];

                    if (typeof(value) == 'object') { //If it is an array,
                        dumped_text += level_padding + "'" + item + "' ...\n";
                        dumped_text += dump(value, level + 1);
                    } else {
                        dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                    }
                }
            } else { //Strings/Chars/Numbers etc.
                dumped_text = "===>" + arr + "<===(" + typeof(arr) + ")";
            }
            return dumped_text;
        }

        $('.type-list .title').click(function(e){
            e.preventDefault();
            $(this).toggleClass('active');
            $(this).next('.list-items').toggleClass('hidden');
        });
    </script>

@endsection