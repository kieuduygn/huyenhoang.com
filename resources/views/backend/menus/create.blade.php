@extends('main')

@section('title', 'Thêm trình đơn')
@section('breadcrumbs')
{!! Breadcrumbs::render('create_menu') !!}
@endsection
@section('content')

{!! Form::model($item, [
        'method' => $item->exists ? 'put' : 'post',
        'route' => $item->exists ? ['backend.menus.update', $item->id] : ['backend.menus.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Thêm trình đơn</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">

        @include('errors.form')

        <!-- Tiêu đề Start -->
        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Tên trình đơn</label>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6">
                <label for="slug">Slug</label>
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6">
                <label for="slug">Slug</label>
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Tiêu đề END -->

        {!! Form::submit($item->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}

    </div>
</div><!-- Panel End -->


{!! Form::close() !!}

@endsection

@section('scripts')
    @include('partials.fill-slug-name')
@endsection
