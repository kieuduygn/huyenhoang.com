@extends('main')

@section('title', 'Xóa menu '.$item->name)
@section('breadcrumbs')
{!! Breadcrumbs::render('confirm_delete_menu', $item) !!}
@endsection
@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['backend.menus.destroy', $item->id]]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xóa thuộc tính</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa  menu <strong>[{{ $item->name }}]</strong>, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa  thuộc tính này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('backend.menus.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
    </div>
</div><!-- Panel End -->

{!! Form::close() !!}
@endsection
