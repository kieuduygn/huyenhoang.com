@extends('main')

@section('title', 'Cài đặt')


@section('content')


        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Lựa chọn</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home-slider">Home Slider</a></li>
            <li><a data-toggle="tab" href="#review">Khách hàng nhận xét</a></li>
            <li><a data-toggle="tab" href="#why_choose_us">Tại sao chọn Premiumlens</a></li>
            <li><a data-toggle="tab" href="#order_guide">Hướng dẫn đặt hàng</a></li>
        </ul>

        {!! Form::open(['route' => 'backend.themeoption.save']) !!}
        <div class="tab-content">


            <div id="home-slider" class="tab-pane fade in active">
                <p>Mỗi một ô dữ liệu dưới đây đại diện cho 1 slide tại trang chủ</p>
                <div class="slide-wrap">
                    @if(theme_option('slide'))
                        @foreach(theme_option('slide') as $slide)
                            <div class="form-group row" class="slide">
                                <div class="col-md-10">
                                    <textarea class="html-editor" name="options[slide][]" class="form-control">{{ html_entity_decode($slide->value) }}</textarea>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-danger delete-slide"><i class="icon-minus2"></i> Xóa</button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group row" class="slide">
                            <div class="form-group row" class="slide">
                                <div class="col-md-10">
                                    <textarea class="html-editor" name="options[slide][]" class="form-control"></textarea>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-danger delete-slide"><i class="icon-minus2"></i> Xóa</button>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <button id="add-slide" type="button" class="btn btn-default"><i class="icon-plus2"></i> Thêm slide mới</button>
                </div>
            </div>



            <div id="review" class="tab-pane fade">
                <div class="review-wrap">
                    @if(theme_option('customer_review'))
                        @foreach(theme_option('customer_review') as $slide)
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <textarea class="html-editor" name="options[customer_review][]" class="form-control">{{ html_entity_decode($slide->value) }}</textarea>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-danger delete-review"><i class="icon-minus2"></i> Xóa</button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group row">
                            <div class="col-md-10">
                                <textarea class="html-editor" name="options[customer_review][]" class="form-control"></textarea>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger delete-review"><i class="icon-minus2"></i> Xóa</button>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <button id="add-review" type="button" class="btn btn-default"><i class="icon-plus2"></i> Thêm review mới</button>
                </div>
            </div>


            <div id="why_choose_us" class="tab-pane fade">
                <div class="choose-wrap">
                    @if(theme_option('why_choose_us'))
                        @foreach(theme_option('why_choose_us') as $slide)
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <textarea class="html-editor" name="options[why_choose_us][]" class="form-control">{{ html_entity_decode($slide->value) }}</textarea>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-danger delete-choose"><i class="icon-minus2"></i> Xóa</button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group row">
                            <div class="col-md-10">
                                <textarea class="html-editor" name="options[why_choose_us][]" class="form-control"></textarea>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger delete-choose"><i class="icon-minus2"></i> Xóa</button>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <button id="add-choose" type="button" class="btn btn-default"><i class="icon-plus2"></i> Thêm mới</button>
                </div>
            </div>


            <div id="order_guide" class="tab-pane fade">
                <div class="guide-wrap">
                    @if(theme_option('order_guide'))
                        @foreach(theme_option('order_guide') as $slide)
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <textarea class="html-editor" name="options[order_guide][]" class="form-control">{{ html_entity_decode($slide->value) }}</textarea>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-danger delete-guide"><i class="icon-minus2"></i> Xóa</button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group row">
                            <div class="col-md-10">
                                <textarea class="html-editor" name="options[order_guide][]" class="form-control"></textarea>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger delete-guide"><i class="icon-minus2"></i> Xóa</button>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <button id="add-guide" type="button" class="btn btn-default"><i class="icon-plus2"></i> Thêm mới</button>
                </div>
            </div>
        </div>
        <hr>
        <button type="submit" class="btn btn-primary">Lưu lại</button>
        {!! Form::close() !!}

    </div>
</div><!-- Panel End -->

@stop

@section('themejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/editors/ace/ace/ace.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/editors/ace/ace/theme-twilight.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/editors/ace/ace/mode-html.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/editors/ace/jquery-ace.min.js"></script>
@endsection

@section('scripts')
    <script>

        $('.html-editor').ace({theme: 'twilight', lang: 'html'})

        function get_append_string(option){
            return '' +
                    '<div class="form-group row" class="slide">'+
                    '<div class="col-md-10">'+
                    '<textarea class="html-editor" name="options['+option+'][]" class="form-control"></textarea>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                    '<button type="button" class="btn btn-default delete-'+option+'"><i class="icon-minus2"></i> Xóa</button>'+
                    '</div>'+
                    '</div>';
        }

        $('#add-slide').click(function () {
            $('.slide-wrap').append(get_append_string('slide'));
            $('.html-editor').ace({theme: 'twilight', lang: 'html'})
        });
        $('#add-review').click(function () {
            $('.review-wrap').append(get_append_string('customer_review'));
            $('.html-editor').ace({theme: 'twilight', lang: 'html'})
        });
        $('#add-choose').click(function () {
            $('.choose-wrap').append(get_append_string('why_choose_us'));
            $('.html-editor').ace({theme: 'twilight', lang: 'html'})
        });
        $('#add-guide').click(function () {
            $('.guide-wrap').append(get_append_string('order_guide'));
            $('.html-editor').ace({theme: 'twilight', lang: 'html'})
        });


        $('body').on('click', '.delete-slide,.delete-review, .delete-choose, .delete-guide', function(){
                $(this).closest('.form-group').remove();
        })



    </script>

@stop