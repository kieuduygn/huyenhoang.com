@extends('main')

@section('title', 'Quản lý lựa chọn sản phẩm')

@section('functions')
    <a href="{{ route('backend.options.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm lựa chọn</span></a>
@endsection

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Tin nhắn</h6>
            <div class="heading-elements">
                <span class="heading-text"><i
                            class="icon-history text-warning position-left"></i> {{ date('d-m-Y H:i:s') }}</span>
                <span class="label bg-success heading-text">Online</span>
            </div>
            <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

        <!-- Numbers -->
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-md-4">
                    <div class="content-group">
                        <h6 class="text-semibold no-margin"><i
                                    class="icon-clipboard3 position-left text-slate"></i> {{ number_format($thisWeek) }}
                        </h6>
                        <span class="text-muted text-size-small">tuần này</span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="content-group">
                        <h6 class="text-semibold no-margin"><i
                                    class="icon-calendar3 position-left text-slate"></i> {{ number_format($thisMonth) }}
                        </h6>
                        <span class="text-muted text-size-small">tháng này</span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="content-group">
                        <h6 class="text-semibold no-margin"><i
                                    class="icon-comments position-left text-slate"></i> {{ number_format(get_total_message()) }}
                        </h6>
                        <span class="text-muted text-size-small">tất cả</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /numbers -->


        <!-- Area chart -->
        <div id="messages-stats"></div>
        <!-- /area chart -->


        <!-- Tabs -->
        <ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">
            @foreach($data as $i => $day)
                <li class="{{ ($i = 0) ? 'active' : '' }}">
                    <a href="#{{  $day['html_id'] }}" class="text-size-small text-uppercase" data-toggle="tab"
                       aria-expanded="true">
                        {{ $day['weekday'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <!-- /tabs -->


        <!-- Tabs content -->
        <div class="tab-content">
            @foreach($data as $i => $day)
                <div class="tab-pane fade has-padding {{ ($i != 0) ?: 'active' }} in" id="{{ $day['html_id'] }}">
                    <ul class="media-list">

                        @if($day['msg']->count() > 0)
                            @foreach($day['msg'] as $item)

                                <li class="media">
                                    <div class="media-left">
                                        <a data-toggle="modal" data-target="#message-model-{{ $item->id }}" href="#"
                                           data-id="{{ $item->id }}"
                                           class="mark-message-read btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                            <span class="letter-icon">{{ strtoupper(substr($item->name, 0, 1)) }}</span>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#" data-toggle="modal" data-id="{{ $item->id }}"
                                           class="mark-message-read" data-target="#message-model-{{ $item->id }}">
                                            @if($item->read == 0)
                                                <span id="message-{{ $item->id }}"
                                                      class="text-success text-bold">{{ $item->name }}</span>
                                            @else
                                                <span style="color: #333" id="message-{{ $item->id }}">{{ $item->name }}</span>
                                            @endif
                                            <span class="media-annotation pull-right">{{ get_vn_full_date($item->created_at) }}</span>
                                        </a>
                                        <span class="display-block text-muted">{{ str_limit($item->message, 100) }}</span>
                                    </div>
                                    <div id="message-model-{{ $item->id }}" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-menu7"></i> Tin nhắn</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <p><strong>Người gửi: </strong> {{ $item->name }}</p>
                                                    <p><strong>Thời gian
                                                            gửi: </strong> {{ get_vn_full_date($item->created_at) }}</p>
                                                    <p><strong>Nội dung:</strong></p>
                                                    <p>{{ $item->message }}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{ route('backend.messages.delete', $item->id) }}" class="btn btn-danger" > Xóa
                                                    </a>
                                                    <button class="btn btn-link" data-dismiss="modal"><i
                                                                class="icon-cross"></i> Đóng
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <p>Không có tin nhắn nào vào ngày này</p>
                        @endif
                    </ul>
                </div>
            @endforeach

        </div>
        <!-- /tabs content -->

    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            $('.mark-message-read').click(function () {
                var message_id = $(this).data('id');
                $.ajax({
                    url: "{{url('backend/messages')}}/" + message_id + '/mark-read',
                    success: function () {
                        $('#message-'+message_id).removeClass('text-success').removeClass('text-bold').css('color', '#333');
                    }
                });
            });
        });
    </script>
@endsection
