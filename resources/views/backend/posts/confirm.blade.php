@extends('main')

@section('title', 'Xóa bài viết '.$post->name)

@section('content'){!! Form::open(['method' => 'delete', 'route' => ['backend.posts.destroy', $post->id]]) !!}<!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xác nhận xóa bài</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <div class="alert alert-danger">
            <strong>Cảnh báo !</strong> Bạn đang muốn xóa bài viết
            <strong>{{ $post->name }}</strong>, việc này không thể phục hồi
        </div>

        {!! Form::submit('Vâng hãy xóa bài này !', ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.posts.index') }}" class="btn btn-success"> <strong>Không, đưa tôi trở lại!</strong>
        </a>
    </div>
</div><!-- Panel End -->{!! Form::close() !!}@endsection
