@extends('main')

@section('title', 'Quản lý nội dung')

@section('breadcrumbs')
    {!! Breadcrumbs::render('post') !!}
    @endsection

@section('functions')
    <a href="{{ route('backend.posts.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm bài viết</span></a>
    @endsection


    @section('content')

@if($posts->total() > 0)
    {!! Form::open(['route' => 'backend.posts.bulk']) !!}
            <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>

        <div class="col-sm-4 col-md-3 col-lg-2">
            @if(category_hierarchy())
                <div class="input-group input-group-xs">
                    <select name="category" class="bootstrap-select form-control">
                        <option value="-1">-- Chọn danh mục</option>
                        @foreach(category_hierarchy() as $category)
                            {!! render_category($category, $_category) !!}
                        @endforeach
                    </select>
                <span class="input-group-btn">
                    <button class="btn btn-default">Lọc</button>
                  </span>
                </div><!-- /input-group -->
            @endif


        </div>

        <div class="col-sm-4 col-md-3 col-lg-2">
            {!! $posts->render(null, 'pagination-xs pull-right') !!}
        </div>

    </div>
    <!-- Bulk Action End -->
    <div class="panel panel-flat">

        <div class="panel-body no-padding">
            <div class="table-responsive">
                    <table class="table table-data">
                        <thead>
                        <tr>
                            <th><input type="checkbox" class="top-check styled"></th>
                            <th>Ảnh đại diện</th>
                            <th>Tên bài viết</th>
                            <th>Slug</th>
                            <th>Tác giả</th>
                            <th>Ngày xuất bản</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr class="{{$post->published_highlight}}">
                                <td><input type="checkbox" name="id[]" value="{{ $post->id }}"
                                           class="item-checkbox styled"></td>
                                <td>
                                    <a href="{{ route('backend.posts.edit', $post->id) }}">
                                        <img src="{{ $post->thumbnail }}" alt="" width="80">
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('backend.posts.edit', $post->id) }}">{{ $post->title }}</a>
                                </td>

                                <td>{{ $post->slug }}</td>
                                <td>{{ $post->author->name }}</td>
                                <td>{{ $post->published_at }}</td>
                                <td>
                                    <a class="btn btn-xs btn-default  btn-icon"
                                       href="{{ route('backend.posts.edit', $post->id) }}">
                                        <i class="icon-pen"></i>
                                    </a>
                                    <a class="btn btn-xs btn-success  btn-icon"
                                       href="{{ route('backend.posts.duplicate', $post->id) }}">
                                        <i class="icon-database-add"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger  btn-icon"
                                       href="{{ route('backend.posts.confirm', $post->id) }}">
                                        <i class="icon-minus2"></i>
                                    </a>
                                    <a target="_blank" class="btn btn-icon btn-xs btn-info"
                                       href="{{ route('blog.show', [$post->slug]) }}">
                                        <i class="icon-eye2"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th><input type="checkbox" class="top-check styled"></th>
                            <th>Ảnh đại diện</th>
                            <th>Tên bài viết</th>
                            <th>Slug</th>
                            <th>Tác giả</th>
                            <th>Ngày xuất bản</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
            </div>
        </div>

    </div>

    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-md-3 col-lg-2">
            {!! $posts->render(null, 'pagination-xs pull-right') !!}
        </div>

    </div>
    <!-- Bulk Action End -->
    {!! Form::close() !!}

    @else
        <p class="no-item">Không tồn tại bài viết nào trên hệ thống <a href="{{ route('backend.posts.create') }}">Nhấn vào đây để thêm bài viết</a></p>
    @endif
@endsection

