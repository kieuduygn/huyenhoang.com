@extends('main')

@section('title', $post->exists ? 'Sửa bài '.$post->name : 'Thêm bài mới')

@section('breadcrumbs')
    @if($post->exists)
        {!! Breadcrumbs::render('edit_post', $post) !!}
    @else
        {!! Breadcrumbs::render('create_post') !!}
    @endif

@endsection

@section('themejs')
    <script src="{{ url('assets') }}/js/plugins/editors/ckeditor/ckeditor.js"></script>
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/tags/tokenfield.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/tags/tagsinput.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/pickers/anytime.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
@endsection

@section('functions')
    <a href="{{ route('backend.posts.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm bài viết</span></a>
@endsection
@section('content'){!! Form::model($post, [
        'method' => $post->exists ? 'put' : 'post',
        'route' => $post->exists ? ['backend.posts.update', $post->id] : ['backend.posts.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}


<div class="row blogs-form">
    <div class="col-md-9">

        <div class="panel panel-flat">
            <div class="panel-body">

                @include('errors.form')

                <ul class="nav nav-tabs" id="theTab">
                    <li class="active"><a data-target="#general" data-toggle="tab">Thông tin chung</a></li>
                    <li><a data-target="#seo" data-toggle="tab">SEO</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="general">

                        <!-- Tiêu đề Start -->
                        <div class="form-group">
                            {!! Form::label('title') !!}
                            {!! Form::text('title', null, ['class' => 'form-control post-title']) !!}
                        </div>
                        <!-- Tiêu đề END -->

                        <!-- Slug Start -->
                        <div class="form-group">
                            {!! Form::label('slug') !!}
                            {!! Form::text('slug', null, ['class' => 'form-control post-slug']) !!}
                        </div>
                        <!-- Slug END -->

                        <!-- Excerpt Start -->
                        <div class="form-group">
                            {!! Form::label('excerpt') !!}
                            {!! Form::textarea('excerpt', null, ['class' => 'form-control post-excerpt']) !!}
                        </div>
                        <!-- Excerpt excerpt -->


                        <!-- Body Start -->
                        <div class="form-group">
                            {!! Form::label('body') !!}
                            {!! Form::textarea('body', null, ['class' => 'form-control', 'rows' => "5"]) !!}
                        </div>
                        <!-- Body End -->
                    </div>
                    <div class="tab-pane" id="seo">

                        <div class="form-group" style="max-width: 600px">
                            @if(! $post->exists)
                                <div class="sample-title">This is sample title</div>
                                <div class="sample-url">{{ url('this-is-sample-title') }}</div>
                                <div class="sample-meta-description">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto corporis
                                    deserunt
                                    iusto laboriosam molestiae nostrum quaerat rerum sed, voluptatum? A dignissimos
                                    dolorem
                                    error iusto molestias quas quisquam rem rerum. Error!
                                </div>
                            @else
                                <div class="sample-title">{{ str_limit(($post->seo_title != '') ? $post->seo_title : $post->title, 80) }}</div>
                                <div class="sample-url">{{ route('blog.show', [$post->id, $post->slug]) }}</div>
                                <div class="sample-meta-description">
                                    {{ ($post->meta_description != '') ? $post->meta_description : $post->excerpt }}
                                </div>
                            @endif
                        </div>
                        <!-- Tiêu đề Start -->
                        <div class="form-group">
                            {!! Form::label('seo_title') !!}
                            {!! Form::text('seo_title', null, ['class' => 'form-control seo-title']) !!}
                        </div>
                        <!-- Tiêu đề END -->
                        <!-- Excerpt Start -->
                        <div class="form-group">
                            {!! Form::label('meta_description') !!}
                            {!! Form::textarea('meta_description', null, ['class' => 'form-control meta-description']) !!}
                        </div>
                        <!-- Excerpt excerpt -->
                    </div>
                </div>


            </div>
        </div>


    </div>
    <div class="col-md-3">
        <div class="panel panel-flat">

            <div class="panel-body">
                <h5>Xuất bản</h5>
                <!-- Published_at Start -->
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="ButtonCreationDemoButton">
                                <i class="icon-calendar3"></i></button>
                        </span>
                        {!! Form::text('published_at', ($post->exists) ? $post->published_at : date('Y-m-d H:i:s'), ['class' => 'form-control', 'id' => 'ButtonCreationDemoInput']) !!}
                    </div>

                </div>
                <!-- Published_at End -->

                <!-- Submit -->
                <div class="">
                    <button name="button" value="save" class="btn btn-primary">Lưu lại</button>
                    <button name="button" value="save_and_exists" class="btn btn-info">Lưu & thoát</button>
                    <a href="{{ route('backend.posts.index') }}" class="btn btn-default">Hủy</a>
                </div>
                <!-- Submit End -->
            </div>
        </div>

        <!-- Panel _start -->
        <div class="panel panel-flat">

            <div class="panel-body">
                <h5>Ảnh đại diện</h5>

                <div style="margin-bottom: 20px;">
                    @if($post->exists)
                        <img id="thumbnail" class="img-responsive img-thumbnail" src="{{ $post->thumbnail  }}">
                    @else
                        <img id="thumbnail" class="img-responsive img-thumbnail"
                             src="{{ config('app.no_image_link') }}">
                    @endif
                </div>
                {!! Form::hidden('thumbnail', ($post->exists) ? $post->thumbnail : null, ['id' => 'input-thumbnail']) !!}
            </div>
        </div>
        <!-- Panel End -->


        <!-- Panel _start -->
        <div class="panel panel-flat">

            <div class="panel-body">
                <h5>Danh mục</h5>

                <div class="form-group">
                    {!! Form::select('category_list[]', $categories, null, ['class' => 'form-control multi-select-full', 'multiple', 'id' => 'category_list']) !!}
                </div>

            </div>
        </div>
        <!-- Panel End -->

        <!-- Panel _start -->
        <div class="panel panel-flat">

            <div class="panel-body">
                <h5>Bài liên quan</h5>

                <div class="form-group">
                    @if($post->exists)
                    {!! Form::select('related_posts[]', list_posts($post->id), null, ['class' => 'form-control', 'multiple']) !!}
                    @else
                        {!! Form::select('related_posts[]', list_posts(), null, ['class' => 'form-control', 'multiple']) !!}
                    @endif
                </div>

            </div>
        </div>
        <!-- Panel End -->


        <div class="panel panel-flat">

            <div class="panel-body">
                <h5>Tags</h5>
                <input type="text" name="tags" id="tags-input" value="{{ $tagString }}" class="form-control">

                <div class="help-block">
                    Gõ tag, sau đó nhấn enter hoặc dấu "," để hoàn thành thêm tag
                </div>
            </div>
        </div>

    </div>
</div>



{!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        CKEDITOR.replace('body', {
            height: 600,
        });


        $(document).ready(function () {
            $('input[name=title]').on('blur', function () {
                var value = $(this).val();
                $.ajax({
                    url: "{{ url('tool/slug/genslug') }}/" + value,
                    success: function (data) {
                        if ($('input[name=slug]').val() == "") {
                            $('input[name=slug]').val(data);
                        }
                    }
                });
            });
        });

        $('#ButtonCreationDemoButton').click(function (e) {
            $('#ButtonCreationDemoInput').AnyTime_noPicker().AnyTime_picker().focus();
            e.preventDefault();
        });
        $('#tags-input').tokenfield({
            //autocomplete_url:'http://myserver.com/api/autocomplete'
        });

        $('#thumbnail').click(function () {
            CKFinder.modal({
                height: 600,
                language: 'vi',
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (event) {
                        var file = event.data.files.first();
                        $('#thumbnail').attr('src', file.getUrl());
                        $('#input-thumbnail').val(file.getUrl());
                    });
                }
            });
        });

    </script>

@endsection
