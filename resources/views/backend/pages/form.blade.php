@extends('main')

@section('title', $page->exists ? 'Sửa trang '.$page->name : 'Thêm trang mới')

@section('breadcrumbs')
    @if($page->exists)
        {!! Breadcrumbs::render('edit_page', $page) !!}
    @else
        {!! Breadcrumbs::render('create_page') !!}
    @endif
@endsection

@section('functions')
    <a href="{{ route('backend.pages.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm trang mới</span></a>
@endsection

@section('content'){!! Form::model($page, [
        'method' => $page->exists ? 'put' : 'post',
        'route' => $page->exists ? ['backend.pages.update', $page->id] : ['backend.pages.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}


<div class="col-md-9">

    <!-- Panel _start -->
    <div class="panel panel-flat">

        <div class="panel-body">

            <ul class="nav nav-tabs" id="theTab">
                <li class="active"><a data-target="#general" data-toggle="tab">Thông tin chung</a></li>
                <li><a data-target="#seo" data-toggle="tab">SEO</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="general">

                    @include('errors.form')
                            <!-- Name Start -->
                    <div class="form-group">
                        <label for="name">Name</label>
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        <div class="help-block">Dùng khởi tạo URL cho trang</div>
                    </div>
                    <!-- Name END -->


                    <!-- URI Start -->
                    <div class="form-group">
                        <label for="uri">URI</label>
                        {!! Form::text('uri', null, ['class' => 'form-control']) !!}
                    </div>
                    <!-- URI END -->

                    <!-- Content Start -->
                    <div class="form-group">
                        <label for="content">Content</label>
                        {!! Form::textarea('content', null, ['id' => 'body']) !!}
                    </div>
                    <!-- Content End -->
                </div>

                <div class="tab-pane" id="seo">

                    <div class="form-group" style="max-width: 600px">
                        @if(! $page->exists)
                            <div class="sample-title">This is sample title</div>
                            <div class="sample-url">{{ url('this-is-sample-title') }}</div>
                            <div class="sample-meta-description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto corporis deserunt
                                iusto laboriosam molestiae nostrum quaerat rerum sed, voluptatum? A dignissimos dolorem
                                error iusto molestias quas quisquam rem rerum. Error!
                            </div>
                        @else
                            <div class="sample-title">{{ str_limit(($page->seo_title != '') ? $page->seo_title : $page->title, 80) }}</div>
                            <div class="sample-url">{{ route('blog.show', [$page->id, $page->slug]) }}</div>
                            <div class="sample-meta-description">
                                {{ ($page->meta_description != '') ? $page->meta_description : $page->excerpt }}
                            </div>
                        @endif
                    </div>
                    <!-- Tiêu đề Start -->
                    <div class="form-group">
                        {!! Form::label('seo_title') !!}
                        {!! Form::text('seo_title', null, ['class' => 'form-control seo-title']) !!}
                    </div>
                    <!-- Tiêu đề END -->
                    <!-- Excerpt Start -->
                    <div class="form-group">
                        {!! Form::label('meta_description') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control meta-description']) !!}
                    </div>
                    <!-- Excerpt excerpt -->
                </div>
            </div>


        </div>
    </div>
    <!-- Panel End -->

</div>
<div class="col-md-3">
    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">

            <div class="form-group">
                <h5>Template</h5>
                {!! Form::select('template', $templates, $page->exists ? $page->template : null, ['class' => 'form-control', 'id' => 'template']) !!}
            </div>
            <!-- Submit -->
            <div class="">
                <button name="button" value="save" class="btn btn-primary">Lưu lại</button>
                <button name="button" value="save_and_exists" class="btn btn-info">Lưu & thoát</button>
                <a href="{{ route('backend.pages.index') }}" class="btn btn-default">Hủy</a>
            </div>
            <!-- Submit End -->
        </div>
    </div>
    <!-- Panel End -->

    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>Tùy chọn</h5>

            <div class="form-group">
                <label class="checkbox-inline">
                    <input class="styled" name="list_posts" type="checkbox" value="1">
                    Trang này hiển thị các bài viết
                </label>
            </div>
            <div class="form-group">
                <label for="">Danh mục bài viết</label>
                {!! Form::select('category', blog_category_array(), null, ['class' => 'form-control select2']) !!}
                <div class="help-block">
                    Bạn có thể bỏ qua tùy chọn này nếu đây là một trang bình thường
                </div>
            </div>
        </div>
    </div>
    <!-- Panel End -->


    <!-- Panel _start -->
    <div class="panel panel-flat">

        <div class="panel-body">
            <h5>Ảnh đại diện</h5>

            <div style="margin-bottom: 20px;">
                @if($page->exists)
                    <img id="thumbnail" class="img-responsive img-thumbnail" src="{{ $page->thumbnail  }}?">
                @else
                    <img id="thumbnail" class="img-responsive img-thumbnail"
                         src="{{ config('app.no_image_link') }}?">
                @endif
            </div>
            {!! Form::hidden('thumbnail', ($page->exists) ? $page->thumbnail : null, ['id' => 'input-thumbnail']) !!}
        </div>
    </div>
    <!-- Panel End -->
</div>




{!! Form::close() !!}@endsection


@section('themejs')
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    <script src="{{ url('assets') }}/js/plugins/editors/ckeditor/ckeditor.js"></script>

@endsection

@section('scripts')
    <script>
        CKEDITOR.replace('body', {
            height: 600,
        });
        $('#thumbnail').click(function () {
            CKFinder.modal({
                height: 600,
                language: 'vi',
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (event) {
                        var file = event.data.files.first();
                        $('#thumbnail').attr('src', file.getUrl());
                        $('#input-thumbnail').val(file.getUrl());
                    });
                }
            });
        });
    </script>

@endsection


