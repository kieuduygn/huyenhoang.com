@extends('main')

@section('title', 'Xóa người dùng '.$page->name)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['backend.pages.destroy', $page->id]]) !!}
    <form action="{{ url('backend.pages.destroy') }}"

    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5>Xác nhận xóa trang</h5>
            @include('partials.backend.heading-elements')
        </div>
        <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa trang, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa trang này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('backend.pages.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
        </div>
    </div>
    <!-- Panel End -->


   </form>
@endsection
