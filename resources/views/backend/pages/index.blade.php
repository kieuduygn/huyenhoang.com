@extends('main')

@section('title', 'Quản lý trang')

@section('breadcrumbs')
    {!! Breadcrumbs::render('page') !!}
@endsection

@section('functions')
    <a href="{{ route('backend.pages.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm trang mới</span></a>
    @endsection

    @section('content')

    @if($pages->total() > 0)
    {!! Form::open(['route' => 'backend.pages.bulk']) !!}
            <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>

        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $pages->render(null, 'pagination-xs pull-right') !!}
        </div>

    </div>
    <!-- Bulk Action End -->


    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body no-padding">
            <table class="table table-hover table-data">
                <thead>
                <tr>
                    <th><input type="checkbox" class="styled top-check"></th>
                    <th>Tiêu đề</th>
                    <th>uri</th>
                    <th>Cập nhật</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td>
                            <input name="id[]" value="{{ $page->id }}" class="styled item-checkbox" type="checkbox">
                        </td>
                        <td>
                            <a href="{{ route('backend.pages.edit', $page->id) }}">{{ $page->name }}</a>
                        </td>
                        <td>
                            <a href="{{ route('backend.pages.edit', $page->id) }}">{{ $page->uri }}</a>
                        </td>
                        <td>{{ $page->updated_at }}</td>
                        <td>
                            {!! functions_button('backend.pages', $page->id) !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th style="width: 40px"><input id="top-check" type="checkbox" class="styled top-check"></th>
                    <th>Tiêu đề</th>
                    <th>uri</th>
                    <th>Cập nhật</th>
                    <th style="width: 150px;" class="text-center"></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>

        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $pages->render(null, 'pagination-xs pull-right') !!}
        </div>

    </div>
    <!-- Bulk Action End -->
    {!! Form::close() !!}

    @else
        <p class="no-item">Không tồn tại bài viết nào trên hệ thống <a href="{{ route('backend.pages.create') }}">Nhấn
                vào đây để thêm trang</a></p>
        @endif
                <!-- Panel End -->
@endsection
