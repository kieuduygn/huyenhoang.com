@extends('main')
@section('title', 'Xóa người dùng '.$item->name)
@section('content'){!! Form::open(['method' => 'delete', 'route' => ['backend.attributegroups.destroy', $item->id]]) !!}
        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xóa nhóm thuộc tính</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <div class="alert alert-danger">
            <strong>Cảnh báo !</strong> Bạn đang muốn xóa nhóm thuộc tính
            <strong>[{{ $item->name }}]</strong>, việc này không thể phục hồi
        </div>

        {!! Form::submit('Vâng hãy xóa nhóm thuộc tính này !', ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.attributegroups.index') }}" class="btn btn-success">
            <strong>Không, đưa tôi trở lại!</strong> </a>
    </div>
</div><!-- Panel End -->
@endsection
