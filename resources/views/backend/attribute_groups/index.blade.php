@extends('main')

@section('title', 'Quản lý nhóm thuộc tính sản phẩm')

@section('functions')
    <a href="{{ route('backend.attributegroups.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Thêm nhóm thuộc tính</span></a>
@endsection

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Danh sách nhóm thuộc tính sản phẩm</h5>
            @include('partials.backend.heading-elements')
            <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Tên nhóm</th>
                    <th style="width: 150px;" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>
                            <a href="{{ route('backend.attributegroups.edit', $item->id) }}">{{ $item->name }}</a>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-default" href="{{ route('backend.attributegroups.edit', $item->id) }}">
                                <i class="icon-pen"></i>
                            </a>
                            <a class="btn btn-icon btn-danger" href="{{ route('backend.attributegroups.confirm', $item->id) }}">
                                <i class="icon-folder-remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {!! $items->render() !!}
@endsection


