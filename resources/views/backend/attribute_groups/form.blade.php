@extends('main')

@section('title', $item->exists ? 'Sửa nhóm thuộc tính '.$item->name : 'Thêm nhóm thuộc tính mới')

@section('content')

{!! Form::model($item, [
        'method' => $item->exists ? 'put' : 'post',
        'route' => $item->exists ? ['backend.attributegroups.update', $item->id] : ['backend.attributegroups.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Thêm nhóm thuộc tính</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">

        @include('errors.form')

        <!-- Tiêu đề Start -->
        <div class="form-group">
            <label for="name">Tên nhóm thuộc tính</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <!-- Tiêu đề END -->

        {!! Form::submit($item->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}
        <button value="save" class="btn btn-primary">Lưu lại</button>
        <button value="save_and_exists" class="btn btn-info">Lưu và thoát</button>
        <a href="" class="btn btn-default">Trở lại</a>

    </div>
</div><!-- Panel End -->
{!! Form::close() !!}
@endsection
