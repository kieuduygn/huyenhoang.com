@extends('main')

@section('title', $product->exists ? 'Sửa sản phẩm '.$product->name : 'Thêm sản phẩm mới')

@section('functions')
    <a href="{{ route('backend.products.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm sản phẩm</span></a>
@endsection

@section('content'){!! Form::model($product, [
        'method' => $product->exists ? 'put' : 'post',
        'route' => $product->exists ? ['backend.products.update', $product->id] : ['backend.products.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}


<div class="col-md-9">
    <!-- Panel _start -->
    <div class="panel panel-flat">


        <div class="panel-body">
            <h5>Thông tin cơ bản</h5>


            @include('errors.form')
            @include('flash')

            <ul class="nav nav-tabs" id="theTab">
                <li class="active"><a data-target="#general" data-toggle="tab">Thông tin chung</a></li>
                <li><a data-target="#full-description" data-toggle="tab">Mô tả</a></li>
                <li><a data-target="#guide" data-toggle="tab">Hướng dẫn sử dụng</a></li>
                <li><a data-target="#images" data-toggle="tab">Hình ảnh</a></li>
                {{--<li><a data-target="#attributes" data-toggle="tab">Thuộc tính</a></li>--}}
                <li><a data-target="#options" data-toggle="tab">Lựa chọn</a></li>
                <li><a data-target="#seo" data-toggle="tab">SEO</a></li>
            </ul>

            <div class="tab-content">
                <!-- General start -->
                <div class="tab-pane active" id="general">

                    <div class="row">
                        <div class="col-md-6">
                            <!-- Tên Sản Phẩm Start -->
                            <div class="form-group">
                                <label for="name">Tên Sản Phẩm</label>
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                <div class="help-block">Điền tên gợi nhớ, chứa từ khóa cần SEO</div>
                            </div>
                            <!-- Tên Sản Phẩm END -->

                            <!-- Slug Start -->
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                                <div class="help-block">Là đường dẫn URI sản phẩm</div>
                            </div>
                            <!-- Slug END -->

                            <!-- Slug Start -->
                            <div class="form-group">
                                <label for="feature">Tính năng chính</label>
                                {!! Form::text('feature', null, ['class' => 'form-control']) !!}
                                <div class="help-block">VD: Dùng để bảo quản các loại len</div>
                            </div>
                            <!-- Slug END -->

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>


                    <!-- Mô Tả ngắn Start -->
                    <div class="form-group">
                        <label for="short_description">Mô tả ngắn</label>
                        {!! Form::textarea('short_description', null, ['id' => 'short_description']) !!}
                        <div class="help-block">Mô tả ngắn cho sản phẩm</div>
                    </div>
                    <!-- Mô tả ngắn End -->


                    <!-- Tags -->

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5>Tags</h5>
                        </div>
                        <div class="panel-body">
                            <input type="text" name="tags" id="tags-input" value="{{ $tagString }}"
                                   class="form-control">

                            <div class="help-block">
                                Gõ tag, sau đó nhấn enter hoặc dấu "," để hoàn thành thêm tag
                            </div>
                        </div>
                    </div>
                    <!-- End Tags -->
                </div>
                <!-- Genderal End -->


                <!-- Full Description Start -->
                <div class="tab-pane" id="full-description">
                    <div class="form-group">
                        <label for="description">Mô tả đầy đủ</label>
                        {!! Form::textarea('description', null, ['id' => 'description']) !!}
                    </div>
                </div>
                <!-- Full Description End -->

                <!-- Guide Start -->
                <div class="tab-pane" id="guide">
                    <div class="form-group">
                        <label for="guide">Hướng dẫn sử dụng</label>
                        {!! Form::textarea('guide', null, ['id' => 'guide-area']) !!}
                    </div>
                </div>
                <!-- Guide End -->

                <!-- Images Start -->
                <div class="tab-pane" id="images">
                    @include('backend.products.manage-images')

                    <div class="clearfix"></div>
                    <div id="add-product-image" class="btn btn-default">Thêm ảnh sản phẩm</div>
                </div>
                <!-- Images End -->

                <!-- Attribute -->
                <div class="tab-pane" id="attributes">
                    @include('backend.products.manage-attributes')
                </div>
                <!-- Attribute End -->


                <!-- Options Start -->
                <div class="tab-pane" id="options">
                    @include('backend.products.manage-options')
                </div>
                <!-- Options End-->

                <!-- SEO -->
                <div class="tab-pane" id="seo">

                    <div class="form-group" style="max-width: 600px">
                        @if(! $product->exists)
                            <div class="sample-title">This is sample title</div>
                            <div class="sample-url">{{ url('this-is-sample-title') }}</div>
                            <div class="sample-meta-description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto corporis deserunt
                                iusto laboriosam molestiae nostrum quaerat rerum sed, voluptatum? A dignissimos dolorem
                                error iusto molestias quas quisquam rem rerum. Error!
                            </div>
                        @else
                            <div class="sample-title">{{ str_limit(($product->seo_title != '') ? $product->seo_title : $product->name, 80) }}</div>
                            <div class="sample-url">{{ route('blog.show', [$product->id, $product->slug]) }}</div>
                            <div class="sample-meta-description">
                                {{ ($product->meta_description != '') ? $product->meta_description : $product->excerpt }}
                            </div>
                        @endif
                    </div>
                    <!-- Tiêu đề Start -->
                    <div class="form-group">
                        {!! Form::label('seo_title') !!}
                        {!! Form::text('seo_title', null, ['class' => 'form-control seo-title']) !!}
                    </div>
                    <!-- Tiêu đề END -->
                    <!-- Excerpt Start -->
                    <div class="form-group">
                        {!! Form::label('meta_description') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control meta-description']) !!}
                    </div>
                    <!-- Excerpt excerpt -->
                </div>
                <!-- SEO end -->

            </div>
        </div>
    </div>
    <!-- Panel End -->
</div>
<div class="col-md-3">
    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>Giá sản phẩm</h5>
            <!-- Giá sản phẩm Start -->
            <div class="form-group">
                <label for="price">Giá niêm yết</label>
                {!! Form::text('price', null, ['class' => 'form-control']) !!}
                <div class="help-block">Giá bản sản phẩm</div>
            </div>
            <!-- Giá sản phẩm END --><!-- Giá sản phẩm Start -->
            <div class="form-group">
                <label for="price">Giá Khuyến mãi</label>
                {!! Form::text('promote_price', null, ['class' => 'form-control']) !!}
                <div class="help-block">Giá bản sản phẩm</div>
            </div>
            <!-- Giá sản phẩm END -->

            <div class="">
                <button name="button" value="save" class="btn btn-primary">Lưu lại</button>
                <button name="button" value="save_and_exists" class="btn btn-info">Lưu & thoát</button>
                <a href="{{ route('backend.products.index') }}" class="btn btn-default">Hủy</a>
            </div>
        </div>
    </div>
    <!-- Panel End -->

    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>Ảnh đại diện</h5>

            <div style="margin-bottom: 20px;">
                @if($product->exists)
                    <img style="max-height: 200px;" id="thumbnail" class="img-responsive img-thumbnail"
                         src="{{ thumbnail($product)}}">
                @else
                    <img style="max-height: 200px;" id="thumbnail" class="img-responsive img-thumbnail"
                         src="{{ config('app.no_image_link') }}">
                @endif
            </div>
            {!! Form::hidden('thumbnail', ($product->exists) ? $product->thumbnail->file_path :null, ['id' => 'input-thumbnail']) !!}
            <a id="select-thumbnail" class="text-blue">Chọn ảnh đại diện</a>
        </div>
    </div>
    <!-- Panel End -->

    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>Tính năng khác</h5>
            <!-- Feature ? -->
            <div class="form-group">
                <label class="checkbox-inline">
                    {!! Form::checkbox('home', 1, null, ['class' => 'styled']) !!}
                    Đặt sản phẩm này lên trang chủ </label>
            </div>
            <!-- End Feature Option -->
            <!-- Suggest Product ? -->
            <div class="form-group">
                <label class="checkbox-inline">
                    {!! Form::checkbox('suggest', 1, null, ['class' => 'styled']) !!}
                    Sản phẩm đề nghị </label>
            </div>
            <!-- End Suggest Product Option -->
        </div>
    </div>
    <!-- Panel End -->


    <!-- Brand -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <h5>{{ trans('brand.label') }}</h5>
            {!! Form::select('brand_id', list_brands(), null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <!-- Brand End -->

</div>




{!! Form::close() !!}
@endsection

@section('themejs')
    <script src="{{ url('assets') }}/js/plugins/editors/ckeditor/ckeditor.js"></script>
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/tags/tokenfield.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/media/fancybox.min.js"></script>
@endsection

@section('pagejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/md5.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/uploader_bootstrap.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/gallery.js"></script>
@stop
@section('scripts')

    <script>
        CKEDITOR.replace('short_description', {
            height: 200,
        });
        CKEDITOR.replace('description', {
            height: 600,
        });
        CKEDITOR.replace('guide-area', {
            height: 600,
        });
        $(document).ready(function () {

            $('.options-container').delegate('.add-option-value', 'click', function () {
                var id = $(this).data('option-id');
                var tbody = $(this).closest('tr').parent().parent().find('tbody');
                var length = tbody.find('tr').length;
                $.ajax({
                    'url': "{{ url() }}" + '/product/add-option/' + id + '/' + length,
                    success: function (data) {
                        tbody.append(data);
                    }
                });
            })


            $('#btn-add-option').click(function () {
                var id = $('#add-option').val(),
                        length = $('.options-container table tbody').length;
                $.ajax({
                    'url': "{{ url() }}" + '/product/add-option/' + id + '/' + length,
                    success: function (data) {
                        $('.options-container table tbody').append(data);
                        $('.options-container tr').last().find('button').click(function () {
                            $(this).closest('tr').remove();
                        })
                    }
                });
            });


            $('#add-attribute, .add-options').selectpicker();

            $('#btn-add-attribute').click(function () {
                var id = $('#add-attribute').val();
                $.ajax({
                    'url': "{{ url() }}" + '/getattributehtml/' + id,
                    success: function (data) {
                        $('.attributes-container').append(data);
                    }
                });
            });


            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            //
            $('.options-container').delegate('.btn-detach-option', 'click', function () {
                var option_id = $(this).data('option-id');
                var product_id = $(this).data('product-id');
                var option_value_id = $(this).parent().parent().find('select').val();
                $(this).closest('tr').remove();
                if (option_id != undefined && product_id != undefined) {
                    $.ajax({url: "{{ url() }}" + "/product/detach-option/" + option_id + '/' + product_id + '/' + option_value_id});
                }
            });


            $('#tags-input').tokenfield({
                //autocomplete_url:'http://myserver.com/api/autocomplete'
            });

            $('#select-thumbnail').click(function (e) {
                e.preventDefault();
                CKFinder.modal({
                    height: 600,
                    language: 'vi',
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (event) {
                            var file = event.data.files.first();
                            $('#thumbnail').attr('src', file.getUrl());
                            $('#input-thumbnail').val(file.getUrl());
                        });
                    }
                });
            });
            $('#add-product-image').click(function (e) {
                e.preventDefault();
                CKFinder.modal({
                    height: 600,
                    language: 'vi',
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (event) {
                            var files = event.data.files;
                            for (var id in files._byId) {
                                $.post(
                                        "{{ url('render-new-product-image') }}",
                                        {
                                            data: files._byId[id].getUrl(),
                                            '_token': $('meta[name=csrf-token]').attr('content'),
                                        },
                                        function (data) {
                                            $('#images').prepend(data);
                                        }
                                );
                            }
                        });
                    }
                });
            });


            $('#images').delegate('.btn-detach-image', 'click', function (e) {
                e.preventDefault();

                if(!$(this).closest('.gallery-item').hasClass('new')){
                    var productID = $(this).data('product-id'),
                            imageID = $(this).data('image-id');
                    $.get("{{ url('product/detach-image/') }}/" + imageID + '/' + productID);
                }

                $(this).closest('.gallery-item').remove();
            });
        });

    </script>


    @include('partials.fill-slug-name')
@endsection

