<div class="col-lg-3 col-sm-6 gallery-item new">
    <div class="thumbnail">
        <div class="thumb">
            <input type="hidden" name="images[]" value="{{ $image }}">
            <img src="{{ config('app.url') . $image }}" alt="">
            <div class="caption-overflow">
                    <span>
                        <a href="{{ config('app.url') . $image }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                        <a  href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 btn-detach-image"><i class="icon-link2"></i></a>
                    </span>
            </div>
        </div>
    </div>
</div>
