@extends('main')

@section('title', 'Quản lý sản phẩm')


@section('functions')
    <a href="{{ route('backend.products.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Thêm sản phẩm</span></a>
@endsection


@section('content')
        @if($products->total() > 0)
{!! Form::open(['route' => 'backend.products.bulk']) !!}
        <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $products->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->

    <!-- List Items Start -->
    <div class="panel panel-flat">
        <div class="panel-body no-padding">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th><input type="checkbox" class="styled top-check"></th>
                    <th>Ảnh sản phẩm</th>
                    <th>Tên sản phẩm</th>
                    <th>Giá</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)

                    <tr>
                        <td><input type="checkbox" class="styled item-checkbox" name="id[]" value="{{ $product->id }}"></td>
                        <td>
                            <a href="{{ route('backend.products.edit', $product->id) }}">
                                <img class="img-thumbnail" width="80" src="{{ get_image_url($product->thumbnail->file_path) }}" alt="">
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('backend.products.edit', $product->id) }}">{{ $product->name }}</a>
                        </td>
                        <td class="price">{{ number_format($product->price) }}</td>
                        <td>
                            <a class="btn btn-xs btn-icon btn-default" href="{{ route('backend.products.edit', $product->id) }}">
                                <i class="icon-pen"></i>
                            </a>
                            <a class="btn btn-xs btn-icon btn-danger" href="{{ route('backend.products.confirm', $product->id) }}">
                                <i class="icon-minus2"></i>
                            </a>
                            <a target="_blank" class="btn btn-xs btn-icon btn-info" href="{{ route('product.show', [$product->id, $product->slug]) }}">
                                <i class="icon-eye2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach


                </tbody>

            </table>
        </div>
    </div>
    </div>

    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $products->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->

    <!-- List Items End -->
{!! Form::close() !!}

    @else
    <p class="no-item">Không có sản phẩm nào trên hệ thống, <a href="{{ route('backend.products.create') }}">Nhấn vào đây để thêm một sản phẩm</a></p>
    @endif
@endsection
