@if($product->exists)
    @foreach($product->images()->get() as $image)
        <div class="col-lg-3 col-sm-6 gallery-item">
            <div class="thumbnail">
                <div class="thumb">
                    <img src="{{ get_image_url($image->file_path) }}" alt="">
                    <input type="hidden" value="{{ $image->file_path }}" name="images[]">
                    <div class="caption-overflow">
                            <span>
                                <a href="{{ get_image_url($image->file_path) }}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                                <a data-image-id="{{ $image->id }}" data-product-id="{{ $product->id }}" href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5 btn-detach-image"><i class="icon-link2"></i></a>
                            </span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif