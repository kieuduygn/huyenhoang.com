@extends('main')

@section('title', 'Xóa người dùng '.$product->name)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['backend.products.destroy', $product->id]]) !!}
    <form action="{{ url('backend.products.destroy') }}"

    <!-- Panel _start -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5>Xác nhận xóa sản phẩm</h5>
            @include('partials.backend.heading-elements')
        </div>
        <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa sản phẩm <strong>[{{ $product->name }}]</strong>, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa sản phẩm này này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('backend.products.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
        </div>
    </div>
    <!-- Panel End -->


   </form>
@endsection
