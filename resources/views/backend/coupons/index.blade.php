@extends('main')

@section('title', 'Quản lý nhóm Mã giảm giá sản phẩm')

@section('functions')
    <a href="{{ route('backend.coupons.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Thêm Mã giảm giá</span></a>
@endsection

@section('content')
@if($items->total() > 0)
{!! Form::open(['route' => 'backend.coupons.bulk']) !!}
        <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $items->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->
    <div class="panel panel-flat">
        <div class="panel-body no-padding table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th><input type="checkbox" class="styled top-check"></th>
                    <th>Tên Mã giảm giá</th>
                    <th>Loại</th>
                    <th>Mã</th>
                    <th>Giảm giá</th>
                    <th>Số lượng</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày hết hạn</th>
                    <th>Đã sử dụng</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td><input type="checkbox" class="styled item-checkbox" name="id[]" value="{{ $item->id }}"></td>
                        <td><a href="{{ route('backend.coupons.edit', $item->id) }}">{{ $item->name }}</a></td>
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->code }}</td>
                        <td>{{ number_format($item->discount) }}</td>
                        <td>{{ $item->total }}</td>
                        <td>{{ $item->date_start }}</td>
                        <td>{{ $item->date_end }}</td>
                        <td>{{ $item->used }}</td>
                        <td>
                            <span class="label label-{{ coupon_status($item)->class }}">{{ coupon_status($item)->label }}</span>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-icon btn-default" href="{{ route('backend.coupons.edit', $item->id) }}">
                                <i class="icon-pen"></i> </a>
                            <a class="btn btn-icon btn-xs btn-danger" href="{{ route('backend.coupons.confirm', $item->id) }}">
                                <i class="icon-minus2"></i> </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th><input type="checkbox" class="styled top-check"></th>
                    <th>Tên Mã giảm giá</th>
                    <th>Loại</th>
                    <th>Mã</th>
                    <th>Giảm giá</th>
                    <th>Số lượng</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày hết hạn</th>
                    <th>Đã sử dụng</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            @include('partials.bulk-action')
        </div>
        <div class="col-sm-4 col-sm-offset-4 col-md-offset-6 col-lg-offset-8 col-md-3 col-lg-2">
            {!! $items->render(null, 'pagination-xs pull-right') !!}
        </div>
    </div>
    <!-- Bulk Action End -->
    {!! Form::close() !!}

    @else
        <p class="no-item">Không có mã giảm giá nào, <a href="{{ route('backend.coupons.create') }}">Nhấn vào đây để thêm một mà giảm giá</a></p>
    @endif

@endsection
