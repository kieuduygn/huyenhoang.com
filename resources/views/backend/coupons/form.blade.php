@extends('main')

@section('title', $item->exists ? 'Sửa mã giảm giá '.$item->name : 'Thêm mã giảm giá mới')

@section('content')

{!! Form::model($item, [
        'method' => $item->exists ? 'put' : 'post',
        'route' => $item->exists ? ['backend.coupons.update', $item->id] : ['backend.coupons.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">

    <div class="panel-body">

        @include('errors.form')


                <!-- row start -->
        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Tên</label>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6">
                <label for="name">Loại</label>
                {!! Form::text('type', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Code</label>
                {!! Form::text('code', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6">
                <label for="name">Giảm giá</label>
                {!! Form::text('discount', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Tổng số lượng</label>
                {!! Form::text('total', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6">
                <label for="name">Trạng thái</label>
                {!! Form::select('status', list_coupon_statuses() ,null, ['class' => 'form-control bootstrap-select']) !!}
            </div>
        </div>


        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Ngày bắt đầu</label>
                <div class="input-group">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="date-start">
                                <i class="icon-calendar3"></i></button>
                        </span>
                    {!! Form::text('date_start', ($item->exists) ? $item->date_start : '', ['class' => 'form-control', 'id' => 'date-start-input']) !!}
                </div>

            </div>
            <div class="col-md-6">
                <label for="name">Ngày kết thúc</label>
                <div class="input-group">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="date-end">
                                <i class="icon-calendar3"></i></button>
                        </span>
                    {!! Form::text('date_end', ($item->exists) ? $item->date_end : '' , ['class' => 'form-control', 'id' => 'date-end-input']) !!}
                </div>
            </div>
        </div>

        <div class="">
            <button name="button" value="save" class="btn btn-primary">Lưu lại</button>
            <button name="button" value="save_and_exists" class="btn btn-info">Lưu & thoát</button>
            <a href="{{ route('backend.coupons.index') }}" class="btn btn-default">Hủy</a>
        </div>

    </div>
</div><!-- Panel End -->


{!! Form::close() !!}

@endsection

@section('themejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/pickers/anytime.min.js"></script>
@endsection


@section('scripts')
    <script>
        $('#date-start').click(function (e) {
            $('#date-start-input').AnyTime_noPicker().AnyTime_picker().focus();
            e.preventDefault();
        });
        $('#date-end').click(function (e) {
            $('#date-end-input').AnyTime_noPicker().AnyTime_picker().focus();
            e.preventDefault();
        });
    </script>

@stop