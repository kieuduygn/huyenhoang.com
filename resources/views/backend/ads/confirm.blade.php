@extends('main')

@section('title', trans('ads.delete'))
@section('breadcrumbs')
        {!! Breadcrumbs::render('delete_ads', $ads) !!}
@endsection

@section('content'){!! Form::open(['method' => 'delete', 'route' => ['backend.ads.destroy', $ads->id]]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>{{ trans('ads.confirm_delete') }}</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <div class="alert alert-danger">
            <strong>{{ trans('common.warning') }}
                !</strong> {{ trans('ads.confirm_delete_message', ['name' => $ads->name]) }}
        </div>

        {!! Form::submit(trans('ads.yes_delete'), ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.ads.index') }}" class="btn btn-success">
            <strong>{{ trans('ads.no_delete') }}</strong>
        </a>
    </div>
</div><!-- Panel End -->
{!! Form::close() !!}@endsection
