@extends('main')

@section('title', 'Quản lý quảng cáo')

@section('breadcrumbs')
{!! Breadcrumbs::render('ads') !!}
@endsection

@section('content')

        <!-- Form Tạo quảng cáo  -->
<div class="col-md-5 col-lg-4">
    {!! Form::model($ads, [
            'method' => $ads->exists ? 'put' : 'post',
            'route' => $ads->exists ? ['backend.ads.update', $ads->id] : ['backend.ads.store'],
        ]) !!}

    @include('errors.form')
            <!-- Vị trí -->
    <div class="form-group">
        <label for="">{{ trans('ads.name') }}</label>
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        <div class="help-block">{{ trans('ads.name_description') }}</div>

    </div>
    <!-- Hết vị trí -->

    <!-- Slug -->
    <div class="form-group">
        <label for="">{{ trans('common.slug') }}</label>
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        <div class="help-block">{{ trans('common.slug_description') }}</div>
    </div>
    <!-- Slug End -->
    <!-- Description  -->
    <div class="form-group">
        <label for="">{{ trans('common.description') }}</label>
        {!! Form::textarea('description', null, ['class' =>'form-control']) !!}
    </div>
    <!-- Description  END-->

    <!-- Ảnh quảng cáo -->
    <div class="form-group">
        <label for="">{{ trans('ads.image') }}</label>
        {!! Form::text('image', null, ['class' =>'form-control']) !!}
        <div class="help-block">{{ trans('ads.image_description') }}</div>
    </div>
    <!-- Ảnh quảng cáo  END-->

    <!-- Liên kết  -->
    <div class="form-group">
        <label for="">{{ trans('ads.link') }}</label>
        {!! Form::text('link', null, ['class' =>'form-control']) !!}
    </div>
    <!-- Liên kết  END-->

    <div class="form-group">
        <button type="submit" class="btn btn-primary">{{ ($ads->exists) ? trans('ads.update') : trans('ads.create') }}</button>
    </div>
    {!! Form::close() !!}
</div>
<!-- Form Tạo quảng cáo  END-->


<div class="col-md-7 col-lg-8">
    {!! Form::open(['route' => 'backend.ads.bulk']) !!}
            <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-6 col-md-5 col-lg-4">
            @include('partials.bulk-action')
        </div>
    </div>
    <!-- Bulk Action End -->

    <div class="clearfix"></div>
    <!-- Danh sách quảng cáo  -->
    <div class="panel panel-flat">
        <div class="panel-body no-padding">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th><input class="styled top-check" type="checkbox"></th>
                        <th>{{ trans('ads.image') }}</th>
                        <th>{{ trans('ads.link') }}</th>
                        <th>{{ trans('ads.name') }}</th>
                        <th>{{ trans('common.slug') }}</th>
                        <th width="250">{{ trans('common.description') }}</th>
                        <th width="150"></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list_ads as $key => $item)
                        <tr>
                            <td><input type="checkbox" name="id[]" value="{{ $item->id }}" class="styled item-checkbox"></td>
                            <td><img src="{{ $item->image }}" width="80"></td>
                            <td><a href="{{ $item->link }}" target="_blank">{{ $item->link }}</a></td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->slug }}</td>
                            <td>{{ $item->description }}</td>
                            <td>
                                <!-- Edit  -->
                                <a class="btn btn-xs btn-default  btn-icon"
                                   href="{{ route('backend.ads.edit', $item->id) }}">
                                    <i class="icon-pen"></i>
                                </a>
                                <!-- Edit  END-->

                                <!-- Duplicate  -->
                                <a class="btn btn-xs btn-success  btn-icon"
                                   href="{{ route('backend.ads.duplicate', $item->id) }}">
                                    <i class="icon-database-add"></i>
                                </a>
                                <!-- Duplicate  END-->

                                <!-- Confirm Delete  -->
                                <a class="btn btn-xs btn-danger  btn-icon"
                                   href="{{ route('backend.ads.confirm', $item->id) }}">
                                    <i class="icon-minus2"></i>
                                </a>
                                <!-- Confirm Delete  END-->
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                    <tfoot>
                    <tr>
                        <th><input class="styled top-check" type="checkbox"></th>
                        <th>{{ trans('ads.image') }}</th>
                        <th>{{ trans('ads.link') }}</th>
                        <th>{{ trans('ads.name') }}</th>
                        <th>{{ trans('common.slug') }}</th>
                        <th width="250">{{ trans('common.description') }}</th>
                        <th></th>
                    </tr>
                    </tfoot>

                </table>
            </div>
        </div>

    </div>
    <!-- Danh sách quảng cáo  END-->


    <!-- Bulk Action -->
    <div class="bulk-action row">
        <div class="col-sm-6 col-md-5 col-lg-4">
            @include('partials.bulk-action')
        </div>
    </div>
    <!-- Bulk Action End -->
    {!! Form::close() !!}
</div>
@endsection



