@extends('main')

@section('title', 'Cài đặt')


@section('content')


{!! Form::model($setting, [
    'class' => 'form-horizontal',
        'method' => $setting->exists ? 'put' : 'post',
        'route' => $setting->exists ? ['backend.setting.update', $setting->id] : ['backend.setting.store'],
        'enctype'   => 'multipart/form-data'
        ])
     !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Cài đặt</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('shop_name', 'Tên cửa hàng', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('shop_name', isset($shop_name) ? $shop_name: null, ['class' => 'form-control']) !!}
                <div class="help-block">Đặt tên cửa hàng của bạn</div>
            </div>
        </div>
        <!-- Setting Item End -->
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('short_description', 'Giới thiệu ngắn gọn', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::textarea('short_description', isset($short_description) ? $short_description: null, ['class' => 'form-control']) !!}
                <div class="help-block">Giới thiệu ngắn gọn về cửa hàng (150 ký tự trở lại)</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('phone', 'Điện thoại', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('phone',  isset($phone) ? $phone: null, ['class' => 'form-control']) !!}
                <div class="help-block">Số điện thoại liên lạc chính</div>
            </div>
        </div>
        <!-- Setting Item End -->
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('mobile', 'Di động', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('mobile',  isset($mobile) ? $mobile: null, ['class' => 'form-control']) !!}
                <div class="help-block">Số điện thoại liên lạc chính</div>
            </div>
        </div>
        <!-- Setting Item End -->
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('email', 'Địa chỉ email', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('email',  isset($email) ? $email: null, ['class' => 'form-control']) !!}
                <div class="help-block">Email liên lạc</div>
            </div>
        </div>
        <!-- Setting Item End -->
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('address', 'Địa chỉ liên hệ', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('address',  isset($address) ? $address: null, ['class' => 'form-control']) !!}
                <div class="help-block">Địa chỉ liên hệ</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('longitude', 'Kinh Độ vĩ độ', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-4">
                {!! Form::text('longitude',  isset($longitude) ? $longitude: null, ['class' => 'form-control']) !!}
                <div class="help-block">Kinh độ</div>
            </div>
            <div class="text-center col-md-1">x</div>
            <div class="col-md-4">
                {!! Form::text('latitude',  isset($latitude) ? $latitude: null, ['class' => 'form-control']) !!}
                <div class="help-block">Vĩ độ</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('facebook_page', 'Link trang Facebook', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('facebook_page',  isset($facebook_page) ? $facebook_page: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền link trang facebook</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('google_page', 'Link trang Google', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('google_page',  isset($google_page) ? $google_page: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền link trang facebook</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('instagram_page', 'Link trang Instagram', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('instagram_page',  isset($instagram_page) ? $instagram_page: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền link trang Instagram</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('youtube', 'Link Youtube Channel', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('youtube',  isset($youtube) ? $youtube: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền link Youtube Channel</div>
            </div>
        </div>
        <!-- Setting Item End -->
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('flickr', 'Link Flickr', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('flickr',  isset($flickr) ? $flickr: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền link Flickr</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('yahoo', 'Yahoo Messenger', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('yahoo',  isset($yahoo) ? $yahoo: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền nick yahoo khách liên lạc với bạn</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('skype', 'Skype', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('skype',  isset($skype) ? $skype: null, ['class' => 'form-control']) !!}
                <div class="help-block">Điền nick skype khách liên lạc với bạn</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('facebook_app_id', 'Facebook Application ID', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('facebook_app_id',  isset($facebook_app_id) ? $facebook_app_id: null, ['class' => 'form-control']) !!}
                <div class="help-block">Facebook Application ID</div>
            </div>
        </div>
        <!-- Setting Item End -->

        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('home_title', 'Tiêu đề SEO trang chủ', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::text('home_title',  isset($home_title) ? $home_title: null, ['class' => 'form-control']) !!}
                <div class="help-block">Tiêu đề này sẽ hiển thị với trên goole search</div>
            </div>
        </div>
        <!-- Setting Item End -->
        <!-- Setting Item -->
        <div class="form-group">
            {!! Form::label('home_meta_description', 'Description SEO trang chủ', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                {!! Form::textarea('home_meta_description',  isset($home_meta_description) ? $home_meta_description: null, ['class' => 'form-control']) !!}
                <div class="help-block">Description này sẽ hiển thị với trên goole search</div>
            </div>
        </div>
        <!-- Setting Item End -->


        <!-- Setting Item -->
        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                {!! Form::submit('Lưu lại', ['class' => 'btn btn-primary']) !!}
                <a href="/backend" class="btn btn-default">Thoát ra</a>
            </div>
        </div>
        <!-- Setting Item End -->

    </div>
</div><!-- Panel End -->
{!! Form::close() !!}@stop
