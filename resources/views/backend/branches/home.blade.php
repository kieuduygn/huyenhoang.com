@extends('main')

@section('title', 'Quản lý chi nhánh')

@section('functions')
    <a href="{{ route('backend.branches.create') }}" class="btn btn-link btn-float has-text"><i
                class="icon-plus2 text-primary"></i><span>Thêm chi nhánh</span></a>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-5" style="margin-bottom: 40px">
            {!! Form::open(['route' => 'backend.branches.store']) !!}

                    <!-- Panel _start -->
            @include('errors.form')

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="province_id">Tỉnh, thành phố</label>
                    {!! Form::select('province_id', list_provinces(), null, ['class' => 'form-control select2', 'id' => 'list-provinces']) !!}
                    <div class="help-block">Tỉnh, thành phố đặt chi nhánh</div>
                </div>

                <div class="col-md-6">
                    <label for="district_id">Quận/Huyện</label>
                    {!! Form::select('district_id', list_districts(), null, ['class' => 'form-control', 'id' => 'list-districts']) !!}
                    <div class="help-block">Quận huyện đặt chi nhánh</div>
                </div>


            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="name">Tên</label>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    <div class="help-block">Tên đại lý, chi nhánh</div>
                </div>
                <div class="col-md-6">
                    <label for="address">Địa chỉ</label>
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    <div class="help-block">Địa chỉ chi nhánh</div>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-md-6">
                    <label for="phone">Điện thoại</label>
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                    <div class="help-block">Điện thoại liên hệ</div>
                </div>
                <div class="col-md-6">
                    <label for="email">Email</label>
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    <div class="help-block">Email liên hệ</div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="lat">Kinh độ</label>
                    {!! Form::text('lat', null, ['class' => 'form-control']) !!}
                    <div class="help-block"><a href="http://mygeoposition.com/" target="_blank">http://mygeoposition.com/</a> </div>
                </div>
                <div class="col-md-6">
                    <label for="lng">Vĩ độ</label>
                    {!! Form::text('lng', null, ['class' => 'form-control']) !!}
                    <div class="help-block"><a href="http://mygeoposition.com/" target="_blank">http://mygeoposition.com/</a> </div>
                </div>
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                    {!! Form::checkbox('home', 1, null, ['class' => 'styled']) !!}
                    Đặt lên trang chủ </label>
            </div>

            {!! Form::submit('Thêm chi nhánh', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
        <div class="col-lg-7">
            {!! Form::open(['route' => 'backend.branches.bulk']) !!}
                    <!-- Bulk Action -->
            <div class="bulk-action row">
                <div class="col-sm-4 col-md-4">
                    @include('partials.bulk-action')
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="input-group input-group-xs">
                        {!! Form::select('province', list_provinces(), $province, ['class' => 'form-control select2 select-bulk-action input-xs']) !!}
                        <span class="input-group-btn">
                    <button class="btn btn-default">Lọc</button>
                  </span>
                    </div><!-- /input-group -->
                </div>

                <div class="col-sm-4 col-md-4">
                    {!! $items->render(null, 'pagination-xs pull-right') !!}
                </div>

            </div>
            <!-- Bulk Action End -->
            <div class="panel panel-flat">
                <div class="panel-body no-padding">
                    <div class="table-responsive">
                        <table class="table table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="styled top-check"></th>
                                <th>Tên chi nhánh</th>
                                <th>Địa chỉ</th>
                                <th>Quận/Huyện</th>
                                <th>Tỉnh/Thành Phố</th>
                                <th>Điện thoại</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td><input type="checkbox" value="{{ $item->id }}" name="id[]" class="styled item-checkbox"></td>
                                    <td>
                                        <a href="{{ route('backend.branches.edit', $item->id) }}">{{ $item->name }}</a>
                                    </td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->province->name }}</td>
                                    <td>{{ $item->province->name }}</td>
                                    <td>{{ $item->phone }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-icon btn-default"
                                           href="{{ route('backend.branches.edit', $item->id) }}">
                                            <i class="icon-pen"></i>
                                        </a>
                                        <a class="btn btn-icon btn-xs btn-danger"
                                           href="{{ route('backend.branches.confirm', $item->id) }}">
                                            <i class="icon-minus2"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><input type="checkbox" class="styled top-check"></th>
                                <th>Tên chi nhánh</th>
                                <th>Địa chỉ</th>
                                <th>Điện thoại</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Bulk Action -->
            <div class="bulk-action row">
                <div class="col-sm-4 col-md-4">
                    @include('partials.bulk-action')
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="input-group input-group-xs">
                        {!! Form::select('province', list_provinces(), $province, ['class' => 'form-control select2 select-bulk-action input-xs']) !!}
                        <span class="input-group-btn">
                    <button class="btn btn-default">Lọc</button>
                  </span>
                    </div><!-- /input-group -->
                </div>

                <div class="col-sm-4 col-md-4">
                    {!! $items->render(null, 'pagination-xs pull-right') !!}
                </div>

            </div>
            <!-- Bulk Action End -->
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {
            var province_id = $('#list-provinces').val();
            fillStateWithoutWard(province_id);

            $('#list-provinces').on('change', function(){
                fillStateWithoutWard($(this).val());
            })
        });
    </script>
@endsection
