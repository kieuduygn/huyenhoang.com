@extends('main')

@section('title', $item->exists ? 'Sửa chi nhánh '.$item->name : 'Thêm chi nhánh mới')

@section('content')

{!! Form::model($item, [
        'method' => $item->exists ? 'put' : 'post',
        'route' => $item->exists ? ['backend.branches.update', $item->id] : ['backend.branches.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Thêm chi nhánh</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">

        @include('errors.form')

        <div class="form-group row">
            <div class="col-md-6">
                <label for="province_id">Tỉnh, thành phố</label>
                {!! Form::select('province_id', list_provinces(), null, ['class' => 'form-control select2', 'id' => 'list-provinces']) !!}
                <div class="help-block">Tỉnh, thành phố đặt chi nhánh</div>
            </div>

            <div class="col-md-6">
                <label for="district_id">Quận/Huyện</label>
                {!! Form::select('district_id', list_districts(), null, ['class' => 'form-control', 'id' => 'list-districts']) !!}
                <div class="help-block">Quận huyện đặt chi nhánh</div>
            </div>


        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Tên</label>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                <div class="help-block">Tên đại lý, chi nhánh</div>
            </div>
            <div class="col-md-6">
                <label for="address">Địa chỉ</label>
                {!! Form::text('address', null, ['class' => 'form-control']) !!}
                <div class="help-block">Địa chỉ chi nhánh</div>
            </div>
        </div>


        <div class="form-group row">
            <div class="col-md-6">
                <label for="phone">Điện thoại</label>
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                <div class="help-block">Điện thoại liên hệ</div>
            </div>
            <div class="col-md-6">
                <label for="email">Email</label>
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                <div class="help-block">Email liên hệ</div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="lat">Kinh độ</label>
                {!! Form::text('lat', null, ['class' => 'form-control']) !!}
                <div class="help-block"><a href="http://mygeoposition.com/" target="_blank">http://mygeoposition.com/</a> </div>
            </div>
            <div class="col-md-6">
                <label for="lng">Vĩ độ</label>
                {!! Form::text('lng', null, ['class' => 'form-control']) !!}
                <div class="help-block"><a href="http://mygeoposition.com/" target="_blank">http://mygeoposition.com/</a> </div>
            </div>
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                {!! Form::checkbox('home', 1, null, ['class' => 'styled']) !!}
                Đặt lên trang chủ </label>
        </div>

        {!! Form::submit($item->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}

    </div>
</div><!-- Panel End -->


{!! Form::close() !!}

@endsection

@section('themejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/pages/form_checkboxes_radios.js"></script>
@endsection
