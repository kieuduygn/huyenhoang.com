@extends('main')

@section('title', 'Xóa người dùng '.$item->name)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['backend.branches.destroy', $item->id]]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xóa chi nhánh</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Cảnh báo !</strong> Bạn đang muốn xóa  chi nhánh <strong>[{{ $item->name }}]</strong>, việc này không thể phục hồi
            </div>

            {!! Form::submit('Vâng hãy xóa  chi nhánh này !', ['class' => 'btn btn-danger']) !!}

            <a href="{{ route('backend.branches.index') }}" class="btn btn-success">
                <strong>Không, đưa tôi trở lại!</strong>
            </a>
    </div>
</div><!-- Panel End -->

{!! Form::close() !!}

@endsection
