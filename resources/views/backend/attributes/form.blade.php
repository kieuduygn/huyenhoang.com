@extends('main')
@section('title', $item->exists ? 'Sửa thuộc tính '.$item->name : 'Thêm thuộc tính mới')
@section('content')

{!! Form::model($item, [
        'method' => $item->exists ? 'put' : 'post',
        'route' => $item->exists ? ['backend.attributes.update', $item->id] : ['backend.attributes.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}
        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Thêm danh mục</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">

        @include('errors.form')

                <!-- Tiêu đề Start -->
        <div class="form-group row">
            <div class="col-md-6">
                <label for="name">Tên thuộc tính</label>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6">
                <label for="name">Nhóm</label>
                {!! Form::select('attribute_group_id', $groups, null, ['class' => 'form-control']) !!}
            </div>

        </div>
        <!-- Tiêu đề END -->

        {!! Form::submit($item->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}

    </div>
</div><!-- Panel End -->
{!! Form::close() !!}
@endsection
