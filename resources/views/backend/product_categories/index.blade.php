@extends('main')

@section('title', 'Quản lý danh mục sản phẩm')

@section('functions')
    <a href="{{ route('backend.productcategories.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Thêm danh mục</span></a>
@endsection

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Danh mục sản phẩm</h5>
            @include('partials.backend.heading-elements')
            <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

        <div class="panel-body">
            Dưới đây là danh mục các sản phẩm đang có trên hệ thống
        </div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Tên danh mục</th>
                    <th>Slug</th>
                    <th style="width: 150px;" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            <a href="{{ route('backend.productcategories.edit', $category->id) }}">{{ $category->name }}</a>
                        </td>
                        <td>{{ $category->slug }}</td>
                        <td>
                            <a class="btn btn-sm btn-default" href="{{ route('backend.productcategories.edit', $category->id) }}">
                                <i class="icon-pen"></i>
                            </a>
                            <a class="btn btn-icon btn-danger" href="{{ route('backend.productcategories.confirm', $category->id) }}">
                                <i class="icon-folder-remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    {!! $categories->render() !!}
@endsection

