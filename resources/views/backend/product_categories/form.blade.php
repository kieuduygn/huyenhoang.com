@extends('main')

@section('title', $category->exists ? 'Sửa danh mục '.$category->name : 'Thêm danh mục mới')

@section('content')

{!! Form::model($category, [
        'method' => $category->exists ? 'put' : 'post',
        'route' => $category->exists ? ['backend.productcategories.update', $category->id] : ['backend.productcategories.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

<div class="row">
    <div class="col-md-8">
        <!-- Panel _start -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5>Thêm danh mục</h5>
                @include('partials.backend.heading-elements')
            </div>
            <div class="panel-body">
                <!-- Tiêu đề Start -->

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="name">Tên danh mục</label>
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="slug">Slug</label>
                        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Diễn giải</label>
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Tiêu đề END -->

                {!! Form::submit($category->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}

            </div>
        </div><!-- Panel End -->
    </div>
    <div class="col-md-4">

        <!-- Single select -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Chọn danh mục cha</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <p class="mb-15">Chọn một danh mục cha cho sản phẩm, mặc định danh mục cha sẽ là Root.</p>

                <div class="tree-radio fancytree-radio well border-left-danger border-left-lg"></div>
            </div>
        </div>
        <!-- /single select -->
    </div>
</div>





{!! Form::close() !!}

@endsection


@section('scripts')
    @include('partials.fill-slug-name')
    <script>
        $(function() {
            // Single selection
            $(".tree-radio").fancytree({
                checkbox: true,
                selectMode: 1,
                source: {
                    url: "{{ url('assets/demo_data/fancytree/fancytree.json') }}"
                }
            });
        });

        $('#select-thumbnail').click(function(){
            CKFinder.modal({
                height: 600,
                language: 'vi',
                chooseFiles: true,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( event ) {
                        var file = event.data.files.first();
                        $('#thumbnail').attr('src', file.getUrl());
                        $('#input-thumbnail').val(file.getUrl());
                    } );
                }
            });
        });
    </script>
@endsection


@section('themejs')
    <script src="{{ url('assets') }}/js/plugins/editors/ckeditor/ckeditor.js"></script>
    <script src="{{ url('ckfinder') }}/ckfinder.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/core/libraries/jquery_ui/core.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/core/libraries/jquery_ui/effects.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/extensions/cookie.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/trees/fancytree_all.min.js"></script>
    <script type="text/javascript" src="{{ url('assets') }}/js/plugins/trees/fancytree_childcounter.js"></script>
@endsection
