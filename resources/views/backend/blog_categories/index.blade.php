@extends('main')

@section('title', 'Quản lý danh mục bài viết')

@section('breadcrumbs')
    {!! Breadcrumbs::render('blog_category') !!}
    @endsection

@section('content')

    <div class="col-md-5 col-lg-4">
        {!! Form::open(['route' => 'backend.blogcategories.store']) !!}
        @include('errors.form')
        <div class="form-group">
            <label for="">Tên danh mục</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <div class="help-block">The name is how it appears on your site.</div>

        </div>
        <div class="form-group">
            <label for="">Slug</label>
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            <div class="help-block">The “slug” is the URL-friendly version of the name. It is usually all lowercase and
                contains only letters, numbers, and hyphens.
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-md-12">Parent</label>
            <div class="col-md-8">
                <select name="parent" class="form-control select2">
                    <option value="0">--Chọn danh mục cha</option>
                    @foreach($categories as $node)
                        {!! render_category($node) !!}
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="help-block col-xs-12">Categories, unlike tags, can have a hierarchy. You might have a Jazz
                category, and under that have children categories for Bebop and Big Band. Totally optional.
            </div>
        </div>

        <div class="form-group">
            <label for="">Diễn giải</label>
            {!! Form::textarea('description', null, ['class' =>'form-control']) !!}
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                {!! Form::checkbox('hide_from_blog', 1, null, ['class' => 'styled']) !!}
                Hide from blog </label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Thêm danh mục</button>
        </div>
        {!! Form::close() !!}
    </div>


    <!-- List Blog Categories Start -->
    <div class="col-md-7 col-lg-8">
        {!! Form::open(['route' => 'backend.blogcategories.bulk']) !!}
        <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="input-group input-group-xs">
                    {!! Form::select('filter', $filters, $filter, ['class' => 'form-control bootstrap-select']) !!}
                  <span class="input-group-btn">
                    <button class="btn btn-default">Lọc</button>
                  </span>
                </div><!-- /input-group -->
            </div>
        </div>
        <!-- Bulk Action End -->

        <div class="clearfix"></div>


        <div class="panel panel-flat">
            <div class="panel-body no-padding">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><input class="styled top-check" type="checkbox"></th>
                            <th>Tên danh mục</th>
                            <th>Slug</th>
                            <th width="250">Diễn giải</th>
                            <th>HFB</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($categories as $category)
                            {!! render_category_tr($category) !!}
                        @endforeach
                        </tbody>

                        <tfoot>
                        <tr>
                            <th><input class="styled top-check" type="checkbox"></th>
                            <th>Tên danh mục</th>
                            <th>Slug</th>
                            <th width="250">Diễn giải</th>
                            <th>HFB</th>
                            <th></th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>

        </div>

        <!-- Bulk Action -->
        <div class="bulk-action row">
            <div class="col-sm-6 col-md-5 col-lg-4">
                @include('partials.bulk-action')
            </div>
        </div>
        <!-- Bulk Action End -->
        {!! Form::close() !!}
    </div>
    <!-- List Blog Categories End -->


@endsection


@section('scripts')
    @include('partials.fill-slug-name')
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        })
    </script>
@endsection

