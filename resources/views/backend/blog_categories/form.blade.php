@extends('main')

@section('title', $category->exists ? 'Sửa danh mục '.$category->name : 'Thêm danh mục mới')

@section('breadcrumbs')
@if($category->exists)
{!! Breadcrumbs::render('edit_blog_category', $category) !!}
@else
{!! Breadcrumbs::render('create_blog_category') !!}
@endif

@endsection

@section('content')

{!! Form::model($category, [
        'method' => $category->exists ? 'put' : 'post',
        'route' => $category->exists ? ['backend.blogcategories.update', $category->id] : ['backend.blogcategories.store'],
        'enctype'   => 'multipart/form-data'
    ]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Thêm danh mục</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <!-- Tiêu đề Start -->
        <div class="form-group">
            <label for="name">Tên danh mục</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <!-- Tiêu đề END -->

        <!-- URI Start -->
        <div class="form-group">
            <label for="slug">Slug</label>
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>
        <!-- URI END -->

        {!! Form::submit($category->exists ? 'Lưu lại' : 'Tiếp tục', ['class' => 'btn btn-primary']) !!}

    </div>
</div><!-- Panel End -->


{!! Form::close() !!}

@endsection


@section('scripts')
    @include('partials.fill-slug-name')
@endsection


@section('themejs')
    <script type="text/javascript" src="{{ url('assets') }}/js/core/app.js"></script>
@endsection
