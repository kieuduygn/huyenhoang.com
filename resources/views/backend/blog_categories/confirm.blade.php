@extends('main')

@section('title', 'Xóa người dùng '.$category->name)

@section('content'){!! Form::open(['method' => 'delete', 'route' => ['backend.blogcategories.destroy', $category->id]]) !!}

        <!-- Panel _start -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Xác nhận xóa danh mục</h5>
        @include('partials.backend.heading-elements')
    </div>
    <div class="panel-body">
        <div class="alert alert-danger">
            <strong>Cảnh báo !</strong> Bạn đang muốn xóa danh mục
            <strong>{{ $category->name }}</strong>, việc này không thể phục hồi
        </div>

        {!! Form::submit('Vâng hãy xóa danh mục này !', ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.blogcategories.index') }}" class="btn btn-success"> <strong>Không, đưa tôi trở lại!</strong>
        </a>
    </div>
</div><!-- Panel End -->
{!! Form::close() !!}@endsection
