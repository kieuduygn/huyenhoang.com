<div class="widget widget-categories">
    <h6 class="heading-alt-style12 widget-title">Gallery</h6>
    <ul>
        @foreach($categories as $category)
        <li><a href="{{ route('gallery.category.show', $category->slug) }}">{{ $category->name }}</a></li>
            @endforeach
    </ul>
</div><!-- /widget -->