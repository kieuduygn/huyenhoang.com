<div class="categories-widget widget">
    <h4>Liên hệ nhanh</h4>
    <ul class="list-unstyled contact widget">
        <li>
            <a href="">
                <img src="{{ theme() }}/img/icon/fb.png">
                fb.com/inphongcachmoi
            </a>
        </li>
        <li>
            <a href="">
                <img src="{{ theme() }}/img/icon/email.png">
                info@inphongcachmoi.com
            </a>
        </li>
        <li class="hotline">
            <a href="tel:123456789">
                <img src="{{ theme() }}/img/icon/mobile.png">
                0909 909 909
            </a>
        </li>
        <li>
            <a href="tel:123456789">
                <img src="{{ theme() }}/img/icon/skype.png">
                thuyvu.newstyle
            </a>
        </li>
    </ul>
</div>