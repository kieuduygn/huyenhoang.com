<div class="contact-banner">
    <img src="{{ theme('img/banner-3.jpg') }}" class="img-responsive" alt="">
    <div class="contact-content">
        <h4>{{ get_settings('shop_name') }}</h4>
        <p>Địa chỉ:
                        <span>
                        {{ get_settings('address') }}
                    </span>

        </p>
        <p>Điện thoại: <a href="tel:{{ get_settings('phone') }}">{{ get_settings('phone') }}</a> - Fax: <span>{{ get_settings('fax') }}</span> - Email: <a
                    href="mailto:{{ get_settings('email') }}">{{ get_settings('email') }}</a></p>
    </div>
</div>