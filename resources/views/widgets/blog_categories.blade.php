<div class="categories-widget widget">
    <h4>Danh mục bài viết</h4>
    <ul class="list-unstyled categories widget">
        @foreach($categories as $category)
            <li>
                <a href="{{ route('blog.category', $category->slug) }}">
                    {{ $category->name }}
                </a>
            </li>
        @endforeach
    </ul>
</div>
