<div class="widget widget-latest-post">
    <h6 class="heading-alt-style12 widget-title">New Photos</h6>
    <div class="post-container blog-post-single  wow fadeInLeft">
        <article class="post format-gallery">
            <figure class="entry-image">
                @foreach($images as $image)
                <img src="{{ $image->file_path }}" alt="Huyenhoang photo">
                    @endforeach
            </figure>
        </article>
    </div>
</div><!-- /widget -->