<div class="widget widget-latest-post">
    <h6 class="heading-alt-style12 widget-title">Latest blog</h6>
    @foreach($posts as $post)
    <div class="latest-postbox latest-postbox-style3">
        <article>
            <figure class="post-thumb">
                <a href="{{ route('blog.show', $post->slug) }}"><img width="80" src="{{ $post->thumbnail }}" alt="{{ $post->title }}"></a>
            </figure>
            <div class="post-content">
                <h1><a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a></h1>
            </div><!-- /post-content -->
            <div class="post-meta">
                <time datetime="{{ $post->created_at->format('Y-m-d') }}"><a  href="#">{{ $post->created_at->format('d M, Y') }}</a></time>
            </div><!-- /post-meta -->
        </article>
    </div><!-- /latest-postbox -->
    @endforeach
</div><!-- /widget -->