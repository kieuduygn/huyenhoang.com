<h2>Khách hàng đặt mua ảnh từ website www.huyenhoang.com</h2>

<ul>
    <li>
        Name: {{ $data['name'] }}
    </li>
    <li>
        Phone: {{ $data['phone'] }}
    </li>
    <li>
        Email: {{ $data['email'] }}
    </li>
    <li>
        Address: {{ $data['address'] }}
    </li>
    <li>
        Note: {{ $data['note'] }}
    </li>
    <li>
        Image: {{ $data['imageName'] }}
    </li>
</ul>