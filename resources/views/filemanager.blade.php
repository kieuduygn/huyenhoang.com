<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <base href="{{ url() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{url('assets')}}/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{url('assets')}}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{url('vendor')}}/filemanager/css/filemanager.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{url('assets')}}/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="{{url('assets')}}/js/core/libraries/bootstrap.min.js"></script>
    <!-- /core JS files -->


</head>

<body style="background: #fff">


<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content container-fluid">

                <ul class="functions-button list-unstyled list-inline">
                    <li><button type="button" class="btn btn-success">Chọn</button></li>
                </ul>

                <div class="line"></div>


                <div class="row">
                    <div class="col-sm-3 list-folders" style="border-right: 1px solid #e5e5e5; min-height: 1000px">
                        <ul class="list-unstyled">
                            @foreach($folders as $folder)
                                <li class="{{ (check_directory_child($folder)) ? 'has-sub' : '' }}">
                                    <span>{{ str_limit($folder, 15) }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-9">
                        sdfsdf
                    </div>
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<script src="{{ url() }}/vendor/filemanager/js/scripts.js"></script>
</body>

</html>
