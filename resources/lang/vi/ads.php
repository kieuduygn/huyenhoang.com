<?php
return [
    'label' => 'Quảng cáo',
    'link'  => 'Liên kết',
    'image' => 'Ảnh quảng cáo',
    'image_description' => 'Các ảnh quảng cáo có thể host tại website http://imgurl.com',
    'name' => 'Vị trí',
    'name_description' => 'Vị trí bạn sẽ render quảng cáo',
    'create' => 'Thêm quảng cáo',
    'create_success_message' => 'Thêm quảng cáo :name thành công',
    'edit' => 'Sửa quảng cáo',
    'update'    => 'Cập nhật',
    'update_success_message'    => 'Cập nhật quảng cáo :name thành công',
    'confirm_delete'    => 'Xác nhận xóa quảng cáo',
    'confirm_delete_message'    => 'Bạn đang muốn xóa quảng cáo :name, việc này không thể phục hồi',
    'yes_delete'    => 'Vâng, hãy xóa quảng cáo này',
    'no_delete' => 'Không, đưa tôi trở lại',
    'delete' => 'Xóa quảng cáo',
    'delete_success_message' => 'Xóa quảng cáo :name thành công',
    'duplicate' => 'Nhân đôi quảng cáo',
    'duplicate_success_message' => 'Bạn đã đôi quảng cáo :name thành công, hãy sửa lại',

];