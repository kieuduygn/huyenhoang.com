<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => ':attribute không được nhiều hơn :max ký tự.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => ':attribute không được ít hơn :min ký tự.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => ':attribute không chính xác.',
    'numeric'              => ':attribute phải là số.',
    'regex'                => 'Định dạng :attribute không đúng.',
    'required'             => 'Bạn chưa nhập :attribute.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => ':attribute đã tồn tại trên hệ thống.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'thumbnail' => 'ảnh đại diện',
        'username'  => 'Tên đăng nhập',
        'password'  => 'Mật khẩu',
        'email'     => 'Địa chỉ email',
        'title'     => 'Tiêu đề',
        'name'      => 'Tên',
        'body'      => 'Nội dung',
        'content'   => 'Nội dung',
        'date_start'   => 'Ngày bắt đầu',
        'date_end'   => 'Ngày kết thúc',
        'type'   => 'Loại',
        'code'   => 'Code',
        'province'  => 'Tỉnh/Thành phố',
        'phone' => 'Số điện thoại',
        'district'  => 'Quận/Huyện',
        'ward'  => 'Phường/Xã',
        'address'   => 'Địa chỉ',
        'total'   => 'Tổng số lượng',
        'discount'   => 'Giảm giá',
        'slug'      => 'slug',
        'message'   => 'Tin nhắn',
        'short_description' => 'mô tả ngắn',
        'description'   => 'Mô tả đầy đủ',
        'password_confirmation' => 'xác thực mật khẩu',
        'accept_term' => 'điều khoản sử dụng',
        'shipping'  => [
            'name'    => 'Tên người nhận hàng',
            'email'    => 'Email người nhận hàng',
            'phone'    => 'Số điện thoại người nhận hàng',
            'address'    => 'Địa chỉ người nhận hàng',
            'ward_id'    => 'Phường/Xã người nhận hàng',
            'district_id'    => 'Quận/Huyện người nhận hàng',
            'province_id'    => 'Tỉnh/Thành phố người nhận hàng',
        ],
        'payment'  => [
            'name'    => 'Tên người thanh toán',
            'email'    => 'Email người thanh toán',
            'phone'    => 'Số điện thoại người thanh toán',
            'address'    => 'Địa chỉ người thanh toán',
            'ward_id'    => 'Phường/Xã người thanh toán',
            'district_id'    => 'Quận/Huyện người thanh toán',
            'province_id'    => 'Tỉnh/Thành phố người thanh toán',
        ],
        'link'  => 'Liên kết',
        'image' => 'Hình ảnh',

    ],

];
