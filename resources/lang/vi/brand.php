<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-02-26
 * Time: 12:26 AM
 */

return [
	'label' => 'Nhãn hiệu',
	'name' => 'Tên nhãn hiệu',
	'logo' => 'Logo',
	'name_description' => 'Tên nhãn hiệu sẽ xuất hiện trên website của bạn',
	'add' => 'Thêm nhãn hiệu',
	'index_page_title' => 'Quản lý nhãn hiệu',
	'confirm_delete' => 'Xác nhận xóa nhãn hiệu',
	'confirm_delete_message' => 'Bạn đang muốn xóa nhãn hiệu :brand_name, việc này không thể phục hồi',
	'yes_delete_this_brand' => 'Vâng, hãy xóa nhãn hiệu này',
	'update_success' => 'Cập nhật nhãn hiệu thành công',
	'create_success' => 'Thêm nhãn hiệu thành công',
];