<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2016-02-26
 * Time: 12:27 AM
 */

return [
	'general_information'             => 'Thông tin cơ bản',
	'seo_title'             => 'SEO title',
	'seo_title_description'             => 'Sử dụng trong SEO',
	'meta_description'      => 'meta_description',
	'slug'                  => 'Slug',
	'slug_desc'  => 'slug là một phiên bản friendly URL của tên, thường chỉ chứa ký tự la tinh, số, và dấu gạch ngang',
	'slug_description'  => 'slug là một phiên bản friendly URL của tên, thường chỉ chứa ký tự la tinh, số, và dấu gạch ngang',
	'slug_duplicated'	=> 'Slug bạn nhập đã tồn tại trên hệ thống',
	'description'   => 'Diễn giải',
	'short_description'	=> 'Mô tả ngắn',
	'full_description'	=> 'Mô tả đầy đủ',
	'thumbnail' => 'Ảnh đại diện',
	'change_thumbnail' => 'Thay đổi ảnh đại diện',
	'bulk_action'   => 'Bulk Action',
	'apply' => 'Áp dụng',
	'logo'  => 'Logo',
	'add_logo'  => 'Chọn logo',
	'warning'   => 'Cảnh báo',
	'success'   => 'Thành công',
	'no_take_me_back'   => 'Không, đưa tôi trở lại',
	'cancel'	=> 'Hủy',
	//Action
	'save'  => 'Lưu lại',
	'save_and_exit' => 'Lưu và thoát',
	'tags' => 'Tags',
	'tags_description'	=> 'Gõ tag, sau đó nhấn enter hoặc dấu "," để hoàn thành thêm tag',
	'other_feature'	=> 'Tính năng khác',
	'order_number'	=> 'Thứ tự',
];