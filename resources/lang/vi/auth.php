<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tài khoản hoặc mật khẩu không đúng.',
    'throttle' => 'Bạn đăng nhập quá nhiều lần, xin hãy thử lại sau ít phút.',

];
